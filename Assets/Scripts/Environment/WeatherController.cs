﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Zenject;

namespace Visualizer.Environment
{
	public interface IWeatherController 
	{
		void Clear();
		void SetGlobalWeather (WeatherCondition condition);
		void AddWeatherSection(WeatherZone zone);
		void StartAnimation(float aSpeed);
		void StopAnimation();
	}

	public enum WeatherCondition { Unknown, Sunny, Rain, Snow, Fog };

	/// <summary>
	/// A weather zone discribes a rectangular area in which a specific weather condition is present. 
	/// The zone must be imagined as a 2D plane layed on the terrain in LookDown view.
	/// </summary>
	public class WeatherZone {
		public Rect Area { set; get; } 
		public WeatherCondition Condition { set; get; }
	}

	/// <summary>
	/// The weather controller can visualize various weather conditions based on the information in the trace file.
	/// Two types of weather conditions are supported: global and zones. Weather zones apply a certain weather condition only to a given area.
	/// </summary>
	/// <remarks>
	/// For performance reasons, the weather animation particle systems of rain and snow only live around the camera rather then 
	/// in the entire weather zone. They always surround the camera and move together with it. In order to imitate a more natural
	/// effect, where the camera is moving among the rain drops and snow flakes, rather than pulling them along, we apply the following mechanism:   
	/// The particle systems have a box collider inside, that is smaller than the area of the particle system's box. As long as the camera stays 
	/// within the bounds of this collider (the navigation zone), the particle system's position is not updated. 
	/// If the camera moves out of the navigation zone, the particle system position is updated so that it surrounds the camera again.
	/// In the moment of this update, we speed up the animation for one frame in order to fill the updated box with particles. 
	/// </remarks>
	public class WeatherController : MonoBehaviour, IWeatherController {

		private ICameraManager cameraManager;
		private List<WeatherSection> weatherSections = new List<WeatherSection>();
		private WeatherCondition currentWeather = WeatherCondition.Unknown;
		private WeatherCondition defaultWeatherCondition = WeatherCondition.Sunny;
		ParticleSystem currentAnimation = null;
		ParticleSystem rainAnimation = null;
		ParticleSystem snowAnimation = null;

		// Note: fog handling has been disabled after upgrading the project to 2018.3 as old image effects are depricated.
		// If we need fog in the future, this handling must be implemented with post-processing stack.
		//GlobalFog fog;
		Light sun;

		private bool isAnimating = false;
		private bool fastForwardingAnimation = false;
		private float animationSpeed = 1f;
		private Vector3 cameraPos = Vector3.zero;
		private float targetFogHeight = 0f;

		private float smoothingVelocity;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ICameraManager cManager){
			cameraManager = cManager;
		}

		/// <summary>
		/// MonoBehaviour function that is called on creation of the WeatherController
		/// </summary>
		private void Start () {
			//fog = Camera.main.GetComponent<GlobalFog> ();
			sun = GameObject.Find ("Sun").GetComponent<Light> ();

			LoadRainAnimation ();
			LoadSnowAnimation ();

			UpdateWeather ();

			// eliminate CS0414 warning until the method UpdateSmoothWeatherChange is enabled
			var dummy = targetFogHeight;
		}

		private void LoadRainAnimation(){
			GameObject rainPrefab = null;
			rainPrefab = Resources.Load("Prefabs/Weather/Rain") as GameObject;
			if (rainPrefab != null) {
				GameObject rainInstance = Instantiate (rainPrefab, Vector3.zero, Quaternion.Euler (90, 0, 0)) as GameObject;
				rainInstance.transform.SetParent (this.transform);
				rainInstance.name = "Rain";
				rainAnimation = rainInstance.GetComponent<ParticleSystem> ();
			}
			else{
				Debug.LogWarning ("Rain prefab could not be found!");
			}
		}

		private void LoadSnowAnimation(){
			GameObject snowPrefab = null;
			snowPrefab = Resources.Load("Prefabs/Weather/Snow") as GameObject;
			if (snowPrefab != null) {
				GameObject snowInstance = Instantiate (snowPrefab, Vector3.zero, Quaternion.Euler (90, 0, 0)) as GameObject;
				snowInstance.transform.SetParent (this.transform);
				snowInstance.name = "Snow";
				snowAnimation = snowInstance.GetComponent<ParticleSystem> ();
			}
			else{
				Debug.LogWarning ("Snow prefab could not be found!");
			}
		}

		/// <summary>
		/// Clears all weather zones and resets to deafult weather
		/// </summary>
		public void Clear(){
			foreach (WeatherSection section in weatherSections)
				GameObject.Destroy (section.gameObject);
			weatherSections.Clear ();
			SetGlobalWeather(WeatherCondition.Sunny);
		}

		/// <summary>
		/// Sets the global weather that is present anywhere out of the specified zones.
		/// </summary>
		public void SetGlobalWeather(WeatherCondition condition){
			defaultWeatherCondition = condition;
			UpdateWeather ();
		}

		/// <summary>
		/// Public interface to create a new weather zone.
		/// </summary>
		public void AddWeatherSection(WeatherZone zone){
			GameObject weatherSectionPrefab = Resources.Load ("Prefabs/Weather/WeatherSectionPrefab", typeof(GameObject)) as GameObject;
			GameObject sectionInstance = Instantiate (weatherSectionPrefab, this.transform) as GameObject;
			WeatherSection section = sectionInstance.GetComponent<WeatherSection> ();
			section.Init (zone);
			weatherSections.Add (section);
			UpdateWeather ();
		}

		/// <summary>
		/// MonoBehaviour function that is called once per frame.
		/// </summary>
		private void Update () {
			SlowDownAnimation ();
			//UpdateSmoothWeatherChange ();
			if (HasCameraMoved()) {
				UpdateAnimationZone ();
				UpdateWeather ();
			}
		}
		
		/*
		/// <summary>
		/// Makes sure that the fog appears/dissapears slowly over several frames to create a more natural looking weather change.
		/// The sun intensity is intentionally not changed slowly as it adds an extreme processor load (at least in Unity 5.5) and slows down the rendering
		/// </summary>
		private void UpdateSmoothWeatherChange(){
			if (targetFogHeight != fog.height) {
				fog.height = Mathf.SmoothDamp (fog.height, targetFogHeight, ref smoothingVelocity, 1.5f);
				if (Mathf.Abs (fog.height - targetFogHeight) < 0.3f)
					fog.height = targetFogHeight;
				fog.enabled = (fog.height > 0f);
			}
		}
		*/

		/// <summary>
		/// Starts the animation of the particle systems (snow or rain).
		/// </summary>
		public void StartAnimation(float aSpeed){
			isAnimating = true;
			animationSpeed = aSpeed;
			if (currentAnimation != null)
				currentAnimation.Play ();
		}

		/// <summary>
		/// Stops the animation of the particle systems (snow or rain).
		/// </summary>
		public void StopAnimation(){
			isAnimating = false;
			if (currentAnimation != null)
				currentAnimation.Pause ();
		}

		/// <summary>
		/// Determines whether the camera moved compared to the previous frame.
		/// </summary>
		private bool HasCameraMoved(){
			Vector3 previousCameraPos = cameraPos;
			cameraPos = cameraManager.GetCameraPos();
			return (cameraPos != previousCameraPos);
		}

		/// <summary>
		/// Checks if the camera is in any of the weather zones and switches the weather if so. If the camera is not in any of the weather zones, we switch to the default weather.
		/// </summary>
		private void UpdateWeather(){
			for (int idx = 0; idx < weatherSections.Count; idx++) {			
				if (weatherSections[idx].ContainsPoint(cameraPos)){
					SetCurrentWeatherCondition(weatherSections[idx].GetCondition());
					return;
				}
			}
			SetCurrentWeatherCondition(defaultWeatherCondition);
		}

		/// <summary>
		/// Updates the weather to the given condition if not already in that condition
		/// </summary>
		private void SetCurrentWeatherCondition(WeatherCondition newCondition){
			if (newCondition == currentWeather)
				return;

			//reset animations
			EnableAnimation(rainAnimation, false);
			EnableAnimation(snowAnimation, false);

			switch (newCondition){
			case WeatherCondition.Fog:
				targetFogHeight = 120f; 
				sun.intensity = 0.3f;
				currentAnimation = null;
				break;
			case WeatherCondition.Rain:
				targetFogHeight = 120f;
				sun.intensity = 0.3f;
				currentAnimation = rainAnimation;
				EnableAnimation(rainAnimation, true);
				break;
			case WeatherCondition.Snow:
				targetFogHeight = 100f;
				sun.intensity = 0.5f;
				currentAnimation = snowAnimation;
				EnableAnimation (snowAnimation, true);
				break;
			case WeatherCondition.Sunny:
				targetFogHeight = 0f;
				sun.intensity = 1f;
				currentAnimation = null;
				break;
			}

			currentWeather = newCondition;
		}

		/// <summary>
		/// Enables/disables the animation of the particle systems (rain/snow).
		/// </summary>
		private void EnableAnimation(ParticleSystem animation, bool enabled){
			animation.gameObject.SetActive (enabled);
			if (enabled) {
				if (isAnimating)
					animation.Play ();
				else
					animation.Pause ();
				FillAnimationZone ();
			}
		}

		/// <summary>
		/// Chech if the camera left the animation zone and if so, update the animation's position to the new camera position
		/// </summary>
		private void UpdateAnimationZone(){
			if (currentAnimation == null)
				return;

			if (!currentAnimation.gameObject.GetComponent<BoxCollider> ().bounds.Contains (cameraPos)) {
				currentAnimation.transform.position = cameraPos - new Vector3 (0f, -10f, 0f);
				FillAnimationZone ();
			}
		}

		/// <summary>
		/// Fast forward the animation for a frame to fill the new weather tile with particles
		/// </summary>
		private void FillAnimationZone(){
			ParticleSystem.MainModule main = currentAnimation.main;
			main.simulationSpeed = 1000f;
			currentAnimation.Play ();

			//this variable makes sure that the simulation speed will be reset after the next update rendered the particles
			fastForwardingAnimation = true;
		}

		/// <summary>
		/// In case in the previous frame we fast forwarded the animation (see FillAnimationZone), we slow down here again 
		/// (or pause if the agent animation is currently not running)
		/// </summary>
		private void SlowDownAnimation(){
			if (fastForwardingAnimation && currentAnimation != null) {
				if (!isAnimating)
					currentAnimation.Pause ();
				ParticleSystem.MainModule main = currentAnimation.main;
				main.simulationSpeed = animationSpeed;
				fastForwardingAnimation = false;
			}
		}
	}
}