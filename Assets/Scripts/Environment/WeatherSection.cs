﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.Environment
{
	/// <summary>
	/// A Weather section is the representation of a WeatherZone in Unity world (a GameObject with nothing but a collider).
	/// </summary>
	/// <remarks>
	/// The zone must be imagined as a 2D plane layed on the terrain in LookDown view. 
	/// The X-Axis of the zone rect is the Unity X axis, the Y-Axis of the rect is the Unity Z axis.
	/// The origin is the left upper point of the zone rect.
	/// </remarks>
	public class WeatherSection : MonoBehaviour {

		private BoxCollider boxCollider;
		private WeatherCondition weatherCondition;

		public void Init (WeatherZone zone) {

			weatherCondition = zone.Condition;

			//set the position
			transform.position = new Vector3 (zone.Area.x, 0f, zone.Area.y);

			//set the size
			Vector3 boxSize = new Vector3 (zone.Area.height, 2000f, zone.Area.width);

			//set the pivot of the box to the corner
			Vector3 boxCenter = new Vector3 (boxSize.x/2f, boxSize.y/2f, boxSize.z/2f);

			//apply the calcualted values to the collider
			boxCollider = GetComponent<BoxCollider> ();
			boxCollider.size = boxSize;
			boxCollider.center = boxCenter;
		}

		/// <summary>
		/// Test if the specified point is in the weather section
		/// </summary>
		/// <param name="pos">Position in world coordinates.</param>
		public bool ContainsPoint(Vector3 pos){
			return boxCollider.bounds.Contains (pos);
		}

		/// <summary>
		/// Returns the condition of this weather section
		/// </summary>
		public WeatherCondition GetCondition(){
			return weatherCondition;
		}
	}
}