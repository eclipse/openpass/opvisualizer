﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

namespace Visualizer.CameraHandling
{
	/// <summary>
	/// Camera controller that fixes the camera to the driver's position in a vehicle. Only horizontal camera rotation is allowed.
	/// </summary>
	public class CCFollowDriver : CCBase {

		Vector3 defaultRotation = new Vector3 (0f, 90f, 0f);
		Vector3 driverLookingAngle = new Vector3 (0f, 0f, 0f);
		private Vector3 mousePosAtMouseDown;
		private Vector3 driverLookingAngleAtMouseDown;
		private float rotationSensitivity = 0.1f;
		private float autoPanningSpeed = 8f;

		void Update () {
			HandleCameraRotation ();

			if (FocusedAgent != null) {
				Vector3 agentPos = FocusedAgent.transform.position;
				Vector3 agentRotation = FocusedAgent.transform.rotation.eulerAngles;
				cameraTarget.transform.position = agentPos + FocusedAgent.transform.rotation * FocusedAgent.DriversEyePos;
				cameraTarget.transform.rotation = Quaternion.Euler(agentRotation + defaultRotation + driverLookingAngle);
			}
		}

		private void HandleCameraRotation(){
			if (Input.GetMouseButtonDown (BTN_LEFT)) {
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
				mousePosAtMouseDown = Input.mousePosition;
				driverLookingAngleAtMouseDown = driverLookingAngle;
			} else if (Input.GetMouseButton (BTN_LEFT) && !mouseOverUIOnMouseDown) {
				Vector3 currentMousePos = Input.mousePosition;

				//calculate the left-right turn offset
				float rotationOffsetHorizontal = (currentMousePos.x - mousePosAtMouseDown.x) * rotationSensitivity;
				driverLookingAngle.y = driverLookingAngleAtMouseDown.y - rotationOffsetHorizontal;
			}

			if (AutoPanning){
				AddPanningAngle ();
			}
		}

		private void AddPanningAngle(){
			if((driverLookingAngle.y > 20f && autoPanningSpeed > 0) || 
				(driverLookingAngle.y < -20f && autoPanningSpeed < 0))
				autoPanningSpeed *= -1;
			driverLookingAngle.y += autoPanningSpeed * Time.deltaTime;
		}
	}
}