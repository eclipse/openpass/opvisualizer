﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Visualizer.World.Environment;
using Visualizer.World.OpenDrive;
using Visualizer.Core;
using Visualizer.AgentHandling;
using Zenject;

namespace Visualizer.CameraHandling
{
	public enum ViewMode { LookDown = 0, Perspective, Driver, Sensor };

	public interface ICameraManager{
		void Clear ();
		void Init ();
		void SetDefaultPosition (Map map);
		void FocusCamera (Agent agent);
		void SwitchViewMode (ViewMode viewMode);
		Vector3 GetCameraPos ();
		void EnableDemoMode (bool enable);
	}

	/// <summary>
	/// Central camera management class that handles the switching of the camera controllers. The script must be attached to the main camera
	/// </summary>
	public class CameraManager : MonoBehaviour, ICameraManager {

		private CCFollowDriver followDriverController;
		private CCFollowSensor followSensorController;
		private CCFreeFlyOrbit freeFlyOrbitController;

		private Agent focusedAgent = null;
		private ViewMode currentViewMode = ViewMode.Perspective;
		private GameObject cameraTarget;
		private Vector3 cameraPos;

		private float switchStartTime = 0f;
		private float switchDuration = 6f; //!<Controls the speed of the smooth camera switch 

		private IAgentManager agentManager;
		private IFocusFrameRenderer focusFrameRenderer;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAgentManager aManager, IFocusFrameRenderer ffRenderer){
			agentManager = aManager;
			focusFrameRenderer = ffRenderer;
		}

		/// <summary>
		/// MonoBehaviour function that is called on creation of the CameraManager
		/// </summary>
		void Start () {
			followDriverController = GetComponent<CCFollowDriver>();
			followSensorController = GetComponent<CCFollowSensor>();
			freeFlyOrbitController = GetComponent<CCFreeFlyOrbit>();
			cameraTarget = GameObject.Find ("CameraTarget");
		}
			
		/// <summary>
		/// Initializes the camera manager. Must be called before navigating
		/// </summary>
		public void Init (){
			//calculate the camera limits according to the terrain edges
			Vector3 margin = new Vector3(50f, 0f, 50f);
			Vector3 minPos = Terrain.activeTerrain.transform.position + margin;
			Vector3 maxPos= Terrain.activeTerrain.transform.position + Terrain.activeTerrain.terrainData.size - margin;
			minPos.y = 0.5f;
			//The highest camera position must be lower than the clipping plane of the camera to avoid disappearing scenery when zooming out in LookDown mode
			maxPos.y = Camera.main.farClipPlane - 100f;
			followDriverController.Init(minPos, maxPos);
			followSensorController.Init(minPos, maxPos);
			freeFlyOrbitController.Init(minPos, maxPos, focusFrameRenderer);
		}

		/// <summary>
		/// Activates the default camera controller and moves the camera to default position
		/// </summary>
		public void SetDefaultPosition(Map map){
			MoveToRoadBegin (map);
			SwitchCameraController (ViewMode.Perspective, null);
		}

		/// <summary>
		/// Moves the camera to the beginning of the loaded road. It finds the road element with the ID that is the lowest in the alphabet.
		/// </summary>
		private void MoveToRoadBegin(Map map){
			Road firstRoad = map.GetRoadNetwork().GetData().Roads.OrderBy (key => key.Key).First ().Value;
			Vector3 roadStartLocation = Vector3.zero;
			Vector3 roadStartForwardDir = Vector3.zero;
			firstRoad.GetWorldLocationAndDirectionAt (0f, 0f, out roadStartLocation, out roadStartForwardDir);
			freeFlyOrbitController.SetFocusPointPos(roadStartLocation);
		}

		/// <summary>
		/// Smoothly follows the CameraTarget GameObject with the camera.
		/// The mechanism is as follows: all camera controllers update only the CameraTarget GameObject, they don't change the real camera position/rotation.
		/// The Camera Target represents the raw camera position, without smoothing. It is here, where the real camera moves towards the Camera Target
		/// with a specific latency to create the smooth movement effect. The latency is normally 0. It only inreases after the user switches the camera controller,
		/// so that it creates the impression of softly moving the camera between positions.
		/// </summary>
		void LateUpdate(){
			float timeSinceSwitch = Mathf.Clamp(Time.time - switchStartTime, 0f, switchDuration);
			float way = timeSinceSwitch / switchDuration;

			// Make the transition parabolic -> speed the camera up as we are getting closer to the target. 
			// This avoids jitter when the playback is running and the camera is attached to an agent. 
			way = Mathf.Pow(way, 3)*100;

			cameraPos = this.transform.position;
			Quaternion cameraRot = this.transform.rotation;
			Vector3 cameraTargetPos = cameraTarget.transform.position;
			Quaternion cameraTargetRot = cameraTarget.transform.rotation;

			this.transform.position = Vector3.Lerp (cameraPos, cameraTargetPos, way);
			this.transform.rotation = Quaternion.Lerp (cameraRot, cameraTargetRot, way);
		}

		public Vector3 GetCameraPos (){
			return cameraPos;
		}

		/// <summary>
		/// Switches the camera controller. Internal function.
		/// </summary>
		private void SwitchCameraController(ViewMode viewMode, Agent agentToFocus){

			//save the time at the moment of the controller switch
			switchStartTime = Time.time;

			DisableAllControllers ();

			switch (viewMode) {
			case ViewMode.LookDown:
			case ViewMode.Perspective:
				freeFlyOrbitController.FocusedAgent = agentToFocus;
				freeFlyOrbitController.Enable(true);
				freeFlyOrbitController.SetViewMode(viewMode);
				break;
			case ViewMode.Driver:
				followDriverController.FocusedAgent = agentToFocus;
				followDriverController.Enable (true);
				break;
			case ViewMode.Sensor:
				followSensorController.FocusedAgent = agentToFocus;
				followSensorController.Enable (true);
				break;
			}
		}

		/// <summary>
		/// Public interface that attaches the camera to the specified agent.
		/// </summary>
		public void FocusCamera(Agent agent){
			SwitchCameraController (currentViewMode, agent);
			focusedAgent = agent;
		}

		/// <summary>
		/// Public interface that switches the view mode to the specified one.
		/// </summary>
		public void SwitchViewMode(ViewMode viewMode){
			agentManager.EnableLightProjection (viewMode == ViewMode.LookDown);
			focusFrameRenderer.ShowFrame(focusedAgent);
			SwitchCameraController (viewMode, focusedAgent);
			currentViewMode = viewMode;
		}

		/// <summary>
		/// Disables all camera controllers.
		/// </summary>
		private void DisableAllControllers(){
			followDriverController.Enable(false);
			followSensorController.Enable(false);
			freeFlyOrbitController.Enable(false);
		}

		/// <summary>
		/// Enables/disables automatic camera panning used for demo mode.
		/// </summary>
		public void EnableDemoMode(bool enable){
			freeFlyOrbitController.AutoPanning = enable;
		}

		/// <summary>
		/// Clears all camera controllers.
		/// </summary>
		public void Clear(){
			followDriverController.Clear();
			followSensorController.Clear();
			freeFlyOrbitController.Clear();
		}
	}
}