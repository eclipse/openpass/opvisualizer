﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.Utilities;
using Visualizer.AgentHandling;
using Visualizer.UI;
using Zenject;

namespace Visualizer.CameraHandling
{
	public interface IFocusCameraHandler{
		void FocusCamera (int agentID);
		void FocusOnEgo ();
		Agent GetFocusedAgent ();
		void ResetFocus();
		void Init ();
		void Clear ();
	}

	/// <summary>
	/// Handles the attachment of the camera to agents
	/// </summary>
	public class FocusCameraHandler : MonoBehaviour, IFocusCameraHandler {

		private Agent focusedAgent = null;

		private IViewModeHandler viewModeHandler;
		private IAgentManager agentManager;
		private ICameraManager cameraManager;
		private IFocusFrameRenderer focusFrameRenderer;
		private IMainUI ui;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IFocusFrameRenderer ffRenderer, IMainUI mUi, ICameraManager cManager, IAgentManager aManager, IViewModeHandler vmHandler){
			cameraManager = cManager;
			viewModeHandler = vmHandler;
			agentManager = aManager;
			focusFrameRenderer = ffRenderer;
			ui = mUi;
		}

		public void Init(){
			Clear();
		}

		/// <summary>
		/// Public interface that any other visualizer component can use to attach the camera to the ego agent.
		/// </summary>
		public void FocusOnEgo(){
			List<int> agentIDs = agentManager.GetAgentIDs ();
			foreach (int aID in agentIDs) {
				Agent agent = agentManager.GetAgent (aID);
				if (!agent.IsHidden () && agent.StaticAgentInfo.TypeGroupName == "Ego") {
					FocusCamera (aID);
				}
			}
		}
			
		/// <summary>
		/// Public interface that any other visualizer component can use to attach the camera to the specified agent.
		/// </summary>
		public void FocusCamera(int agentID){
			if(agentID != -1 && !agentManager.GetAgentIDs().Contains(agentID)){
				Debug.Log("Silently failed attaching the camera as the requested agent ID ('" + agentID + "') is unknown");
				return;
			}

			Focus (agentID);
		}

		/// <summary>
		/// Public interface that any other visualizer component can use to find the currently focused agent. Returns null if the camera is not attached to any agent.
		/// </summary>
		public Agent GetFocusedAgent(){
			return focusedAgent;
		}

		/// <summary>
		/// Public interface to center the camera back to the focus agent (useful if the agent is out of view)
		/// </summary>
		public void ResetFocus(){
			if(focusedAgent == null)
				return;
				
			cameraManager.FocusCamera (focusedAgent);
			focusFrameRenderer.ShowFrame(focusedAgent);
		}

		/// <summary>
		/// Internal function that attaches the camera to the specified agent
		/// </summary>
		private void Focus(int agentID){
			if (agentID == -1)
				focusedAgent = null;
			else
				focusedAgent = agentManager.GetAgent (agentID);
			viewModeHandler.EnableDriverViewMode (focusedAgent != null);
			cameraManager.FocusCamera (focusedAgent);
			ui.ShowFocusAgentPanel(focusedAgent);
			focusFrameRenderer.ShowFrame(focusedAgent);
		}
			
		/// <summary>
		/// Clear this instance.
		/// </summary>
		public void Clear(){
			focusedAgent = null;
			viewModeHandler.EnableDriverViewMode(false);
		}
	}
}