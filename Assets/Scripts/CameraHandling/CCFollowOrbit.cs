﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace Visualizer.CameraHandling
{
	/// <summary>
	/// Camera controller that fixes the camera at a certain distance to a vehicle and rotates around it. 
	/// - Mouse-Scroll = camera distance to the vehicle
	/// - Moving mouse left-right while holding the left button = rotate camera horizontally around the agent
	/// - Moving mouse up-down while holding the left button = rotate camera vertically around the agent
	/// </summary>
	public class CCFollowOrbit : CCBase {

		private float turnSpeed = 5f;
		private float distance = 10f;
		private float offsetX;
		private float offsetY;
		private float minY = 5f;
		private float maxY = 70f;
		private float minDistance = 5f;
		private float maxDistance = 70f;	
		private float smoothedAgentRot;
		private float smoothVel;
		private float autoPanningSpeed = 13f;
		private float smoothingRotationTime = 0.5f;
		private float smoothingRotationLimit = 0.05f; //the angle under which rotation smoothing should not be aplied (to avoid disturbing very slow camera movement at the end of smoothing ramp)

		protected override void ResetCamera(){		
			offsetX = 90f;
			offsetY = 16f;
			distance = 20f;
		}

		private void Update(){
			if (Input.GetMouseButtonDown (BTN_LEFT)) {
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
			}
			if (Input.GetMouseButtonUp (BTN_LEFT)) {
				mouseOverUIOnMouseDown = false;
			}
			if (!mouseOverUIOnMouseDown && Input.GetMouseButton (BTN_LEFT)) {
				offsetX += Input.GetAxis ("Mouse X") * turnSpeed;
				offsetY -= Input.GetAxis ("Mouse Y") * turnSpeed;
				offsetY = Mathf.Clamp (offsetY, minY, maxY);
			}

			if (scrollWheelDelta != 0 && !EventSystem.current.IsPointerOverGameObject ()) {
				//Note that the offset is not changing linearly with the scroller. 
				//It feels more natural if we move the camera faster up/down when the camera is higher above the ground
				distance -= (scrollWheelDelta * distance);
				distance = Mathf.Clamp (distance, minDistance, maxDistance);
				scrollWheelDelta = 0;
			}

			if (AutoPanning)
				offsetX += autoPanningSpeed * Time.deltaTime;
			
			if (FocusedAgent != null) {
				Vector3 agentPos = FocusedAgent.gameObject.GetComponent<BoxCollider> ().bounds.center;
				float agentRot = FocusedAgent.transform.rotation.eulerAngles.y;
				Vector3 direction = new Vector3 (0, 0, -distance);

				if (Mathf.Abs (smoothedAgentRot - agentRot) < smoothingRotationLimit)
					smoothedAgentRot = agentRot;
				smoothedAgentRot = Mathf.SmoothDampAngle (smoothedAgentRot, agentRot, ref smoothVel, smoothingRotationTime);

				Quaternion rotation = Quaternion.Euler (offsetY, offsetX + smoothedAgentRot, 0);
				// with the lookOffsetwe make sure that is in the lower third of the screen (less ground visible) 
				Vector3 lookOffset = new Vector3(0f, 0.1f * distance, 0f);
				cameraTarget.transform.position = agentPos + rotation * direction;
				cameraTarget.transform.LookAt (agentPos + lookOffset);
			}
		}
	}
}