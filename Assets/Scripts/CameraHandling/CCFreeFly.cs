﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Visualizer.CameraHandling
{
	/// <summary>
	/// Camera controller that can move freely in the scene. 
	/// - Mouse-Scroll = camera height above the ground
	/// - Moving mouse while holding the left button = drag camera
	/// - Moving mouse while holding the right button = rotate camera around it's own axis
	/// </summary>
	public class CCFreeFly : CCBase {

		private Plane plane = new Plane(Vector3.up, Vector3.zero);

		private float minTiltAngle = 0f;
		private float maxTiltAngle = 90f;

		private float rotationSensitivity = 0.1f;

		private Vector2 cameraRotation;
		private Vector3 cameraOffset;

		private Vector2 cameraRotationAtMouseDown;
		private Vector3 mousePosAtMouseDown;

		protected override void ResetCamera(){		
			cameraRotation.x = 12f;	//up-down rotation 
			cameraOffset.y = 10f; //height of camera
		}

		void Update () {
			HandleMouseScroll ();
			HandleCameraDrag ();
			HandleCameraRotation ();
			UpdateCamera ();
		}

		private void UpdateCamera(){
			cameraOffset.x = Mathf.Clamp (cameraOffset.x, minCameraPos.x, maxCameraPos.x);
			cameraOffset.z = Mathf.Clamp (cameraOffset.z, minCameraPos.z, maxCameraPos.z);
				
			cameraTarget.transform.position = cameraOffset;
			cameraTarget.transform.rotation = Quaternion.Euler (cameraRotation.x, cameraRotation.y, cameraTarget.transform.rotation.eulerAngles.z); 
		}

		private void HandleMouseScroll(){
			bool mouseOverGUIElement = EventSystem.current.IsPointerOverGameObject ();
			if (!mouseOverGUIElement && scrollWheelDelta != 0) {
				//Note that the offset is not changing linearly with the scroller. 
				//It feels more natural if we move the camera faster up/down when the camera is higher above the ground
				float scrollOffset = cameraOffset.y - (scrollWheelDelta * cameraOffset.y);
				cameraOffset.y = Mathf.Clamp (scrollOffset, minCameraPos.y, maxCameraPos.y);
				scrollWheelDelta = 0;
			}
		}

		private void HandleCameraDrag(){	
			if (Input.GetMouseButton (BTN_LEFT))
				return;

			if (Input.GetMouseButtonDown (BTN_RIGHT)) {
				//first, make sure, the mouse pointer is not over a GUI element
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				float distance;
				plane.Raycast (ray, out distance);
				mousePosAtMouseDown = ray.GetPoint (distance);
				mousePosAtMouseDown.y = 0;
			} else if (Input.GetMouseButton (BTN_RIGHT) && !mouseOverUIOnMouseDown) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				float distance;
				plane.Raycast (ray, out distance);
				//limit how far the user can drag, to avoid accidental large jump (when clicking close to the horizont)
				if (distance < maxCameraPos.y + 50f) {
					Vector3 currentMousePos = ray.GetPoint (distance);
					currentMousePos.y = 0;
					cameraOffset -= (currentMousePos - mousePosAtMouseDown);
				}
			}
		}

		private void HandleCameraRotation(){
			if (Input.GetMouseButton (BTN_RIGHT))
				return;

			if (Input.GetMouseButtonDown (BTN_LEFT)) {
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
				mousePosAtMouseDown = Input.mousePosition;
				cameraRotationAtMouseDown = cameraTarget.transform.rotation.eulerAngles;
			} else if (Input.GetMouseButton (BTN_LEFT) && !mouseOverUIOnMouseDown) {
				Vector3 currentMousePos = Input.mousePosition;

				//calculate the left-right turn offset
				float rotationOffsetHorizontal = (currentMousePos.x - mousePosAtMouseDown.x) * rotationSensitivity;
				cameraRotation.y = cameraRotationAtMouseDown.y - rotationOffsetHorizontal;

				//calculate the up-down turn offset
				float rotationOffsetVertical = (currentMousePos.y - mousePosAtMouseDown.y) * rotationSensitivity;
				cameraRotation.x = Mathf.Clamp (cameraRotationAtMouseDown.x + rotationOffsetVertical, minTiltAngle, maxTiltAngle);
			}
		}

		public override void Enable (bool enable) {
			cameraOffset = cameraTarget.transform.position;
			cameraRotation.x = cameraTarget.transform.rotation.eulerAngles.x;
			cameraRotation.y = cameraTarget.transform.rotation.eulerAngles.y;
			base.Enable (enable);
		}
	}
}