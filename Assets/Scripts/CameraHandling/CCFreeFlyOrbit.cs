﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace Visualizer.CameraHandling
{

	public class ViewProfile {
		public float minV { get; set; } 			//!<minimal vertical camera angle
		public float maxV  { get; set; } 			//!<maximal vertical camera angle
		public float minZoomDist  { get; set; } 	//!<minimal distance between camera and the focus point
		public float maxZoomDist  { get; set; } 	//!<maximal distance between camera and the focus point
		public float zoomDist { get; set; } 		//!<current distance between camera and the focus point
		public float cameraOffsetH { get; set; } 	//!<current horizontal camera position on an orbit around the focus point
		public float cameraOffsetV { get; set; } 	//!<current vertical camera position on an orbit around the focus point
	}

	/// <summary>
	/// Camera controller that can roate on an orbit of a focus point. The focus point can be attached to an agent.
	/// The controller can handle two views: Perspective and LookDown. The difference is only the limits of the camera.
	/// The camera chain is as follows: Agent -> FocusPoint -> CameraTarget -> Camera. The user can adjust the 
	/// offset between the attached agent and focus point (focusPointAgentOffset), and also the offset between 
	/// the camera target and the focus point (focusPointCameraOffset), which represents the position of the camera
	/// target on an orbit around the focus point. 
	/// - Mouse-Scroll = zoom on the focus point
	/// - Moving mouse while holding the left button = drag camera
	/// - Moving mouse while holding the right button = rotate camera around the focus point
	/// </summary>
	public class CCFreeFlyOrbit : CCBase {

		private Plane plane = new Plane(Vector3.up, Vector3.zero);
		private float turnSpeed = 5f;
		private bool moveCameraBehindAgent;
		private Vector3 focusPointCameraOffset;
		private Vector3 focusPointAgentOffset;
		private Vector3 focusPointPos;	
		private Vector2 cameraRotationAtMouseDown;
		private Vector3 mousePosAtMouseDown;
		private Vector3 smoothVel;
		private ViewMode vMode;

		/// <summary>
		/// These are the view profiles that store a set of view mode specific variables
		/// </summary>
		private Dictionary<ViewMode, ViewProfile> profiles = new Dictionary<ViewMode, ViewProfile> {
			{ ViewMode.Perspective, new ViewProfile { 
					minV = 1f,
					maxV = 60f,
					minZoomDist = 5f,
					maxZoomDist = 500f,
					cameraOffsetH = 0f,
					cameraOffsetV = 15f,
					zoomDist = 15f
				}
			}, 
			{ ViewMode.LookDown, new ViewProfile { 
					minV = 88f,
					maxV = 88f,
					minZoomDist = 20f,
					maxZoomDist = 1900f,
					cameraOffsetH = 0f,
					cameraOffsetV = 88f,
					zoomDist = 50f
				}
			}
		};

		/// <summary>
		/// Clears the camera controller. Called after file loading completes.
		/// </summary>
		public override void Clear() {
			moveCameraBehindAgent = true;
		}

		/// <summary>
		/// Public interface to move the focus point to the given coordinate
		/// </summary>
		public void SetFocusPointPos(Vector3 pos) {
			focusPointPos = pos;
		}

		/// <summary>
		/// Resets the camera controller. Called after each view mode switch.
		/// </summary>
		protected override void ResetCamera(){		
			focusPointAgentOffset = Vector3.zero;
		}

		/// <summary>
		/// Sets the internal view mode
		/// </summary>
		public void SetViewMode(ViewMode viewMode) {
			this.vMode = viewMode;
		}

		/// <summary>
		/// Monobehaviour function called once per frame
		/// </summary>
		void Update () {
			HandleAgentFollowing ();
			HandleCameraDrag ();
			HandleZoom();
			HandleCameraRotation (false);
			UpdateCamera ();
		}

		/// <summary>
		/// Moves the focus point position to a user defined offset from the (moving) agent
		/// </summary>
		private void HandleAgentFollowing(){
			if (FocusedAgent != null) {
				if (Input.GetMouseButton (BTN_RIGHT))
					return;

				Vector3 agentPos = FocusedAgent.gameObject.GetComponent<BoxCollider> ().bounds.center;
				float agentRot = FocusedAgent.transform.rotation.eulerAngles.y;

				if (Input.GetMouseButtonUp (BTN_RIGHT))
					focusPointAgentOffset = focusPointPos - agentPos;

				focusPointPos = agentPos + focusPointAgentOffset;	
			}		
		}

		/// <summary>
		/// Handles user input (right mouse) that moves the focus point the terrain
		/// We limit how far the user can drag, to avoid accidental large jump when clicking close to the horizont
		/// </summary>
		private void HandleCameraDrag(){	
			if (Input.GetMouseButton (BTN_LEFT))
				return;

			if (Input.GetMouseButtonDown (BTN_RIGHT)) {
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				float distance;
				bool valid = plane.Raycast (ray, out distance);
				if (valid && distance < profiles[vMode].maxZoomDist*1.2f) {
					mousePosAtMouseDown = ray.GetPoint (distance);
					mousePosAtMouseDown.y = 0;
				} else {
					mousePosAtMouseDown = Vector3.zero;
				}
			} else if (Input.GetMouseButton (BTN_RIGHT) && !mouseOverUIOnMouseDown && mousePosAtMouseDown != Vector3.zero) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				float distance;
				bool valid = plane.Raycast (ray, out distance);
				if (valid && distance < profiles[vMode].maxZoomDist*1.2f) {
					Vector3 currentMousePos = ray.GetPoint (distance);
					currentMousePos.y = 0;
					Vector3 oldFocusPointPos = focusPointPos;
					focusPointPos -= (currentMousePos - mousePosAtMouseDown);
					if(focusFrameRenderer != null && FocusedAgent != null)
						focusFrameRenderer.ShowFrame(FocusedAgent);
				}
			}
		}

		/// <summary>
		/// Handles user input (mouse scroll) that moves the camera closer/farther from the focus point
		/// Note that the offset is not changing linearly with the scroller. 
		/// It feels more natural if we zoom faster when the camera is higher above the ground
		/// </summary>
		private void HandleZoom() {
			if (scrollWheelDelta != 0 && !EventSystem.current.IsPointerOverGameObject ()) {
				profiles[vMode].zoomDist -= (scrollWheelDelta * profiles[vMode].zoomDist);
				profiles[vMode].zoomDist = Mathf.Clamp (profiles[vMode].zoomDist, profiles[vMode].minZoomDist, profiles[vMode].maxZoomDist);
				scrollWheelDelta = 0;
				if(focusFrameRenderer != null && FocusedAgent != null)
					focusFrameRenderer.ShowFrame(FocusedAgent);
			}
		}

		/// <summary>
		/// Handles user input (left mouse) that rotates the camera on an orbit around the focus point
		/// Note that the first time after loading a simulation, we rotate the camera so, 
		/// that it is behind the agent
		/// </summary>
		private void HandleCameraRotation(bool init) {
			if (Input.GetMouseButtonDown (BTN_LEFT)) 
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
			if (Input.GetMouseButtonUp (BTN_LEFT))
				mouseOverUIOnMouseDown = false;

			if (!mouseOverUIOnMouseDown && Input.GetMouseButton (BTN_LEFT) || init) {
				profiles[vMode].cameraOffsetH += Input.GetAxis ("Mouse X") * turnSpeed;
				profiles[vMode].cameraOffsetV -= Input.GetAxis ("Mouse Y") * turnSpeed;
				profiles[vMode].cameraOffsetV = Mathf.Clamp (profiles[vMode].cameraOffsetV, profiles[vMode].minV, profiles[vMode].maxV);

				if(focusFrameRenderer != null && FocusedAgent != null)
					focusFrameRenderer.ShowFrame(FocusedAgent);
			}

			if(moveCameraBehindAgent && FocusedAgent != null){
				float agentRot = FocusedAgent.transform.rotation.eulerAngles.y;
				profiles[vMode].cameraOffsetH = agentRot + 90f;
				moveCameraBehindAgent = false;
			}

			Vector3 direction = new Vector3 (0, 0, -profiles[vMode].zoomDist);
			Quaternion rotation = Quaternion.Euler (profiles[vMode].cameraOffsetV, profiles[vMode].cameraOffsetH, 0);
			focusPointCameraOffset = Vector3.SmoothDamp (focusPointCameraOffset, rotation * direction, ref smoothVel, 0.15f);
		}

		/// <summary>
		/// Moves the camera target (that the camera follows smoothly - see CameraManager > LateUpdate) 
		/// to the position we precalculated based on mouse events and agent position. 
		/// </summary>
		private void UpdateCamera(){
			Vector3 cameraTargetPos = focusPointPos + focusPointCameraOffset;
			cameraTargetPos.x = Mathf.Clamp (cameraTargetPos.x, minCameraPos.x, maxCameraPos.x);
			cameraTargetPos.z = Mathf.Clamp (cameraTargetPos.z, minCameraPos.z, maxCameraPos.z);
			cameraTarget.transform.position = cameraTargetPos;
			cameraTarget.transform.LookAt (focusPointPos);
		}
	}
}