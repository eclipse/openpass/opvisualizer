﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Visualizer.AgentHandling;

namespace Visualizer.CameraHandling
{
	/// <summary>
	/// The base class of all camera controllers
	/// </summary>
	public abstract class CCBase : MonoBehaviour {

		public Agent FocusedAgent { get; set; }
		public bool AutoPanning { get; set; }

		protected Vector3 minCameraPos;
		protected Vector3 maxCameraPos;
		protected bool mouseOverUIOnMouseDown = false;
		protected GameObject cameraTarget;
		protected float scrollWheelDelta = 0f;
		private float scrollWheelSensitivity = 0.15f;
		protected IFocusFrameRenderer focusFrameRenderer = null;

		protected const int BTN_LEFT = 0;
		protected const int BTN_RIGHT = 1;

		void Awake () {
			cameraTarget = GameObject.Find ("CameraTarget");
			Clear ();
			if (Application.platform == RuntimePlatform.WebGLPlayer) 
				scrollWheelSensitivity = 0.25f;
		}

		public virtual void Init(Vector3 minPos, Vector3 maxPos, IFocusFrameRenderer ffRenderer = null){
			minCameraPos = minPos;
			maxCameraPos = maxPos;
			focusFrameRenderer = ffRenderer;
		}
		
		public virtual void Clear(){
			FocusedAgent = null;
			ResetCamera ();
		}

		public virtual void Enable (bool enable) {
			this.enabled = enable;
			ResetCamera ();
		}
			
		protected virtual void ResetCamera(){
		}

		/// <summary>
		/// This is a workaround to detect touchpad scroll events as Input.GetAxis("Mouse Scrollwheel") does not work for touch pads (in Unity 5.5)
		/// </summary>
		public void OnGUI()
		{
			if (Event.current.type == EventType.ScrollWheel && !EventSystem.current.IsPointerOverGameObject ())
				scrollWheelDelta = -1 * Event.current.delta.y * scrollWheelSensitivity;
		}
	}
}