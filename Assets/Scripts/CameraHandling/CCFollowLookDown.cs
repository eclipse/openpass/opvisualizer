﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Visualizer.CameraHandling
{
	/// <summary>
	/// Camera controller that fixes the camera at a certain height above a vehicle and looks down to it.
	/// - Mouse-Scroll = camera height above ground. 
	/// - Moving mouse while holding the left button = rotate camera
	/// </summary>
	public class CCFollowLookDown: CCBase {

		private float rotationSensitivity = 0.1f;

		private Vector2 cameraRotation;
		private Vector3 cameraOffset;

		private Vector2 cameraRotationAtMouseDown;
		private Vector3 mousePosAtMouseDown;

		protected override void ResetCamera(){		
			cameraRotation.x = 90f;	//up-down rotation 	
			cameraRotation.y = 0f; //left-right rotation
			cameraOffset.y = 100f; //height of camera
		}

		void Update () {
			if (FocusedAgent != null) {
				Vector3 agentPos = FocusedAgent.transform.position;
				cameraOffset.x = agentPos.x;
				cameraOffset.z = agentPos.z;

				HandleMouseScroll ();
				HandleCameraRotation ();
				UpdateCamera ();
			}
		}

		private void UpdateCamera(){
			cameraOffset.x = Mathf.Clamp (cameraOffset.x, minCameraPos.x, maxCameraPos.x);
			cameraOffset.z = Mathf.Clamp (cameraOffset.z, minCameraPos.z, maxCameraPos.z);

			cameraTarget.transform.position = cameraOffset;
			cameraTarget.transform.rotation = Quaternion.Euler (cameraRotation.x, cameraRotation.y, cameraTarget.transform.rotation.eulerAngles.z); 
		}

		private void HandleMouseScroll(){
			bool mouseOverGUIElement = EventSystem.current.IsPointerOverGameObject ();
			if (!mouseOverGUIElement && scrollWheelDelta != 0) {
				//Note that the offset is not changing linearly with the scroller. 
				//It feels more natural if we move the camera faster up/down when the camera is higher above the ground
				float scrollOffset = cameraOffset.y - (scrollWheelDelta * cameraOffset.y);
				cameraOffset.y = Mathf.Clamp (scrollOffset, minCameraPos.y, maxCameraPos.y);
				scrollWheelDelta = 0;
			}
		}

		private void HandleCameraRotation(){
			if (Input.GetMouseButtonDown (BTN_LEFT)) {
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
				mousePosAtMouseDown = Input.mousePosition;
				cameraRotationAtMouseDown = cameraTarget.transform.rotation.eulerAngles;
			} else if (Input.GetMouseButton (BTN_LEFT) && !mouseOverUIOnMouseDown) {
				Vector3 currentMousePos = Input.mousePosition;

				//calculate the left-right turn offset
				float rotationOffsetHorizontal = (currentMousePos.x - mousePosAtMouseDown.x) * rotationSensitivity;
				cameraRotation.y = cameraRotationAtMouseDown.y - rotationOffsetHorizontal;
			}

			if (AutoPanning)
				cameraRotation.y += 8f * Time.deltaTime;
		}

		public override void Enable (bool enable) {
			cameraOffset = cameraTarget.transform.position;
			cameraRotation.x = cameraTarget.transform.rotation.eulerAngles.x;
			cameraRotation.y = cameraTarget.transform.rotation.eulerAngles.y;
			base.Enable (enable);
		}
	}
}