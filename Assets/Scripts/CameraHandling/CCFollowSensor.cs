﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Visualizer.AgentHandling;

namespace Visualizer.CameraHandling
{
	/// <summary>
	/// Camera controller that fixes the camera at a certain distance to a vehicle and rotates around it. 
	/// - Mouse-Scroll = camera distance to the vehicle
	/// - Moving mouse left-right while holding the left button = rotate camera horizontally around the agent
	/// - Moving mouse up-down while holding the left button = rotate camera vertically around the agent
	/// </summary>
	public class CCFollowSensor : CCBase {

		private float turnSpeed = 5f;
		private float distance = 10f;
		private float offsetX;
		private float offsetY;
		private float minY = 1f;
		private float maxY = 70f;
		private float minDistance = 0.4f;
		private float maxDistance = 70f;	
		private Vector3 smoothVel;
		private Vector3 smoothedSensorPos;
		private Transform sensor = null;

		public override void Clear () {
			sensor = null;
			base.Clear();
		}

		protected override void ResetCamera(){		
			offsetX = -60f;
			offsetY = 10f;
			distance = 15f;
			if(FocusedAgent != null){
				sensor = FocusedAgent.GetComponentInChildren<PositionerSensorModel> ()?.transform;
			}
		}

		private void Update(){
			if (Input.GetMouseButtonDown (BTN_LEFT)) {
				mouseOverUIOnMouseDown = EventSystem.current.IsPointerOverGameObject ();
			}
			if (Input.GetMouseButtonUp (BTN_LEFT)) {
				mouseOverUIOnMouseDown = false;
			}
			if (!mouseOverUIOnMouseDown && Input.GetMouseButton (BTN_LEFT)) {
				offsetX += Input.GetAxis ("Mouse X") * turnSpeed;
				offsetY -= Input.GetAxis ("Mouse Y") * turnSpeed;
				offsetY = Mathf.Clamp (offsetY, minY, maxY);
			}

			if (scrollWheelDelta != 0 && !EventSystem.current.IsPointerOverGameObject ()) {
				//Note that the offset is not changing linearly with the scroller. 
				//It feels more natural if we move the camera faster up/down when the camera is higher above the ground
				distance -= (scrollWheelDelta * distance/7);
				distance = Mathf.Clamp (distance, minDistance, maxDistance);
				scrollWheelDelta = 0;
			}
			
			if (sensor != null) {
				Vector3 direction = new Vector3 (0, 0, -distance);

				Quaternion rotation = Quaternion.Euler (offsetY, offsetX, 0);
				smoothedSensorPos = Vector3.SmoothDamp(smoothedSensorPos, sensor.position, ref smoothVel, 1.5f);
				cameraTarget.transform.position = smoothedSensorPos + rotation * direction;
				cameraTarget.transform.LookAt (smoothedSensorPos);
			}
		}
	}
}