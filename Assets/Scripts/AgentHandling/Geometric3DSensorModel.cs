﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// This component creates and holds a mesh with the shape of a 3D sensor's detection range (slice of a sphere).
	/// </summary>
	public class Geometric3DSensorModel : SensorModelBase {

		private int pivotVerticeIdx = 0;
		private int rangeShieldMaxH = 0;
		private int rangeShieldMaxV = 0;
		private Vector3 defaultOrientation = new Vector3 (0f, 1.57f, 0f);
		private List <Vector3> rangeShield;
		private float shieldStepH = 0.0017f; //!<Horizontal sampling step of the circle in radian (~0.1 degree)
		private float shieldStepV = 0.0017f; //!<Vertical sampling step of the circle in radian (~0.1 degree)

		/// <summary>
		/// Creates the list of vertices that the sensor detection range mesh is composed of. The form is like a piece cut out of a sphere.
		/// </summary>
		protected override void CreateSensorField(SensorAttributes sensor)
		{
			CreateRangeShield (sensor);
			GenerateRangeMesh ();
		}

		/// <summary>
		/// Creates the list of vertices that the sensor detection range mesh is composed of. The form is like a piece cut out of a sphere.
		/// </summary>
		private void CreateRangeShield(SensorAttributes sensor)
		{
			rangeShieldMaxH = (int)(sensor.OpeningAngleH / shieldStepH) + 1;
			rangeShieldMaxV = (int)(sensor.OpeningAngleV / shieldStepV) + 1;

			rangeShield = new List<Vector3>();
			float startHAngle = -1 * (sensor.OpeningAngleH / 2) + defaultOrientation.x;
			float startVAngle = -1 * (sensor.OpeningAngleV / 2) + defaultOrientation.y;
			float currentHAngle = startHAngle;
			float currentVAngle = startVAngle;

			for (int stepIdxH = 0; stepIdxH < rangeShieldMaxH; stepIdxH++) {
				for (int stepIdxV = 0; stepIdxV < rangeShieldMaxV; stepIdxV++) {
					float x = sensor.DetectionRange * Mathf.Sin (currentVAngle) * Mathf.Cos (currentHAngle);
					float y = sensor.DetectionRange * Mathf.Cos (currentVAngle);
					float z = sensor.DetectionRange * Mathf.Sin (currentVAngle) * Mathf.Sin (currentHAngle);
					rangeShield.Add (new Vector3 (x, y, z));
					currentVAngle += shieldStepV;
				}
				currentHAngle += shieldStepH;
				currentVAngle = startVAngle;
			}

			//add sensor position to pivot point
			rangeShield.Add(Vector3.zero);
			pivotVerticeIdx = rangeShield.Count-1; 
		}

		/// <summary>
		/// Generates the range mesh from the vertices. The vertices must be calculated before calling this function.
		/// </summary>
		private void GenerateRangeMesh()
		{
			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			Mesh mesh = filter.mesh;
			mesh.Clear();

			List<int> triangles = new List<int>();

			//create the rangeShield
			int tri1P1 = 0;
			int tri1P2 = rangeShieldMaxV;
			int tri1P3 = 1;
			int tri2P1 = 1;
			int tri2P2 = rangeShieldMaxV;
			int tri2P3 = rangeShieldMaxV+1;

			for (int stepIdxH = 0; stepIdxH < rangeShieldMaxH; stepIdxH++) {
				for (int stepIdxV = 0; stepIdxV < rangeShieldMaxV; stepIdxV++) {
					if (stepIdxV < rangeShieldMaxV - 1 && 
						stepIdxH < rangeShieldMaxH - 1) {
						triangles.Add(tri1P1);
						triangles.Add(tri1P2);
						triangles.Add(tri1P3);

						triangles.Add(tri2P1);
						triangles.Add(tri2P2);
						triangles.Add(tri2P3);
					}
					tri1P1++;
					tri1P2++;
					tri1P3++;
					tri2P1++;
					tri2P2++;
					tri2P3++;
				}
			}

			//create the left side of the range
			int leftTopVerticeIdx = 0;
			for (int stepIdxV = 0; stepIdxV < rangeShieldMaxV - 1; stepIdxV++) {
				triangles.Add (pivotVerticeIdx);
				triangles.Add (stepIdxV + leftTopVerticeIdx);
				triangles.Add (stepIdxV + leftTopVerticeIdx + 1);
			}

			//create the right side of the range
			int rightTopVerticeIdx = (rangeShieldMaxH-1) * rangeShieldMaxV;
			for (int stepIdxV = 0; stepIdxV < rangeShieldMaxV - 1; stepIdxV++) {
				triangles.Add (pivotVerticeIdx);
				triangles.Add (stepIdxV + rightTopVerticeIdx + 1);
				triangles.Add (stepIdxV + rightTopVerticeIdx);
			}

			//create the top of the range
			for (int stepIdxH = 0; stepIdxH < rangeShieldMaxH-1; stepIdxH++) {
				triangles.Add (stepIdxH * rangeShieldMaxV);
				triangles.Add (pivotVerticeIdx);
				triangles.Add ((stepIdxH * rangeShieldMaxV) + rangeShieldMaxV);
			}

			//create the bottom of the range
			for (int stepIdxH = 0; stepIdxH < rangeShieldMaxH - 1; stepIdxH++) {
				triangles.Add (((stepIdxH + 1) * rangeShieldMaxV) - 1);
				triangles.Add (((stepIdxH + 2) * rangeShieldMaxV) - 1);
				triangles.Add (pivotVerticeIdx);
			}

			mesh.vertices = rangeShield.ToArray();
			mesh.triangles = triangles.ToArray();
			Vector2[] uvs = new Vector2[rangeShield.Count];
			int i = 0;
			while (i < uvs.Length) {
				uvs[i] = new Vector2(0.5f, 0.5f);
				i++;
			}
			mesh.uv = uvs;
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
		}
	}
}