﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Utilities;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Class that represents an agent. The script must be attached to all agent prefabs. The attributes of this class always represent the current status of an agent, updated by the AgentManager during playback.
	/// </summary>
	public class Agent : MonoBehaviour {

		public int ID { get; set; }  //!<The unique ID of the agent
		public Vector3 SourcePosition { get; set; } //!<The position of the agent at the beginning of the time frame of a sample. Used for position interpolation between samples.
		public Vector3 TargetPosition { get; set; } //!<The position of the agent at the end of the time frame of a sample.
		public bool BrakeLight { get; set; } //!<Indicates whether the brake light is on or off 
		public int TurnSignal { get; set; } //!<Indicates the status of the turn lights (for valid values see enum TurnSignalState)
		public float SourceYawAngle { get; set; } //!<The yaw angle of the agent at the beginning of the time frame of a sample. Used for position interpolation between samples.
		public float TargetYawAngle { get; set; } //!<The yaw angle of the agent at the end of the time frame of a sample.
		public float SourceRollAngle { get; set; } //!<The roll angle of the agent at the beginning of the time frame of a sample. Used for position interpolation between samples.
		public float TargetRollAngle { get; set; } //!<The roll angle of the agent at the end of the time frame of a sample.
		public int AreaOfInterest { get; set; } //!<The direction at which the driver is currently looking (for valid values see AOIController)
		public Crash CrashStatus { get; set; } //!<Indicates if the agent at the current point of time is before a crash, after a crash or is not going to crash
		public int CrashTimeStamp { get; set; } //!<The time stamp at which the agent has a collision 
		public float LastUpdate { get; set; } //!<The time of the last update in seconds since app start. Used for movement interpolation.
		public float AnimationInterval { get; set; } //!<The time period between two sample updates. Used for movement interpolation.
		public Vector3 DriversEyePos { get; private set; } //!<The offset of the driver's eyes measured from the vehicle's pivot point 
		public float DriverViewDistance { get; set; } //!<The distance how far he driver can see (due to environment conditions)
		public AgentProperties StaticAgentInfo { get; set; } = new AgentProperties(); //!<Various static parameters of the agent
		public Dictionary<string, string> OptionalAttributes { get; set; } //!<Agent attributes that can be displayed in the AgentInfoPanel. It contains all information (interpreted and noninterpreted) read from the clyclics tag of the trace file.

		#region editor options
		[SerializeField]
		[Tooltip("Drag here one of the tires. If the tire is included in the wheel GameObject, drag here one of the wheels. The bounds of this object must represent the total size of the wheel. We need the size for the calculation of the rotation angle during movement")]
		private GameObject modelTire; //!<One of the model's tire GameObject. The bounds of this object must represent the total size of the wheel. We need the size for the calculation of the rotation angle during movement

		[SerializeField]
		[Tooltip("Drag here in the Unity inspector all wheels of the vehicle model that should be rotated around the X axis. Which wheels these are, depends on the 3D model.")]
		private GameObject[] modelWheelsX; //!<All wheels of the vehicle model that should be rotated around the X axis. Which wheels these are, depends on the 3D model.

		[SerializeField]
		[Tooltip("Drag here in the Unity inspector all wheels of the vehicle model that should be rotated around the Y axis. Which wheels these are, depends on the 3D model.")]
		private GameObject[] modelWheelsY; //!<All wheels of the vehicle model that should be rotated around the Y axis. Which wheels these are, depends on the 3D model.

		[SerializeField]
		[Tooltip("Drag here in the Unity inspector all wheels of the vehicle model that should be rotated around the Y axis. Which wheels these are, depends on the 3D model.")]
		private GameObject[] modelWheelsZ; //!<All wheels of the vehicle model that should be rotated around the Z axis. Which wheels these are, depends on the 3D model.

		[SerializeField]
		[Tooltip("Drag here in the Unity inspector all wheels of the vehicle model that should be rotated around the Y axis. Which wheels these are, depends on the 3D model.")]
		private bool modelWheelRotateBackwards = false; //!<The direction of the rotation of the vehicle model's wheels. It depends on the implementation vehicle model.

		[SerializeField]
		[Tooltip("Drag here in the Unity inspector the material of the model that represents the paint color of the vehicle.")]
		private Material modelPaint = null; //!<The material of the model that represents the paint color of the vehicle

		[SerializeField]
		[Tooltip("Set here in the Unity inspector the colors that should be randomly assigned to the vehicle.")]
		private Color[] bodyColorOptions; //!<The colors that are randomly assigned to the vehicle

		[SerializeField]
		[Tooltip("Drag here in the Unity inspector an empty (invisible) GameObject of the vehicle model that represents the position of the driver's eyes")]
		private GameObject driversEyes = null; //!<An empty (invisible) GameObject of the vehicle model that represents the position of the driver's eyes
		#endregion

		private BoxCollider boundingBox = null; //<!Bounding box of the vehicle
		private Vector3 hidePos = new Vector3 (0f, 3000f, 0f); //!<The position in word coordinates where agents should be moved to "become invisible". Make sure it is further from the terrain than the cameras clipping range.
		private float tireRadius; //!<The radius of a tire
		private AOIController aoiController = null; //!<The component that handles the area of interest
		private DVRController dvrController = null; //!<The component that handles the driver's visual range
		private SensorController sensorController = null; //!<The component that handles the low range sensors
		private TurnSignalController turnSignalController = null; //!<The component that animates the turn lights
		private BrakeLightController brakeLightController = null; //!<The component that animates the brake lights
		private Transform rootTransform = null; //!<The root game object of the agent that is independent of the pivot point's position  
		private Animator agentAnimator = null; //!<The animation controller that is attached to the agent, in case it is a humanoid
		private bool playbackActive = false; //!<Flag to signal if the simulation is being played back
		private float animationSpeed = 1f; //!<The playback speed that the user sets in the UI

		/// <summary>
		/// The state of the local animation in case this agent is a character
		/// </summary>
		private enum CharacterAnimationState { 
			Unknown = -1, 
			Idle = 0, 
			Walking = 1, 
			Running = 2
		};

		/// <summary>
		/// MonoBehaviour function that is called on creation of an agent instance
		/// </summary>
		public void Start(){
			if(modelTire != null)
				tireRadius = modelTire.transform.GetComponent<Renderer> ().bounds.extents.y;

			//add the ID to the name (only for debugging in Unity)  
			transform.gameObject.name = transform.gameObject.tag + " " + this.ID;

			if(driversEyes != null)
				DriversEyePos = driversEyes.transform.position;

			turnSignalController = gameObject.GetComponent<TurnSignalController> ();
			brakeLightController = gameObject.GetComponent<BrakeLightController> ();

			if(transform.gameObject.tag == "Agent") {
				aoiController = gameObject.GetComponent<AOIController> ();
				if(aoiController == null)
					aoiController = gameObject.AddComponent<AOIController> ();

				Vector3 aoiArrowPos = DriversEyePos;
				if (boundingBox != null) {
					aoiArrowPos.y = boundingBox.center.y + boundingBox.size.y/2f + 0.5f;
					aoiArrowPos.z = boundingBox.center.z;
				}
				aoiController.CreateAOIArrows (this.transform, aoiArrowPos);
				aoiController.ShowAOIArrow = true;
				
				dvrController = gameObject.GetComponent<DVRController> ();
				if(dvrController == null)
					dvrController = gameObject.AddComponent<DVRController> ();
				
				dvrController.Init (this.transform, DriversEyePos, DriverViewDistance);
			}

			if (rootTransform != null)
				agentAnimator = rootTransform.GetComponent<Animator>();
		}

		/// <summary>
		/// Set defaults
		/// </summary>
		public void Awake(){
			CrashTimeStamp = 0;

			rootTransform = this.transform.Find ("PivotPositioner");
			if (rootTransform == null) {
				Debug.LogError ("PivotPositioner not found in agent prefab!");
				return;
			}
		}

		/// <summary>
		/// Sets the tag on the Agent's GameObject (it will help us to identify agent game objects (e.g. when clicking on an object))
		/// </summary>
		public void SetTag(string tag) {
			transform.gameObject.tag = tag;
		}

		/// <summary>
		/// Creates the graphical representation of vehicle sensors
		/// </summary>
		public void CreateSensors(List<SensorAttributes> sensors){
			if (sensorController == null) {
				sensorController = gameObject.AddComponent<SensorController> ();
				sensorController.CreateSensors (this.transform, sensors);
			}
		}

		/// <summary>
		/// Scales the vehicles body according to the specified dimensions
		/// Note that we do not scale agents that are animated (like pedestrians), as their animation might get broken
		/// if their body is scaled improportionally.
		/// </summary>
		public void ScaleVehicle(AgentProperties agentProperties){	
			VehicleAttributes attributes = agentProperties.VehicleInfo;

			GetBounds();

			if (attributes == null || boundingBox == null || rootTransform == null)
				return;
		
			Vector3 scale = Vector3.zero;
			scale.z = (attributes.Width > 0f) ? attributes.Width / boundingBox.size.z : 1f;
			scale.y = (attributes.Height > 0f) ? attributes.Height / boundingBox.size.y : 1f;
			scale.x = (attributes.Length > 0f) ? attributes.Length / boundingBox.size.x : 1f;

			if (rootTransform != null && rootTransform.GetComponent<Animator>() != null)
				scale = new Vector3(1f, 1f, 1f);

			//scale and translate the vehicle
			rootTransform.localScale = scale;
			Vector3 pos = rootTransform.position;
			pos.x = -attributes.PivotOffset;
			rootTransform.position = pos;

			//scale and translate also the bounding box
			boundingBox.size = Vector3.Scale(boundingBox.size, scale);
			boundingBox.center = new Vector3(-attributes.PivotOffset, boundingBox.center.y*scale.y, boundingBox.center.z);
		}

		/// <summary>
		/// MonoBehaviour function that is called once per frame
		/// </summary>
		public void Update () {
			UpdatePosition ();
			UpdateRotation ();
			UpdateWheelRotation ();
			UpdateBreakLights ();
			UpdateAOI ();
			UpdateTurnLights ();
			UpdateCharacterAnimation();
		}

		/// <summary>
		/// Updates the agent animator component (used for pedestrian agents).
		/// </summary>
		private void UpdateCharacterAnimation(){
			if (agentAnimator == null)
				return;

			if(this.OptionalAttributes == null || !this.OptionalAttributes.ContainsKey("VelocityEgo"))
				return;

			float agentVelocity = Helpers.Str2Float(this.OptionalAttributes["VelocityEgo"]);

			//defaultCharacterMovementSpeed is the speed in m/s of the character's animated motion at 100% animation speed
			float defaultCharacterMovementSpeed = 0f; 

			CharacterAnimationState currentState = CharacterAnimationState.Unknown;
			if(agentVelocity < 0.1f) { // under 0.03km/h the character is idling
				currentState = CharacterAnimationState.Idle;
				defaultCharacterMovementSpeed = 0f;
			} else if (agentVelocity < 2.5f){ // under 8km/h the character is walking
				currentState = CharacterAnimationState.Walking;
				defaultCharacterMovementSpeed = agentAnimator.GetFloat("defaultWalkingSpeed");
			} else { // above 8km/h the character is running
				currentState = CharacterAnimationState.Running;
				defaultCharacterMovementSpeed = agentAnimator.GetFloat("defaultRunningSpeed");
			}

			agentAnimator.SetBool("isWalking", (currentState == CharacterAnimationState.Walking));
			agentAnimator.SetBool("isRunning", (currentState == CharacterAnimationState.Running));

			float timeSinceLastUpdate = Time.time - LastUpdate;
			float interpolationValue = timeSinceLastUpdate / AnimationInterval;

			if (interpolationValue < 2){
				agentAnimator.speed = Mathf.Clamp(agentVelocity / defaultCharacterMovementSpeed, 0f, 2f) * animationSpeed;
			} else {
				agentAnimator.speed = 0f;		
					agentAnimator.speed = 0f;		
				agentAnimator.speed = 0f;		
			}

			//override the calculated speed if the character is idle
			if(playbackActive && currentState == CharacterAnimationState.Idle) {
				agentAnimator.speed = 1f * animationSpeed;
			}
		}

		/// <summary>
		/// Updates the agent's yaw angle.
		/// </summary>
		private void UpdateRotation(){
			float timeSinceLastUpdate = Time.time - LastUpdate;
			float interpolationValue = timeSinceLastUpdate / AnimationInterval;
			Quaternion sourceRot = Quaternion.Euler(this.SourceRollAngle, this.SourceYawAngle, 0f);
			Quaternion targetRot = Quaternion.Euler(this.TargetRollAngle, this.TargetYawAngle, 0f);
			transform.rotation = Quaternion.Lerp(sourceRot, targetRot, interpolationValue);
		}

		/// <summary>
		/// Updates the agent's position.
		/// </summary>
		private void UpdatePosition(){
			float timeSinceLastUpdate = Time.time - LastUpdate;
			float interpolationValue = timeSinceLastUpdate / AnimationInterval;
			if (this.TargetPosition == Vector3.zero) {
				//going to zero ->  skip interpolation, move right away to hide pos 
				this.Hide ();
				return;
			}
			else if (this.SourcePosition == Vector3.zero) {
				//coming from zero -> skip interpolation, make it appear in the target pos at the end of the time frame
				if(interpolationValue > 0.9) 
					transform.position = this.TargetPosition;
				return;
			}
			//interpolate the movement
			transform.position = Vector3.Lerp (this.SourcePosition, this.TargetPosition, interpolationValue);
		}

		/// <summary>
		/// Updates the agent's wheel rotation.
		/// </summary>
		private void UpdateWheelRotation(){
			if (modelTire == null)
				return;
			
			if (transform.position != this.TargetPosition) {
				float distanceToTravelInCurrentSample = Vector3.Distance (this.SourcePosition, this.TargetPosition);
				if (distanceToTravelInCurrentSample == 0)
					return;
				
				float wheelCircumference = 2 * Mathf.PI * tireRadius;
				float rotationFraction = distanceToTravelInCurrentSample / wheelCircumference;
				float angleToMoveWheelInCurrentSample = rotationFraction * 360 * (modelWheelRotateBackwards?1:-1);
				float angleToMoveWheelInCurrentFrame = (Time.deltaTime / AnimationInterval) * angleToMoveWheelInCurrentSample;
			
				for (int idx = 0; idx < modelWheelsX.Length; idx++)
					if (modelWheelsX [idx])
						modelWheelsX [idx].transform.Rotate (new Vector3 (angleToMoveWheelInCurrentFrame, 0f, 0f)); 
				
				for (int idx = 0; idx < modelWheelsY.Length; idx++)
					if (modelWheelsY [idx])
						modelWheelsY [idx].transform.Rotate (new Vector3 (0f, angleToMoveWheelInCurrentFrame, 0f)); 

				for (int idx = 0; idx < modelWheelsZ.Length; idx++)
					if (modelWheelsZ [idx])
						modelWheelsZ [idx].transform.Rotate (new Vector3 (0f, 0f, angleToMoveWheelInCurrentFrame)); 
			}
		}

		/// <summary>
		/// Updates the agent's crash status.
		/// </summary>
		/// <param name="currentTimeStamp">Current time stamp.</param>
		public void UpdateCrashStatus(int currentTimeStamp){
			if (this.CrashTimeStamp == 0)
				this.CrashStatus = Crash.NoCrash;
			else if (currentTimeStamp < this.CrashTimeStamp)
				this.CrashStatus = Crash.WillCrash;
			else
				this.CrashStatus = Crash.HasCrashed;
		}

		/// <summary>
		/// Updates the break lights of the agent.
		/// </summary>
		private void UpdateBreakLights(){
			if (brakeLightController != null)
				brakeLightController.UpdateBrakeLights (this.BrakeLight);
		}

		/// <summary>
		/// Updates the area of interest arrow.
		/// </summary>
		private void UpdateAOI (){
			if (aoiController != null)
				aoiController.UpdateAOIArrow (this.AreaOfInterest);
		}

		/// <summary>
		/// Updates the turn lights of the agent.
		/// </summary>
		private void UpdateTurnLights (){
			if (turnSignalController == null)
				return;

			int turnSignal = this.TurnSignal;
			if (this.CrashStatus == Crash.HasCrashed)
				turnSignal = (int)TurnSignalState.Hazard;
			
			turnSignalController.UpdateTurnSignals (turnSignal);
		}

		/// <summary>
		/// Updates the attributes (position and orientation) of the sensors mounted on the agent
		/// </summary>
		public void UpdateSensorAttributes (List<SensorAttributes> sensors){
			if (sensorController != null)
				sensorController.UpdateSensorAttributes (sensors);
		}

		/// <summary>
		/// Starts the animation of agent components.
		/// </summary>
		/// <param name="aSpeed">Animation speed (1.0 = normal speed).</param>
		public void StartAnimation(float aSpeed){
			playbackActive = true;
			animationSpeed = aSpeed;
			if (turnSignalController != null)
				turnSignalController.StartAnimation (aSpeed);
		}

		/// <summary>
		/// Stops the animation of agent components.
		/// </summary>
		public void StopAnimation(){
			playbackActive = false;
			if (turnSignalController != null)
				turnSignalController.StopAnimation();
		}

		/// <summary>
		/// Gets the agents bounding box, which is the size of the box collider.
		/// A box collider should always be present on an agent and it must be manually 
		/// set to the total dimensions of the vehicle. 
		/// Note that dynamically calculating the bounding box (by adding all 
		/// subcomponent bounds of the vehicle) is not possible, if model parts are rotated
		/// as also the bounding boxes of the rotated parts are rotated and are therefore incorrect
		/// </summary>
		private void GetBounds()
		{
			boundingBox = gameObject.GetComponent<BoxCollider> ();
			if (boundingBox == null)
				Debug.LogError("Missing BoxCollider of agent " + ID + ".");
		}

		/// <summary>
		/// Returns the 8 vertices of the agent's bounding box in world space including current rotation.
		/// </summary>
		public Vector3[] GetBoundingBox (){
			Vector3[] vertices = new Vector3[8];

			if (boundingBox == null)
				GetBounds ();
			
			if (boundingBox != null) {
				Matrix4x4 thisMatrix = transform.localToWorldMatrix;
				Vector3 extents = boundingBox.size/2f;
				Vector3 center = boundingBox.center; 
				vertices [0] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x + extents.x, center.y + extents.y, center.z + extents.z));
				vertices [1] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x + extents.x, center.y + extents.y, center.z - extents.z));
				vertices [2] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x + extents.x, center.y - extents.y, center.z + extents.z));
				vertices [3] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x + extents.x, center.y - extents.y, center.z - extents.z));
				vertices [4] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x - extents.x, center.y + extents.y, center.z + extents.z));
				vertices [5] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x - extents.x, center.y + extents.y, center.z - extents.z));
				vertices [6] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x - extents.x, center.y - extents.y, center.z + extents.z));
				vertices [7] = thisMatrix.MultiplyPoint3x4(new Vector3 (center.x - extents.x, center.y - extents.y, center.z - extents.z));
			}
			return vertices;
		}

		/// <summary>
		/// Sets the color of the agent vehicle based on it's ID. 
		/// </summary>
		/// <remarks>
		/// It is coupled to the agent ID to make sure that a specific agent in a specific trace file will always get the same color. 
		/// It recolors all sub components of the agent model that share the material that we set in the inspector as modelPaint
		/// </remarks>
		public void SetPseudoRandomBodyColor(int agentID){
			if (modelPaint == null || bodyColorOptions.Length == 0 || !Application.isPlaying)
				return;

			//Choose a color based on the agent's ID
			int colorIdx = agentID % bodyColorOptions.Length;
			Color color = bodyColorOptions [colorIdx];

			foreach (Renderer renderer in GetComponentsInChildren<Renderer>()) {
				foreach (Material mat in renderer.materials) {
					if (mat.name.Contains(modelPaint.name)) {
						mat.color = color;
					}
				}
			}
		}

		/// <summary>
		/// Shows/hides the specified agent option.
		/// </summary>
		/// <param name="optionKey">The key of the option that should be shown/hidden.</param>
		/// <param name="value">Show or hide</param>
		public void ShowDisplayOption(string optionKey, bool value){
			switch (optionKey) {
			case "Visualized.AOIArrow":
				if(aoiController != null)
					aoiController.ShowAOIArrow = value;
				break;
			case "Visualized.VisibilityDistance":
				if (dvrController != null)
					dvrController.ShowDVR = value;
				break;
			case "Visualized.SensorRange":
				if (sensorController != null)
					//TODO: find a way of defining multiple sensors as agent options according to the sensor specification in SimulationOutput 
					sensorController.ShowSensors = value;
				break;
			default:
				// unknown option key -> skip
				break;
			}
		}

		/// <summary>
		/// Clears all visualized agent display options
		/// </summary>
		/// <param name="optionKey">The key of the option that should be shown/hidden.</param>
		/// <param name="value">Show or hide</param>
		public void ClearDisplayOptions(){
			if(aoiController != null)
				aoiController.ShowAOIArrow = false;
			if (dvrController != null)
				dvrController.ShowDVR = false;
			if (sensorController != null)
				sensorController.ShowSensors = false;
		}

		/// <summary>
		/// Specifies wether the brake and turning lights of the agents should be projected on the ground (useful in lookdown mode)
		/// </summary>
		public void EnableLightProjection (bool enable) {
			if (turnSignalController != null)
				turnSignalController.EnableLightProjection (enable);
			if (brakeLightController != null)
				brakeLightController.EnableLightProjection (enable);
		}

		/// <summary>
		/// Hide the agent (moves out of sight of the camera).
		/// </summary>
		private void Hide(){
			transform.position = hidePos;
		}

		/// <summary>
		/// Determines whether the agent is hidden.
		/// </summary>
		/// <returns><c>true</c> if the agent is hidden; otherwise, <c>false</c>.</returns>
		public bool IsHidden(){
			return (TargetPosition == hidePos || TargetPosition == Vector3.zero || gameObject.activeSelf == false);
		}
	}
}