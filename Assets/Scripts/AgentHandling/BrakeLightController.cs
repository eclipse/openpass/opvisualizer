﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Controls the display of brake lights and brake lamps of an agent. Must be manually added to every agent prefab as component.
	/// </summary>
	public class BrakeLightController : MonoBehaviour {

		[Tooltip("Drag here in the Unity inspector the parent GameObject of all brake lights of the agent's 3D model.")]
		public GameObject modelBrakeLights; //!<The parent GameObject of all brake lights of the agent's 3D model. Must be set manually for every agent prefab in the Unity inspector.

		[Tooltip("Drag here in the Unity inspector all of the GameObjects of the vehicle model that represent a brake lamp")]
		public Renderer[] modelBrakeLamps; //!<An array of all GameObjects of the car model that represent a brake lamp. Must be set manually for every agent prefab in the Unity inspector.

		[Tooltip("Drag here in the Unity inspector the material that should be applied to the brake lamps when they are turned on")]
		public Material brakeLampONMat = null; //!<The material that should be applied to the brake lamps when they are turned on. Must be set manually for every agent prefab in the Unity inspector.

		[Tooltip("Drag here in the Unity inspector the material that should be applied to the brake lamps when they are turned off")]
		public Material brakeLampOFFMat = null; //!<The material that should be applied to the brake lamps when they are turned on. Must be set manually for every agent prefab in the Unity inspector.

		private bool enableProjection = false;

		void Start(){
			if (brakeLampONMat == null)
				brakeLampONMat = Resources.Load ("Materials/BrakeLampON", typeof(Material)) as Material;
			if (brakeLampOFFMat == null)
				brakeLampOFFMat = Resources.Load ("Materials/BrakeLampOFF", typeof(Material)) as Material;
		}

		/// <summary>
		/// Updates the material of the brake lamps and turns brake lights on/off.
		/// </summary>
		/// <param name="value">On or off</param>
		public void UpdateBrakeLights(bool value){
			if(modelBrakeLights != null)
				modelBrakeLights.SetActive (value && enableProjection);

			for (int idx = 0; idx < modelBrakeLamps.Length; idx++)
				if (modelBrakeLamps [idx] != null && brakeLampONMat != null && brakeLampOFFMat != null)
					modelBrakeLamps [idx].material = value ? brakeLampONMat : brakeLampOFFMat;
		}

		/// <summary>
		/// Specifies wether the brake lights should be projected on the ground
		/// </summary>
		public void EnableLightProjection (bool enable) {
			enableProjection = enable;
		}
	}
}