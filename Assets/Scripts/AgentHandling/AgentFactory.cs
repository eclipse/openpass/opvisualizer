﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.AgentHandling
{
	public interface IAgentFactory{
		Agent SpawnAgent(AgentProperties agentProps, bool randomColor = true, bool scaleVehicle = true);
	}

	/// <summary>
	/// A class that is responsible for agent creation.
	/// </summary>
	public class AgentFactory : IAgentFactory {

		private GameObject loadedInstance;
		private IUnityIO unityIO;
		
		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IUnityIO uIO){
			unityIO = uIO;
		}

		/// <summary>
		/// Spawns an agent with the given parameters and and with pseudo random color.
		/// </summary>
		/// <returns>The spawned agent.</returns>
		public Agent SpawnAgent(AgentProperties agentProps, bool randomColor = true, bool scaleVehicle = true){
			GameObject agentInstance = LoadVehicle(agentProps.VehicleModelName);

			Agent agent = agentInstance.GetComponent<Agent> ();
			agent.ID = agentProps.ID;
			agent.CreateSensors (agentProps.Sensors);	
			agent.DriverViewDistance = agentProps.DriverViewDistance;
			agent.StaticAgentInfo = agentProps;
			agent.SetTag("Agent");
			if (scaleVehicle)
				agent.ScaleVehicle(agentProps);
			if (randomColor)
				agent.SetPseudoRandomBodyColor (agentProps.ID);
			return agent;
		}

		/// <summary>
		/// Loads an addressable vehicle prefab resource by the given asset key. 
		/// If the resource is not found, it falls back to default vehicle
		/// </summary>
		private GameObject LoadVehicle(string assetKey){	
			GameObject vehiclePrefab = null;
			vehiclePrefab = unityIO.LoadAsset(assetKey);
			if (vehiclePrefab == null) {
				Debug.LogError("Could not load vehicle type '" + assetKey + "'. Falling back to default vehicle.");
				vehiclePrefab = Resources.Load ("Prefabs/Vehicles/Unknown/UnknownVehicle_Prefab", typeof(GameObject)) as GameObject;
			}
			GameObject agentInstance = MonoBehaviour.Instantiate(vehiclePrefab, Vector3.zero, Quaternion.identity) as GameObject;
			return agentInstance;
    	}
	}
}