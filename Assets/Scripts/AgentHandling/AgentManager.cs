﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Utilities;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.UI;
using System;
using Zenject;

namespace Visualizer.AgentHandling
{
	public interface IAgentManager{
		void SpawnAgents (List<AgentProperties> agentProps);
		void UpdateAgents (AgentSample[] sample, int timeStamp);
		void SetAnimationInterval (float animationInterval);
		void UpdateDisplayOptionVisibility(List<DisplayOption> focusAgentDisplayOptions, List<DisplayOption> nonFocusAgentDisplayOptions);
		void UpdateCrashTimeStamp(int agentID, int timeStamp);
		void StartAgentAnimation (float animationSpeed);
		void StopAgentAnimation ();
		void EnableLightProjection (bool enable);
		void ExtendStaticAgentProps (List<AgentProperties> agentProps);
		Agent GetAgent (int agentID, bool mustExist = true);
		int GetAgentCount ();
		List<int> GetAgentIDs ();
		void Clear ();
	}

	static class AgentTypes
	{
		public const string Vehicle = "Vehicle";
		public const string Object = "Object";
	}

	/// <summary>
	/// Manages all the agents and their updates.
	/// </summary>
	public class AgentManager : IAgentManager{

		private Dictionary<int, Agent> agents = null; //<!This list holds a reference to all agents. 
		private IAgentFactory agentFactory;	
		private IFocusCameraHandler focusCameraHandler;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAgentFactory aFactory, IFocusCameraHandler acHandler){
			agentFactory = aFactory;
			focusCameraHandler = acHandler;
		}

		/// <summary>
		/// Creates the agents.
		/// </summary>
		/// <param name="agentProps">List of agent properties that describe the agent</param>
		public void SpawnAgents (List<AgentProperties> agentProps){
			agents = new Dictionary<int, Agent>();

			foreach (AgentProperties agentProp in agentProps) {
				Agent agent = agentFactory.SpawnAgent(agentProp);
				agents.Add (agentProp.ID, agent);
			}
		}

		/// <summary>
		/// Adds static attributes to the existing agents
		/// </summary>
		public void ExtendStaticAgentProps (List<AgentProperties> agentProps){
			foreach (AgentProperties agentProp in agentProps) {
				Agent agent = GetAgent(agentProp.ID, false);
				if (agent != null)
					agent.StaticAgentInfo.NonInterpretedProps.AddRange(agentProp.NonInterpretedProps);
			}
		}

		/// <summary>
		/// The core funtion of AgentManager. It takes a sample from the SampleContainernd and passes them to the agents. 
		/// It is called every time the AnimationController triggers a new frame update.
		/// Note that it is valid to pass null as sample, which means that there is no data for the given time stamp
		/// and the agents are removed from the world for this time stamp
		/// </summary>
		/// <param name="sample">Snapshot of all agents at the specified point in time</param>
		/// <param name="timeStamp">Time stamp</param>
		public void UpdateAgents (AgentSample[] sample, int timeStamp) {
			if (sample == null) 
				return;
			
			if (sample.Length > 0) {
				for (int aSampleIdx = 0; aSampleIdx < sample.Length; aSampleIdx++) {
					AgentSample agentSample = sample [aSampleIdx];
					Agent agent = GetAgent(agentSample.AgentID);
					if (agent != null) {
						agent.SourcePosition = agent.TargetPosition;
						agent.TargetPosition = new Vector3 (agentSample.XPosition, 0f, agentSample.YPosition);
						agent.BrakeLight = agentSample.BrakeLight;
						agent.TurnSignal = agentSample.TurnSignal;
						agent.SourceYawAngle = agent.TargetYawAngle;
						agent.TargetYawAngle = agentSample.YawAngle;
						agent.SourceRollAngle = agent.TargetRollAngle;
						agent.TargetRollAngle = agentSample.RollAngle;
						agent.AreaOfInterest = agentSample.AOI;
						agent.LastUpdate = Time.time;
						agent.UpdateCrashStatus (timeStamp);
						agent.OptionalAttributes = agentSample.OptionalAttributes;
					}
				}
			} else {
				foreach(Agent agent in agents.Values){
					agent.TargetPosition = Vector3.zero;
					agent.LastUpdate = Time.time;
					agent.OptionalAttributes = new Dictionary<string, string>();
				}
			}
		}

		/// <summary>
		/// Passes the animation interval to the agents. Used to ensure that the agents local animation (e.g. wheel rotation) corresponds to the current plyback speed
		/// </summary>
		/// <param name="animationInterval">Animation interval.</param>
		public void SetAnimationInterval (float animationInterval) {
			if(agents != null)
				foreach (int aID in agents.Keys)
					agents [aID].AnimationInterval = animationInterval;
		}

		/// <summary>
		/// Passes the animation start event to the agents so that the agents can start their local animation (e.g. wheel rotation).
		/// </summary>
		/// <param name="animationSpeed">Animation speed.</param>
		public void StartAgentAnimation (float animationSpeed) {
			if(agents != null)
				foreach (int agentID in agents.Keys)
					agents [agentID].StartAnimation(animationSpeed);
		}

		/// <summary>
		/// Passes the animation stop event to the agents so that the agents can stop their local animation (e.g. wheel rotation).
		/// </summary>
		public void StopAgentAnimation () {
			if(agents != null)
				foreach (int aID in agents.Keys)
					agents [aID].StopAnimation();
		}

		/// <summary>
		/// Passes the enable brake lights event to the agents
		/// </summary>
		public void EnableLightProjection (bool enable) {
			if(agents != null)
				foreach (int aID in agents.Keys)
					agents [aID].EnableLightProjection(enable);
		}

		/// <summary>
		/// Returns an agent safely from the local dictionary based on agent ID. 
		/// It throws an exception if the flag mustExists is set.
		/// </summary>
		public Agent GetAgent(int agentID, bool mustExist = true){
			if (agents == null)
				return null;
			
			if (!agents.ContainsKey (agentID))
				if(mustExist)
					throw new Exception ("Agent ID '" + agentID + "' is invalid");
				else
					return null;

			return agents [agentID];
		}

		/// <summary>
		/// Returns the agent count in the local dictionary.
		/// </summary>
		public int GetAgentCount(){
			if (agents == null)
				return 0;
			
			return agents.Count; 
		}
			
		/// <summary>
		/// Gets the list of agent IDs currently loaded.
		/// </summary>
		public List<int> GetAgentIDs(){
			if (agents == null)
				return new List<int> ();
			
			return new List<int>(agents.Keys);
		}

		/// <summary>
		/// Triggers a refresh of the visibility of agent options 
		/// </summary>
		public void UpdateDisplayOptionVisibility(List<DisplayOption> focusAgentDisplayOptions, List<DisplayOption> nonFocusAgentDisplayOptions){
			if (agents == null)
				return;

			Agent focusAgent = focusCameraHandler.GetFocusedAgent();
			int focusAgentID = (focusAgent != null) ? focusAgent.ID : -1;
			foreach (int aID in agents.Keys){
				if (focusAgentID == agents[aID].ID)
					ActivateDisplayOptions(agents[aID], focusAgentDisplayOptions);
				else
					ActivateDisplayOptions(agents[aID], nonFocusAgentDisplayOptions);
			}
		}

		/// <summary>
		/// Shows/Hides the options of the specified agent
		/// </summary>
		private void ActivateDisplayOptions(Agent agent, List<DisplayOption> displayOptions) {
			agent.ClearDisplayOptions();
			foreach (DisplayOption displayOption in displayOptions){
				agent.ShowDisplayOption(displayOption.Key, displayOption.Enabled);
			}
		}

		/// <summary>
		/// Destroys all Agents, the AgentManager and frees up memory.
		/// </summary>
		public void Clear(){
			if (agents != null) 
				foreach (int aID in agents.Keys)
					GameObject.Destroy (agents [aID].gameObject);					

			agents = null;
		}

		/// <summary>
		/// Sets the CrashTimeStamp of an agent. 
		/// It does not update the value, if it is a following collision (not the first collision of the agent in this run)
		/// </summary>
		public void UpdateCrashTimeStamp(int agentID, int timeStamp){
			if(!GetAgentIDs().Contains(agentID)){
				Debug.Log("Silently failed updating crash sample index as the requested agent ID ('" + agentID + "') is unknown");
				return;
			}

			Agent agent = GetAgent (agentID);
			if (agent == null) 
				return;
			
			if(agent.CrashTimeStamp == 0 || timeStamp < agent.CrashTimeStamp) 
				agent.CrashTimeStamp = timeStamp;
		}
	}
}