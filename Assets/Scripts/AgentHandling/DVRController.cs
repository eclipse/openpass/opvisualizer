﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Controls the display of the driver's visual range. Must be added to every agent as component.
	/// </summary>
	public class DVRController : MonoBehaviour {

		public bool created = false;
		private bool visible = false;
		private GameObject dvr = null; //<!The GameObject that represents the visual range. Child of an agent.
		private Transform parentTransform = null;
		private Vector3 driverPos = Vector3.zero;
		private float viewDistance = 0;

		/// <summary>
		/// Creates the DVR GameObject. The DVR is just like a special type of sensor.
		/// </summary>
		/// <param name="driverPos">Driver's eye position.</param>
		/// <param name="viewDistance">Driver's view distance.</param>
		public void Init (Transform parent, Vector3 driversEye, float driverViewDistance)
		{
			parentTransform = parent;
			driverPos = driversEye;
			viewDistance = driverViewDistance;			
			ShowDVR = false;
		}

		/// <summary>
		/// Creates the DVR GameObject. The DVR is just like a special type of sensor.
		/// </summary>
		private void CreateDVR ()
		{
			GameObject sensorPrefab = Resources.Load ("Prefabs/SensorModels/Geometric2DSensorModel", typeof(GameObject)) as GameObject;
			dvr = Instantiate (sensorPrefab, driverPos, Quaternion.Euler (0, 0, 0)) as GameObject;
			dvr.transform.SetParent (parentTransform, false);

			SensorAttributes dvrAttr = new SensorAttributes ();
			dvrAttr.MountingPosition = driverPos;
			dvrAttr.OpeningAngleH = 6.28f; //360°
			dvrAttr.DetectionRange = viewDistance;
			dvrAttr.OrientationYaw = 0f;
			dvrAttr.OrientationPitch = 0f;
			dvrAttr.RangeSurfaceMaterial = Resources.Load ("Materials/DVR", typeof(Material)) as Material;
			dvr.GetComponent<Geometric2DSensorModel> ().Create (parentTransform, dvrAttr);
			created = true;
		}

		/// <summary>
		/// Gets or sets the visibility of the DVR
		/// Note that we create the mesh only the first time it is switched to visible as it can take 
		/// a couple of milliseconds -> if we created all agent DVRs at once, it
		/// would cause a noticable simulation loading delay.
		/// </summary>
		public bool ShowDVR { 
			get{ return visible; }
			set{ 
				visible = value; 
				if(visible && !created)
					CreateDVR();
				if(dvr != null)
					dvr.SetActive (visible); 
			}
		}
	}
}