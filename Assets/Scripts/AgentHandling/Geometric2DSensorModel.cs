﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// This component creates and holds a mesh with the shape of a 2D sensor's detection range (triangle).
	/// </summary>
	public class Geometric2DSensorModel : SensorModelBase {
		private Vector3 defaultOrientation = new Vector3 (0f, 1.57f, 0f);
		private List <Vector3> rangeShield;
		private float latStepAngle = 0.085f; //!<Sampling step of the circle in radian (~5 degree)
		private int waveCounter = 0;

		/// <summary>
		/// Creates the list of vertices that the sensor detection range mesh is composed of.
		/// </summary>
		protected override void CreateSensorField(SensorAttributes sensor)
		{
			CreateRangeShield (sensor);
			GenerateRangeMesh ();
		}

		/// <summary>
		/// Creates the list of vertices that the sensor detection range mesh is composed of. The form is a circle fraction.
		/// </summary>
		private void CreateRangeShield(SensorAttributes sensor)
		{
			rangeShield = new List<Vector3>();
			float startAngle = -1 * (sensor.OpeningAngleH / 2) + defaultOrientation.x;
			float endAngle = startAngle + sensor.OpeningAngleH;
			float currentAngle = startAngle;
			float currentDistance = sensor.DetectionRange;
			bool toggler = true;
			while(currentDistance > 0f) {
				while(true) {
					float x = currentDistance * Mathf.Cos (currentAngle);
					float z = currentDistance * Mathf.Sin (currentAngle);
					rangeShield.Add (new Vector3 (x, 0, z));
					if(currentAngle >= endAngle)
						break;
					currentAngle += latStepAngle;
					if(currentAngle > endAngle)
						currentAngle = endAngle;
				}
				currentAngle = startAngle;
				if (toggler)
					currentDistance -= Mathf.Max (currentDistance * 0.05f, 0.1f);
				else
					currentDistance -= Mathf.Max (currentDistance * 0.1f, 0.3f);
				toggler = !toggler;
				waveCounter++;
			}

			//DrawVertices ();
		}

		/// <summary>
		/// Generates the range mesh from the vertices. The vertices must be calculated before calling this function.
		/// </summary>
		private void GenerateRangeMesh()
		{
			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			Mesh mesh = filter.mesh;
			mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
			mesh.Clear();

			List<int> triangles = new List<int>();
			int stepCount = rangeShield.Count / waveCounter;

			for (int currentWaveIdx = 0; currentWaveIdx < waveCounter - 1; currentWaveIdx=currentWaveIdx+2) {
				for (int stepIdxH = 0; stepIdxH < stepCount - 1; stepIdxH++) {
					triangles.Add (stepIdxH + currentWaveIdx*stepCount);
					triangles.Add (stepIdxH + currentWaveIdx*stepCount + stepCount);
					triangles.Add (stepIdxH + currentWaveIdx*stepCount + stepCount + 1);

					triangles.Add (stepIdxH + currentWaveIdx*stepCount);		
					triangles.Add (stepIdxH + currentWaveIdx*stepCount + stepCount + 1);
					triangles.Add (stepIdxH + currentWaveIdx*stepCount + 1);
				}
			}

			mesh.vertices = rangeShield.ToArray();
			mesh.triangles = triangles.ToArray();
			Vector2[] uvs = new Vector2[rangeShield.Count];
			int i = 0;
			while (i < uvs.Length) {
				uvs[i] = new Vector2(0.5f, 0.5f);
				i++;
			}
			mesh.uv = uvs;
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
		}

		/// <summary>
		/// Creates a sphere in each vertex. Only for debugging purposes.
		/// </summary>
		private void DrawVertices() {
			GameObject prefab = null;
			prefab = Resources.Load ("Prefabs/Sphere") as GameObject;
			int idx = 0;
			foreach (Vector3 point in rangeShield) {
				GameObject p = GameObject.Instantiate (prefab, point, Quaternion.identity) as GameObject;
				p.transform.SetParent (gameObject.transform);
				p.name = idx.ToString ();
				idx++;
			}
		}
	}
}