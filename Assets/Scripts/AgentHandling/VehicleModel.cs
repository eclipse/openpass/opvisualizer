﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	[Serializable]
	public class VehicleModel
	{
		[SerializeField]
		private string type;

		[SerializeField]
		private GameObject prefab;

		public string Type{
			get { return type; }
			set { type = value; }
		}

		public GameObject Prefab{
			get { return prefab; }
			set { prefab = value; }
		}
	}
}

