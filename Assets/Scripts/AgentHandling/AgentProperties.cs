﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using Visualizer.Utilities;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Holds static attributes of an agent that are not changing during a simulation
	/// </summary>
	public class AgentProperties
	{
		public string TypeGroupName { set; get; } = "";
		public int ID { set; get; }
		public VehicleModelType VehicleModelType { set; get; }
		public VehicleAttributes VehicleInfo { set; get; }
		public float DriverViewDistance { set; get; }
		public List<SensorAttributes> Sensors { set; get; }
		public List<GenericListItem> NonInterpretedProps {set; get; } //!<List of all static agent attributes that are read from the trace file and displayed in the Static Agent Info Window but not specifically interpreted and used

		private string _vehicleModelName;
		public string VehicleModelName{
			get { return _vehicleModelName; }
			set { 
				_vehicleModelName = value; 
				VehicleModelType = GetModelType(_vehicleModelName);
			}
		}

		/// <summary>
		/// Returns the type of vehicle that the agent has
		/// </summary>
		private VehicleModelType GetModelType(string vehicleModelName) {
			if(vehicleModelName.StartsWith("car_"))
				return VehicleModelType.Car;
			if(vehicleModelName.StartsWith("bus_") || 
			   vehicleModelName.StartsWith("truck_"))
				return VehicleModelType.Truck;
			if(vehicleModelName.StartsWith("pedestrian_"))
				return VehicleModelType.Pedestrian;
			return VehicleModelType.Unknown;
		}
	}

	/// <summary>
	/// Holds static attributes of a vehicle
	/// </summary>
	public class VehicleAttributes
	{
		public float Height { set; get; }
		public float Width { set; get; }
		public float Length { set; get; }
		public float PivotOffset { set; get; } //the longitudinal position of the vehicles reference point, measured from the center of the vehicles bounding box
	}

	public enum VehicleModelType { 
		Unknown,
		Car, 
		Truck, 
		Pedestrian 
	}
}

