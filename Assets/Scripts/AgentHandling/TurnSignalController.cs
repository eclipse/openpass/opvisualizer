﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Turn signal state. Must match the values the OpenPASS Simulator is outputing in the trace file
	/// </summary>
	public enum TurnSignalState { 
		Unknown = -1, 
		Off = 0, 
		Left = 1, 
		Right = 2, 
		Hazard = 3
	};

	/// <summary>
	/// Controls the display and animation of turn lights and turn lamps of an agent. Must be manually added to every agent prefab as component.
	/// </summary>
	public class TurnSignalController : MonoBehaviour {

		[Tooltip("Drag here in the Unity inspector all the left turn lamp GameObjects (rear/front/side) of the agent's 3D model.")]
		public Renderer[] modelTurnSignalLampsLeft = null; //!<List of the left turn lamp GameObjects (rear/front/side) of the agent's 3D model.

		[Tooltip("Drag here in the Unity inspector all the right turn lamp GameObjects (rear/front/side) of the agent's 3D model.")]
		public Renderer[] modelTurnSignalLampsRight = null; //!<List of the right turn lamp GameObjects (rear/front/side) of the agent's 3D model.

		[Tooltip("Drag here in the Unity inspector the parent GameObject of all left turn lights (rear/front/side) of the agent's 3D model.")]
		public GameObject modelTurnLightsLeft = null; //<!The parent GameObject of all left turn lights (rear/front/side) of the agent's 3D model.

		[Tooltip("Drag here in the Unity inspector the parent GameObject of all right turn lights (rear/front/side) of the agent's 3D model.")]
		public GameObject modelTurnLightsRight = null; //<!The parent GameObject of all right turn lights (rear/front/side) of the agent's 3D model.

		[Tooltip("Optional. Drag here in the Unity inspector the material that should be applied to the turn signal lamps when they are turned on. If this field is left empty, the default turn signal on material is applied.")]
		public Material turnSignalONMat = null; //!<The material that should be applied to the turn signal lamps when they are turned on. Must be set manually for every agent prefab in the Unity inspector.

		[Tooltip("Optional. Drag here in the Unity inspector the material that should be applied to the turn signal lamps when they are turned off. If this field is left empty, the default turn signal off material is applied.")]
		public Material turnSignalOFFMat = null; //!<The material that should be applied to the turn signal lamps when they are turned on. Must be set manually for every agent prefab in the Unity inspector.

		private TurnSignalState currentState = TurnSignalState.Unknown;
		private bool flashSemaphore = true;
		private bool animating = false;
		private bool enableProjection = false;
		private float animationSpeed = 1f;

		void Start(){
			if (turnSignalONMat == null)
				turnSignalONMat = Resources.Load ("Materials/TurnSignalON", typeof(Material)) as Material;
			if (turnSignalOFFMat == null)
				turnSignalOFFMat = Resources.Load ("Materials/TurnSignalOFF", typeof(Material)) as Material;
		}

		/// <summary>
		/// Updates the turn signals.
		/// </summary>
		public void UpdateTurnSignals(int value){
			TurnSignalState state = (TurnSignalState)value;
			if (currentState != state) {
				currentState = state;
				if (animating)
					ShowStateDynamic (false);
				else
					ShowStateStatic ();
			}
		}

		/// <summary>
		/// Starts the flashing animation at the specified speed to correspond to the playback speed.
		/// </summary>
		public void StartAnimation(float aSpeed){
			animating = true;
			animationSpeed = aSpeed;
			ShowStateDynamic (true);
		}

		/// <summary>
		/// Stops the flashing animation.
		/// </summary>
		public void StopAnimation(){
			animating = false;
			ShowStateStatic ();
		}

		/// <summary>
		/// When the playback is not running, the turn signals ar continuously on
		/// </summary>
		private void ShowStateStatic(){
			CancelInvoke ("Flash");
			switch (currentState){
			case TurnSignalState.Off:
				SwitchLeft (false);
				SwitchRight (false);
				break;
			case TurnSignalState.Left:
				SwitchLeft (true);
				SwitchRight (false);
				break;
			case TurnSignalState.Right:
				SwitchLeft (false);
				SwitchRight (true);
				break;
			case TurnSignalState.Hazard:
				SwitchLeft (true);
				SwitchRight (true);
				break;
			}
		}

		/// <summary>
		/// When the playback is running, the turn signals flash
		/// </summary>
		/// <param name="randomStartDelay">True if the flashing should start after a random delay (0...0.5s). Used to imitate a cut in animation. When starting the playback and calling this function on every agent, it would look very unnatural, when all vehicles flashed synchronously.</param>
		private void ShowStateDynamic(bool randomStartDelay){
			if (currentState == TurnSignalState.Off) {
				ShowStateStatic ();
			}
			else{
				CancelInvoke ("Flash");
				float startDelay = 0f;
				if (randomStartDelay)
					startDelay = Random.Range (0f, 0.5f);
				InvokeRepeating ("Flash", startDelay, (1 / animationSpeed) * 0.4f);
			}
		}

		/// <summary>
		/// The core of the flashing logic
		/// </summary>
		private void Flash(){
			switch (currentState){
			case TurnSignalState.Left:
				SwitchLeft (flashSemaphore);
				SwitchRight (false);
				break;
			case TurnSignalState.Right:
				SwitchLeft (false);
				SwitchRight (flashSemaphore);
				break;
			case TurnSignalState.Hazard:
				SwitchLeft (flashSemaphore);
				SwitchRight (flashSemaphore);
				break;
			}
			flashSemaphore = !flashSemaphore;
		}

		/// <summary>
		/// Specifies wether the turn lights should be projected on the ground
		/// </summary>
		public void EnableLightProjection (bool enable) {
			enableProjection = enable;
		}

		/// <summary>
		/// Switches all left turn lights and lamps on/off
		/// </summary>
		private void SwitchLeft(bool value){
			if(modelTurnLightsLeft != null)
				modelTurnLightsLeft.SetActive (value && enableProjection);

			for (int idx = 0; idx < modelTurnSignalLampsLeft.Length; idx++)
				if (modelTurnSignalLampsLeft [idx] != null && turnSignalONMat != null && turnSignalOFFMat != null)
					modelTurnSignalLampsLeft [idx].material = value ? turnSignalONMat : turnSignalOFFMat;
		}

		/// <summary>
		/// Switches all right turn lights and lamps on/off
		/// </summary>
		private void SwitchRight(bool value){
			if (modelTurnLightsRight)
				modelTurnLightsRight.SetActive (value && enableProjection);

			for (int idx = 0; idx < modelTurnSignalLampsRight.Length; idx++)
				if (modelTurnSignalLampsRight [idx] != null && turnSignalONMat != null && turnSignalOFFMat != null)
					modelTurnSignalLampsRight [idx].material = value ? turnSignalONMat : turnSignalOFFMat;
		}
	}
}