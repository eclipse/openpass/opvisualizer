﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Controls the display of sensor detection fields. Can control multiple sensors per agent. Must be added to every agent as component.
	/// </summary>
	public class SensorController : MonoBehaviour {

		private bool visible = false;
		private List<GameObject> sensorsObjects = new List<GameObject>(); //<!List of GameObjects that represents the sensor fields. Children of an agent.
		private Transform pivot;

		/// <summary>
		/// Creates the Sensor GameObjects if the agent has any.
		/// </summary>
		/// <param name="sensors">List of sensors (and their attributes) to create</param>
		public void CreateSensors (Transform parent, List<SensorAttributes> sensors)
		{
			if(sensors == null)
				return;
				
			foreach(SensorAttributes sensor in sensors){	
				GameObject sensorPrefab = null;
				GameObject sensorInstance = null;
				pivot = parent;
				switch (sensor.Model) {
				case "SensorPositioner":
					sensorPrefab = Resources.Load ("Prefabs/SensorModels/PositionerSensorModel", typeof(GameObject)) as GameObject;
					sensor.RangeSurfaceMaterial = Resources.Load ("Materials/SensorPositionerField", typeof(Material)) as Material;
					sensorInstance = Instantiate (sensorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
					sensorInstance.transform.SetParent (pivot, false);
					sensorInstance.GetComponent<PositionerSensorModel> ().Create (pivot, sensor);
					break;
				case "Geometric2D":
					sensorPrefab = Resources.Load ("Prefabs/SensorModels/Geometric2DSensorModel", typeof(GameObject)) as GameObject;
					sensor.RangeSurfaceMaterial = Resources.Load ("Materials/SensorField", typeof(Material)) as Material;
					sensorInstance = Instantiate (sensorPrefab, sensor.MountingPosition, Quaternion.identity) as GameObject;
					sensorInstance.transform.SetParent (pivot, false);
					sensorInstance.GetComponent<Geometric2DSensorModel> ().Create (pivot, sensor);
					break;
				case "Geometric3D":
					sensorPrefab = Resources.Load ("Prefabs/SensorModels/Geometric3DSensorModel", typeof(GameObject)) as GameObject;
					sensor.RangeSurfaceMaterial = Resources.Load ("Materials/SensorField", typeof(Material)) as Material;
					sensorInstance = Instantiate (sensorPrefab, sensor.MountingPosition, Quaternion.identity) as GameObject;
					sensorInstance.transform.SetParent (pivot, false);
					sensorInstance.GetComponent<Geometric3DSensorModel> ().Create (pivot, sensor);
					break;
				}
				if(sensorInstance != null)
					sensorsObjects.Add (sensorInstance);
				else {
					Debug.LogWarning ("Displaying detection field of referenced sensor model '" + sensor.Model + "' is not supported.");
				}
			}
			ShowSensors = false;
		}

		/// <summary>
		/// Gets or sets the visibility of all LRS objects
		/// </summary>
		public bool ShowSensors { 
			get{ return visible; }
			set{ 
				visible = value; 
				foreach (GameObject sensorObject in sensorsObjects) {
					sensorObject.SetActive (visible); 
				}
			}
		}

		/// <summary>
		/// Updates the attributes (position and orientation) of the sensors
		/// </summary>
		public void UpdateSensorAttributes (List<SensorAttributes> sensors) {
			int idx = 0;
			foreach (GameObject sensorObject in sensorsObjects) {
				sensorObject.GetComponent<PositionerSensorModel> ().UpdateAttributes (pivot, sensors[idx]);
				idx++;
			}
		}
	}
}