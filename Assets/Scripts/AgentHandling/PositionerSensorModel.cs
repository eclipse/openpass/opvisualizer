﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// This component creates and holds a mesh with the shape of a 2D sensor's detection range (triangle).
	/// </summary>
	public class PositionerSensorModel : SensorModelBase {
		private int maxLatIdx = 0;
		private Vector3 defaultOrientation = new Vector3 (0f, 1.57f, 0f);
		private List <Vector3> rangeShield;
		private float latStepAngle = 0.0017f; //!<Sampling step of the circle in radian (~0.1 degree)
		private int waveCount = 0;

		/// <summary>
		/// Creates the list of vertices that the sensor detection range mesh is composed of.
		/// </summary>
		protected override void CreateSensorField(SensorAttributes sensor)
		{
			CreateRangeShield (sensor);
			GenerateRangeMesh ();
		}

		/// <summary>
		/// Creates the list of vertices that the sensor detection range mesh is composed of. The form is like a piece cut out of a sphere.
		/// </summary>
		private void CreateRangeShield(SensorAttributes sensor)
		{
			maxLatIdx = (int)(sensor.OpeningAngleH / latStepAngle) + 1;
			rangeShield = new List<Vector3>();

			AddWaveVertices (0.001f, sensor, ref rangeShield);
			AddWaveVertices (-0.001f, sensor, ref rangeShield);

			//DrawVertices ();
		}

		private void AddWaveVertices (float height, SensorAttributes sensor, ref List<Vector3> vertices) {
			float startAngle = -1 * (sensor.OpeningAngleH / 2) + defaultOrientation.x;
			float currentAngle = startAngle;
			float currentDistance = sensor.DetectionRange;
			waveCount = 0;

			bool toggler = true;
			while(currentDistance > 0f) {
				for (int latStepIdx = 0; latStepIdx < maxLatIdx; latStepIdx++) {
					float x = currentDistance * Mathf.Cos (currentAngle);
					float z = currentDistance * Mathf.Sin (currentAngle);
					rangeShield.Add (new Vector3 (x, 0.001f, z));
					currentAngle += latStepAngle;
				}
				currentAngle = startAngle;
				if (toggler)
					currentDistance -= Mathf.Max (currentDistance * 0.05f, 0.1f);
				else
					currentDistance -= Mathf.Max (currentDistance * 0.1f, 0.1f);
				toggler = !toggler;
				waveCount++;
			}
		}

		/// <summary>
		/// Generates the range mesh from the vertices. The vertices must be calculated before calling this function.
		/// </summary>
		private void GenerateRangeMesh()
		{
			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			Mesh mesh = filter.mesh;
			mesh.Clear();

			List<int> triangles = new List<int>();

			//create upper side of wave mesh (visible from top)
			for (int currentWaveIdx = 0; currentWaveIdx < waveCount - 1; currentWaveIdx=currentWaveIdx+2) {
				for (int stepIdxH = 0; stepIdxH < maxLatIdx - 1; stepIdxH++) {
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + maxLatIdx);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + maxLatIdx + 1);

					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx);		
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + maxLatIdx + 1);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + 1);
				}
			}

			//create lower side of wave mesh (visible from bottom)
			for (int currentWaveIdx = waveCount; currentWaveIdx < waveCount*2 - 1; currentWaveIdx=currentWaveIdx+2) {
				for (int stepIdxH = 0; stepIdxH < maxLatIdx - 1; stepIdxH++) {
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + maxLatIdx + 1);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + maxLatIdx);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx);

					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + 1);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx + maxLatIdx + 1);
					triangles.Add (stepIdxH + currentWaveIdx*maxLatIdx);		
				}
			}

			mesh.vertices = rangeShield.ToArray();
			mesh.triangles = triangles.ToArray();
			Vector2[] uvs = new Vector2[rangeShield.Count];
			int i = 0;
			while (i < uvs.Length) {
				uvs[i] = new Vector2(0.5f, 0.5f);
				i++;
			}
			mesh.uv = uvs;
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
		}

		/// <summary>
		/// Creates a sphere in each vertex. Only for debugging purposes.
		/// </summary>
		private void DrawVertices() {
			GameObject prefab = null;
			prefab = Resources.Load ("Prefabs/Sphere") as GameObject;
			int idx = 0;
			foreach (Vector3 point in rangeShield) {
				GameObject p = GameObject.Instantiate (prefab, point, Quaternion.identity) as GameObject;
				p.transform.SetParent (gameObject.transform);
				p.name = idx.ToString ();
				idx++;
			}
		}
	}
}