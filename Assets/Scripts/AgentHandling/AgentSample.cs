﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Crash status of an agent at a specific point of time
	/// </summary>
	public enum Crash { 
		NoCrash, 
		WillCrash, 
		HasCrashed 
	};

	/// <summary>
	/// Represents the snapshot of a single agent at a specific point of time.
	/// </summary>
	public class AgentSample {
		public int AgentID = 0;
		public float XPosition = 0;
		public float YPosition = 0;
		public float YawAngle = 0;	
		public float RollAngle = 0;	
		public int AOI = 0;
		public bool BrakeLight = false;
		public int TurnSignal = 0;
		public Dictionary<string, string> OptionalAttributes = new Dictionary<string, string>(); //!<These attributes are read from the trace file and passed directly to the agent's InfoPanel without evaluating the content of the attribute.
	}
}