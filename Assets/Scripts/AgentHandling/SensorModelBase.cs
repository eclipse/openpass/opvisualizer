﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// This component is the base class of all sensor models (sensor models create and hold a mesh with the shape of the detection field)
	/// </summary>
	public abstract class SensorModelBase : MonoBehaviour {

		/// <summary>
		/// Create the specified sensor.
		/// </summary>
		/// <param name="sensor">Attributes of the sensor detection range to create</param>
		virtual public void Create (Transform pivot, SensorAttributes sensor) {
			transform.rotation = Quaternion.Euler (Mathf.Rad2Deg*-sensor.OrientationRoll, Mathf.Rad2Deg*-sensor.OrientationYaw, Mathf.Rad2Deg*-sensor.OrientationPitch);
			gameObject.GetComponent<MeshRenderer> ().material = sensor.RangeSurfaceMaterial; 
			
			if(Application.isPlaying) //make sure we don't execute procedual mesh generation in editor mode (e.g. while unit testing)
				CreateSensorField(sensor);
		}

		/// <summary>
		/// Updates the attributes of the specified sensor.
		/// </summary>
		/// <param name="sensor">Attributes of the sensor detection range to update</param>
		virtual public void UpdateAttributes (Transform pivot, SensorAttributes sensor) {
			transform.localPosition = sensor.MountingPosition - pivot.localPosition;
			transform.rotation = Quaternion.Euler (Mathf.Rad2Deg*-sensor.OrientationRoll, Mathf.Rad2Deg*-sensor.OrientationYaw, Mathf.Rad2Deg*-sensor.OrientationPitch);
		}

		/// <summary>
		/// The virtual create funtion that must be overriden in inherited classes. It must create a mesh in the shape of the detection field.
		/// </summary>
		protected abstract void CreateSensorField(SensorAttributes sensor);
	}
}