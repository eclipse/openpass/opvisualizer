﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Attributes of the sensor model.
	/// </summary>
	public class SensorAttributes {
		public string ID { get; set; } //!<The unique ID of the sensor 
		public string Name { get; set; } //!<The user friendly name of the sensor that is displayed in the menu 
		public string Model { get; set; } //!<The sensor model that this sensor is based on (e.g. Geometric2D, Geometric3D etc) 
		public Vector3 MountingPosition { get; set; } //!<The local position of the sensor on the vehicle measured from the vehicle's pivot.
		public float DetectionRange { get; set; } //!<The distance how far the sensor 'sees' in meter
		public float OrientationYaw { get; set; } //!<The horizontal direction at which the sensor is looking (in radian)
		public float OrientationPitch { get; set; } //!<The vertical direction at which the sensor is looking (in radian)
		public float OrientationRoll { get; set; } //!<The roll angle of the sensor orientation (in radian)
		public float OpeningAngleH { get; set; } //!<Horizontal opening angle (in degree)
		public float OpeningAngleV { get; set; } //!<Vertical opening angle (in degree)
		public Material RangeSurfaceMaterial { get; set; } //!<The material of the surface of the sensor range (usually a semi-transparent material)
	}
}

