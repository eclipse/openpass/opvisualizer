﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.AgentHandling
{
	/// <summary>
	/// Controls the display of arrows that represent driver's area of interest. Must be added to every agent as component.
	/// </summary>
	public class AOIController : MonoBehaviour{
		public bool ShowAOIArrow { get; set; } //!<Showh/hide arrows
		private GameObject[] aoiArrows = new GameObject[20]; //!<Array of arrows

		private static class ArrowType
		{
			public const string Close = "Prefabs/AOIArrowClose";
			public const string Far = "Prefabs/AOIArrowFar";
			public const string NextLane = "Prefabs/AOIArrowNextLane";
		}

		private enum AOI
		{
			LeftFront 			= 0,
			LeftFrontFar 		= 1,
			RightFront 			= 2,
			RightFrontFar 		= 3,
			LeftRear 			= 4,
			RightRear 			= 5,
			Front 				= 6,
			FrontFar 			= 7,
			Rear 				= 8,
			Left 				= 9,
			Right 				= 10,
			InstrumentCluster 	= 11,
			Infotainment		= 12,
			HeadUpDisplay		= 13,
			LeftLeftFront 		= 14,
			RightRightFront 	= 15,
			LeftLeftRear 		= 16,
			RightRightRear 		= 17,
			LeftLeftSide 		= 18,
			RightRightSide 		= 19
		}

		/// <summary>
		/// Creates the AOI arrows. The index of the arrows in the array must match the index that the OpenPASS Simulator is using internally.
		/// </summary>
		/// <param name="arrowPosition">Arrow position. Usually the upper center of the vehicle's bounds</param>
		public void CreateAOIArrows (Transform parent, Vector3 arrowPosition){
			GameObject arrowGroup = new GameObject();
			arrowGroup.transform.parent = parent;
			arrowGroup.name = "AOI";
			aoiArrows [(int)AOI.LeftFront] = CreateAOIArrow (arrowGroup, arrowPosition, -45f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.LeftFrontFar] = CreateAOIArrow (arrowGroup, arrowPosition, -45f, 0f, ArrowType.Far);
			aoiArrows [(int)AOI.RightFront] = CreateAOIArrow (arrowGroup, arrowPosition, 45f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.RightFrontFar] = CreateAOIArrow (arrowGroup, arrowPosition, 45f, 0f, ArrowType.Far);
			aoiArrows [(int)AOI.LeftRear] = CreateAOIArrow (arrowGroup, arrowPosition, -135f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.RightRear] = CreateAOIArrow (arrowGroup, arrowPosition, 135f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.Front] = CreateAOIArrow (arrowGroup, arrowPosition, 0f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.FrontFar] = CreateAOIArrow (arrowGroup, arrowPosition, 0f, 0f, ArrowType.Far);
			aoiArrows [(int)AOI.Rear] = CreateAOIArrow (arrowGroup, arrowPosition, 180f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.Left] = CreateAOIArrow (arrowGroup, arrowPosition, -90f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.Right] = CreateAOIArrow (arrowGroup, arrowPosition, 90f, 0f, ArrowType.Close);
			aoiArrows [(int)AOI.InstrumentCluster] = CreateAOIArrow (arrowGroup, arrowPosition, 0f, -30f, ArrowType.Close);
			aoiArrows [(int)AOI.Infotainment] = CreateAOIArrow (arrowGroup, arrowPosition, 0f, -40f, ArrowType.Close);
			aoiArrows [(int)AOI.HeadUpDisplay] = CreateAOIArrow (arrowGroup, arrowPosition, 0f, -10f, ArrowType.Close);		
			aoiArrows [(int)AOI.LeftLeftFront] = CreateAOIArrow (arrowGroup, arrowPosition, -45f, 0f, ArrowType.NextLane);
			aoiArrows [(int)AOI.RightRightFront] = CreateAOIArrow (arrowGroup, arrowPosition, 45f, 0f, ArrowType.NextLane);
			aoiArrows [(int)AOI.LeftLeftRear] = CreateAOIArrow (arrowGroup, arrowPosition, -135f, 0f, ArrowType.NextLane);
			aoiArrows [(int)AOI.RightRightRear] = CreateAOIArrow (arrowGroup, arrowPosition, 135f, 0f, ArrowType.NextLane);
			aoiArrows [(int)AOI.LeftLeftSide] = CreateAOIArrow (arrowGroup, arrowPosition, -90f, 0f, ArrowType.NextLane);
			aoiArrows [(int)AOI.RightRightSide] = CreateAOIArrow (arrowGroup, arrowPosition, 90f, 0f, ArrowType.NextLane);
		}

		/// <summary>
		/// Creates an AOI arrow from the prefab
		/// </summary>
		/// <returns>The AOI arrow GameObject instance.</returns>
		/// <param name="arrowPosition">Arrow position.</param>
		/// <param name="arrowAngle">Arrow angle.</param>
		private GameObject CreateAOIArrow (GameObject parent, Vector3 arrowPosition, float arrowAngleV, float arrowAngleH, string arrowType){
			GameObject arrowPrefab;
			arrowPrefab = Resources.Load (arrowType, typeof(GameObject)) as GameObject;
			
			GameObject arrowInstance = Instantiate (arrowPrefab, arrowPosition, Quaternion.Euler (0, arrowAngleV, arrowAngleH)) as GameObject;
			arrowInstance.transform.SetParent (parent.transform, false);
			return arrowInstance;
		}

		/// <summary>
		/// Displays the currently active AOI arrow based on the index.
		/// </summary>
		/// <param name="aoiIdx">Aoi index. Must match the index that the OpenPASS Simulator is using internally.</param>
		public void UpdateAOIArrow(int aoiIdx){
			ShowAllArrows (false);

			if (ShowAOIArrow && isIdxValid(aoiIdx))
				ShowArrow (true, aoiIdx);
		}

		/// <summary>
		/// Shows/hides all arrows.
		/// </summary>
		private void ShowAllArrows(bool show){
			for (int idx = 0; idx < aoiArrows.Length; idx++)
				ShowArrow(show, idx);
		}

		/// <summary>
		/// Shows/hides the specified arrow.
		/// </summary>
		private void ShowArrow(bool show, int idx){
			aoiArrows [idx].SetActive (show);
		}

		/// <summary>
		/// Checks if the passed AOI Index is a valid array index
		/// </summary>
		private bool isIdxValid(int idx){
			if (idx < 0 || idx >= aoiArrows.Length)
				return false;
			else
				return true;
		}
	}
}