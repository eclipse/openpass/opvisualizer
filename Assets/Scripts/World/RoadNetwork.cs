﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using Visualizer.World.Core;

namespace Visualizer.World.OpenDrive
{
	public class RoadNetwork
	{
		private RoadNetworkData data = null;
		private Dictionary<MeshFactory.LaneSegmentMetaData, MeshFactory.MeshData> laneMeshes = null;

		private static XmlSerializer _xmlSerializer = new XmlSerializer(typeof(RoadNetworkData));

		//cached, to avoid re-reading the files over and over again.
		//lazy initialization, as we don't want Unity dependencies in the static constructor.
		private static string _schema14 = null, _schema13 = null;

		//---------------------------------------------------------------------------//

		public RoadNetworkData GetData() { return data; }
		public Dictionary<MeshFactory.LaneSegmentMetaData, MeshFactory.MeshData> GetLaneMeshes() { return laneMeshes; }

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Constructor that takes a file path and loads the road network from this.
		/// Does validate against OpenDRIVE 1.4 and 1.3 (if 1.4 validation fails).
		/// Requires the Unity project to find the schema files.
		/// </summary>
		/// <param name="file">Path to or content of the OpenDRIVE file to load (depending on the parameter loadingFromDisk).</param>
		/// <param name="meshFactory"></param>
		/// <param name="loadingFromDisk">Defines if the parameter "file" is the content or the name of the file</param>
		public RoadNetwork(string file, MeshFactory meshFactory, bool loadingFromDisk)
		{
			string fileContent = loadingFromDisk ? LoadFile(file) : file;
			//ValidateFile(fileContent); //temporarily disabled as validation always fails currently - more investigation needed to find out why
			Initialize(fileContent, meshFactory);
		}

		/// <summary>
		/// Loads the given file.
		/// </summary>
		/// <param name="filename">Path to the file to load</param>
		string LoadFile(string filename)
		{
			//load file, load schema
			string driveData = null;
			using (FileStream fs = new FileStream(filename, FileMode.Open))
			{
				using(StreamReader reader = new StreamReader(fs))
					driveData = reader.ReadToEnd();
			}
			return driveData;
		}

		/// <summary>
		/// Validates against OpenDRIVE 1.4. If that does not work, load schema for OpenDRIVE 1.3 and try to validate against that.
		/// </summary>
		/// <param name="fileContent">The content of the file that should be validated</param>
		void ValidateFile(string fileContent)
		{
			if(_schema14 == null)
				_schema14 = Resources.Load<TextAsset>("Schemas/OpenDRIVE_1.4H.xsd").text;
			if(_schema13 == null)
				_schema13 = Resources.Load<TextAsset>("Schemas/OpenDRIVE_1.3.xsd").text;

			string error14, error13;
			if (!ValidateAgainstSchema(fileContent, _schema14, out error14) && 
				!ValidateAgainstSchema(fileContent, _schema13, out error13))
			{
				Debug.LogError("Xml validation of given OpenDRIVE file failed for both, OpenDRIVE 1.4 and 1.3. OpenDRIVE 1.4 errors: " + error14 + ". OpenDRIVE 1.3 errors:" + error13);
				throw new XmlSchemaValidationException("Xml validation of given OpenDRIVE file failed for both, OpenDRIVE 1.4 and 1.3. OpenDRIVE 1.4 errors: " + error14 + ". OpenDRIVE 1.3 errors:" + error13);
			}
		}

		private bool ValidateAgainstSchema(string openDriveData, string schemaData, out string errormessage)
		{
			try
			{
				XmlReaderSettings settings = new XmlReaderSettings();

				settings.Schemas.Add(XmlSchema.Read(new StringReader(schemaData), null));
				settings.ValidationType = ValidationType.Schema;


				using (XmlReader reader = XmlReader.Create(new StringReader(openDriveData), settings))
				{
					XmlDocument document = new XmlDocument();
					document.Load(reader);
				}
			}
			catch (XmlSchemaValidationException validationException)
			{
				errormessage = validationException.Message;
				return false;
			}
			errormessage = "";
			return true;
		}

		void Initialize(string openDriveData, MeshFactory meshFactory)
		{
			data = (RoadNetworkData)_xmlSerializer.Deserialize(new StringReader(openDriveData));

			if (data == null)
				throw new Exception("Failed to deserialize OpenDRIVE file.");

			//TODO: Clarify if we need this at all:
			if (!CreateReferences())
				Debug.LogWarning("Could not create references in loaded OpenDRIVE file.");

			//========================================//

			//generate the meshes using the meshfactory
			laneMeshes = new Dictionary<MeshFactory.LaneSegmentMetaData, MeshFactory.MeshData>();
			foreach (var road in data.Roads)
				laneMeshes.Combine(meshFactory != null ? meshFactory.GenerateRoadMeshes(road.Value) : null);
		}


		/// <summary>
		/// Sets all kinds of references within the data structures that cannot be set during deserialization.
		/// </summary>
		private bool CreateReferences()
		{
			SetJunctionRoadReferences();

			AssignRoadPredecessorSuccessorLinks();

			if (!AssignJunctionConnectionLaneLinks())
				return false;

			//AssignRoadObjectReferences();

			return true;
		}

		/// <summary>
		/// RoadObjectReferences reference a RoadObject. The C# object to be referenced does not exist during deserialization, so this
		/// method is needed to set up the C# references.
		/// </summary>
		private void AssignRoadObjectReferences()
		{
			//Gather all objects on all roads:
			Dictionary<string, RoadObjectBase> allObjects = new Dictionary<string, RoadObjectBase>();
			foreach (var road in data.Roads)
			{
				foreach (RoadObjectBase obj in road.Value.RoadObjects.GeneralObjects)
				{
					allObjects.Add(obj.ID,obj);
				}
				foreach (RoadObjectBase obj in road.Value.RoadObjects.Tunnels)
				{
					allObjects.Add(obj.ID, obj);
				}
				foreach (RoadObjectBase obj in road.Value.RoadObjects.Bridges)
				{
					allObjects.Add(obj.ID, obj);
				}
			}

			foreach (var road in data.Roads)
			{
				foreach (RoadObjectReference reference in road.Value.RoadObjects.References)
				{
					RoadObjectBase referencedObject;
					if(!allObjects.TryGetValue(reference.ReferencedID, out referencedObject))
						throw new KeyNotFoundException("Referenced RoadObject was not found within OpenDRIVE file. File seems not to be valid.");
					reference.ReferencedRoadObject = referencedObject;
				}
			}
		}

		/// <summary>
		/// Junctions have lanelinks in their connections, that detail the transition from the incoming road (should be a standard road) to the
		/// connecting road (a path) on the junction.
		/// This method sets up C# object references to avoid having to look up the correct object every time.
		/// </summary>
		/// <returns>false if an error occured</returns>
		private bool AssignJunctionConnectionLaneLinks()
		{
			foreach (var junction in data.Junctions)
			{
				foreach (Connection conn in junction.Value.RoadConnections)
				{
					var connectingLaneSection =
						conn.ConnectingRoad.LaneSections[
							conn.ContactPoint == ContactPoint.START ? 0 : conn.ConnectingRoad.LaneSections.Length - 1];


					//Code below this commented out block should be more stable regarding roads that end/start at the same junction.
					//						var relevantIncomingLanes = conn.IncomingRoad.LaneSections[
					//							conn.IncomingRoad.RoadLink.Predecessor.LinkedElement == junction.Value
					//								? 0 //if the junction is the predecessor of the road, we need to connect the first lane section
					//								: conn.IncomingRoad.LaneSections.Length - 1]; //otherwise the last.

					ContactPoint p = conn.ContactPoint == ContactPoint.START
						? conn.ConnectingRoad.RoadLink.Predecessor.LinkedContactPoint //incomingRoad is before connectingRoad
						: conn.ConnectingRoad.RoadLink.Successor.LinkedContactPoint; //incomingRoad is after connectingRoad

					LaneSection incomingLaneSection =
						conn.IncomingRoad.LaneSections[p == ContactPoint.END ? conn.IncomingRoad.LaneSections.Length - 1 : 0];

					/*System.Diagnostics.Debug.Assert(conn.IncomingRoad.RoadLink.Successor?.LinkedElement == junction.Value &&
													conn.IncomingRoad.RoadLink.Predecessor?.LinkedElement != junction.Value ||
													conn.IncomingRoad.RoadLink.Successor?.LinkedElement != junction.Value &&
													conn.IncomingRoad.RoadLink.Predecessor?.LinkedElement == junction.Value);*/
					
					//Replaced null propagating operator from the above code to support C# 4.0:
					if (conn.IncomingRoad.RoadLink.Successor != null && conn.IncomingRoad.RoadLink.Predecessor != null) {
						System.Diagnostics.Debug.Assert (conn.IncomingRoad.RoadLink.Successor.LinkedElement == junction.Value &&
						conn.IncomingRoad.RoadLink.Predecessor.LinkedElement != junction.Value ||
						conn.IncomingRoad.RoadLink.Successor.LinkedElement != junction.Value &&
						conn.IncomingRoad.RoadLink.Predecessor.LinkedElement == junction.Value);
					}
					//toss all lanes on both roads into a big list, as OpenDRIVE 1.4 states in 5.3.7.2.1:
					/*
					 * Important Note:
					 * the actual distinction of lanes according to their meaning for the traffic is
					 * performed by looking at each lane's id (see next chapter; left lanes: positive,
					 * right lanes: negative, center lane: zero). The delimiters <left> / <center>
					 * / <right> merely serve a better navigation within the road database.
					 */
					List<Lane> allIncomingLanes =
						new List<Lane>(incomingLaneSection.LeftLanes.Length + incomingLaneSection.RightLanes.Length +
									   (incomingLaneSection.CenterLane != null ? 1 : 0));
					List<Lane> allConnectingLanes =
						new List<Lane>(connectingLaneSection.LeftLanes.Length + connectingLaneSection.RightLanes.Length +
									   (connectingLaneSection.CenterLane != null ? 1 : 0));

					allIncomingLanes.AddRange(incomingLaneSection.LeftLanes);
					if (incomingLaneSection.CenterLane != null)
						allIncomingLanes.Add(incomingLaneSection.CenterLane);
					allIncomingLanes.AddRange(incomingLaneSection.RightLanes);

					allConnectingLanes.AddRange(connectingLaneSection.LeftLanes);
					if (connectingLaneSection.CenterLane != null)
						allConnectingLanes.Add(connectingLaneSection.CenterLane);
					allConnectingLanes.AddRange(connectingLaneSection.RightLanes);

					if (conn.LaneLinker.Length == 0)
					{
						//element has been omitted in XML, generate it here
						//for all lanes on the connectingRoad there is a lane of the same ID on the incomingRoad.
						conn.LaneLinker = new LaneLink[allConnectingLanes.Count];
						for (var index = 0; index < allConnectingLanes.Count; index++)
						{
							Lane connectingLane = allConnectingLanes[index];
							conn.LaneLinker[index] = new LaneLink();
							conn.LaneLinker[index].FromID = conn.LaneLinker[index].ToID = connectingLane.ID;
						}
					}
					foreach (LaneLink laneLink in conn.LaneLinker)
					{
						laneLink.From = allIncomingLanes.Find((l) => l.ID == laneLink.FromID);
						laneLink.To = allConnectingLanes.Find((l) => l.ID == laneLink.ToID);
						if (laneLink.From == null || laneLink.To == null)
							return false;
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Roads can have predecessors/successors. This sets up C# object references to the corresponding objects.
		/// </summary>
		private void AssignRoadPredecessorSuccessorLinks()
		{
			foreach (var road in data.Roads)
			{
				if (road.Value.RoadLink != null)
				{
					if (!AssignLink(road.Value.RoadLink.Predecessor))
					{
						road.Value.RoadLink.Predecessor = null;
					}
					if (!AssignLink(road.Value.RoadLink.Successor))
					{
						road.Value.RoadLink.Successor = null;
					}
				}
			}
		}

		/// <summary>
		/// Junctions consist of numerous roads ("paths"). This sets up a C# object reference to each of the corresponding Road objects.
		/// </summary>
		private void SetJunctionRoadReferences()
		{
			foreach (var junction in data.Junctions)
			{
				foreach (Connection conn in junction.Value.RoadConnections)
				{
					Road tmpRoad;
					conn.IncomingRoad = data.Roads.TryGetValue(conn.IncomingRoadID, out tmpRoad) ? tmpRoad : null;
					conn.ConnectingRoad = data.Roads.TryGetValue(conn.ConnectingRoadID, out tmpRoad) ? tmpRoad : null;
				}
			}
		}

		/// <summary>
		/// Helper function for the AssignRoadPredecessorSuccessorLinks() method.
		/// </summary>
		/// <param name="link">The link object to set up</param>
		private bool AssignLink(LinkTo link)
		{
			if(link != null)
			{
				if(link.LinkedElementType == ElementType.ROAD)
				{
					Road road;
					if(data.Roads.TryGetValue(link.ElementID, out road))
					{
						link.LinkedElement = road;
						return true;
					}
					else
					{
						link.LinkedElement = null;
						return false;
					}
				}

				//WIP: roll back to normal, because of errors in different versions of test assigning!
				else if(link.LinkedElementType == ElementType.JUNCTION)
				{
					Junction junction;
					if (data.Junctions.TryGetValue(link.ElementID, out junction))
					{
						link.LinkedElement = junction;
						return true;
					}
					else
					{
						link.LinkedElement = null;
						return false;
					}
				}
			}
			return true;
		}
	}
}
