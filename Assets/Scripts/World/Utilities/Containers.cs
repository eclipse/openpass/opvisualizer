﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

namespace Visualizer.World.Core
{
	[System.Serializable]
	public class Pair<T1, T2>
	{
		public T1 first;
		public T2 second;

		/// <summary>
		/// Needed for SASSerializer!!
		/// </summary>
		public Pair() { }
		public Pair(T1 a, T2 b) { first = a; second = b; }
	}
	[System.Serializable]
	public class Triple<T1, T2, T3>
	{
		public T1 first;
		public T2 second;
		public T3 third;

		/// <summary>
		/// Needed for SASSerializer!!
		/// </summary>
		public Triple() { }
		public Triple(T1 a, T2 b, T3 c) { first = a; second = b; third = c; }
	}
}
