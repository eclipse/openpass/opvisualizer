﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;

namespace Visualizer.World.Core
{
	public static class Extensions
	{
		/// <summary>
		/// Combines the "other" dictionary into the current one. In case a key already exists in current, that is also in "other", it is simply skipped.
		/// </summary>
		public static void Combine<K, T>(this Dictionary<K, T> curr, Dictionary<K, T> other)
		{
			if (other == null)
				return;
			foreach(var it in other)
				if(!curr.ContainsKey(it.Key))
					curr[it.Key] = it.Value;
		}

		public static bool IsSorted<T>(this IList<T> curr, Comparison<T> comparison)
		{
			if (curr.Count <= 1)
				return true;
			var lastval = curr[0];
			for (int i = 1; i < curr.Count; ++i)
			{
				int res = comparison(lastval,curr[i]);
				if (res == 1)
					return false;
			}
			return true;
		}
	}
}
