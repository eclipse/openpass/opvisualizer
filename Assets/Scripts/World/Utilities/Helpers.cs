﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;

namespace Visualizer.World.Core.Utilities
{
	/// <summary>
	/// General algorithms and helpers that don't fit into any other class
	/// </summary>
	public class Helpers
	{
		/// <summary>
		/// This method usese the bisection algorithm to find a root of the function quantifier. This is guaranteed to find exactly one root, if the input range starts with one end greater than Zero, and the other end smaller than Zero.
		/// If both ends of the search range have the same sign, this method will fail.
		/// </summary>
		/// <typeparam name="ParameterType">The type to operate on. Input of quantifier.</typeparam>
		/// <typeparam name="QuantifierType">The IComparable type returend by quantifier.</typeparam>
		/// <param name="lowerLimit">One limit of the search range.</param>
		/// <param name="upperLimit">The other limit of the search range.</param>
		/// <param name="averager">Delegate that is called to get an intermediate value between the limits of the previous step. A good example is (x,y) => (return 0.5f*(x+y)) </param>
		/// <param name="quantifier">Function of which a root should be found.</param>
		/// <param name="Zero">The zero value of QuantifierType (just set this to 0 if your quantifier is numeric)</param>
		/// <param name="endCondition">Delegate used to determine the end-condition. If this returns true, the algorithm will stop. It takes the current search range as input.</param>
		/// <param name="result">Out value: The root position.</param>
		/// <param name="maxIterations">Hard iteration limit.</param>
		/// <returns>false if no root could be found (or if the given limits are of the same sign)</returns>
		public static bool Bisect<ParameterType, QuantifierType>(ParameterType lowerLimit, ParameterType upperLimit,
			Func<ParameterType, ParameterType, ParameterType> averager, Func<ParameterType, QuantifierType> quantifier, QuantifierType Zero, Func<ParameterType, ParameterType, bool> endCondition, out ParameterType result, int maxIterations = 100) where QuantifierType : IComparable<QuantifierType>
		{
			ParameterType[] borders = new ParameterType[3];
			borders[0] = lowerLimit;
			borders[2] = upperLimit;
			QuantifierType[] scores = new QuantifierType[3];
			scores[0] = quantifier(lowerLimit);
			scores[2] = quantifier(upperLimit);

			if (scores[0].CompareTo(Zero) == 0)
			{
				result = borders[0];
				return true;
			}

			if (scores[2].CompareTo(Zero) == 0)
			{
				result = borders[2];
				return true;
			}

			if (scores[0].CompareTo(Zero) == scores[2].CompareTo(Zero))
			{
				result = averager(lowerLimit, upperLimit);
				return false;
			}

			int iterations = 0;

			do
			{
				borders[1] = averager(borders[0], borders[2]);
				scores[1] = quantifier(borders[1]);

				if (scores[1].CompareTo(Zero) == 0)
				{
					result = borders[1];
					return true;
				}
				
				byte target = (scores[1].CompareTo(Zero) == scores[2].CompareTo(Zero)) ? (byte)2 : (byte)0;
				borders[target] = borders[1];
				scores[target] = scores[1];
			}
			while (!endCondition(borders[0], borders[2]) && ++iterations < maxIterations);

			result =  (averager(borders[0], borders[2]));
			return iterations < maxIterations;
		}

		/// <summary>
		/// This method implements a bisection algorithm to minimize the output of quantifier, using the given averager to determine the intermediate position between two parameters.
		/// Beware that this is not stable, unless Quantifier is more or less linear.
		/// </summary>
		/// <typeparam name="ParameterType">The type to operate on. Input of quantifier.</typeparam>
		/// <typeparam name="QuantifierType">The IComparable type returend by quantifier.</typeparam>
		/// <param name="lowerLimit">One limit of the search range.</param>
		/// <param name="upperLimit">The other limit of the search range.</param>
		/// <param name="averager">Delegate that is called to get an intermediate value between the limits of the previous step. A good example is (x,y) => (return 0.5f*(x+y)) </param>
		/// <param name="quantifier">Delegate that is called to determine the value to be minimized.</param>
		/// <param name="endCondition">Delegate used to determine the end-condition. If this returns true, the algorithm will stop. It takes the current search range as input.</param>
		/// <param name="maxIterations">Hard iteration limit.</param>
		/// <returns></returns>
		public static ParameterType BisectMinimize<ParameterType, QuantifierType>(ParameterType lowerLimit, ParameterType upperLimit,
			Func<ParameterType, ParameterType, ParameterType> averager, Func<ParameterType, QuantifierType> quantifier, Func<ParameterType,ParameterType,bool> endCondition, int maxIterations = 100) where QuantifierType : IComparable<QuantifierType>
		{
			ParameterType[] borders = new ParameterType[3];
			borders[0] = lowerLimit;
			borders[2] = upperLimit;
			QuantifierType[] scores = new QuantifierType[3];
			scores[0] = quantifier(lowerLimit);
			scores[2] = quantifier(upperLimit);

			byte closest = byte.MaxValue;

			int iterations = 0;

			do
			{
				borders[1] = averager(borders[0], borders[2]);
				scores[1] = quantifier(borders[1]);
				QuantifierType minDist = scores[0];
				closest = 0;
				for (byte i = 1; i < 3; ++i)
				{
					if (scores[i].CompareTo(minDist) < 0)
					{
						minDist = scores[i];
						closest = i;
					}
				}
				byte target = (closest == 0 || (closest == 1 && scores[0].CompareTo(scores[2]) < 0)) ? (byte)2 : (byte)0;
				borders[target] = borders[1];
				scores[target] = scores[1];
			}
			while (!endCondition(borders[0],borders[2]) && ++iterations < maxIterations);

			return (averager(borders[0], borders[2]));
		}

		/// <summary>
		/// Implements a simple Newton's method root finder, double variant.
		/// This is not a generic, as that would require custom subtraction and division methods.
		/// </summary>
		/// <param name="startingValue">Starting value for the root finding</param>
		/// <param name="function">The function of which the root should be found</param>
		/// <param name="derivative">The first derivative of the function of which the root should be found</param>
		/// <param name="error">The root search is considered finished as soon as the last iteration step has been smaller than this.</param>
		/// <param name="estimateMultiplicityAfterIteration">If you expect the root to be of multiplicity > 1, 
		/// you can have the step adaptively adjust by estimating said multiplicity.
		/// *Beware, this is only numerically stable in close proximity to the root!* 
		/// Set this parameter to a value > 0 to use adaptive adjustment starting from the given iteration. 
		/// If this parameter is set to 0 or below, no adaptation will be performed. 
		/// Beware that this is expensive per iteration, as it uses Math.Pow, so use this only if your function itself is expensive to calculate.</param>
		/// <param name="functionValueAndIterations">If this is not null, it will contain the last function value encountered</param>
		/// <param name="maxIterations">Hard limit of iterations.</param>
		/// <param name="convergenceThreshold">Only used if estimateMultiplicityAfterIteration is greater than 0. If logarithmic convergence is worse than this,
		/// the last iteration will be undone, and instead carried out with assumend multiplicity = 1. The next iteration will then re-evaluate multiplicity.</param>
		/// <returns>The found root. Beware that if maxIterations is reached, only the result of the last iteration will be returned. Check functionValueAndIterations for validity.</returns>
		public static double Newton(double startingValue,
			Func<double, double> function, Func<double, double> derivative,
			double error, int estimateMultiplicityAfterIteration = 0, Pair<double, int> functionValueAndIterations = null , int maxIterations = 100, double convergenceThreshold = 1.8)
		{
			int iterations = 0;
			double k = 1;
			double step = 1.0;
			double lastF = function(startingValue);
			do
			{
				var lastStep = step;
				step = lastF / derivative(startingValue);
				if (estimateMultiplicityAfterIteration > 0)
				{
					if (iterations >= estimateMultiplicityAfterIteration && k < 1.5)
					{
						k = Math.Max(1.0, Math.Round(1.0 / (1.0 - Math.Abs(step / lastStep))));
					}
					else
					{
						double lastAbs = Math.Abs(lastStep);
						double thisAbs = Math.Abs(step);
						//if (k > 1.5 && (lastAbs <= thisAbs || (thisAbs >= 1 ? lastAbs < Math.Pow(thisAbs, convergenceThreshold) : Math.Pow(lastAbs, convergenceThreshold) < thisAbs)))
						if (k > 1.5 && (lastAbs <= thisAbs || lastAbs < Math.Pow(thisAbs,(thisAbs >= 1 ? convergenceThreshold : 1.0/convergenceThreshold))))
						{
							//adjustment didn't speed up enough. Do one regular Newton iteration instead.
							startingValue += lastStep * (k - 1.0);
							step = function(startingValue) / derivative(startingValue);
							k = 1;
						}
					}
				}
				
				startingValue = startingValue - k*step;
				lastF = function(startingValue);

			} while (Math.Abs(k*step) > error && ++iterations < maxIterations && !double.IsNaN(startingValue) && !double.IsInfinity(startingValue));
			if (functionValueAndIterations != null)
			{
				functionValueAndIterations.first = lastF;
				functionValueAndIterations.second = iterations;
			}
			return startingValue;
		}
	}
}
