﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;
using JetBrains.Annotations;

/// <summary>
/// A serializable dictionary that implements the ISerializationCallbackReceiver in order to fill a
/// member list that is used to serialize the key value pairs and fill the dictionary again after 
/// deserialization.
/// NOTE: in inspector this is not shown at all!
/// </summary>
[System.Serializable]
public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
	//------------------------- Property declaration -----------------------------//
	#region Property declaration

	[SerializeField]
	private List<TKey> keys;

	[SerializeField]
	private List<TValue> values;

	#endregion
	//--------------------------- Property access --------------------------------//
	#region Property access

	#endregion
	//-------------------------- Method declarations -----------------------------//
	#region Method declarations

	//---------------------------------------------------------------------------//
	//default constructors

	public SerializableDictionary() { }
	public SerializableDictionary(IEqualityComparer<TKey> comparer) : base(comparer) { }
	public SerializableDictionary([NotNull] IDictionary<TKey, TValue> dictionary) : base(dictionary) { }
	public SerializableDictionary(int capacity) : base(capacity) { }
	public SerializableDictionary([NotNull] IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer) : base(dictionary, comparer) { }
	public SerializableDictionary(int capacity, IEqualityComparer<TKey> comparer) : base(capacity, comparer) { }
	protected SerializableDictionary(SerializationInfo info, StreamingContext context) : base(info, context) { }

	//---------------------------------------------------------------------------//
	//serialization

	public void OnAfterDeserialize()
	{
		if(keys == null || values == null) return;
		if(keys.Count != values.Count) Debug.LogWarning("SerializableDictionary: key count != value count");
		int maxIndex = Mathf.Min(keys.Count, values.Count);
		for(int i = 0; i < maxIndex; ++i)
		{
			if(!ContainsKey(keys[i]))
				Add(keys[i], values[i]);
			else
				Debug.LogWarning("SerializableDictionary: duplicate key!");
		}
	}

	public void OnBeforeSerialize()
	{
		if(keys == null)
			keys = new List<TKey>(this.Count);
		else
			keys.Clear();

		if(values == null)
			values = new List<TValue>(this.Count);
		else
			values.Clear();

		foreach(var pair in this)
		{
			keys.Add(pair.Key);
			values.Add(pair.Value);
		}
	}

	#endregion
}