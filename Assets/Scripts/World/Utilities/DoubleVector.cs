﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.World.Core.Utilities
{
	public struct DoubleVector
	{
		public double x;
		public double y;
		public double z;

		public DoubleVector(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public DoubleVector(Vector3 unityVector) : this(unityVector.x,unityVector.y,unityVector.z)
		{}

		public static explicit operator Vector3(DoubleVector ours)
		{
			return new Vector3((float)ours.x,(float)ours.y,(float)ours.z);
		}

		public static DoubleVector operator *(double other, DoubleVector ours)
		{
			return ours * other;
		}

		public static DoubleVector operator *(DoubleVector ours, double other)
		{
			return new DoubleVector(ours.x * other, ours.y * other, ours.z * other);
		}

		public static DoubleVector operator +(DoubleVector ours, DoubleVector other)
		{
			return new DoubleVector(ours.x + other.x, ours.y + other.y, ours.z + other.z);
		}

		public static DoubleVector operator -(DoubleVector ours, DoubleVector other)
		{
			return ours + (-1.0*other);
		}

		public double Dot(DoubleVector other)
		{
			return x * other.x + y * other.y + z * other.z;
		}

		public DoubleVector Cross(DoubleVector other)
		{
			DoubleVector retval;
			retval.x = y * other.z - other.y * z;
			retval.y = z * other.x - other.z * x;
			retval.z = x * other.y - other.x * y;
			return retval;
		}

		public double Magnitude()
		{
			return Math.Sqrt(x * x + y * y + z * z);
		}


		public void Normalize()
		{
			double magnitude = Magnitude();
			if (magnitude < 1e-32)
				return;
			x = x / magnitude;
			y = y / magnitude;
			z = z / magnitude;
		}

		public DoubleVector GetNormal()
		{
			double magnitude = Magnitude();
			if (magnitude < 1e-32)
				return new DoubleVector();
			return new DoubleVector(x / magnitude, y / magnitude, z / magnitude);
		}

		public double this[int i]
		{
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;
					case 1:
						y = value;
						break;
					case 2:
						z = value;
						break;
					default:
						throw new IndexOutOfRangeException();
				}
			}
			get
			{
				switch (i)
				{
					case 0:
						return x;
					case 1:
						return y;
					case 2:
						return z;
					default:
						throw new IndexOutOfRangeException();
				}
			}
		}
	}
}
