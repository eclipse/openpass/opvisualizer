﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using UnityEngine;

namespace Visualizer.World.Core.Utilities
{
    public class UnityAsyncProxy : MonoBehaviour
    {
        /// <summary>
        /// Execute async operation and call callback action afterwards.
        /// </summary>
        /// <param name="Operation">Operation to execute.</param>
        /// <param name="Callback">Action to call.</param>
        public void Execute(AsyncOperation Operation, Action Callback)
        {
            StartCoroutine(ExecuteAsync(Operation, Callback));
        }

        private IEnumerator ExecuteAsync(AsyncOperation Operation, Action Callback)
        {
            yield return Operation;

			if(Callback != null)
            	Callback.Invoke();
        }
    }
}
