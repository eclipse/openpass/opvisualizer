﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Globalization;
using System.Threading;

namespace Visualizer.World.Core.Utilities
{
	public static class ParsingUtilities
	{
		public static T ParseType<T>(this object value, CultureInfo cultureInfo)
		{
			var toType = typeof(T);

			if (value == null) return default(T);

			if (value is string)
			{
				if (toType == typeof(Guid))
				{
					return ParseType<T>(new Guid(Convert.ToString(value, cultureInfo)), cultureInfo);
				}
				if ((string)value == string.Empty && toType != typeof(string))
				{
					return ParseType<T>(null, cultureInfo);
				}
			}
			else
			{
				if (typeof(T) == typeof(string))
				{
					return ParseType<T>(Convert.ToString(value, cultureInfo), cultureInfo);
				}
			}

			if (toType.IsGenericType &&
				toType.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				toType = Nullable.GetUnderlyingType(toType); ;
			}

			bool canConvert = toType is IConvertible || (toType.IsValueType && !toType.IsEnum);
			if (canConvert)
			{
				return (T)Convert.ChangeType(value, toType, cultureInfo);
			}
			return (T)value;
		}

		public static T ParseType<T>(this object value)
		{
			return ParseType<T>(value, CultureInfo.CurrentCulture);
		}

		public static T ToEnum<T>(this string value)
		{
			return (T)Enum.Parse(typeof(T), value, true);
		}

		public static T ToProtoEnum<T>(this string value)
		{
			var valueWithType = typeof(T).Name.ToString() + "_" + value;

			return (T)Enum.Parse(typeof(T), valueWithType, true);
		}
	}
}