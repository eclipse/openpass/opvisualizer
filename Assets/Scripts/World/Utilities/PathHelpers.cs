﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.IO;
using System.Linq;

namespace Visualizer.World.Core.Utilities
{
    public static class PathHelpers
    {
        public static string CombinePaths(params string[] args)
        {
            return args.Aggregate("", Path.Combine);
        }
    }
}
