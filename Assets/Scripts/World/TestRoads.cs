/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

namespace Visualizer.World.Environment
{
    public class TestRoads {
        public const string SimpleStraight = "<OpenDRIVE>" +
        "<header revMajor='1' revMinor='1'/>" +
        "<road name='' length='500' id='0' junction='-1'>" +
            "<planView>" +
                "<geometry s='0' x='-250' y='5.5' hdg='0' length='5100'>" +
                    "<line />" +
                "</geometry>" +
            "</planView>" +
            "<lanes>" +
                "<laneSection s='0'>" +
                    "<left />" +
                    "<center>" +
                        "<lane id='0' type='border' level='true'>" +
                            "<link>" +
                                "<successor id='0' />" +
                            "</link>" +
                            "<roadMark weight='standard' sOffset='0' type='solid' color='standard' width='0.12' laneChange='both'/>" +
                        "</lane>" +
                    "</center>" +
                    "<right>" +
                        "<lane id='-1' type='driving' level='true'>" +
                            "<link>" +
                                "<successor id='-1' />" +
                            "</link>" +
                            "<width sOffset='0' a='3.75' b='0' c='0' d='0' />" +
                            "<roadMark weight='standard' sOffset='0' type='broken' color='standard' width='0.12' laneChange='both' />" +
                        "</lane>" +
                        "<lane id='-2' type='driving' level='true'>" +
                            "<link>" +
                                "<successor id='-2' />" +
                            "</link>" +
                            "<width sOffset='0' a='3.75' b='0' c='0' d='0' />" +
                            "<roadMark weight='standard' sOffset='0' type='solid' color='standard' width='0.12' laneChange='both' />" +
                        "</lane>" +
                    "</right>" +
                "</laneSection>" +
            "</lanes>" +
        "</road>" +
    "</OpenDRIVE>";
    }
}