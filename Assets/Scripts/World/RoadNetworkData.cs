﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Visualizer.World.OpenDrive
{
	public class RoadNetworkHeader
	{
		[XmlElement("geoReference",Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
		public string GeoReference;

		[XmlAttribute("name")]
		public string Name;

		[XmlAttribute("north")]
		public double North;

		[XmlAttribute("south")]
		public double South;

		[XmlAttribute("east")]
		public double East;

		[XmlAttribute("west")]
		public double West;
	}

	/// <summary>
	/// Class containing all loaded data relating to the definition of the current road network.
	///
	/// To access the graph of the road network, this data must be converted to a RoadNetworkGraph object
	/// </summary>
	[Serializable]
	[XmlRoot(ElementName = "OpenDRIVE",IsNullable = false)]
	public class RoadNetworkData
	{
		[SerializeField]
		private Dictionary<string, Junction> junctions;

		[SerializeField]
		private Dictionary<string, Road> roads;

		[XmlIgnore]
		public Bounds NetworkBounds
		{
			get
			{
				if (Header == null)
				{
					return new Bounds();
				}
				return new Bounds(
					0.5f*(new Vector3(
						(float)(Header.East) + (float)(Header.West),
						0,
						(float)(Header.North) + (float)(Header.South))),
					new Vector3(
						(float)(Header.East) - (float)(Header.West),
						0,
						(float)(Header.North) - (float)(Header.South)));
			}
		}

		[XmlElement("header")]
		public RoadNetworkHeader Header { get; set; }

		[XmlElement("junction")]
		public Junction[] DeserializeJunctions
		{
			set
			{
				Junctions.Clear();
				if(value != null)
				{
					foreach (Junction element in value)
					{
						Junctions.Add(element.GetID(),element);
					}
				}
			}
			get
			{
				Debug.LogError("DeserializeJunctions must have a getter for the serializer to work, but this should never be used!");
				return Junctions.Values.ToArray();
			}
		}

		[XmlIgnore]
		public Dictionary<string, Junction> Junctions
		{
			get { return junctions; }
			private set { junctions = value; }
		}

		[XmlElement("road")]
		public Road[] DeserializeRoads
		{
			set
			{
				Roads.Clear();
				if(value != null)
				{
					foreach (Road element in value)
					{
						Roads.Add(element.GetID(),element);
					}
				}
			}
			get
			{
				Debug.LogError("DeserializeRoads must have a getter for the serializer to work, but this should never be used!");
				return Roads.Values.ToArray();
			}
		}

		[XmlIgnore]
		public Dictionary<string, Road> Roads
		{
			get { return roads; }
			private set { roads = value; }
		}

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Empty constructor, needed for serialization.
		/// </summary>
		public RoadNetworkData()
		{
			Roads = new Dictionary<string, Road>();
			Junctions = new Dictionary<string, Junction>();
		}
	}
}