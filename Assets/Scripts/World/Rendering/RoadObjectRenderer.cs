﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.World.OpenDrive;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.World.Environment
{

	public interface IRoadObjectRenderer {
		void CreateObjects (Road road, GameObject rootObject);
	}
	public class RoadObjectRenderer : RoadObjectRendererBase, IRoadObjectRenderer {

		private IUnityIO unityIO;
		// private const string objectLibraryPath = "Prefabs/Objects/";
		// private GameObject unknownObject = Resources.Load (objectLibraryPath + "FlexCube") as GameObject;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IUnityIO uIO){
			unityIO = uIO;
		}

		/// <summary>
		/// Create game objects for each road object on the given road
		/// </summary>
		public void CreateObjects(Road road, GameObject rootObject){

			//Create general objects
			foreach(var generalRoadObject in road.RoadObjects.GeneralObjects)
			{
				CreateGeneralObject (generalRoadObject, rootObject, road);
			}

			//Create tunnels
			foreach (Tunnel tunnel in road.RoadObjects.Tunnels)
			{
				CreateTunnel (tunnel);
			}

			//Create bridges
			foreach (Bridge bridge in road.RoadObjects.Bridges)
			{
				CreateBridge (bridge);
			}

			//Create object references
			foreach (RoadObjectReference reference in road.RoadObjects.References)
			{
				CreateObjectReference (reference);
			}
		}

		/// <summary>
		/// Creates a static representation of an OpenDRIVE Road Object Record
		/// </summary>
		/// <remarks>
		/// The record's "name" attribute must match the name of a prefab in the object library.
		/// If no addressable asset is found with the record's name, 
		/// an asset is tried to be instanteated with the record's type.
		/// If that does not succeed either, FlexCube is instanteated.
		/// </remarks>
		private void CreateGeneralObject(GeneralRoadObject generalObject, GameObject rootObject, Road road){
			GameObject prefab = null;
			prefab = LoadObjectPrefab(generalObject.Name, generalObject.Type);
				
			if (generalObject.Repeat.Length == 0)
				CreateSingleObject (prefab, generalObject, rootObject, road);
			else
				CreateRepeatingObject(prefab, generalObject, rootObject, road);
		}

		private void CreateTunnel(Tunnel tunnel){
			//TO BE IMPLEMENTED	
		}

		private void CreateBridge(Bridge bridge){
			//TO BE IMPLEMENTED	
		}

		private void CreateObjectReference(RoadObjectReference reference){
			//TO BE IMPLEMENTED	
		}

		/// <summary>
		/// Loads an addressable object prefab resource by the given asset key.
		/// As key it tries first the object name, if not found, the object type.  
		/// If the resource is still not found, it falls back to default unknown prefab
		/// </summary>
		private GameObject LoadObjectPrefab(string objectName, string objectType){	
			GameObject objectPrefab = null;
			objectPrefab = unityIO.LoadAsset("object_" + objectName.ToLower());
			if (objectPrefab == null) {
				objectPrefab = unityIO.LoadAsset("object_" + objectType.ToLower());
				if (objectPrefab == null) {
					Debug.LogError ("The road object [" + objectName + "] could not be found in the object library. Falling back to deafult representation (cube)."); 
					objectPrefab = Resources.Load ("Prefabs/Objects/Unknown") as GameObject;
				}
			}
			return objectPrefab;
    	}

		private void CreateSingleObject(GameObject prefab, GeneralRoadObject obj, GameObject rootObject, Road road){
			if(prefab == null)
				return;

			GameObject objInstance = GameObject.Instantiate (prefab, rootObject.transform) as GameObject;
			objInstance.name = "Object " + obj.ID;
			objInstance.tag = "RoadObject";
			Vector3 prefabSize = GetPrefabSize(prefab);
			SetObjectPosition(obj.S, obj.T, obj.Z, objInstance, road);
			SetObjectOrientation (obj.S, obj.Hdg, obj.Roll, obj.Pitch, obj.Orientation, objInstance, road);
			SetObjectSize(obj.Width, obj.Height, obj.Length, objInstance, prefabSize);

			RoadObjectInfo objectMetaInfo = objInstance.AddComponent<RoadObjectInfo>();
			objectMetaInfo.data.id = obj.ID;
			objectMetaInfo.data.type = obj.Type;
			objectMetaInfo.data.name = obj.Name;
			objectMetaInfo.data.repeatId = -1;
			objectMetaInfo.data.s = obj.S;
			objectMetaInfo.data.t = obj.T;
			objectMetaInfo.data.width = obj.Width;
			objectMetaInfo.data.height = obj.Height;
			objectMetaInfo.data.length = obj.Length;
		}

		private void CreateRepeatingObject(GameObject mainObject, GeneralRoadObject generalObject, GameObject rootObject, Road road){
			if(mainObject == null)
				return;

			foreach (RoadObjectRepeat repeatRule in generalObject.Repeat) {
				//localSPos is the S position in the repeating sequence (repeatRule.S is the S position of the beginning of the repeating section)
				double localSPos = 0;

				int repeatID = 0;
				Vector3 prefabSize = GetPrefabSize(mainObject);

				while ((road.Length > repeatRule.S + localSPos) && localSPos < repeatRule.Length) {
					//create a copy of the main object
					GameObject objInstance = GameObject.Instantiate (mainObject, rootObject.transform) as GameObject;
					objInstance.name = "Object " + generalObject.ID + "_" + (repeatID++);
					objInstance.tag = "RoadObject";

					GeneralRoadObject repeatObj = new GeneralRoadObject ();
					// take over all attributes from the parent object and then overwrite the repeating parameters
					repeatObj = generalObject;
					repeatObj.S = repeatRule.S + localSPos;
					repeatObj.T = CalculateRepeatTOffset(localSPos, repeatRule, generalObject);
					repeatObj.Z = CalculateRepeatZOffset(localSPos, repeatRule, generalObject);
					repeatObj.Height = CalculateRepeatHeight(localSPos, repeatRule, generalObject);
					repeatObj.Hdg = CalculateRepeatHeading(repeatRule, generalObject);

					SetObjectPosition(repeatObj.S, repeatObj.T, repeatObj.Z, objInstance, road);
					SetObjectOrientation (repeatObj.S, repeatObj.Hdg, repeatObj.Roll, repeatObj.Pitch, repeatObj.Orientation, objInstance, road);
					SetObjectSize(repeatObj.Width, repeatObj.Height, repeatObj.Length, objInstance, prefabSize);

					float sizeFactor = road.GetTangentSizeFactorAt (repeatObj.S, repeatObj.T);
					if (repeatRule.Distance == 0) {
						localSPos += sizeFactor * prefabSize.z;
					} else {
						localSPos += repeatRule.Distance;
					}

					RoadObjectInfo objectMetaInfo = objInstance.AddComponent<RoadObjectInfo>();
					objectMetaInfo.data.id = generalObject.ID;
					objectMetaInfo.data.type = generalObject.Type;
					objectMetaInfo.data.name = generalObject.Name;
					objectMetaInfo.data.repeatId = repeatID;
					objectMetaInfo.data.s = repeatObj.S;
					objectMetaInfo.data.t = repeatObj.T;
				}
			}
		}

		//Calculate T for a repeating object sequence
		private double CalculateRepeatTOffset(double s, RoadObjectRepeat repeatRule, GeneralRoadObject generalObject){
			if (repeatRule.TStart == repeatRule.TEnd)
				return generalObject.T;
			 else 
				return repeatRule.TStart + s * (repeatRule.TEnd - repeatRule.TStart) / repeatRule.Length;
		}

		//Calculate Z for a repeating object sequence 
		private double CalculateRepeatZOffset(double s, RoadObjectRepeat repeatRule, GeneralRoadObject generalObject){
			if (repeatRule.ZOffsetStart == repeatRule.ZOffsetEnd)
				return generalObject.Z;
			else 
				return repeatRule.ZOffsetStart + s * (repeatRule.ZOffsetEnd - repeatRule.ZOffsetStart) / repeatRule.Length;
		}

		//Calculate object height for a repeating object sequence 
		private double CalculateRepeatHeight(double s, RoadObjectRepeat repeatRule, GeneralRoadObject generalObject){
			if (repeatRule.HeightStart == repeatRule.HeightEnd)
				return generalObject.Height;
			else 
				return repeatRule.HeightStart + s * (repeatRule.HeightEnd - repeatRule.HeightStart) / repeatRule.Length;
		}

		//Calculate object heading for a repeating object sequence (this is only the component of the rotation that adds to general heading
		private double CalculateRepeatHeading(RoadObjectRepeat repeatRule, GeneralRoadObject generalObject){
			if (repeatRule.TStart == repeatRule.TEnd)
				return generalObject.Hdg;
			else
				return Math.Atan2 (repeatRule.TStart - repeatRule.TEnd, repeatRule.Length);
		}
	}
}