﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.World.OpenDrive;

namespace Visualizer.World.Environment
{
	public abstract class RoadObjectRendererBase {

		protected virtual Vector3 GetPrefabSize(GameObject prefab) {
			//add up all sub-objects that the object consists of
			Bounds prefabBounds = new Bounds(prefab.transform.position, Vector3.zero);
			foreach(Renderer renderer in prefab.GetComponentsInChildren<Renderer>())
				prefabBounds.Encapsulate(renderer.bounds);
			return prefabBounds.size;
		}

		protected virtual void SetObjectPosition(double s, double t, double z, GameObject objectInstance, Road road){
			Vector3 objectGroundPos = road.GetWorldCoordinatesAt ((float)s, (float)t);
			Vector3 objectHeightAboveGround = new Vector3 (0f, (float)z, 0f);
			objectInstance.transform.position = objectGroundPos + objectHeightAboveGround;
		}

		protected virtual void SetObjectOrientation(double s, double hdg, double roll, double pitch, RoadObjectOrientation orientation, GameObject objectInstance, Road road){
			//set the direction of the object to the forward dir of the road
			Vector3 forwardDir = road.GetForwardDirectionAt(s);			
			objectInstance.transform.rotation = Quaternion.LookRotation(-forwardDir);

			//set heading
			objectInstance.transform.Rotate(Vector3.up, (float)-hdg*Mathf.Rad2Deg);

			//set roll
			objectInstance.transform.Rotate(Vector3.forward, (float)roll*Mathf.Rad2Deg);

			//set pitch
			objectInstance.transform.Rotate(Vector3.left, (float)pitch*Mathf.Rad2Deg);

			//set orientation
			if (orientation == RoadObjectOrientation.BACKWARD) {
				objectInstance.transform.Rotate(Vector3.up, 180f);
			}
		}

		protected virtual void SetObjectSize(double width, double height, double length, GameObject objectInstance, Vector3 origSize){
			Vector3 scale = new Vector3 (1f, 1f, 1f);
			if (width != 0) {
				scale.x = (float)width / origSize.x;
			}
			if (height != 0) {
				scale.y = (float)height / origSize.y;
			}
			if (length != 0) {
				scale.z = (float)length / origSize.z;
			}
			objectInstance.transform.localScale = scale;
		}
	}
}