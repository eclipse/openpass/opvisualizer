﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.World.OpenDrive;
using Visualizer.TrafficSignHandling;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.World.Environment
{

	public interface IRoadSignalRenderer {
		void Clear();
		void CreateSignals (Road road, GameObject rootObject);
		List<TrafficSignController> GetSignalControllers();
	}

	public class RoadSignalRenderer : RoadObjectRendererBase, IRoadSignalRenderer {

		private IUnityIO unityIO;

		private static readonly List<string> ExtensionSignals = new List<string>
		{
			{"1004"}
		};

		private List<TrafficSignController> signalControllers = new List<TrafficSignController>();

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IUnityIO uIO){
			unityIO = uIO;
		}

		/// <summary>
		/// Clears the instance
		/// </summary>
		public void Clear(){
			signalControllers.Clear();
		}

		/// <summary>
		/// Returns the list of signal controllers
		/// </summary>
		public List<TrafficSignController> GetSignalControllers(){
			return signalControllers;
		}

		/// <summary>
		/// Create game objects for each signal on the given road
		/// </summary>
		public void CreateSignals(Road road, GameObject rootObject){
			foreach(var signal in road.RoadSignals.Signals)
			{
				CreateTrafficSign (signal, rootObject, road);
			}

			foreach (var reference in road.RoadSignals.References)
			{
				CreateSignalReference (reference);
			}
		}

		private void CreateTrafficSign(RoadSignal signal, GameObject rootObject, Road road){
			GameObject prefab = null;
			GameObject signalInstance = null;

			string signalCode = GetSignalCode(signal);
			
			prefab = LoadSignalPrefab(signalCode);
			if( prefab != null)
				signalInstance = GameObject.Instantiate (prefab, rootObject.transform) as GameObject;

			if (signalInstance == null) {
				Debug.LogWarning ("The traffic sign could not be instantiated!"); 
				return;
			}
				
			signalInstance.name = "TrafficSign " + signal.ID + " (" + signalCode + ")";
			signalInstance.tag = "TrafficSign";

			if(signal.Dynamic.ToLower() == "yes"){
				AddDynamicSignalController(signalInstance, signal);
			}

			Vector3 prefabSize = GetPrefabSize(prefab);
			SetObjectPosition(signal.S, signal.T, signal.ZOffset, signalInstance, road);
			SetObjectOrientation (signal.S, signal.HOffset, signal.Roll, signal.Pitch, signal.Orientation, signalInstance, road);
			SetObjectSize(signal.Width, signal.Height, 0, signalInstance, prefabSize);

			RoadObjectInfo objectMetaInfo = signalInstance.AddComponent<RoadObjectInfo>();
			objectMetaInfo.data.id = signal.ID;
			objectMetaInfo.data.type = signal.Type;
			objectMetaInfo.data.subType = signal.SubType;	
			objectMetaInfo.data.name = signal.Name;
			objectMetaInfo.data.repeatId = -1;
			objectMetaInfo.data.s = signal.S;
			objectMetaInfo.data.t = signal.T;
			objectMetaInfo.data.width = signal.Width;
			objectMetaInfo.data.height = signal.Height;
		}

		private void AddDynamicSignalController(GameObject signalInstance, RoadSignal signal) {
			if(!signalControllers.Any(c => c.SignalID == signal.ID)) {
				TrafficSignController controller = signalInstance.GetComponent<TrafficSignController>();
				if(controller != null) {
					controller.SignalID = signal.ID;
					signalControllers.Add(controller);
				}
			} else { 
				Debug.LogError("Found multiple dynamic road signals with the same id (" + signal.ID + ").");
			}
		}

		private string GetSignalCode(RoadSignal signal) {
			string signalCode = "signal_";

			if(signal.Country == "" || signal.Country.ToLower() == "opendrive")
				signalCode += "de_";
			else				
				signalCode += signal.Country.ToLower() + "_";
			
			signalCode += signal.Type.ToLower();

			if (signal.SubType != "none" && signal.SubType != "-1" && signal.SubType != "" && signal.SubType != "0")
				signalCode += "-" + signal.SubType.ToLower();

			//for extension signals we need to consider the value attribute
			//so that we can find the matching prefab
			if (ExtensionSignals.Contains(signal.Type) && signal.Value != "")
				signalCode += "-" + signal.Value.ToLower();

			return signalCode;
		}

		/// <summary>
		/// Loads an addressable signal prefab resource by the given asset key. 
		/// If the resource is not found, it falls back to default unknown prefab
		/// </summary>
		private GameObject LoadSignalPrefab(string signalCode){	
			GameObject signalPrefab = null;
			signalPrefab = unityIO.LoadAsset(signalCode);
			if (signalPrefab == null) {
				Debug.LogError ("The traffic sign with code " + signalCode + " could not be found in the signal library. Signal will be displayed as unknown."); 
				signalPrefab = Resources.Load ("Prefabs/Signals/Unknown") as GameObject;
			}
			return signalPrefab;
    	}

		private void CreateSignalReference(SignalReference reference){
			//TO BE IMPLEMENTED	
		}
	}
}