/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;

namespace Visualizer.World.Environment
{
	public struct RoadObjectMetaData
	{
		public string id;
		public string name;
		public string type;
		public string subType;
		public int repeatId;
		public double s;
		public double t;
		public double width;
		public double height;
		public double length;
	}

	/// <summary>
	/// Class that represents the metadata of an openDRIVE road object. It can be attached to a GameObject as component 
	/// </summary>
	public class RoadObjectInfo : MonoBehaviour {
		public RoadObjectMetaData data;
    }
}