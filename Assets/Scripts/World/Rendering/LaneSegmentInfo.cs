/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Visualizer.World.OpenDrive;

namespace Visualizer.World.Environment
{
	/// <summary>
	/// Class that wraps the metadata of a lane segment meta data so that it can be attached to a GameObject as component 
	/// </summary>
	public class LaneSegmentInfo : MonoBehaviour {
		public MeshFactory.LaneSegmentMetaData metaData; 
		public Dictionary<Vector3, Vector2> roadCoordinates = new Dictionary<Vector3, Vector2>();
    }
}