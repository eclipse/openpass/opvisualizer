﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Visualizer.World.OpenDrive;
using Zenject;

namespace Visualizer.World.Environment
{

	public interface IRoadNetworkRenderer {
		void BuildRoad (Map map);
		GameObject GetRootObject();
	}

	public class RoadNetworkRenderer : IRoadNetworkRenderer
	{
		private IRoadObjectRenderer objectRenderer;
		private IRoadSignalRenderer signalRenderer;

		private GameObject rootObject = null;
		private float roadHeightOffset = 0f;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IRoadObjectRenderer roRenderer, IRoadSignalRenderer rsRenderer){
			objectRenderer = roRenderer;
			signalRenderer = rsRenderer;
		}

		/// <summary>
		/// Creates all roads, signals and object from the given mesh file
		/// </summary>
		public void BuildRoad(Map map){
			if(map == null) 
				return;

			rootObject = new GameObject("RoadNetwork");

			CreateStreets(map);

			foreach(var it in map.GetRoadNetwork().GetData().Roads)
			{
				GameObject roadObject = FindOrCreateRoadGameObject("Road " + it.Value.ID);
				objectRenderer.CreateObjects(it.Value, roadObject);
				signalRenderer.CreateSignals(it.Value, roadObject);
			}
		}

		/// <summary>
		/// Returns the root road game object
		/// </summary>
		public GameObject GetRootObject(){
			return rootObject;
		}

		/// <summary>
		/// Creates all meshes of all roads in the given map file
		/// </summary>
		private void CreateStreets(Map map){
			foreach(var it in map.GetRoadNetwork().GetLaneMeshes()) //looping trhough roads
			{
				GameObject roadObject = FindOrCreateRoadGameObject("Road " + it.Key.roadID);

				foreach(var mesh in it.Value.GetGeneratedMeshes()) //looping through lane segments
				{
					GameObject laneGameObject = new GameObject("LaneSection " + it.Key.laneID);
					laneGameObject.transform.parent = roadObject.transform;
					laneGameObject.tag = "LaneSection";
					var laneSegmentInfo = laneGameObject.AddComponent<LaneSegmentInfo>();	
					laneSegmentInfo.metaData = it.Key;
					laneSegmentInfo.roadCoordinates = it.Value.GetRoadCoordinates();
					
					if(it.Key.laneID != 0) {
						var renderer = laneGameObject.AddComponent<MeshRenderer>();
						var laneMesh = laneGameObject.AddComponent<MeshFilter>();			
						laneMesh.sharedMesh = mesh;
						renderer.material = map.GetLocalization().GetLaneMaterial(it.Key.laneType);
						renderer.shadowCastingMode = ShadowCastingMode.Off;
						laneGameObject.AddComponent<MeshCollider>();
					}

					if (it.Key.roadMark != null && it.Key.roadMark.SimplifiedType != SimplifiedRoadMarkType.NONE) {
						Material laneMarkMaterial = map.GetLocalization().GetRoadMarkMaterial(it.Key.roadMark.SimplifiedType);
						UnityEngine.Color laneMarkColor = it.Key.roadMark.GetUnityColor();
						AddLaneMark(laneGameObject.transform, mesh, laneMarkMaterial, laneMarkColor);
					}					
					FixRoadMeshZFighting(laneGameObject);
				}
			}
		}

		/// <summary>
		/// Adds a road GameObject to the root road network GameObject.
		/// </summary>
		private GameObject FindOrCreateRoadGameObject(string roadID) {
			GameObject roadObject = rootObject.transform.Find(roadID)?.gameObject;
			if(roadObject == null) {
				roadObject = new GameObject();
				roadObject.transform.parent = rootObject.transform;
				roadObject.name = roadID;
			}
			return roadObject;
		}

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Adds road marks to a lane section.
		/// </summary>
		private void AddLaneMark(Transform laneObj, Mesh laneSectionMesh, Material markMaterial, UnityEngine.Color markColor){  
            //add a new Transform as child of the lane object
            GameObject laneMarkObj = new GameObject();
            laneMarkObj.transform.parent = laneObj;

            //make sure the lane mark is always above the parent (lane)
            AddHeightOffset(laneMarkObj.transform, 0.01f);
            laneMarkObj.name = "LaneMark";

            //copy the mesh of the parent lane section and set the default material
            MeshFilter laneMarkMesh = laneMarkObj.gameObject.AddComponent<MeshFilter> ();
            MeshRenderer laneMarkRenderer = laneMarkObj.gameObject.AddComponent<MeshRenderer> ();
            laneMarkRenderer.shadowCastingMode = ShadowCastingMode.Off;
            laneMarkMesh.mesh = laneSectionMesh;
            laneMarkRenderer.material = markMaterial;
			laneMarkRenderer.material.SetColor ("_Color", markColor);
        }

        /// <summary>
		/// Avoids Z-Fighting of road meshes Adds the given offset to the y component of the tranform
		/// </summary>
		private void FixRoadMeshZFighting(GameObject lane){
			roadHeightOffset += 0.0001f;
			if (roadHeightOffset >= 0.01f)
				roadHeightOffset= 0f;
            AddHeightOffset(lane.transform, roadHeightOffset);
        }

		/// <summary>
		/// Adds the given offset to the y component of the tranform
		/// </summary>
		private void AddHeightOffset(Transform transform, float amount){
            Vector3 pos = transform.position;
            pos.y += amount;
            transform.position = pos;
        }
	}
}