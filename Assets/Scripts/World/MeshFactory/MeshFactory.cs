﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Visualizer.World.OpenDrive
{
	/// <summary>
	/// Exception used throughout the MeshFactory. Whenever something fails during generation of meshes!)
	/// </summary>
	[System.Serializable]
	public class MeshGenerationException : System.Exception
	{
		//
		// For guidelines regarding the creation of new exception types, see
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
		// and
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
		//

		public MeshGenerationException() { }
		public MeshGenerationException(string message) : base(message) { }
		public MeshGenerationException(string message, System.Exception inner) : base(message, inner) { }

		protected MeshGenerationException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

	//---------------------------------------------------------------------------//

	public class MeshFactory
	{
		//temporary arrays to hold the vertex and triangle data for every single lane.
		public class MeshData
		{
			private List<int> tmpTriangles = new List<int>(4096);
			private List<Vector3> tmpVertices = new List<Vector3>(4096);
			private List<Vector2> tmpUVs = new List<Vector2>(4096);
			private List<Mesh> generatedMeshes = new List<Mesh>();
			
			//This map stores the road coordinates corresponding to each world coordinates of the vertices of the mesh (Key: world pos, Value: road pos) 
			private Dictionary<Vector3, Vector2> roadCoordinates = new Dictionary<Vector3, Vector2>();

			public int GetVertexCount() { return tmpVertices.Count; }
			public int GetUVCount() { return tmpUVs.Count; }
			public Dictionary<Vector3, Vector2> GetRoadCoordinates() { return roadCoordinates; }

			public void ClearTempMesh()
			{
				tmpTriangles.Clear();
				tmpVertices.Clear();
				tmpUVs.Clear();
			}

			public void GenerateAndAddMeshFromTempBuffer()
			{
				if(tmpTriangles.Count > 0)
					generatedMeshes.Add(CreateMeshFromTempBuffers());
			}

			/// <summary>
			/// Creates a mesh out of the existing temporary data.
			/// </summary>
			/// <returns>Returns the newly created mesh.</returns>
			Mesh CreateMeshFromTempBuffers()
			{
				//create the actual mesh
				var mesh = new Mesh();
				mesh.SetVertices(tmpVertices);
				mesh.uv = tmpUVs.ToArray();
				mesh.SetTriangles(tmpTriangles, 0);
				mesh.RecalculateNormals();
				mesh.RecalculateBounds();

				ClearTempMesh();
				return mesh;
			}

			public Mesh[] GetGeneratedMeshes()
			{
				GenerateAndAddMeshFromTempBuffer();
				return generatedMeshes.ToArray();
			}

			public void AddNewTriangles(int[] localIndices, Vector3[] localVertices, Vector2[] localUVs, Dictionary<Vector3, Vector2> roadCoords)
			{
				if(localIndices == null || localVertices == null)
					return;
				if(localVertices.Length > 65000)
					return;

				//check if the resulting mesh would have more than 65000 vertices, as that is not supported by unity.
				if(localVertices.Length + tmpVertices.Count > 65000)
					GenerateAndAddMeshFromTempBuffer();

				var offset = tmpVertices.Count;
				tmpVertices.AddRange(localVertices);

				for(int i = 0; i < localIndices.Length; ++i)
					tmpTriangles.Add(localIndices[i] + offset);

				if(localUVs != null)
					tmpUVs.AddRange(localUVs);

				foreach (KeyValuePair<Vector3, Vector2> roadCoord in roadCoords)
					roadCoordinates.TryAdd(roadCoord.Key, roadCoord.Value);
			}
		}
		private Dictionary<LaneSegmentMetaData, MeshData> _tmpMeshData = new Dictionary<LaneSegmentMetaData, MeshData>();

		private enum LaneSide { Left, Right, Center }
		/// <summary>
		/// Describes the edge of a quad. This is used to provide the outer edge of a lane-quad to the neighboring lane, so it can append its geometry right next to it.
		/// </summary>
		private struct QuadEdge
		{
			public Vector3 start;
			public Vector3 end;
		}
		private struct QuadEdgeDirections
		{
			public Vector3 startDir;
			public Vector3 endDir;
		}

		/// <summary>
		/// This struct contains all information that is used to render lane segments. 
		/// A lane section can contain multiple lane segments and a lane segment consists of multiple triangles/quads.
		/// The complete hierarchy is as follows: Road -> Lane -> LaneSection -> LaneSegment -> LaneQuad
		/// Note that a LaneSegmentMetaData struct is used as key in _tmpMeshData dictionary. This allows us to
		/// merge all segments into one mesh, whose metadata is identical.
		/// </summary>
		public struct LaneSegmentMetaData
		{
			public string roadID;
			public int laneID;
			public float s;
			public LaneType laneType;
			public RoadMark roadMark;
		}
		//---------------------------------------------------------------------------//

		/// <summary>
		/// Adds provided triangles (self-contained) to the current temporary mesh (_tmpTriangles, etc.).
		/// </summary>
		/// <param name="metaData">The meta data of the lane segment that this batch displays.</param>
		/// <param name="localIndices">Indices only pointing to the vertices added in this very same step. Indices will automatically be corrected to point to the correct ones in the big array.</param>
		/// <param name="localVertices">Vertex Positions for this batch</param>
		/// <param name="localUVs">The UVs associated with these vertices</param>
		private void AddNewTriangles(LaneSegmentMetaData metaData, int[] localIndices, Vector3[] localVertices, Vector2[] localUVs, Dictionary<Vector3, Vector2> roadCoords)
		{
			if(localVertices == null)
				return;
			//This cannot happen right now, as we only ever add 4 verticies. Therefore no test of this is even possible.
			if(localVertices.Length > 65000)
				throw new MeshGenerationException("Cannot add more than 65000 vertices in a single batch. Unity does not support that!");

			MeshData data;
			if(!_tmpMeshData.TryGetValue(metaData, out data))
			{
				data = new MeshData();
				_tmpMeshData[metaData] = data;
			}

			data.AddNewTriangles(localIndices, localVertices, localUVs, roadCoords);
		}

		private Vector3 CalcGeometryTangent(AbstractGeometry currentGeoNode, float sOffset0)
		{
			sOffset0 -= (float) currentGeoNode.Start;
			return currentGeoNode.CalculateForwardVector(sOffset0).normalized;
		}

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Generates the meshes for a single road.
		/// </summary>
		/// <param name="road">the road the meshes should be generated for</param>
		/// <returns>A dictionary containing the entire meshdata for every single lane instance that is part of this road (every lane-section has the full set of lanes)</returns>
		public Dictionary<LaneSegmentMetaData, MeshData> GenerateRoadMeshes(Road road)
		{
			_tmpMeshData.Clear();
			if(road == null)
				return _tmpMeshData;

			for(int i = 0; i < road.LaneSections.Length; ++i)
			{
				var currLaneSection = road.LaneSections[i];
				float startSOffset = (float)currLaneSection.SOffset;
				float endSOffset = i+1 < road.LaneSections.Length ? (float)road.LaneSections[i+1].SOffset : (float)road.Length;
				List<AbstractGeometry> geometrySection = road.GetGeometryForLanes(startSOffset, endSOffset);

				CorrectLaneSides(currLaneSection);
				SortLanes(currLaneSection);
				GenerateLaneSectionMeshes(road, currLaneSection, startSOffset, endSOffset, geometrySection);
			}

			return _tmpMeshData;
		}

		/// <summary>
		/// We read the lanes from the OpenDRIVE XML into two arrays: LeftLanes from <lanes> -> <left>
		/// and RightLanes from <lanes> -> <right>. But the actual distinction of lanes is
		/// performed by looking at each lane's id (left lanes: positive, right lanes: negative). 
		/// So here we resort the lists based on the IDs, to make sure the above statement is applied.
		/// </summary>
		/// <param name="laneSection">The lane section whose lanes should be sorted</param>
		void CorrectLaneSides(LaneSection laneSection)
		{
			List<Lane> allLanes = laneSection.LeftLanes.ToList();
			allLanes.AddRange(laneSection.RightLanes.ToList());
			laneSection.LeftLanes = allLanes.Where((lane, i) => lane.ID > 0).ToArray();
			laneSection.RightLanes = allLanes.Where((lane, i) => lane.ID < 0).ToArray();
		}

		/// <summary>
		/// Sorts the lanes in a section according to their ID. This is necessary as the sequence of the lanes in the openDRIVE file can be any.
		/// What we need is a sequence according to the lane ID -> increasing order for left lanes and decreasing for the right ones 
		/// </summary>
		/// <param name="laneSection">The lane section whose lanes should be sorted</param>
		void SortLanes(LaneSection laneSection)
		{
			Array.Sort(laneSection.LeftLanes, delegate(Lane lane1, Lane lane2) {
                return lane1.ID.CompareTo(lane2.ID);
            });
			Array.Sort(laneSection.RightLanes, delegate(Lane lane1, Lane lane2) {
                return lane2.ID.CompareTo(lane1.ID);
            });
		}

		/// <summary>
		/// Creates the geometry of the road strip for strip (iterating along the road and foreach iteration across it).
		/// NOTE: stores the generated triangles and meshes into the internal dictionary. Simply appends it, therefore make sure to clear it yourself.
		/// </summary>
		/// <param name="road">The parent road of the lane section.</param>
		/// <param name="laneSection">The lane section for which the geometry should be created</param>
		/// <param name="fromSOffset">starting SOffset of the section</param>
		/// <param name="toSOffset">ending SOffset of the section in question</param>
		/// <param name="geometryList">the geometry data for all lanes as AbstractGeometry list.</param>
		void GenerateLaneSectionMeshes(Road road, LaneSection laneSection, float fromSOffset, float toSOffset, List<AbstractGeometry> geometryList)
		{
			// Calculate the offset starting values
			float lastSOffset = fromSOffset;
			float currentSOffset = lastSOffset;
			float parentOffset = (float)laneSection.SOffset;

			// Loop until there is no valid geometry data (every lane-section can have multiple geometrynodes)
			foreach(var currentGeoNode in geometryList)
			{
				float geoEnd = (float)(currentGeoNode.Start + currentGeoNode.Length);
				float stepSize = currentGeoNode.GetRenderStepSize();

				// Loop until either the end of the current geometry, or until the end of the defined section
				//this creates the entire lane quad for quad.
				while(lastSOffset < geoEnd && lastSOffset < toSOffset)
				{
					// Increment offset
					currentSOffset = GetNextStepSOffset(stepSize, lastSOffset, Mathf.Min(geoEnd, toSOffset), laneSection, road);

					//Currently we put the entire road network onto the XZ plane, with height 0. Later on we'll need to evaluate the elevation-profile, therefore I left the 
					//lines doing that here for reference purpose:
					//float startHeight = Utilities.GetControlAtOffset(lastSOffset, ParentRoadObject.ParentRoad.ElevationProfileData.ElevationChanges).CalculateValueF(lastSOffset - (float)currentGeoNode.Value.Start);
					//float endHeight = Utilities.GetControlAtOffset(currentSOffset, ParentRoadObject.ParentRoad.ElevationProfileData.ElevationChanges).CalculateValueF(currentSOffset - (float)currentGeoNode.Value.Start);
					float startHeight = 0.0f;
					float endHeight = 0.0f;

					// Center lane first. used as reference later on
					QuadEdge centerEdge = new QuadEdge
					{
						start = currentGeoNode.CalculatePoint(lastSOffset - (float)currentGeoNode.Start, startHeight),
						end = currentGeoNode.CalculatePoint(currentSOffset - (float)currentGeoNode.Start, endHeight)
					};
					QuadEdgeDirections centerTangents = new QuadEdgeDirections()
					{
						startDir = CalcGeometryTangent(currentGeoNode, lastSOffset),
						endDir = CalcGeometryTangent(currentGeoNode, currentSOffset)
					};

					centerEdge.start = ApplyLaneOffset(centerEdge.start, centerTangents.startDir, lastSOffset, road);
					centerEdge.end = ApplyLaneOffset(centerEdge.end, centerTangents.endDir, currentSOffset, road);

					// startT and endT cumulate the T offset of the lanes to get global (road) T offset 
					float startT = 0;
					float endT = 0;
					AddLaneQuad(laneSection.CenterLane, centerEdge, lastSOffset, currentSOffset, parentOffset, ref startT, ref endT, centerTangents, LaneSide.Center, road);
					
					// Iterate over all left-lanes (innermost to outermost)
					QuadEdge innerEdge = centerEdge; //as the center lane has no width, we can use the s0 and e0 instead of the s1 and e1
					startT = 0;
					endT = 0;
					foreach(var leftLane in laneSection.LeftLanes)
						//for the next iteration the outer-edge of this iteration becomes the inner-edge of the next (creating a geometry without seams).
						innerEdge = AddLaneQuad(leftLane, innerEdge, lastSOffset, currentSOffset, parentOffset, ref startT, ref endT, centerTangents, LaneSide.Left, road);

					// Iterate over all right-lanes (innermost to outermost)
					innerEdge = centerEdge;
					startT = 0;
					endT = 0;
					foreach(var rightLane in laneSection.RightLanes)
						innerEdge = AddLaneQuad(rightLane, innerEdge, lastSOffset, currentSOffset, parentOffset, ref startT, ref endT, centerTangents, LaneSide.Right, road);

					lastSOffset = currentSOffset;
				}
			}
		}
		/// <summary>
		/// Adds the lane offset to the given point at the given S coordinate along the road 
		/// The point is translated by the offset in lateral direction
		/// </summary>
		/// <param name="point">The point to be translated</param>
		/// <param name="tangentDir">The forward direction in the given point</param>
		/// <param name="sOffset">The S offset of the point along the road</param>
		/// <param name="road">The road that contains the point</param>
		Vector3 ApplyLaneOffset(Vector3 point, Vector3 tangentDir, float sOffset, Road road)
		{
			float laneOffset = road.GetCenterLaneOffsetAtSOffset(sOffset);
			return point + Vector3.Cross(tangentDir, Vector3.up).normalized * laneOffset;
		}

		/// <summary>
		/// Calculates the next SOffset until which the next mesh should reach
		/// It considers the sampling resolution of the given geometry,
		//  lane width changes (as it requieres higher sampling rate) 
		/// and road mark changes of the lane section (as at these points new lane segments need to be created) 
		/// </summary>
		/// <param name="geoStepSize">The sampling resolution of the current geometry</param>
		/// <param name="startSOffset">The starting S offset from which look for the next step size</param>
		/// <param name="endSOffset">The ending S offset of the section in question</param>
		/// <param name="laneSection">The lane section for which the next mesh should be created</param>
		/// <param name="road">The road that contains the given S coordinates</param>
		float GetNextStepSOffset(float geoStepSize, float startSOffset, float endSOffset, LaneSection laneSection, Road road)
		{
			//first, propose that the next mesh border should be at the step size of the geometry
			float laneStepSize = geoStepSize; 
			
			//if there is a width-, laneOffset or road mark change of any lane within the proposed section, we increase the sampling time accordingly (reduce the step size)
			foreach(var leftLane in laneSection.LeftLanes){
				laneStepSize = Mathf.Min(leftLane.GetWidthChangeStepSize(startSOffset, endSOffset), laneStepSize);
				laneStepSize = Mathf.Min(road.GetLaneOffsetChangeStepSize(startSOffset, endSOffset), laneStepSize);
				laneStepSize = Mathf.Min(leftLane.GetNextRoadMarkChange(startSOffset, endSOffset), laneStepSize);
			}

			laneStepSize = Mathf.Min(laneSection.CenterLane.GetNextRoadMarkChange(startSOffset, endSOffset), laneStepSize);

			foreach(var rightLane in laneSection.RightLanes){
				laneStepSize = Mathf.Min(rightLane.GetWidthChangeStepSize(startSOffset, endSOffset), laneStepSize);
				laneStepSize = Mathf.Min(road.GetLaneOffsetChangeStepSize(startSOffset, endSOffset), laneStepSize);
				laneStepSize = Mathf.Min(rightLane.GetNextRoadMarkChange(startSOffset, endSOffset), laneStepSize);
			}
			
			float nextStepSOffset = startSOffset + laneStepSize;
			nextStepSOffset = Mathf.Clamp(nextStepSOffset, 0, endSOffset);
			
			return nextStepSOffset;
		}

		QuadEdgeDirections CalcLaneLeftDirection(QuadEdgeDirections tangents, LaneSide laneSide)
		{
			//take the center-lane-forward direction if avaialble, otherwise go from the edge-values.
			//NOTE: not entirely clear why this was done this way, but it works, and calculating it anew every time, produces slightly different results.
			//NOTE: Flip the left vector for right lanes!
			return new QuadEdgeDirections()
			{
				startDir = (laneSide == LaneSide.Right ? -1f : 1f) * Vector3.Cross(tangents.startDir, Vector3.up).normalized,
				endDir = (laneSide == LaneSide.Right ? -1f : 1f) * Vector3.Cross(tangents.endDir, Vector3.up).normalized
			};
		}

		/// <summary>
		/// Adds a single lane quad to the internal mesh. Takes care whether the lane is a left/right/center lane.
		/// </summary>
		/// <param name="lane">The lane for which to create the quad.</param>
		/// <param name="innerEdge">The starting world-position of the inner-edge of the new quad. (this needs to be provided via the outer edge of the neighboring quad)</param>
		/// <param name="startSOffset">The starting s-offset on the road where our s0 lies.</param>
		/// <param name="endSOffset">The s-offset on the road where our e0 lies.</param>
		/// <param name="parentSOffset">On lane sections this would be the s-offset of the lane-section</param>
		/// <param name="centerLaneTangents">The tangent vectors (normalized) of the center lane edge (world space)</param>
		/// <param name="laneSide">What side is this lane on?</param>
		/// <param name="road">The parent road of this lane.</param>
		/// <returns>the outer edge of the generated quad. This should be used as inner edge for the next neighboring quad.</returns>
		QuadEdge AddLaneQuad(Lane lane, QuadEdge innerEdge, float startSOffset, float endSOffset, float parentSOffset, ref float startTOffset, ref float endTOffset, QuadEdgeDirections centerLaneTangents, LaneSide laneSide, Road road)
		{
			QuadEdgeDirections leftVectors = CalcLaneLeftDirection(centerLaneTangents, laneSide);

			// Get the S1/E1 positions for the lane given the current width
			var startPoly = lane.GetWidthControllerAtOffset(startSOffset - parentSOffset);
			var startWidth = startPoly.CalculateValueF(startSOffset - ((float)startPoly.SOffset + parentSOffset));
			var endPoly = lane.GetWidthControllerAtOffset(endSOffset - parentSOffset);
			var endWidth = endPoly.CalculateValueF(endSOffset - ((float)endPoly.SOffset + parentSOffset));
			QuadEdge outerEdge = new QuadEdge
			{
				start = innerEdge.start + (leftVectors.startDir * startWidth),
				end = innerEdge.end + (leftVectors.endDir * endWidth)
			};

			RoadMark roadMark = lane.GetRoadMarkAtOffset(startSOffset - parentSOffset);
			LaneSegmentMetaData metaData = new LaneSegmentMetaData
			{
				roadID = road.ID,
				laneID = lane.ID,
				s = parentSOffset,
				laneType = lane.LaneType,
				roadMark = roadMark
			};
			//========================================//

			//actually generate the quad
			Vector3[] vertices = null;
			if(laneSide == LaneSide.Center)
			{
				//The center lane has zero width in OpenDrive ->
				//So here we "inflate" the lane mesh by adding a half road mark width to left and to right 
				//Note that currently only one road mark per lane is supported
				float roadMarkWidth = 0f;
				if(lane.RoadMarks.Length > 0)
					roadMarkWidth = lane.RoadMarks[0].Width > 0 ? (float)lane.RoadMarks[0].Width : 0.12f;
				vertices = CalcCenterLaneVertices(innerEdge, outerEdge, roadMarkWidth, centerLaneTangents);
			}
			else if(laneSide == LaneSide.Left)
				vertices = CalcLeftLaneVertices(innerEdge, outerEdge);
			else if(laneSide == LaneSide.Right)
				vertices = CalcRightLaneVertices(innerEdge, outerEdge);
 
			//Adding here the road coordinates to a world->road coordinate map
			Dictionary<Vector3, Vector2> roodCoords = new Dictionary<Vector3, Vector2>();
			roodCoords.TryAdd(innerEdge.start, new Vector2(startSOffset, startTOffset));  
			roodCoords.TryAdd(innerEdge.end, new Vector2(endSOffset, endTOffset));
			//Note that we sum up the T offset to get the global (road) coordinates from the local (lane) coordinates  
			if (laneSide == LaneSide.Left) {
				startTOffset += startWidth;
				endTOffset += endWidth;
			} else {
				startTOffset -= startWidth;
				endTOffset -= endWidth;
			}
			roodCoords.TryAdd(outerEdge.start, new Vector2(startSOffset, startTOffset));
			roodCoords.TryAdd(outerEdge.end, new Vector2(endSOffset, endTOffset));

			AddNewTriangles(metaData, new int[] { 0, 1, 2, 2, 3, 0 }, vertices, CalculateLaneUV(vertices, startSOffset, endSOffset, laneSide), roodCoords);
			
			return outerEdge;
		}

		//---------------------------------------------------------------------------//

		Vector3[] CalcLeftLaneVertices(QuadEdge innerEdge, QuadEdge outerEdge)
		{
			//left side lane (assumes a triangle indexing of: 0, 1, 2, 2, 3, 0)
			/*
			 * e1------e0
			 * |        |
			 * |        |
			 * |        |
			 * s1------s0
			*/
			return new Vector3[] { outerEdge.start, outerEdge.end, innerEdge.end, innerEdge.start };
		}

		Vector3[] CalcCenterLaneVertices(QuadEdge innerEdge, QuadEdge outerEdge, float roadMarksWidth, QuadEdgeDirections centerTangents)
		{
			//center lane (assumes a triangle indexing of: 0, 1, 2, 2, 3, 0)
			/*
			 * e1------e0
			 * |        |
			 * |        |
			 * |        |
			 * s1------s0
			*/
			//We also increase the height of the mesh minimally to ensure the center lane is always rendered above other overlapping lanes
			var laneOffset = new Vector3(0f, 0.001f, 0f);

			QuadEdgeDirections leftVectors = CalcLaneLeftDirection(centerTangents, LaneSide.Center);

			var e0_ = innerEdge.end - (leftVectors.endDir * roadMarksWidth/2f) + laneOffset;
			var e1_ = outerEdge.end + (leftVectors.endDir * roadMarksWidth/2f) + laneOffset;
			var s0_ = innerEdge.start - (leftVectors.startDir * roadMarksWidth/2f) + laneOffset;
			var s1_ = outerEdge.start + (leftVectors.startDir * roadMarksWidth/2f) + laneOffset;
			return new Vector3[] { s1_, e1_, e0_, s0_ };
		}

		Vector3[] CalcRightLaneVertices(QuadEdge innerEdge, QuadEdge outerEdge)
		{
			//right side lane (assumes a triangle indexing of: 0, 1, 2, 2, 3, 0)
			/*
			 * e0------e1
			 * |        |
			 * |        |
			 * |        |
			 * s0------s1
			*/
			return new Vector3[] { innerEdge.start, innerEdge.end, outerEdge.end, outerEdge.start };
		}

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Creates the UV coords for the generated meshes, along the lane. Horizontally the texture coords always go from 0 to width. Along the road
		/// they reflect the actual distance on the road.
		/// </summary>
		/// <param name="width">Width of the lane</param>
		/// <param name="startSOffset">starting S offset is used as longitude-UV coord (Y component)</param>
		/// <param name="endSOffset">ending UV Y-coord.</param>
		/// <param name="laneSide">What side this lane is from (left, right, center)</param>
		/// <returns></returns>
		Vector2[] CalculateLaneUV(Vector3[] vertices, float startSOffset, float endSOffset, LaneSide laneSide)
		{
			float meshWidthTop = Vector3.Distance (vertices [1], vertices [2]);
			float meshWidthBottom = Vector3.Distance (vertices [0], vertices [3]);

			Vector2[] uvsLeft = new Vector2[4];
			uvsLeft[0] = new Vector2(0f, startSOffset);
			uvsLeft[1] = new Vector2(0f, endSOffset);
			uvsLeft[2] = new Vector2(meshWidthTop, endSOffset);
			uvsLeft[3] = new Vector2(meshWidthBottom, startSOffset);

			//rotate uv map (and hence the texture on the mesh) if it's a right lane
			if(laneSide == LaneSide.Right)
			{
				var uvsRight = new Vector2[4];
				uvsRight[0] = uvsLeft[3];
				uvsRight[1] = uvsLeft[2];
				uvsRight[2] = uvsLeft[1];
				uvsRight[3] = uvsLeft[0];
				return uvsRight;
			}
			return uvsLeft;
		}
	}
}

