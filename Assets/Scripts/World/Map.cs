﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using Visualizer.World.OpenDrive;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Visualizer.World.Environment
{
	public interface IMapFactory
	{
		IMap Create(string file, bool loadingFromDisk, MapConfig mapConfig);
	}

	public interface IRoadNetworkDataProvider
	{
		RoadNetworkData GetRoadNetworkData();
	}

	public interface IMap : IRoadNetworkDataProvider
	{
		MapConfig GetLocalization();
	}

	public class Map : IMap
	{
		public class Factory : IMapFactory
		{
			private readonly MeshFactory meshFactory;
			private readonly MapConfig defaultMapConfig;

			public Factory(MeshFactory meshFactory)
			{
				if (meshFactory == null) throw new ArgumentNullException(nameof(meshFactory));
				this.meshFactory = meshFactory;

				defaultMapConfig = Object.Instantiate(Resources.Load(@"MapConfigs\DefaultMapConfig")) as MapConfig;
			}

			public IMap Create(string file, bool loadingFromDisk, MapConfig mapConfig = null)
			{
				return new Map(file, meshFactory, mapConfig ?? defaultMapConfig, loadingFromDisk);
			}
		}

		private RoadNetwork loadedRoadNetwork;
		private MapConfig mapConfig;
		private MeshFactory meshFactory;

		public RoadNetwork GetRoadNetwork() { return loadedRoadNetwork; }
		public MapConfig GetLocalization() { return mapConfig; }

		public Map(string file, MeshFactory meshFactory, MapConfig mapConfig, bool loadingFromDisk)
		{
			this.meshFactory = meshFactory;
			this.mapConfig = mapConfig;

			//load the road network from the opendrive file
			loadedRoadNetwork = new RoadNetwork(file, meshFactory, loadingFromDisk);

			if(meshFactory != null && mapConfig != null){
				ResizeTerrain();
			}
		}

		private void ResizeTerrain()
		{
			if(Terrain.activeTerrain == null) return;
			
 			//at the moment applying a large margin to make sure all is covered even if the opendrive file doesn't correctly specify terrain size
			Vector3 margin = new Vector3 (10000f, 0f, 10000f);
			Vector3 terrainPos = Terrain.activeTerrain.transform.position;
			terrainPos.x = GetRoadNetwork().GetData().NetworkBounds.min.x;
			terrainPos.z = GetRoadNetwork().GetData().NetworkBounds.min.z;
			Terrain.activeTerrain.transform.position = terrainPos - margin;
			Terrain.activeTerrain.terrainData.size = GetRoadNetwork().GetData().NetworkBounds.size + 2*margin;
		}

		public RoadNetworkData GetRoadNetworkData()
		{
			return loadedRoadNetwork.GetData();
		}
	}
}
