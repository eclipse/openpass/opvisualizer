﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections.Generic;
using UnityEngine;
using Visualizer.World.OpenDrive;

namespace Visualizer.World.Environment
{
	[JetBrains.Annotations.UsedImplicitly]
	public class MapConfig : ScriptableObject
	{
		[SerializeField]
		private Material defaultRoadMaterial;

		[SerializeField]
		private Material defaultRoadMarkMaterial;

		[SerializeField]
		private List<LaneMarkMaterial> roadMarkMaterials = new List<LaneMarkMaterial>();
		private Dictionary<SimplifiedRoadMarkType, Material> cachedRoadMarkMaterials = null;

		[System.Serializable]
		private class LaneMarkMaterial
		{
			public SimplifiedRoadMarkType type;
			public Material mat;
		}

		[System.Serializable]
		private class LaneMaterial
		{
			public LaneType type;
			public Material mat;
		}

		[SerializeField]
		private List<LaneMaterial> roadMaterials = new List<LaneMaterial>();
		private Dictionary<LaneType, Material> cachedRoadMaterials = null;

		//---------------------------------------------------------------------------//

		[JetBrains.Annotations.UsedImplicitly]
		public void Awake()
		{
			Init();
		}

		void Init()
		{
			cachedRoadMaterials = CreateDictionaryFrom(roadMaterials, it => it.type, it => it.mat);
			cachedRoadMarkMaterials = CreateDictionaryFrom(roadMarkMaterials, it => it.type, it => it.mat);
		}

		Dictionary<K, V> CreateDictionaryFrom<K, V, L>(List<L> list, System.Func<L, K> GetKey, System.Func<L, V> GetValue)
		{
			var dic = new Dictionary<K, V>();
			if(list != null)
				for(int i = 0; i < list.Count; ++i)
					if(!dic.ContainsKey(GetKey(list[i])))
						dic[GetKey(list[i])] = GetValue(list[i]);
			return dic;
		}

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Gets the material for a given lane-type. If avaialble as road material, it will return that one, otherwise it returns the default one.
		/// </summary>
		/// <param name="laneType">lane type to get the material for</param>
		/// <returns>The render material for that lane type</returns>
		public Material GetLaneMaterial(LaneType laneType)
		{
			if(cachedRoadMaterials == null)
				Init();
			Material mat = null;
			return !cachedRoadMaterials.TryGetValue(laneType, out mat) ? defaultRoadMaterial : mat;
		}

		/// <summary>
		/// Gets the material for a given road mark type.
		/// </summary>
		/// <returns>The render material for that road mark type</returns>
		public Material GetRoadMarkMaterial(SimplifiedRoadMarkType markType)
		{
			if(cachedRoadMarkMaterials == null)
				Init();
			Material mat = null;
			return !cachedRoadMarkMaterials.TryGetValue(markType, out mat) ? defaultRoadMarkMaterial : mat;
		}
	}
}
