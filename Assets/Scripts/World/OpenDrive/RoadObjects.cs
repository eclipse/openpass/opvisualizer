﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using System.Xml.Serialization;
using JetBrains.Annotations;

namespace Visualizer.World.OpenDrive
{
	public enum TunnelType
	{
		[XmlEnum("standard")]
		STANDARD = 0,
		[XmlEnum("underpass")]
		UNDERPASS, //i.e. sides are open for daylight
	}

	public enum BridgeType
	{
		[XmlEnum("concrete")]
		CONCRETE = 0,
		[XmlEnum("steel")]
		STEEL,
		[XmlEnum("brick")]
		BRICK,
		[XmlEnum("wood")]
		WOOD,
	}

	public enum RoadObjectOrientation
	{
		[XmlEnum("none")]
		NONE,
		[XmlEnum("+")]
		FORWARD,
		[XmlEnum("-")]
		BACKWARD
	}

	public enum ParkingSpaceAccess
	{
		[XmlEnum("all")]
		ALL,
		[XmlEnum("car")]
		CAR,
		[XmlEnum("women")]
		WOMEN,
		[XmlEnum("handicapped")]
		HANDICAPPED,
		[XmlEnum("bus")]
		BUS,
		[XmlEnum("truck")]
		TRUCK,
		[XmlEnum("electric")]
		ELECTRIC,
		[XmlEnum("residents")]
		RESIDENTS
	}

	public enum ParkingSpaceMarkingSide
	{
		[XmlEnum("front")]
		FRONT,
		[XmlEnum("rear")]
		REAR,
		[XmlEnum("left")]
		LEFT,
		[XmlEnum("right")]
		RIGHT
	}

	public class RoadObjects
	{
		private GeneralRoadObject[] generalObjects = new GeneralRoadObject[0];
		private RoadObjectReference[] references = new RoadObjectReference[0];
		private Tunnel[] tunnels = new Tunnel[0];
		private Bridge[] bridges = new Bridge[0];


		[XmlElement("object")]
		public GeneralRoadObject[] GeneralObjects
		{
			get{return generalObjects;}
			set { generalObjects = value ?? new GeneralRoadObject[0]; }
		}

		/// <summary>
		/// Reference to an object defined in one of the other object categories.
		/// </summary>
		[XmlElement("objectReference")]
		public RoadObjectReference[] References
		{
			get { return references; }
			set { references = value ?? new RoadObjectReference[0]; }
		}

		[XmlElement("tunnel")]
		public Tunnel[] Tunnels
		{
			get { return tunnels;}
			set { tunnels = value ?? new Tunnel[0]; }
		}

		[XmlElement("bridge")]
		public Bridge[] Bridges
		{
			get { return bridges;}
			set { bridges = value ?? new Bridge[0]; }
		}
	}

	public abstract class RoadObjectBase
	{
		private LaneValidity[] validity = new LaneValidity[0];

		[XmlAttribute("id")]
		public string ID { get; set; }

		[XmlAttribute("name")]
		public string Name { get; set; }

		//AndreasGrois: Moved fields that are not common to all child classes to those child classes that have them.

		/// <summary>
		/// S-position of the object's origin
		/// </summary>
		[XmlAttribute("s")]
		public double S { get; set; }

		[XmlElement("validity")]
		public LaneValidity[] Validity
		{
			get { return validity; }
			set { validity = value ?? new LaneValidity[0]; }
		}

		public RoadObjectBase()
		{
			Validity = new LaneValidity[0];
		}
	}

	public class GeneralRoadObject : RoadObjectBase
	{
		private RoadObjectRepeat[] repeat = new RoadObjectRepeat[0];

		[XmlAttribute("type")]
		public string Type { get; set; }

		//id, s, name are in RoadObjectBase.

		/// <summary>
		/// T-position of object's origin
		/// </summary>
		[XmlAttribute("t")]
		public double T { get; set; }

		[XmlAttribute("zOffset")]
		public double Z { get; set; }

		[XmlAttribute("validLength")]
		public double ValidLength { get; set; }

		[XmlAttribute("orientation")]
		public RoadObjectOrientation Orientation { get; set; }

		[XmlAttribute("length")]
		public double Length { get; set; }

		[XmlAttribute("width")]
		public double Width { get; set; }

		[XmlAttribute("radius")]
		public double Radius { get; set; }

		[XmlAttribute("height")]
		public double Height { get; set; }

		[XmlAttribute("hdg")]
		public double Hdg { get; set; }

		[XmlAttribute("pitch")]
		public double Pitch { get; set; }

		[XmlAttribute("roll")]
		public double Roll { get; set; }

		[XmlElement("repeat")]
		public RoadObjectRepeat[] Repeat
		{
			get { return repeat; }
			set { repeat = value ?? new RoadObjectRepeat[0]; }
		}

		[XmlElement("outline")]
		public RoadObjectOutline Outline { get; set; }

		[XmlElement("material")]
		public RoadObjectMaterial Material { get; set; }

		//Lane Validity is in RoadObjectBase.

		[XmlElement("parkingSpace")]
		public ParkingSpace Parking { get; set; }
	}

	public class RoadObjectReference
	{
		private LaneValidity[] validity = new LaneValidity[0];

		/// <summary>
		/// S-position of the object's origin
		/// </summary>
		[XmlAttribute("s")]
		public double S { get; set; }

		/// <summary>
		/// T-position of object's origin
		/// </summary>
		[XmlAttribute("t")]
		public double T { get; set; }

		/// <summary>
		/// The ID of the **referenced** object. That's also why this is not a RoadObjectBase.
		/// </summary>
		[XmlAttribute("id")]
		public string ReferencedID { get; set; }

		[XmlAttribute("zOffset")]
		public double ZOffset { get; set; }

		[XmlAttribute("validLength")]
		public double ValidLength { get; set; }

		[XmlAttribute("orientation")]
		public RoadObjectOrientation Orientation { get; set; }

		[XmlElement("validity")]
		public LaneValidity[] Validity
		{
			get { return validity;}
			set { validity = value ?? new LaneValidity[0]; }
		}

		[XmlIgnore]
		public RoadObjectBase ReferencedRoadObject = null;
	}

	public class Tunnel : RoadObjectBase
	{
		//s, name, id are in RoadObjectBase

		[XmlAttribute("length")]
		public double Length { get; set; }

		[XmlAttribute("type")]
		public TunnelType Type { get; set; }

		[XmlAttribute("lighting")]
		public double Lighting { get; set; }

		[XmlAttribute("daylight")]
		public double Daylight { get; set; }

		//validity is in RoadObjectBase
	}

	public class Bridge : RoadObjectBase
	{

		//s, id, name are in RoadObjectBase
		[XmlAttribute("length")]
		public double Length { get; set; }

		[XmlAttribute("type")]
		public BridgeType Type { get; set; }

		//LaneValidity is in RoadObjectBase
	}

	public class RoadObjectRepeat
	{
		[XmlAttribute("s")]
		public double S { get; set; }

		[XmlAttribute("length")]
		public double Length { get; set; }

		[XmlAttribute("distance")]
		public double Distance { get; set; }

		[XmlAttribute("tStart")]
		public double TStart { get; set; }

		[XmlAttribute("tEnd")]
		public double TEnd { get; set; }

		[XmlAttribute("widthStart")]
		public double WidthStart { get; set; }

		[XmlAttribute("widthEnd")]
		public double WidthEnd { get; set; }

		[XmlAttribute("heightStart")]
		public double HeightStart { get; set; }

		[XmlAttribute("heightEnd")]
		public double HeightEnd { get; set; }

		[XmlAttribute("zOffsetStart")]
		public double ZOffsetStart { get; set; }

		[XmlAttribute("zOffsetEnd")]
		public double ZOffsetEnd { get; set; }
	}
		
	public class RoadObjectOutline
	{
		private CornerRoad[] cornerR = new CornerRoad[0];
		private CornerLocal[] cornerL = new CornerLocal[0];

		[XmlElement("cornerRoad")]
		public CornerRoad[] CornerR
		{
			get {  return cornerR;}
			set { cornerR = value ?? new CornerRoad[0]; }
		}

		[XmlElement("cornerLocal")]
		public CornerLocal[] CornerL
		{
			get { return cornerL;}
			set { cornerL = value ?? new CornerLocal[0]; }
		}

	}

	public class CornerRoad
	{
		[XmlAttribute("s")]
		public double S { get; set; }		

		[XmlAttribute("t")]
		public double T { get; set; }

		[XmlAttribute("dz")]
		public double Dz { get; set; }

		[XmlAttribute("height")]
		public double Height { get; set; }
	}

	public class CornerLocal
	{
		[XmlAttribute("u")]
		public double U { get; set; }		

		[XmlAttribute("v")]
		public double V { get; set; }

		[XmlAttribute("z")]
		public double Z { get; set; }

		[XmlAttribute("height")]
		public double Height { get; set; }
	}

	public class RoadObjectMaterial
	{
		[XmlAttribute("surface")]
		public string Surface { get; set; }		

		[XmlAttribute("friction")]
		public double Friction { get; set; }

		[XmlAttribute("roughness")]
		public double Roughness { get; set; }
	}

	public class LaneValidity
	{
		[XmlAttribute("fromLane")]
		public int FromLane { get; set; }		
		[XmlAttribute("toLane")]
		public int ToLane { get; set; }
	}

	public class ParkingSpace
	{
		private ParkingSpaceMarking[] markings = new ParkingSpaceMarking[0];

		[XmlAttribute("access")]
		public ParkingSpaceAccess Access { get; set; }

		[XmlAttribute("restrictions")]
		public string Restrictions { get; set; }

		[XmlElement("marking")]
		public ParkingSpaceMarking[] Markings
		{
			get { return markings; }
			set { markings = value ?? new ParkingSpaceMarking[0]; }
		}

	}

	public class ParkingSpaceMarking
	{
		[XmlAttribute("side")]
		public ParkingSpaceMarkingSide Side { get; set; }

		[XmlAttribute("type")]
		public SimplifiedRoadMarkType Type { get; set; }

		[XmlAttribute("width")]
		public double Width { get; set; }

		[XmlAttribute("color")]
		public RoadMarkColor Color { get; set; }
	}
}