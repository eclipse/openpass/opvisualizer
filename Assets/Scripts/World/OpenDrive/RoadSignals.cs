﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml.Serialization;
using JetBrains.Annotations;

namespace Visualizer.World.OpenDrive
{
	public class RoadSignals
	{		
		private RoadSignal[] signals = new RoadSignal[0];
		private SignalReference[] references = new SignalReference[0];

		[XmlElement("signal")]
		public RoadSignal[] Signals
		{
			get { return signals; }
			set { signals = value ?? new RoadSignal[0]; }
		}

		[XmlElement("signalReference")]
		public SignalReference[] References
		{
			get { return references; }
			set { references = value ?? new SignalReference[0]; }
		}
	}

	public class RoadSignal
	{
		private SignalValidity[] validity = new SignalValidity[0];
		private SignalDependency[] dependencies = new SignalDependency[0];
		
		// float versions
		public float GetT() { return (float)T; }
		public float GetS() { return (float)S; }
		public float GetZOffset() { return (float)ZOffset; }

		[XmlAttribute("s")]
		public double S { get; set; }

		[XmlAttribute("t")]
		public double T { get; set; }

		[XmlAttribute("id")]
		public string ID { get; set; }

		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("dynamic")]
		public string Dynamic { get; set; }

		[XmlAttribute("orientation")]
		public RoadObjectOrientation Orientation { get; set; }

		[XmlAttribute("zOffset")]
		public double ZOffset { get; set; }

		[XmlAttribute("country")]
		public string Country { get; set; }

		[XmlAttribute("type")]
		public string Type { get; set; }

		[XmlAttribute("subtype")]
		public string SubType { get; set; }

		[XmlAttribute("value")]
		public string Value { get; set; }

		[XmlAttribute("unit")]
		public string Unit { get; set; }

		[XmlAttribute("height")]
		public double Height { get; set; }

		[XmlAttribute("width")]
		public double Width { get; set; }

		[XmlAttribute("text")]
		public string Text { get; set; }

		[XmlAttribute("hOffset")]
		public double HOffset { get; set; }

		[XmlAttribute("pitch")]
		public double Pitch { get; set; }

		[XmlAttribute("roll")]
		public double Roll { get; set; }

		[XmlElement("validity")]
		public SignalValidity[] Validity
		{
			get { return validity; }
			set { validity = value ?? new SignalValidity[0]; }
		}

		[XmlElement("dependency")]
		public SignalDependency[] Dependencies
		{
			get { return dependencies; }
			set { dependencies = value ?? new SignalDependency[0]; }
		}
	}

	public class SignalValidity
	{
		[XmlAttribute("fromLane")]
		public int FromLane { get; set; }	

		[XmlAttribute("toLane")]
		public int ToLane { get; set; }
	}

	public class SignalDependency {
		[XmlAttribute("id")]
		public string ID { get; set; }	

		[XmlAttribute("type")]
		public string Type { get; set; }
	}

	public class SignalReference
	{
		[XmlAttribute("s")]
		public double S { get; set; }

		[XmlAttribute("t")]
		public double T { get; set; }

		[XmlAttribute("id")]
		public string ReferencedID { get; set; }

		[XmlAttribute("orientation")]
		public RoadObjectOrientation Orientation { get; set; }
	}
}