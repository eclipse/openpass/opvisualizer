﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	public enum ContactPoint
	{
		[XmlEnum("start")]
		START = 0,
		[XmlEnum("end")]
		END
	}

	public enum Direction
	{
		[XmlEnum("same")]
		SAME = 0,
		[XmlEnum("opposite")]
		OPPOSITE
	}

	public enum ElementType
	{
		[XmlEnum("road")]
		ROAD = 0,
		[XmlEnum("junction")]
		JUNCTION
	}

	public enum Side
	{
		[XmlEnum("left")]
		LEFT = 0,
		[XmlEnum("right")]
		RIGHT
	}

	public interface ILinkable
	{
		string GetID();
		void SetID(string id);
	}

	public class Connection
	{
		private Road connectingRoad;
		private ContactPoint contactPoint;
		private string id;
		private Road incomingRoad;
		private LaneLink[] laneLinker;

		[XmlAttribute("id")]
		public string ID
		{
			get { return id; }
			set { id = value; }
		}

		[XmlAttribute("connectingRoad")]
		public string ConnectingRoadID;

		[XmlAttribute("incomingRoad")]
		public string IncomingRoadID;

		[XmlIgnore] //we must set this later, manually...
		public Road ConnectingRoad
		{
			get { return connectingRoad; }
			set { connectingRoad = value; }
		}
		
		[XmlIgnore] //we must set this later, manually...
		public Road IncomingRoad
		{
			get { return incomingRoad; }
			set { incomingRoad = value; }
		}

		[XmlAttribute("contactPoint")]
		public ContactPoint ContactPoint
		{
			get { return contactPoint; }
			set { contactPoint = value; }
		}

		[XmlElement("laneLink")]
		public LaneLink[] LaneLinker
		{
			get { return laneLinker; }
			set { laneLinker = (value ?? new LaneLink[0]); }
		}

		public Connection()
		{
			LaneLinker = new LaneLink[0];
		}
	}

	//EDITED BY SADJ [START]
	public class LaneLink
	{
		private Lane from;
		private Lane to;

		private int fromID;
		private int toID;

		[XmlIgnore]
		public Lane From
		{
			get { return from; }
			set { from = value; }
		}

		[XmlIgnore]
		public Lane To
		{
			get { return to; }
			set { to = value; }
		}

		[XmlAttribute("from")]
		public int FromID
		{
			get { return fromID; }
			set { fromID = value; }
		}

		[XmlAttribute("to")]
		public int ToID
		{
			get { return toID; }
			set { toID = value; }
		}
	}

	public class Link
	{
		private RoadNeighbour[] neighbours;
		private LinkTo predecessor;
		private LinkTo successor;

		[XmlElement("predecessor")]
		public LinkTo Predecessor
		{
			get { return predecessor; }
			set { predecessor = value; }
		}

		[XmlElement("successor")]
		public LinkTo Successor
		{
			get { return successor; }
			set { successor = value; }
		}

		[XmlElement("neighbor")]
		public RoadNeighbour[] Neighbours
		{
			get { return neighbours; }
			set { neighbours = (value ?? new RoadNeighbour[0]); }
		}

		public Link()
		{
			predecessor = null;
			successor = null;
			neighbours = new RoadNeighbour[2];
		}

		public RoadNeighbour GetNeighbour(Side side)
		{
			foreach (var neighbour in neighbours)
			{
				if (neighbour.neighbourSide == side)
					return neighbour;
			}

			Debug.Log("No neighbour assigned for side \"" + side.ToString() + "\"");
			return null;
		}
	}

	[Serializable]
	public class LinkTo
	{
		private string elementID;
		private ContactPoint linkedContactPoint;

		private ILinkable linkedElement;

		private ElementType linkedElementType;

		[XmlAttribute("elementId")]
		public string ElementID
		{
			get { return elementID; }
			set { elementID = value; }
		}

		/// <summary>
		/// Does not deserialize, will later be filled by CreateLinks().
		/// </summary>
		[XmlIgnore()]
		public ILinkable LinkedElement
		{
			get { return linkedElement;}
			set { linkedElement = value; }
		}

		[XmlAttribute("contactPoint")]
		public ContactPoint LinkedContactPoint
		{
			get { return linkedContactPoint; }
			set { linkedContactPoint = value; }
		}

		[XmlAttribute("elementType")]
		public ElementType LinkedElementType
		{
			get { return linkedElementType; }
			set { linkedElementType = value; }
		}
	}

	public class RoadNeighbour
	{
		[XmlAttribute("elementId")]
		public string elementID;
		[XmlAttribute("side")]
		public Side neighbourSide;
		[XmlAttribute("direction")]
		public Direction neighbourDirection;
	}
}