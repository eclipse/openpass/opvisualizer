﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	public class PolynomicControl
	{
		private double a;
		private double b;
		private double c;
		private double d;
		private double sOffset;

		[XmlAttribute("a")]
		public double A
		{
			get { return a; }
			set { a = value; }
		}

		[XmlAttribute("b")]
		public double B
		{
			get { return b; }
			set { b = value; }
		}

		[XmlAttribute("c")]
		public double C
		{
			get { return c; }
			set { c = value; }
		}

		[XmlAttribute("d")]
		public double D
		{
			get { return d; }
			set { d = value; }
		}

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		//NOTE: some OpenDRIVE polynomic properties use the attribute name "sOffset" (e.g. lane width), 
		//some others use "s" (e.g. laneOffset). Here we streamline the two, ans use internally the same variable.  
		[XmlAttribute("s")]
		public double S
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		public PolynomicControl()
		{
			SOffset = 0;
			A = 0;
			B = 0;
			C = 0;
			D = 0;
		}

		public static readonly PolynomicControl zero = new PolynomicControl();

		public double CalculateValueD(double time)
		{
			return A + (B * time) + (C * Math.Pow(time, 2)) + (D * Math.Pow(time, 3));
		}

		public float CalculateValueF(float time)
		{
			return (float)A + ((float)B * time) + ((float)C * Mathf.Pow(time, 2)) + ((float)D * Mathf.Pow(time, 3));
		}
	}
}