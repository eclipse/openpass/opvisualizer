﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	[Serializable]
	public class JunctionPriority
	{
		[XmlAttribute("high")]
		public string High;

		[XmlAttribute("low")]
		public string Low;
	}

	[Serializable]
	public class Junction : ILinkable
	{

		private string id;

		[XmlAttribute("id")]

		public string ID
		{
			get { return id; }
			set { id = value; }
		} //public, because of XML serializer

		[SerializeField]
		private string name;

		[SerializeField]
		private Connection[] roadConnections;

		[XmlAttribute("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		[XmlElement("connection")]
		public Connection[] RoadConnections
		{
			get { return roadConnections; }
			set { roadConnections = (value ?? new Connection[0]); }
		}

		[XmlElement("priority")]
		public JunctionPriority Priority { get; set; }

		public string GetID()
		{
			return ID;
		}

		public void SetID(string id)
		{
			this.ID = id;
		}
	}
}