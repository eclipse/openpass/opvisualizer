﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	[Serializable]
	public enum LaneRestriction : byte //OpenDRIVE 1.7
	{
		[XmlEnum("none")]
		NONE = 0,
		[XmlEnum("simulator")]
		SIMULATOR,
		[XmlEnum("autonomousTraffic")]
		AUTONOMOUSTRAFFIC,
		[XmlEnum("pedestrian")]
		PEDESTRIAN,
		[XmlEnum("bicycle")]
		BICYCLE,
		[XmlEnum("bus")]
		BUS,
		[XmlEnum("delivery")]
		DELIVERY,
		[XmlEnum("emergency")]
		EMERGENCY,
		[XmlEnum("motorcycle")]
		MOTORCYCLE,
		[XmlEnum("passengerCar")]
		PASSENGERCAR,
		[XmlEnum("taxi")]
		TAXI,
		[XmlEnum("throughTraffic")]
		THROUGHTRAFFIC,
		[XmlEnum("truck")]
		TRUCK,
		[XmlEnum("trucks")]
		TRUCKS
}

	[Serializable]
	public enum LaneType : byte
	{
		[XmlEnum("none")]
		NONE = 0,
		[XmlEnum("driving")]
		DRIVING,
		[XmlEnum("stop")]
		STOP,
		[XmlEnum("shoulder")]
		SHOULDER,
		[XmlEnum("biking")]
		BIKING,
		[XmlEnum("sidewalk")]
		SIDEWALK,
		[XmlEnum("border")]
		BORDER,
		[XmlEnum("restricted")]
		RESTRICTED,
		[XmlEnum("parking")]
		PARKING,
		[XmlEnum("bidirectional")]
		BIDIRECTIONAL, //actually meaning: continuous two-way left turn lane
		[XmlEnum("median")]
		MEDIAN,
		[XmlEnum("special1")]
		SPECIAL1,
		[XmlEnum("special2")]
		SPECIAL2,
		[XmlEnum("special3")]
		SPECIAL3,
		[XmlEnum("roadWorks")]
		ROADWORKS,
		[XmlEnum("tram")]
		TRAM,
		[XmlEnum("rail")]
		RAIL,
		[XmlEnum("entry")]
		ENTRY,
		[XmlEnum("exit")]
		EXIT,
		[XmlEnum("offRamp")]
		OFFRAMP,
		[XmlEnum("onRamp")]
		ONRAMP,
	}

	[Serializable]
	public class Lane
	{
		[SerializeField]
		private LaneAccess[] access;

		[SerializeField]
		private LaneHeight[] heights;

		[SerializeField]
		private int id;

		[SerializeField]
		private bool isLevel = false;

		[SerializeField]
		private PolynomicControl[] laneBorder;

		[SerializeField]
		private LaneType laneType;

		[SerializeField]
		private PolynomicControl[] laneWidth;

		[SerializeField]
		private int predecessorLink;

		[SerializeField]
		private RoadMark[] roadMarks;

		[SerializeField]
		private RoadMaterial[] roadMaterials;

		[SerializeField]
		private int successorLink;

		[SerializeField]
		private LaneRule[] rules;

		private Visibility[] visibility = new Visibility[0];

		private LaneSpeed[] laneSpeeds = new LaneSpeed[0];

		[XmlAttribute("id")]
		public int ID
		{
			get { return id; }
			set { id = value; }
		}

		[XmlAttribute("type")]
		public LaneType LaneType
		{
			get { return laneType; }
			set { laneType = value; }
		}

		[XmlAttribute("level")]
		public bool IsLevel
		{
			get { return isLevel; }
			set { isLevel = value; }
		}

		[XmlIgnore]
		public int PredecessorLink
		{
			get { return predecessorLink; }
			set { predecessorLink = value; }
		}

		[XmlIgnore]
		public int SuccessorLink
		{
			get { return successorLink; }
			set { successorLink = value; }
		}

		[XmlElement("link")]
		public LinkDeserializationHelper DeserializeLinks
		{
			set
			{
				//SZR: removed null propagating ? operator (c# 6.0)
				if (value.Predecessor != null)
					PredecessorLink = value.Predecessor.id;
				else 
					PredecessorLink = int.MaxValue;

				if (value.Successor != null)
					SuccessorLink = value.Successor.id;
				else 
					SuccessorLink = int.MaxValue;
			}
			get
			{
				Debug.LogError("Lane.LinkDeserializationHelper has been read. This should never be done, as this is a deserialization helper property.");
				LinkDeserializationHelper tmp = new LinkDeserializationHelper();
				if (PredecessorLink != int.MaxValue)
				{
					tmp.Predecessor = new LinkDeserializationHelper.LinkDeserializationEntry();
					tmp.Predecessor.id = PredecessorLink;
				}
				if (SuccessorLink != int.MaxValue)
				{
					tmp.Successor = new LinkDeserializationHelper.LinkDeserializationEntry();
					tmp.Successor.id = SuccessorLink;
				}
				return tmp;
			}
		}

		[XmlElement("width")]
		//we right away sort the width values according to increasing sOffset as it is not guaranteed that the values are in ordered
		public PolynomicControl[] LaneWidth
		{
			get { return laneWidth; }
			set { 
				laneWidth = value ?? new PolynomicControl[0]; 
				Array.Sort(laneWidth, delegate(PolynomicControl pc1, PolynomicControl pc2) {
                	return pc1.SOffset.CompareTo(pc2.SOffset);
            	});
			}
		}

		[XmlElement("border")]
		public PolynomicControl[] LaneBorder
		{
			get { return laneBorder; }
			set { laneBorder = value ?? new PolynomicControl[0]; }
		}

		[XmlElement("roadMark")]
		public RoadMark[] RoadMarks
		{
			get { return roadMarks; }
			set { roadMarks = value ?? new RoadMark[0]; }
		}

		[XmlElement("material")]
		public RoadMaterial[] RoadMaterials
		{
			get { return roadMaterials; }
			set { roadMaterials = value ?? new RoadMaterial[0]; }
		}

		[XmlElement("visibility")]
		public Visibility[] LaneVisibility
		{
			get { return visibility; }
			set { visibility = value ?? new Visibility[0]; }
		}

		[XmlElement("speed")]
		public LaneSpeed[] LaneSpeeds
		{
			get { return laneSpeeds; }
			set { laneSpeeds = value ?? new LaneSpeed[0]; }
		}

		[XmlElement("access")]
		public LaneAccess[] Access
		{
			get { return access; }
			set { access = value ?? new LaneAccess[0]; }
		}

		[XmlElement("height")]
		public LaneHeight[] Heights
		{
			get { return heights; }
			set { heights = value ?? new LaneHeight[0]; }
		}

		[XmlElement("rule")]
		public LaneRule[] Rules
		{
			set { rules = value ?? new LaneRule[0]; }
			get { return rules; }
		}

		public Lane()
		{
			PredecessorLink = int.MaxValue;
			SuccessorLink = int.MaxValue;

			LaneWidth = new PolynomicControl[0];
			Access = new LaneAccess[0];
			Heights = new LaneHeight[0];
			LaneBorder = new PolynomicControl[0];
			RoadMarks = new RoadMark[0];
			RoadMaterials = new RoadMaterial[0];
			LaneVisibility = new Visibility[0];
		}

		/// <summary>
		/// Finds the width controller at the specified S offset
		/// This function assumes that the laneWidth list is in ascending S-Offset order 
		/// </summary>
		public PolynomicControl GetWidthControllerAtOffset(double sOffset)
		{
			for (int i = LaneWidth.Length - 1; i >= 0; --i)
			{
				if (LaneWidth[i].SOffset <= sOffset)
					return LaneWidth[i];
			}
			return new PolynomicControl();
		}

		/// <summary>
		/// This function returns the sampling rate (step size) that is required to generate the lane mesh in 
		/// reasonable quality. Right now it is a very simplified funtion, 
		/// that can be improved to specific step size calculation based on the amount/speed of the width change 
		/// </summary>
		public float GetWidthChangeStepSize(float startSOffset, float endSOffset)
		{
			if(LaneWidth.Length > 1) 
				return 1f;
			else
				return endSOffset - startSOffset;
		}

		/// <summary>
		/// Calculate the distance of the next road mark from a given S-offset.
		/// If no road mark change is found after the given S-offset in this lane, 
		/// return the distance to the end of the search area
		/// </summary>
		public float GetNextRoadMarkChange(float startSOffset, float endSOffset)
		{
			foreach(RoadMark roadMark in RoadMarks) {
				if((float)roadMark.SOffset > startSOffset)
					return (float)roadMark.SOffset - startSOffset;
			}
			return endSOffset - startSOffset;
		}

		/// <summary>
		/// Finds the active road mark at a given S-offset
		/// </summary>
		public RoadMark GetRoadMarkAtOffset(float sOffset)
		{
			for (int i = RoadMarks.Length - 1; i >= 0; --i)
			{
				if (RoadMarks[i].SOffset <= sOffset)
					return RoadMarks[i];
			}
			return null;
		}

		public class LinkDeserializationHelper
		{
			private LinkDeserializationEntry predecessor = null;
			private LinkDeserializationEntry successor = null;

			[XmlElement("predecessor")]
			public LinkDeserializationEntry Predecessor
			{
				set { predecessor = value; }
				get { return predecessor;}
			}

			[XmlElement("successor")]
			public LinkDeserializationEntry Successor
			{
				set { successor = value; }
				get { return successor;}
			}

			public class LinkDeserializationEntry
			{
				[XmlAttribute("id")] public int id;
			}
		}

	}

	[Serializable]
	public class LaneAccess
	{
		[SerializeField]
		private LaneRestriction restriction;

		[SerializeField]
		private double sOffset = 0;

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		[XmlAttribute("restriction")]
		public LaneRestriction Restriction
		{
			get { return restriction; }
			set { restriction = value; }
		}
	}

	[Serializable]
	public class LaneHeight
	{
		[SerializeField]
		private double inner;

		[SerializeField]
		private double outer;

		[SerializeField]
		private double sOffset = 0;

		[XmlAttribute("inner")]
		public double Inner
		{
			get { return inner; }
			set { inner = value; }
		}

		[XmlAttribute("outer")]
		public double Outer
		{
			get { return outer; }
			set { outer = value; }
		}

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}
	}

	/// <summary>
	/// This class is for additional rules applicable to lanes. Currently not really standardized, just write whatever you think is cool.
	/// There are some recommendations though: "no stopping at any time", "disabled parking", "car pool"
	/// </summary>
	[Serializable]
	public class LaneRule
	{
		[SerializeField]
		private string rule;

		[SerializeField]
		private double sOffset = 0;

		[XmlAttribute("value")]
		public string Rule
		{
			get { return rule; }
			set { rule = value; }
		}

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}
	}

	[Serializable]
	public class LaneSpeed
	{
		[SerializeField]
		private double max = 0;

		[SerializeField]
		private double sOffset = 0;

		[SerializeField]
		private SpeedUnit unit;

		[XmlAttribute("max")]
		public double Max
		{
			get { return max; }
			set { max = value; }
		}

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		[XmlAttribute("unit")]
		public SpeedUnit Unit
		{
			get { return unit; }
			set { unit = value; }
		}
	}

	[Serializable]
	public class RoadMaterial
	{
		[SerializeField]
		private double friction = 0;

		[SerializeField]
		private double roughness = 0;

		[SerializeField]
		private double sOffset = 0;

		[SerializeField]
		private string surface;

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		[XmlAttribute("surface")]
		public string Surface
		{
			get { return surface; }
			set { surface = value; }
		}

		[XmlAttribute("friction")]
		public double Friction
		{
			get { return friction; }
			set { friction = value; }
		}

		[XmlAttribute("roughness")]
		public double Roughness
		{
			get { return roughness; }
			set { roughness = value; }
		}
	}

	[Serializable]
	public class Visibility
	{
		[SerializeField]
		private double back = 0;

		[SerializeField]
		private double forward = 0;

		[SerializeField]
		private double left = 0;

		[SerializeField]
		private double right = 0;

		[SerializeField]
		private double sOffset = 0;

		[XmlAttribute("back")]
		public double Back
		{
			get { return back; }
			set { back = value; }
		}

		[XmlAttribute("forward")]
		public double Forward
		{
			get { return forward; }
			set { forward = value; }
		}

		[XmlAttribute("left")]
		public double Left
		{
			get { return left; }
			set { left = value; }
		}

		[XmlAttribute("right")]
		public double Right
		{
			get { return right; }
			set { right = value; }
		}

		[XmlAttribute("sOffset")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}
	}
}