﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Visualizer.World.OpenDrive
{
	public enum RoadMarkColor
	{
		[XmlEnum("standard")]
		STANDARD = 0, //(white)
		[XmlEnum("blue")]
		BLUE,
		[XmlEnum("green")]
		GREEN,
		[XmlEnum("red")]
		RED,
		[XmlEnum("white")]
		WHITE,
		[XmlEnum("yellow")]
		YELLOW,
	}

	public enum RoadMarkLaneChange
	{
		[XmlEnum("increase")]
		INCREASE = 0,
		[XmlEnum("decrease")]
		DECREASE,
		[XmlEnum("both")]
		BOTH,
		[XmlEnum("none")]
		NONE,
	}

	/// <summary>
	/// This enum is used for simplified road mark types, which are given as an attribute to the roadMark XML Element.
	/// </summary>
	public enum SimplifiedRoadMarkType
	{
		[XmlEnum("none")]
		NONE = 0,
		[XmlEnum("unknown")]
		UNKNOWN,
		[XmlEnum("solid")]
		SOLID,
		[XmlEnum("broken")]
		BROKEN,
		[XmlEnum("solid solid")]
		SOLIDSOLID, //(for double solid line)
		[XmlEnum("solid broken")]
		SOLIDBROKEN, //(from inside to outside, exception: center lane - from left to right)
		[XmlEnum("broken solid")]
		BROKENSOLID, //(from inside to outside, exception: center lane - from left to right)
		[XmlEnum("broken broken")]
		BROKENBROKEN, //(from inside to outside, exception: center lane - from left to right)
		[XmlEnum("botts dots")]
		BOTTSDOTS,
		[XmlEnum("grass")]
		GRASS, //(meaning a grass edge)
		[XmlEnum("curb")]
		CURB,
	}

	public enum RoadMarkWeight
	{
		[XmlEnum("standard")]
		STANDARD = 0,
		[XmlEnum("bold")]
		BOLD,
	}

	/// <summary>
	/// RoadMarkType lines can have three possible traffic rule effects:
	/// </summary>
	public enum RoadMarkTypeRule
	{
		[XmlEnum("no passing")]
		NOPASSING,
		[XmlEnum("caution")]
		CAUTION,
		[XmlEnum("none")]
		NONE
	}

	public class RoadMark
	{
		private RoadMarkType type = null;

		[XmlAttribute("sOffset")]
		public double SOffset { get; set; }

		[XmlAttribute("type")]
		public SimplifiedRoadMarkType SimplifiedType { get; set; }

		[XmlAttribute("weight")]
		public RoadMarkWeight Weight { get; set; }

		[XmlAttribute("color")]
		public RoadMarkColor Color { get; set; }
		
		/// <summary>
		/// This is in the standard, and should at some point become an enum, but as of OpenDRIVE 1.4 the valid enum entries have not been specified, so we keep it as string for now.
		/// </summary>
		[XmlAttribute("material")]
		public string Material { get; set; }

		[XmlAttribute("width")]
		public double Width { get; set; }

		[XmlAttribute("laneChange")]
		public RoadMarkLaneChange LaneChange { get; set; }

		[XmlAttribute("height")]
		public double Height { get; set; }

		[XmlElement("type")]
		public RoadMarkType Type
		{
			get { return type; }
			set { type = value; }
		}

		//---------------------------------------------------------------------------//

		/// <summary>
		/// Converts the Color attribute into a UnityColor class.
		/// </summary>
		/// <returns>Returns the Color as UnityEngine.Color</returns>
		public UnityEngine.Color GetUnityColor()
		{
			Color softener = new Color(0.8f, 0.8f, 0.8f, 1f);
			switch(Color)
			{
			case RoadMarkColor.BLUE: return UnityEngine.Color.blue * softener;
			case RoadMarkColor.GREEN: return UnityEngine.Color.green * softener;
			case RoadMarkColor.RED: return UnityEngine.Color.red * softener;
			case RoadMarkColor.YELLOW: return UnityEngine.Color.yellow * softener;

			case RoadMarkColor.STANDARD:
			case RoadMarkColor.WHITE:
			default:
				return UnityEngine.Color.white;
			}
		}
	}
	/// <summary>
	/// This class contains the full road mark type information, as given by an XML child element of the roadMark element.
	/// </summary>
	public class RoadMarkType
	{
		private RoadMarkLine[] lines = new RoadMarkLine[0];

		[XmlAttribute("name")]
		public string Name { set; get; }

		[XmlAttribute("width")]
		public double Width { set; get; }

		[XmlElement("line")]
		public RoadMarkLine[] Lines
		{
			get {  return lines;}
			set { lines = value ?? new RoadMarkLine[0]; }
		}
	}

	/// <summary>
	/// This describes the lines of a RoadMarkType class.
	/// </summary>
	public class RoadMarkLine
	{
		[XmlAttribute("length")]
		public double Length { set; get; }

		[XmlAttribute("space")]
		public double Space { set; get; }

		[XmlAttribute("tOffset")]
		public double TOffset { set; get; }

		[XmlAttribute("sOffset")]
		public double SOffset { set; get; }

		[XmlAttribute("rule")]
		public RoadMarkTypeRule Rule { set; get; }

		[XmlAttribute("width")]
		public double Width { set; get; }
	}
}