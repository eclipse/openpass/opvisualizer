﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	[Serializable]
	public class LaneSection
	{
		[SerializeField]
		private bool singleSided = false;

		[SerializeField]
		private double sOffset = 0;

		[SerializeField]
		private Lane centerLane = null;

		[SerializeField]
		private Lane[] leftLanes;

		[SerializeField]
		private Lane[] rightLanes;

		private List<Lane> allLanes = null;

		[XmlAttribute("s")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		[XmlAttribute("singleSide")]
		public bool SingleSided
		{
			get { return singleSided; }
			set { singleSided = value; }
		}

		/// <summary>
		/// The lane noted between center-elements in OpenDRIVE. Note that this need not be the real center lane (See OpenDRIVE 1.4, point 5.3.7.2.1)
		/// </summary>
		[XmlIgnore]
		public Lane CenterLane
		{
			get { return centerLane; }
			set { centerLane = value; }
		}

		[XmlArray("center")]
		[XmlArrayItem("lane")]
		public Lane[] DeserializeCenterLane
		{
			get { return CenterLane == null ? null : new Lane[] {CenterLane};}
			set { CenterLane = value != null && value.Length == 1 ? value[0] : null; }
		}

		/// <summary>
		/// The lanes noted between left-elements in OpenDRIVE. Note that this need not be the real left lanes (See OpenDRIVE 1.4, point 5.3.7.2.1)
		/// </summary>
		[XmlArray("left")]
		[XmlArrayItem("lane")]
		public Lane[] LeftLanes
		{
			get { return leftLanes; }
			set { leftLanes = value ?? new Lane[0]; }
		}

		/// <summary>
		/// The lanes noted between right-elements in OpenDRIVE. Note that this need not be the real right lanes (See OpenDRIVE 1.4, point 5.3.7.2.1)
		/// </summary>
		[XmlArray("right")]
		[XmlArrayItem("lane")]
		public Lane[] RightLanes
		{
			get { return rightLanes; }
			set { rightLanes = value ?? new Lane[0]; }
		}

		/// <summary>
		/// All lanes on this LaneSection, sorted in ascending order.
		/// Lazy initialization.
		/// </summary>
		[XmlIgnore]
		public List<Lane> AllLanes
		{
			get
			{
				if (allLanes == null && (leftLanes.Length+rightLanes.Length+(CenterLane != null ? 1 : 0) > 0))
				{
					allLanes = new List<Lane>(leftLanes.Length+rightLanes.Length+1);
					allLanes.AddRange(RightLanes);
					if(CenterLane != null)
						allLanes.Add(CenterLane);
					allLanes.AddRange(LeftLanes);
					allLanes.Sort((x, y) => x.ID < y.ID ? -1 : (x.ID == y.ID ? 0 : 1));
				}
				return allLanes;
			}
		}

		public LaneSection()
		{
			leftLanes = new Lane[0];
			rightLanes = new Lane[0];
		}

		public Lane GetLaneWithID(int id)
		{
			return AllLanes.Find((x) => x.ID == id);

			//again: legacy code, again wrong as of OpenDRIVE 1.4, point 5.3.7.2.1
//			if (id == 0)
//			{
//				return centerLane;
//			}
//			if (id < 0)
//			{
//				foreach (Lane lane in rightLanes)
//				{ //SR
//					if (lane.ID == id)
//					{
//						return lane;
//					}
//				}
//			}
//			else
//			{
//				foreach (Lane lane in leftLanes)
//				{ //SR
//					if (lane.ID == id)
//					{
//						return lane;
//					}
//				}
//
//			}
//			return null;
		}
	}
}