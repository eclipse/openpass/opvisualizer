﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	public class Spiral : AbstractGeometry
	{
		//the curvature at and above which minStepSize should be used
		private float minStepCurvature = 0.008f;

		//the curvature at and below minStepSize should be used
		private float maxStepCurvature = 0.003f;

		//the minimum step size at and above minStepCurvature
		private float minStepSize = 0.5f;

		//the maximum step size at and below maxStepCurvature
		private float maxStepSize = 10f;

        private double SMALL_NUMBER = 1E-10;

		[XmlAttribute("curvEnd")]
		public double CurveEnd { get; set; }
		[XmlAttribute("curvStart")]
		public double CurveStart { get; set; }

		public override void CalculateForwardVector(double geometryOffset, out double x, out double y, out double z)
		{
			// SZR: as currently the implementation below does not correctly work, we fall back to the generic 
			// forward vector calculation of AbstractGeometry
			// TODO: fix the spiral specific forward vector calculation 
			base.CalculateForwardVector(geometryOffset, out x, out y, out z);
			return;

			//Depending on the moving direction (isReveresed), calculate the length of the curve from its beginning to the current point and normalize
			//it by multiplying with the "a" normalization term

			// bool isReveresed = !((Math.Abs(CurveEnd) > 1.00e-15) && (Math.Abs(CurveStart) <= 1.00e-15));
			// double curvature = isReveresed ? CurveStart : CurveEnd;

			// //Calculate the normalization term : a = 1.0/sqrt(2*End_Radius*Total_Curve_Length) 
			// double mA = 1.0f / Math.Sqrt(2 * 1.0f / Math.Abs(curvature) * Length);

			// //Calculate the heading at the found position
			// double l = geometryOffset * mA;
			// double tangentAngle = (l * l);
			// if (curvature < 0)
			// 	tangentAngle = -tangentAngle;
			// float sampleHeading = (float)(HeadingD + tangentAngle);

			// x=Math.Cos(sampleHeading);
			// y = 0;
			// z=Math.Sin(sampleHeading);
		}

        // This calculation was taken over from esmini (https://github.com/esmini/esmini)
		public override void CalculatePoint(double time, double height, out double xOut, out double yOut, out double zOut)
		{
			double cDot;
            double s0 = 0;
            double x0 = 0;
            double y0 = 0;
            double h0 = 0;
			double xTmp = 0;
			double yTmp = 0;
			double tTmp = 0;
            double x1, x2, y1, y2;

            xOut = 0;
            yOut = height;
            zOut = 0;

            //calculate the first derivative of the curvature
		    cDot = (CurveEnd - CurveStart) / Length;

	        if (Math.Abs(cDot) < SMALL_NUMBER)
	        {
                Debug.LogError("Invalid Spiral attributes");
                return;
            }

            if (Math.Abs(CurveStart) > SMALL_NUMBER)
		    {
                // not starting from zero curvature (straight line)
                // How long do we need to follow the spiral to reach start curve value?
                s0 = CurveStart / cDot;

                // Find out x, y, heading of start position
                odrSpiral(s0, cDot, ref x0, ref y0, ref h0);
            }

            odrSpiral(s0 + time, cDot, ref xTmp, ref yTmp, ref tTmp);

            // transform spline segment to origo and start angle = 0
            x1 = xTmp - x0;
            y1 = yTmp - y0;
            x2 = x1 * Math.Cos(-h0) - y1 * Math.Sin(-h0);
            y2 = x1 * Math.Sin(-h0) + y1 * Math.Cos(-h0);

            // Then transform according to segment start position and heading
            xOut = XPos + x2 * Math.Cos(HeadingD) - y2 * Math.Sin(HeadingD);
            zOut = YPos + x2 * Math.Sin(HeadingD) + y2 * Math.Cos(HeadingD);
        }

		public override double GetApproximatelyStraightLength(double startLocalOffset, double maxLengthDeviation = 0.01, double minTangentDotProduct = 0.95)
		{
			//This is an approximate solution, based on the code of the Arc (which itself is already approximate).
			//we always calculate the maximum length at the startLocalOffset.
			//If the magnitude of the curvature decreases along the spiral, we are happy with that result (as it's always better than the limiting case).
			//In any other case, we calculate the maximum length at the result of the previous step, and give that back.
			double t = (startLocalOffset / LengthD);
			double curvatureAtPoint = (CurveEnd*(t) - CurveStart*(1.0-t));
			double firstResult = CalculateMaxArcLengthNearlyEqualToTangentLength(curvatureAtPoint, maxLengthDeviation);
			firstResult = Math.Min(firstResult, LengthD - startLocalOffset);
			firstResult = Math.Min(firstResult, CalculateMaxArcLengthWhereTangentDotProductLargerThan(curvatureAtPoint, minTangentDotProduct));
			if (Math.Abs(CurveEnd) < Math.Abs(CurveStart))
				return firstResult;

			t = (startLocalOffset + firstResult) / LengthD;
			curvatureAtPoint = (CurveEnd*t - CurveStart*(1-t));
			firstResult = CalculateMaxArcLengthNearlyEqualToTangentLength(curvatureAtPoint, maxLengthDeviation);
			firstResult = Math.Min(firstResult, LengthD - startLocalOffset);
			firstResult = Math.Min(firstResult, CalculateMaxArcLengthWhereTangentDotProductLargerThan(curvatureAtPoint, minTangentDotProduct));
			return firstResult;

		}

		/// <summary>
		/// Calculates the optimal step size for the given geometry (the length of a snippet), to keep the number of snippets at the minimum
		/// At large curvatures the step size is smaller (sampling rate higher), at lower curvatures the step size is larger (sampling rate lower)
		/// </summary>
		/// <returns>The size of the step </returns>
		public override float GetRenderStepSize()
		{
			float currentCurv = (float)Math.Abs(CurveEnd - CurveStart);

			float curvRange = minStepCurvature - maxStepCurvature;
			float stepRange = maxStepSize - minStepSize;
			float curvFactor = (currentCurv - maxStepCurvature) / curvRange;
			float stepSize = maxStepSize - (stepRange * curvFactor);
			stepSize = Mathf.Clamp(stepSize, minStepSize, maxStepSize);
			return stepSize;
		}

        		/**
		* compute the actual "standard" spiral, starting with curvature 0
		* @param s      run-length along spiral
		* @param cDot   first derivative of curvature [1/m2]
		* @param x      resulting x-coordinate in spirals local co-ordinate system [m]
		* @param y      resulting y-coordinate in spirals local co-ordinate system [m]
		* @param t      tangent direction at s [rad]
		*/

		private static void odrSpiral(double s, double cDot, ref double x, ref double y, ref double t)
		{
			double a;

			a = 1.0 / Math.Sqrt(Math.Abs(cDot));
			a *= Math.Sqrt(Math.PI);

            Fresnel.Calc(s / a, ref y, ref x);

			x *= a;
			y *= a;

			if (cDot < 0.0)
				y *= -1.0;

			t = s * s * cDot * 0.5;
		}
	}
}