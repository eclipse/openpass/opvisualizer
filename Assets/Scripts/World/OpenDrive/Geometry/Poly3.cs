﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	public class Poly3 : AbstractGeometry
	{
		private double a;
		private double b;
		private double c;
		private double d;

		[XmlAttribute("a")]
		public double A
		{
			get { return a; }
			set { a = value; }
		}

		[XmlAttribute("b")]
		public double B
		{
			get { return b; }
			set { b = value; }
		}

		[XmlAttribute("c")]
		public double C
		{
			get { return c; }
			set { c = value; }
		}

		[XmlAttribute("d")]
		public double D
		{
			get { return d; }
			set { d = value; }
		}

		public Poly3()
		{
		}

		public override void CalculateForwardVector(double geometryOffset, out double x, out double y, out double z)
		{
			//calculate the derivative of the polynome to get to the tangent vector!
			double u = geometryOffset; // GetParameterFromSOffset(geometryOffset);
			double v = B + (2f * C * u) + (3f * D * System.Math.Pow(u, 2f));
			double sampleHeading = HeadingD + Math.Atan(v);
			x = Math.Cos(sampleHeading);
			y = 0;
			z = Math.Sin(sampleHeading);
		}

		public override void CalculatePoint(double geometryOffset, double height, out double x, out double y, out double z)
		{
			double uPoly3 = geometryOffset; //GetParameterFromSOffset(geometryOffset);
			double vPoly3 = A + (B * uPoly3) + (C * System.Math.Pow(uPoly3, 2.0)) + (D * System.Math.Pow(uPoly3, 3.0));
			double xPoly3 = XPos + (uPoly3 * Math.Cos(HeadingD) - vPoly3 * Math.Sin(HeadingD));
			double yPoly3 = YPos + (uPoly3 * Math.Sin(HeadingD) + vPoly3 * Math.Cos(HeadingD));

			x = xPoly3;
			y = height;
			z = yPoly3;
		}

		public override double GetApproximatelyStraightLength(double startLocalOffset, double maxLengthDeviation = 0.01, double minTangentDotProduct = 0.95)
		{
			throw new NotImplementedException("GetApproximatelyStraightLength is not yet implemented for Poly3. See comments in code.");

			//UPDATE 03.01.2020 SZR: The current Poly3 implementation seems to work correctly, also without the 
			//below described complex calculation. I left the thoughts below here for reference in case we find out 
			//that the implementation is still not right.

			//----------------------------->

			//as CalculatePoint does not work currently, this has not been implemented yet.
			//Just some hints on how to do it:
			//Step 1: Calculate the locations where curvature has an extremum. In u/v coordinates those are given by
			//u = -sqrt(-(4 * sqrt((9 * b ^ 2 + 5) * d ^ 2 - 6 * b * c ^ 2 * d + c ^ 4)) / (15 * d ^ 2) - (24 * b * d - 8 * c ^ 2) / (45 * d ^ 2)) / 2 - c / (3 * d)
			//u = sqrt(-(4 * sqrt((9 * b ^ 2 + 5) * d ^ 2 - 6 * b * c ^ 2 * d + c ^ 4)) / (15 * d ^ 2) - (24 * b * d - 8 * c ^ 2) / (45 * d ^ 2)) / 2 - c / (3 * d)
			//u = -sqrt((4 * sqrt((9 * b ^ 2 + 5) * d ^ 2 - 6 * b * c ^ 2 * d + c ^ 4)) / (15 * d ^ 2) - (24 * b * d - 8 * c ^ 2) / (45 * d ^ 2)) / 2 - c / (3 * d)
			//u = sqrt((4 * sqrt((9 * b ^ 2 + 5) * d ^ 2 - 6 * b * c ^ 2 * d + c ^ 4)) / (15 * d ^ 2) - (24 * b * d - 8 * c ^ 2) / (45 * d ^ 2)) / 2 - c / (3 * d)

			//one can safely toss away all extrema outside the interesting range (0, u(length)).
			//Also, those extrema are constant, so one should cache them.

			//then calculate the curvature at those extrema. In u/v coordinates this is
			//c = (6 * d * u + 2 * c) / ((3 *d * u ^ 2 + 2 * c * u + b) ^ 2 + 1) ^ (3 / 2)

			//then calculate the curvature at the starting point (for instance convert startLocalOffset to u/v coordinates and use the above formula)
			//use this curvature to determine the valid length (in s/t coordinates) via CalculateMaxArcLengthNearlyEqualToTangentLength.
			
			//calculate the curvature at startLocalOffset + the value just obtained (again, make sure to convert to u/v coordinates if using the above formula)
			//use this curvature to determine the valid length there, and keep the smaller one of the two valid lengths.

			//if between startLocalOffset and startLocalOffset+validLength there is an extremum, check if the magnitude of the curvature at the extremum is larger than at the ends
			//if yes (meaning the extremum is a local maximum, or local minimum with negative value), calculate the valid length again, at the extremum and use that one, 
			//otherwise use the previously determined value.

			//don't forget about minTangentDotProduct.

			//The function "double GetParameterFromSOffset(double sOffset)" would calculates the u-value corresponding to the given s offset.

			//The polynom is defined in a local coordinate system (the OpenDRIVE specification calls the coordinates u/v) located at the beginning of the geometry. 
			//All methods on AbstractGeometry rely on track coordinates (called s/t in OpenDRIVE), located on the reference line of the road,
			//the very reference line described by this geometry.
			//So, for the external interface of this geometry, we need a way to convert between internal representation (u/v at beginning), and s/t coordinates (directly on the line).
			//or, mathematically speaking, we need to change the parametrisation of the curve to arc-length parametrization.

			//For a general third order polynomial there exists no closed form for said parametrisation (at least as far as I know...), so we'll have to use numeric integration here.
			//My suggestion would be to generate a table of corresponding u/s values, and interpolate between them (linear, spline, whatever) to keep this reasonably fast.
			//Also, this allows to easily invert the conversion.

			//So, on to the gory details:
			//The length of an arc is given by the integration over the norm of the tangent vector, which in turn is given by the first derivative of the parametric representation.
			//The parametric representation for our curve is in u/v coordinates given by
			//uPoly3 = u
			//vPoly3 = a + b * u + c * u ^ 2 + d * u ^ 3

			//with the (trivial) first derivative 
			//uPoly3d = 1
			//vPoly3d = b + 2 * c * u + 3 * d * u ^2

			//so, the arc length is then given by
			//s = integrate(sqrt(1+(b+2*c*ut+3*d*ut^2)^2),ut,0,u);
		}

		/// <summary>
		/// Calculates the optimal step size for the given geometry (the length of a snippet), to keep the number of snippets at the minimum
		/// </summary>
		/// <returns>The size of the step </returns>
		public override float GetRenderStepSize()
		{
			return 1f;  //TODO: implement calculation of optimal sampling rate
		}
	}
}