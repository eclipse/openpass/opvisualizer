﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Visualizer.World.OpenDrive
{
	public enum CrossfallSide
	{
		[XmlEnum("left")]
		LEFT = 0,
		[XmlEnum("right")]
		RIGHT,
		[XmlEnum("both")]
		BOTH,
	}

	public class CrossfallData : PolynomicControl
	{
		private CrossfallSide side;

		[XmlAttribute("side")]
		public CrossfallSide Side
		{
			get { return side; }
			set { side = value; }
		}

		public CrossfallData()
		{
			Side = CrossfallSide.LEFT;
		}

		public CrossfallData(PolynomicControl control)
		{
			Side = CrossfallSide.LEFT;

			SOffset = control.SOffset;
			A = control.A;
			B = control.B;
			C = control.C;
			D = control.D;
		}

		public void SetSide(string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				Side = (CrossfallSide)Enum.Parse(typeof(CrossfallSide), value, true);
			}
		}
	}

	public class ElevationProfile
	{
		private PolynomicControl[] elevationChanges;

		[XmlElement("elevation")]
		public PolynomicControl[] ElevationChanges
		{
			get { return elevationChanges; }
			set { elevationChanges = value ?? new PolynomicControl[0]; }
		}

		public ElevationProfile()
		{
			elevationChanges = new PolynomicControl[0];
		}
	}

	public class LateralProfile
	{
		private PolynomicControl[] superElevations = new PolynomicControl[0];
		private CrossfallData[] crossfalls = new CrossfallData[0];
		private ShapeData[] shapes = new ShapeData[0];

		[XmlElement("superelevation")]
		public PolynomicControl[] SuperElevations
		{
			get { return superElevations; }
			set { superElevations = value ?? new PolynomicControl[0]; }
		}

		[XmlElement("crossfall")]
		public CrossfallData[] Crossfalls
		{
			get { return crossfalls; }
			set { crossfalls = value ?? new CrossfallData[0]; }
		}

		[XmlElement("shape")]
		public ShapeData[] Shapes
		{
			get { return shapes; }
			set { shapes = value ?? new ShapeData[0]; }
		}

		public PolynomicControl GetSuperElevationAtOffset(float sOffset)
		{
			for (int i = SuperElevations.Length - 1; i >= 0; --i)
			{
				if (SuperElevations[i].SOffset <= sOffset)
					return SuperElevations[i];
			}
			return new PolynomicControl();
		}
	}

	public class ShapeData : PolynomicControl
	{
		private double tOffset;

		[XmlAttribute("t")]
		public double TOffset
		{
			get { return tOffset; }
			set { tOffset = value; }
		}

		/// <summary>
		/// Needed for XML de-serialization.
		/// </summary>
		public ShapeData()
		{ }

		public ShapeData(PolynomicControl control)
		{
			TOffset = 0;

			SOffset = control.SOffset;
			A = control.A;
			B = control.B;
			C = control.C;
			D = control.D;
		}
	}
}