﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	public enum PRanges
	{
		[XmlEnum("normalized")]
		NORMALIZED,
		[XmlEnum("arcLength")]
		ARCLENGTH
	}

	[Serializable]
	public class ParamPoly3 : AbstractGeometry
	{
		[SerializeField]
		private double aU = 0.0;

		[SerializeField]
		private double aV = 0.0;

		[SerializeField]
		private double bU = 0.0;

		[SerializeField]
		private double bV = 0.0;

		[SerializeField]
		private double cU = 0.0;

		[SerializeField]
		private double cV = 0.0;

		[SerializeField]
		private double dU = 0.0;

		[SerializeField]
		private double dV = 0.0;

		private PRanges pRange = PRanges.NORMALIZED;

		[XmlAttribute("aU")]
		public double AU
		{
			get { return aU; }
			set { aU = value; }
		}

		[XmlAttribute("aV")]
		public double AV
		{
			get { return aV; }
			set { aV = value; }
		}

		[XmlAttribute("bU")]
		public double BU
		{
			get { return bU; }
			set { bU = value; }
		}

		[XmlAttribute("bV")]
		public double BV
		{
			get { return bV; }
			set { bV = value; }
		}

		[XmlAttribute("cU")]
		public double CU
		{
			get { return cU; }
			set { cU = value; }
		}

		[XmlAttribute("cV")]
		public double CV
		{
			get { return cV; }
			set { cV = value; }
		}

		[XmlAttribute("dU")]
		public double DU
		{
			get { return dU; }
			set { dU = value; }
		}

		[XmlAttribute("dV")]
		public double DV
		{
			get { return dV; }
			set { dV = value; }
		}

		[XmlAttribute("pRange")]
		public PRanges PRange
		{
			get { return pRange; }
			set { pRange = value; }
		}

		public ParamPoly3()
		{
		}

		public override void CalculateForwardVector(double geometryOffset, out double x, out double y, out double z)
		{
			double param = (pRange == PRanges.NORMALIZED ? geometryOffset / length : geometryOffset);
			double du = (BU) + (2f * CU * param) + (3f * DU * System.Math.Pow(param, 2.0));
			double dv = (BV) + (2f * CV * param) + (3f * DV * System.Math.Pow(param, 2.0));
			double sampleHeading = HeadingD + Math.Atan2(dv, du);
			x = Math.Cos(sampleHeading);
			y = 0;
			z = Math.Sin(sampleHeading);
		}

		public override void CalculatePoint(double geometryOffset, double height, out double x, out double y, out double z)
		{
			//Based on OpenDRIVE Manual 1.4, p. 46 & OpenSource Project 'Simulation of Urban MObility'
			double param = (pRange == PRanges.NORMALIZED ? geometryOffset / length : geometryOffset);

			double uParamPoly3 = AU + (BU * param) + (CU * System.Math.Pow(param, 2.0)) + (DU * System.Math.Pow(param, 3.0));
			double vParamPoly3 = AV + (BV * param) + (CV * System.Math.Pow(param, 2.0)) + (DV * System.Math.Pow(param, 3.0));
			double xParamPoly3 = (uParamPoly3 * Math.Cos(HeadingD) - vParamPoly3 * Math.Sin(HeadingD));
			double yParamPoly3 = (uParamPoly3 * Math.Sin(HeadingD) + vParamPoly3 * Math.Cos(HeadingD));

			x = xParamPoly3 + XPos;
			y = height;
			z = yParamPoly3 + YPos;
		}

		public override double GetApproximatelyStraightLength(double startLocalOffset, double maxLengthDeviation = 0.01, double minTangentDotProduct = 0.95)
		{
			throw new NotImplementedException("GetApproximatelyStraightLength is not yet implemented for ParamPoly3. See comments in code.");
			
			//UPDATE 03.01.2020 SZR: The current ParamPoly3 implementation seems to work correctly, also without the 
			//below described complex calculation. I left the thoughts below here for reference in case we find out 
			//that the implementation is still not right.

			//----------------------------->
			
			//Not implemented yet, as this requires GetParameterFromSOffset in a working condition.
			//The algorithm I'd suggest for this is exactly the same as for Poly3, but the formulas are a 
			//bit more complicated here.
			//the formula for the curvature is actually given by (in u/v)
			//k = (diff(u,p)*diff(v,p,2)-diff(v,p)*diff(u,p,2))/((diff(u,p)^2+diff(v,p)^2)^(3/2))
			
			//apart from that difference, one can just follow the same algorithm as in Poly3.

			//again: don't forget about minTangentProduct

			//The function "double GetParameterFromSOffset(double sOffset)" would calculate the input parameter (which can be between 0 and 1, 
			//or 0 and arcLength) that yields the supplied curve length.

			//To correctly handle ParamPoly3, we need to be able to convert between local u/v and track s/t coordinates,
			//as the external API of AbstractGeometry only uses s/t.

			//To get this working, we need to get the arc length parametrisation of this curve. Just as in the Poly3 case, this is generally not analytically solveable (to my knowledge)
			//My suggested solution is the same as for Poly3: Make a lookup table, and interpolate between tabulated values.
			//The maths is the same, but the formulas are a bit more comples, as now both, u and v are a polynom.
			//u = AU + (BU * p) + (CU * p^2.0) + (DU p^3.0);
			//v = AV + (BV * p) + (CV * p^2.0) + (DV p^3.0);

			//du/dp = BU+2*CU*p+3*DU*p^2
			//dv/dp = BV+2*CV*p+3*DV*p^2

			//so, the arc length s for a given parameter is then given by
			//integrate(sqrt((BU+2*CU*pp+3*DU*pp^2)^2+(BV+2*CV*pp+3*DV*pp^2)^2),pp,0,p)
		}

		/// <summary>
		/// Calculates the optimal step size for the given geometry (the length of a snippet), to keep the number of snippets at the minimum
		/// </summary>
		/// <returns>The size of the step </returns>
		public override float GetRenderStepSize()
		{
			return 1f;  //TODO: implement calculation of optimal sampling rate
		}
	}
}