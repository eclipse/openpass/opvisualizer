﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using Visualizer.World.Core;
using Visualizer.World.Core.Utilities;

namespace Visualizer.World.OpenDrive
{
	public class Arc : AbstractGeometry
	{
		private double curvature;

		private float minStepCurvature = 0.008f; //the curvature at and above which minStepSize should be used
		private float maxStepCurvature = 0.003f; //the curvature at and below minStepSize should be used
		private float minStepSize = 0.5f; //the minimum step size at and above minStepCurvature
		private float maxStepSize = 10f; //the maximum step size at and below maxStepCurvature

		[XmlAttribute("curvature")]
		public double Curvature
		{
			get { return curvature; }
			set { curvature = value; }
		}

		public override void CalculateForwardVector(double geometryOffset, out double x, out double y, out double z)
		{
			//Calculation based on the paper
			//"Open-source road generation and editing software"
			//Dimitri Kurteanu, Egor Kurteanu
			//Chalmers University of Technology, University of Goetheburg
			//June 2010

			double radius = 0.0;
			//if curvature is 0, radius is also 0, otherwise, radius is 1/curvature
			if (Math.Abs(Curvature) > 1.00e-15)
				radius = Math.Abs(1 / Curvature);

			double theta = geometryOffset / radius;
			double sampleHeading = Curvature <= 0 ? heading - theta : heading + theta;

			x=Math.Cos(sampleHeading);
			z=Math.Sin(sampleHeading);
			y = 0;
		}

		public override void CalculatePoint(double time, double height, out double x, out double y, out double z)
		{
			//Calculation based on the paper
			//"Open-source road generation and editing software"
			//Dimitri Kurteanu, Egor Kurteanu
			//Chalmers University of Technology, University of Goetheburg
			//June 2010

			double radius = 0.0;
			//if curvature is 0, radius is also 0, otherwise, radius is 1/curvature
			if (Math.Abs(Curvature) > 1.00e-15f)
			{
				radius = Math.Abs(1.0 / Curvature);
			}

			double theta = time / radius;

			double startAngle = 0.0;


			double circleCenterX;
			double circleCenterY;

			if (Curvature <= 0)
			{
				startAngle = HeadingD + (Math.PI / 2);
				circleCenterX = XPos + Math.Cos(startAngle - Math.PI) * radius;
				circleCenterY = YPos + Math.Sin(startAngle - Math.PI) * radius;
				x = circleCenterX + Math.Cos(startAngle - theta) * radius;
				z = circleCenterY + Math.Sin(startAngle - theta) * radius;
			}
			else
			{
				startAngle = HeadingD - (Mathf.PI / 2);
				circleCenterX = XPos + Math.Cos(startAngle - Math.PI) * radius;
				circleCenterY = YPos + Math.Sin(startAngle - Math.PI) * radius;
				x = circleCenterX + Math.Cos(startAngle + theta) * radius;
				z = circleCenterY + Math.Sin(startAngle + theta) * radius;
			}
			y = height;
		}

		public override double GetApproximatelyStraightLength(double startLocalOffset, double maxLengthDeviation = 0.01, double minTangentDotProduct = 0.95)
		{
			double retval = Math.Min(CalculateMaxArcLengthNearlyEqualToTangentLength(Curvature, maxLengthDeviation),
				LengthD - startLocalOffset);
			retval = Math.Min(CalculateMaxArcLengthWhereTangentDotProductLargerThan(Curvature, minTangentDotProduct),retval);
			return retval;
		}

		/// <summary>
		/// Calculates the optimal step size for the given geometry (the length of a snippet), to keep the number of snippets at the minimum
		/// At large curvatures the step size is smaller (sampling rate higher), at lower curvatures the step size is larger (sampling rate lower)
		/// </summary>
		/// <returns>The size of the step </returns>
		public override float GetRenderStepSize()
		{
			float currentCurv = (float)Math.Abs(curvature);

			float curvRange = minStepCurvature - maxStepCurvature;
			float stepRange = maxStepSize - minStepSize;
			float curvFactor = (currentCurv - maxStepCurvature) / curvRange;
			float stepSize = maxStepSize - (stepRange * curvFactor);
			stepSize = Mathf.Clamp(stepSize, minStepSize, maxStepSize);
			return stepSize;
		}
	}
}