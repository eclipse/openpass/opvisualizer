﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using Visualizer.World.Core;
using Visualizer.World.Core.Utilities;

namespace Visualizer.World.OpenDrive
{
	[Serializable]
	public abstract class AbstractGeometry 
	{
		[SerializeField]
		protected double heading;

		[SerializeField]
		protected double length;

		[SerializeField]
		protected double start;

		[SerializeField]
		protected double xPos;

		[SerializeField]
		protected double yPos;

		[XmlAttribute("hdg")]
		public double HeadingD
		{
			set { heading = value; }
			get { return heading; }
		}

		[XmlAttribute("length")]
		public double LengthD
		{
			set { length = value; }
			get { return length; }
		}

		[XmlAttribute("s")]
		public double StartD
		{
			get { return start;}
			set { start = value; }
		}

		[XmlAttribute("x")]
		public double XPosD
		{
			set { xPos = value; }
			get { return xPos;}
		}

		[XmlAttribute("y")]
		public double YPosD
		{
			set { yPos = value; }
			get { return yPos; }
		}

		public float Heading
		{
			get { return (float)heading; }
		}

		public float Length
		{
			get { return (float)length; }
		}

		public float Start
		{
			get { return (float)start; }
		}

		public float XPos
		{
			get { return (float)xPos; }
		}

		public float YPos
		{
			get { return (float)yPos; }
		}

		/// <summary>
		/// A generally applicable function that calculates the forward heading vector of any geometry at the given offset.
		/// </summary>
		/// <param name="geometryOffset">The position along the geometric section, where start < time < start+length</param>
		/// <returns>Forward heading vector (given in radiants. +X being zero.</returns>
		public virtual Vector3 CalculateForwardVector(double geometryOffset)
		{
			double x, y, z;
			CalculateForwardVector(geometryOffset, out x, out y, out z);

			return new Vector3((float)x, (float)y, (float)z);
		}

		public virtual void CalculateForwardVector(double geometryOffset, out double x, out double y, out double z)
		{
			//get two points left and right around the sOffset on the reference line (+/- 5cm)
			double delta = 0.05f;
			double reflineOffset1 = geometryOffset - delta;
			double reflineOffset2 = geometryOffset + delta;

			//if we get out of bounds we move our window back to the closest valid position
			if (reflineOffset1 < 0) {
				reflineOffset1 = 0;
				reflineOffset2 = 2*delta;
			}
			if (reflineOffset2 > Length) {
				reflineOffset1 = Length - 2*delta;
				reflineOffset2 = Length;
			}		

			//calculate the word coordinates at the two delta offsets
			Vector3 refLinePos1 = CalculatePoint ((float)reflineOffset1, 0f);
			Vector3 refLinePos2 = CalculatePoint ((float)reflineOffset2, 0f);

			//calculate the heading from the delta offsets
			Vector3 forwardDir = (refLinePos2 - refLinePos1).normalized;

			x = forwardDir.x;
			y = forwardDir.y;
			z = forwardDir.z;
		}

		/// <summary>
		/// Calculates a point along the geometric section, based on the 'time' parameter
		/// </summary>
		/// <param name="geometryOffset">The position along the geometric section, where start < time < start+length</param>
		/// <param name="height">Height position of the point in 3D space</param>
		/// <returns>Position of the point in 3D space</returns>
		public virtual Vector3 CalculatePoint(double geometryOffset, double height)
		{
			double x, y, z;
			CalculatePoint(geometryOffset, height, out x, out y, out z);
			return new Vector3((float)x,(float)y,(float)z);
		}

		public abstract void CalculatePoint(double geometryOffset, double height, out double x, out double y, out double z);

		/// <summary>
		/// Gives the length starting at startLocalOffset for which the given geometry will be nearly straight, meaning 
		/// that the length along the geometry's reference line will not deviate more from the length along the tangent than
		/// maxLengthDeviation (in absolute units, meaning: meters), and that the dot product between the forward-vector at the start and
		/// at the end will not be smaller than minTangentDotProduct.
		/// </summary>
		/// <param name="startLocalOffset">The local offset from which the query runs.</param>
		/// <param name="maxLengthDeviation">The maximum difference between the reference line length, and the tangent length.</param>
		/// <param name="minTangentDotProduct">The minimum value for the dot product between the tangent vectors at the start and at the end.</param>
		/// <returns>The length along the reference line that can be considered nearly straight</returns>
		public abstract double GetApproximatelyStraightLength(double startLocalOffset, double maxLengthDeviation = 0.01, double minTangentDotProduct = 0.95);

		public abstract float GetRenderStepSize();

		protected double CalculateMaxArcLengthNearlyEqualToTangentLength(double curvature, double maxLengthDeviation)
		{
			double radius;
			//radius is 1/curvature
			if (Math.Abs(curvature) > 1.00e-15f)
			{
				radius = Math.Abs(1.0 / curvature);
			}
			else
				return double.PositiveInfinity;
			//for the chosen criterion, namely that the length along the tangent and the length along the curve must differ by less than the absolute malLengthDeviation,
			//there exists no analytic solution, as the equation to solve is (l-d)/r = sin(l/r)
			//an approximate solution could be obtained by expanding the sine to a Taylor series, but
			//this Taylor series has to go at least till fifth order to get an upper estimate for the sine (the third order Taylor series is a lower estimate)
			//Needless to say, this again cannot be solved, as it's an equation of fifth order...
			//The solution for this dilemma: Use the Taylor series of third order as a starting value, then use Newton's method to get the "real" solution.
			//This Taylor series of course runs off to Vegas if the condition maxLengthDeviation <<<<< radius is not fulfilled.
			maxLengthDeviation = Math.Min(radius * 0.01, maxLengthDeviation);

			double estimate = Math.Pow(6 * maxLengthDeviation * radius * radius, 1.0 / 3.0);

			Func<double, double> equation = (x) => Math.Sin(x / radius) - (x - maxLengthDeviation) / radius;
			Func<double, double> deriv = (x) => (Math.Cos(x / radius) - 1.0) / radius;

			Pair<double, int> newtonData = new Pair<double, int>();

			double betterResult = Helpers.Newton(estimate, equation, deriv, maxLengthDeviation * 1e-10, 0, newtonData);
			if (newtonData.second > 99)
				return estimate;
			else
				return betterResult;
		}

		protected double CalculateMaxArcLengthWhereTangentDotProductLargerThan(double curvature,
			double minimumTangentDotProduct)
		{
			double radius;
			//radius is 1/curvature
			if (Math.Abs(curvature) > 1.00e-15f)
			{
				radius = Math.Abs(1.0 / curvature);
			}
			else
				return double.PositiveInfinity;
			return Math.Acos(minimumTangentDotProduct) * radius;
		}
	}
}