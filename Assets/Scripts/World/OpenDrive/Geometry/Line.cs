﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Xml;
using UnityEngine;

namespace Visualizer.World.OpenDrive
{
	public class Line : AbstractGeometry
	{
		public override void CalculateForwardVector(double geometryOffset, out double x, out double y, out double z)
		{
			x = Math.Cos(HeadingD);
			y = 0;
			z = Math.Sin(HeadingD);
		}

		public override void CalculatePoint(double geometryOffset, double height, out double x, out double y, out double z)
		{
			geometryOffset = Math.Min(geometryOffset, LengthD);
			geometryOffset = Math.Max(0,geometryOffset);
			x = XPosD + geometryOffset * Math.Cos(HeadingD);
			y = height;
			z = YPosD + geometryOffset * Math.Sin(HeadingD);
			/*
			geometryOffset = geometryOffset / LengthD;
			// Forward velocities of each component
			var velocityX = LengthD * Math.Cos((HeadingD));
			var velocityZ = LengthD * Math.Sin((HeadingD));

			var endPosX = XPosD + velocityX;
			var endPosZ = YPosD + velocityZ;
			x = geometryOffset * (endPosX - XPosD) + XPosD;
			y = height;
			z = geometryOffset * (endPosZ - YPosD) + YPosD;
			*/
		}

		public override double GetApproximatelyStraightLength(double startLocalOffset, double maxLengthDeviation = 0.01, double minTangentDotProduct = 0.95)
		{
			return length - startLocalOffset;
		}

		public override Vector3 CalculateForwardVector(double time)
		{
			//Calculation based on the paper
			//"Open-source road generation and editing software"
			//Dimitri Kurteanu, Egor Kurteanu
			//Chalmers University of Technology, University of Goetheburg
			//June 2010

			return new Vector3(Mathf.Cos(Heading), 0f, Mathf.Sin(Heading));
		}

		/// <summary>
		/// Calculates the optimal step size for the given geometry (the length of a snippet), to keep the number of snippets at the minimum
		/// </summary>
		/// <returns>The size of the step </returns>
		public override float GetRenderStepSize()
		{
			return Length;
		}
	}
}