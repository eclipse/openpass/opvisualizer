﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Assertions.Comparers;
using Visualizer.World.Core;
using Visualizer.World.Core.Utilities;

namespace Visualizer.World.OpenDrive
{
	public enum RoadTypes
	{
		[XmlEnum("unknown")]
		UNKNOWN = 0,
		[XmlEnum("rural")]
		RURAL,
		[XmlEnum("motorway")]
		MOTORWAY,
		[XmlEnum("town")]
		TOWN,
		[XmlEnum("lowSpeed")]
		LOWSPEED,
		[XmlEnum("pedestrian")]
		PEDESTRIAN,
		[XmlEnum("bicycle")]
		BICYCLE,
	}

	public enum SpeedUnit
	{
		[XmlEnum("m/s")]
		MPS = 0,
		[XmlEnum("km/h")]
		KPH,
		[XmlEnum("mph")]
		MPH,
	}

	public struct LaneSnapshotPoints
	{
		/// <summary>
		/// Point on the inside of the lane.
		/// </summary>
		public Vector3 insidePoint;

		/// <summary>
		/// Center point in the lane, with lateral offset included if provided.
		/// </summary>
		public Vector3 centerPoint;

		/// <summary>
		/// Point on the outside of the lane.
		/// </summary>
		public Vector3 outsidePoint;

		/// <summary>
		/// Forward vector of the lane at the center point position.
		/// </summary>
		public Vector3 forward;
	}

	[Serializable]
	public class Road : ILinkable
	{
		[SerializeField]
		private ElevationProfile elevationProfileData;

		[SerializeField]
		private string id;

		[SerializeField]
		private string junctionId;

		[SerializeField]
		private PolynomicControl[] laneOffsets;

		[SerializeField]
		private LaneSection[] laneSections;

		[SerializeField]
		private LateralProfile lateralProfileData;

		[SerializeField]
		private double length;

		[SerializeField]
		private string name;

		[SerializeField]
		private AbstractGeometry[] planViewSections;

		[SerializeField]
		private Link roadLink;

		[SerializeField]
		private RoadType[] type;

		[SerializeField]
		private RoadObjects roadObjects;

		[SerializeField]
		private RoadSignals roadSignals;

		[XmlAttribute("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		[XmlAttribute("length")]
		public double Length
		{
			get { return length; }
			set { length = value; }
		}

		[XmlAttribute("id")]
		public string ID
		{
			set { id = value; }
			get { return id; }
		}

		[XmlAttribute("junction")]
		public string JunctionID
		{
			get { return junctionId; }
			set { junctionId = value; }
		}

		[XmlElement("link")]
		public Link RoadLink
		{
			get { return roadLink; }
			set { roadLink = value; }
		}

		[XmlElement("type")]
		public RoadType[] Type
		{
			get { return type; }
			set { type = value ?? new RoadType[0]; }
		}

		[XmlArray("planView")]
		[XmlArrayItem("geometry")]
		public GeometryWrapper[] DeserializePlanViewSections
		{
			set
			{
				if (value != null)
				{
					planViewSections = new AbstractGeometry[value.Length];
					for (int i = 0; i < value.Length; ++i)
					{
						planViewSections[i] = value[i].inner;
						planViewSections[i].LengthD = value[i].length;
						planViewSections[i].HeadingD = value[i].hdg;
						planViewSections[i].StartD = value[i].s;
						planViewSections[i].XPosD = value[i].x;
						planViewSections[i].YPosD = value[i].y;
					}
				}
				else
				{
					planViewSections = new AbstractGeometry[0];
				}
			}
			get
			{
				Debug.LogWarning("Someone used the getter of DeserializePlanViewSections. This is for internal use by the serializer only.");
				GeometryWrapper[] retval = new GeometryWrapper[planViewSections.Length];
				for (int i = 0; i < planViewSections.Length; ++i)
				{
					retval[i] = new GeometryWrapper();
					retval[i].s = planViewSections[i].StartD;
					retval[i].x = planViewSections[i].XPosD;
					retval[i].y = planViewSections[i].YPosD;
					retval[i].hdg = planViewSections[i].HeadingD;
					retval[i].length = planViewSections[i].Length;
					retval[i].inner = planViewSections[i];
				}
				return retval;
			}
		}

		[XmlIgnore]
		public AbstractGeometry[] PlanViewSections
		{
			get { return planViewSections; }
			set { planViewSections = value; }
		}

		[XmlElement("elevationProfile")]
		public ElevationProfile ElevationProfileData
		{
			get { return elevationProfileData; }
			set { elevationProfileData = value; }
		}

		[XmlElement("lateralProfile")]
		public LateralProfile LateralProfileData
		{
			get { return lateralProfileData; }
			set { lateralProfileData = value; }
		}


		[XmlIgnore]
		public PolynomicControl[] LaneOffsets
		{
			get { return laneOffsets; }
			set { laneOffsets = value; }
		}

		[XmlIgnore]
		public LaneSection[] LaneSections
		{
			get { return laneSections; }
			set { laneSections = value; }
		}

		[XmlElement("lanes")]
		public LanesDeserializer DeserializeLanes
		{
			set
			{
				LaneOffsets = value.laneOffsets;
				LaneSections = value.laneSections;
			}
			get
			{
				LanesDeserializer retval = new LanesDeserializer
				{
					laneOffsets = LaneOffsets,
					laneSections = LaneSections
				};
				return retval;
			}
		}

		[XmlElement("objects")]
		public RoadObjects RoadObjects
		{
			get { return roadObjects; }
			set { roadObjects = value; }
		}

		[XmlElement("signals")]
		public RoadSignals RoadSignals
		{
			get { return roadSignals; }
			set { roadSignals = value; }
		}

		public Road()
		{
			name = "";
			length = 0;
			id = "0";
			junctionId = "-1";

			planViewSections = new AbstractGeometry[0];

			laneSections = new LaneSection[0];
			laneOffsets = new PolynomicControl[0];

			ElevationProfileData = new ElevationProfile();
			LateralProfileData = new LateralProfile();

			roadObjects = new RoadObjects();
			roadSignals = new RoadSignals();
		}

		public List<AbstractGeometry> GetGeometryForLanes(float fromSOffset, float toSOffset)
		{
			List<AbstractGeometry> validGeometry = new List<AbstractGeometry>();

			foreach (AbstractGeometry currentGeomerty in PlanViewSections)
			{
				var start = currentGeomerty.Start;
				var currentLength = currentGeomerty.Length;
				var end = start + currentLength;
				
				Assert.IsTrue(end >= start);
				if(start <= toSOffset && end >= fromSOffset)
				{
					validGeometry.Add(currentGeomerty);
				}
			}

			return validGeometry;
		}

		public string GetID()
		{
			return id;
		}

		public LaneSection GetLaneSectionAtOffset(double offset)
		{
			if (LaneSections.Length == 0)
				return null;
			if (offset < 0)
				return laneSections[0];
			for (var index = 0; index < laneSections.Length; ++index)
			{
				LaneSection section = laneSections[index];
				double sectionLength = GetLaneSectionLengthD(index);
				if (section.SOffset <= offset && section.SOffset + sectionLength > offset)
					return section;
			}

			return LaneSections[LaneSections.Length - 1];
		}

		public float GetLaneSectionLength(LaneSection section)
		{
			return (float) GetLaneSectionLengthD(section);
		}

		public double GetLaneSectionLengthD(LaneSection section)
		{
			for (var index = 0; index < LaneSections.Length; ++index)
			{
				if (LaneSections[index] == section)
				{
					return GetLaneSectionLengthD(index);
				}
			}
			return double.NaN;
		}

		public float GetLaneSectionLength(int sectionIndex)
		{
			return (float)GetLaneSectionLengthD(sectionIndex);
		}

		public double GetLaneSectionLengthD(int sectionIndex)
		{
			return (sectionIndex == laneSections.Length - 1
						? this.Length - laneSections[sectionIndex].SOffset
						: laneSections[sectionIndex + 1].SOffset - laneSections[sectionIndex].SOffset);
		}

		/// <summary>
		/// Find all Lane Sections between a range of offsets
		/// </summary>
		/// <param name="startOffset"></param>
		/// <param name="endOffset"></param>
		/// <returns></returns>
		public List<LaneSection> GetLaneSectionsForRange(float startOffset, float endOffset)
		{
			List<LaneSection> foundSections = new List<LaneSection>();

			for (var index = 0; index < LaneSections.Length; ++index)
			{
				LaneSection section = LaneSections[index];
				double sectionLength = GetLaneSectionLengthD(index);
				if (section.SOffset <= endOffset && sectionLength + section.SOffset >= startOffset)
					foundSections.Add(section);
			}

			return foundSections;
		}

		/// <summary>
		/// This function returns the sampling rate (step size) that is required to generate the lane mesh in 
		/// reasonable quality. Right now it is a very simplified funtion, 
		/// that can be improved to specific step size calculation based on the amount/speed of the laneOffset change 
		/// </summary>
		public float GetLaneOffsetChangeStepSize(float startSOffset, float endSOffset)
		{
			if(laneOffsets?.Length > 1) 
				return 1f;
			else
				return endSOffset - startSOffset;
		}

		public PolynomicControl GetCenterLaneOffsetControllerAtSOffset(double sOffset)
		{
			if (laneOffsets == null || laneOffsets.Length == 0)
			{
				return PolynomicControl.zero;
			}
			for (int i = laneOffsets.Length - 1; i >=0; --i)
			{
				if (laneOffsets[i].SOffset <= sOffset)
					return laneOffsets[i];
			}
			return laneOffsets[0];
		}

		public float GetCenterLaneOffsetAtSOffset(float sOffset)
		{
			sOffset = Mathf.Max(0, sOffset);
			sOffset = Mathf.Min((float)Length,sOffset);
			var controller = GetCenterLaneOffsetControllerAtSOffset((double)sOffset);
			return (float)controller.CalculateValueD(sOffset-controller.SOffset);
		}

		public PolynomicControl GetHeightControllerAtSOffset(double sOffset)
		{
			if(elevationProfileData == null || elevationProfileData.ElevationChanges == null || elevationProfileData.ElevationChanges.Length == 0)
				return PolynomicControl.zero;
			for (int i = elevationProfileData.ElevationChanges.Length-1; i >= 0; --i)
			{
				if (elevationProfileData.ElevationChanges[i].SOffset <= sOffset)
					return elevationProfileData.ElevationChanges[i];
			}
			return elevationProfileData.ElevationChanges[0];
		}

		public void GetTRangeOfRoad(double sOffset, out double lowerTLimit, out double upperTLimit)
		{
			var section = GetLaneSectionAtOffset(sOffset);
			lowerTLimit = 0;
			upperTLimit = 0;
			for (int i = 0; i < -section.AllLanes[0].ID; ++i)
			{
				var controller = section.AllLanes[i].GetWidthControllerAtOffset(sOffset);
				lowerTLimit -= controller.CalculateValueD(sOffset - controller.SOffset);
			}
			for (int i = -section.AllLanes[0].ID; i < section.AllLanes.Count; ++i)
			{
				var controller = section.AllLanes[i].GetWidthControllerAtOffset(sOffset);
				upperTLimit += controller.CalculateValueD(sOffset - controller.SOffset);
			}

			//correct by ref line offset:
			var offsetController = GetCenterLaneOffsetControllerAtSOffset(sOffset);
			double offset = offsetController.CalculateValueD(sOffset - offsetController.SOffset);
			upperTLimit += offset;
			lowerTLimit += offset;
		}

		/// <summary>
		/// Outputs the T-offset range of the given lane at the given S-offset.
		/// </summary>
		/// <param name="laneID"></param>
		/// <param name="sOffset"></param>
		/// <param name="lowerTLimit"></param>
		/// <param name="upperTLimit"></param>
		public void GetTRangeOfLane(int laneID, double sOffset, out double lowerTLimit, out double upperTLimit)
		{
			LaneSection targetSection = GetLaneSectionAtOffset(sOffset);
			Lane lane = targetSection.GetLaneWithID(laneID);

			var allLanes = targetSection.AllLanes;
			double outsideWidth = 0;

			System.Diagnostics.Debug.Assert(allLanes.IsSorted((x, y) => x.ID < y.ID ? -1 : (x.ID == y.ID ? 0 : 1)));
			//allLanes is sorted in ascending order.
			int curr, delta;
			var end = allLanes.FindIndex((x) => x.ID == 0);
			if (laneID < 0)
			{
				curr = 0;
				delta = 1;
			}
			else
			{
				curr = allLanes.Count - 1;
				delta = -1;
			}
			bool found = false;
			for (; curr != end; curr += delta) //can use != here, as lane 0 must have width=0.
			{
				if (allLanes[curr].ID == laneID)
					found = true;
				if (found)
				{
					var controller = allLanes[curr].GetWidthControllerAtOffset(sOffset);
					outsideWidth += allLanes[curr].GetWidthControllerAtOffset(sOffset).CalculateValueD(sOffset - controller.SOffset);
				}
			}

			//correct outsideWidth by center lane offset.
			{
				var controller = GetCenterLaneOffsetControllerAtSOffset(sOffset);
				outsideWidth += controller.CalculateValueD(sOffset - controller.SOffset);
			}

			double insideWidth = outsideWidth;
			if (laneID != 0)
			{
				var controller = lane.GetWidthControllerAtOffset(sOffset);
				double targetLaneWidth = controller.CalculateValueD(sOffset - controller.SOffset);
				insideWidth = outsideWidth - targetLaneWidth;
			}

			if (laneID < 0)
			{
				//T-offset is negative
				lowerTLimit = -outsideWidth;
				upperTLimit = -insideWidth;
			}
			else
			{
				lowerTLimit = insideWidth;
				upperTLimit = outsideWidth;
			}
			System.Diagnostics.Debug.Assert(lowerTLimit <= upperTLimit);
		}

		/// <summary>
		/// Returns Points on the inside and outside border of the given lane, and a point inside the lane with lateralLaneOffset from the lane center.
		/// Also returns the lane's forward vector.
		/// </summary>
		/// <param name="laneID"></param>
		/// <param name="roadOffset">The s-offset along the road</param>
		/// <param name="lateralLaneOffset">Offset of the point in the lane, wrt the lane center line.</param>
		/// <returns>Forward vector for the position in respect to a positive travel direction, and points on the lane-borders, and inside lane.</returns>
		public LaneSnapshotPoints GetPositionForLaneAtOffset(int laneID, float roadOffset, float lateralLaneOffset = 0.0f)
		{
			AbstractGeometry targetGeometry = GetGeometryAtOffset(roadOffset);

			double lowerT, upperT;
			GetTRangeOfLane(laneID, roadOffset, out lowerT, out upperT);

			//get height at offset
			float height = 0;
			{
				var controller = GetHeightControllerAtSOffset(roadOffset);
				height = controller.CalculateValueF(roadOffset - (float)controller.SOffset);
			}

			Vector3 currentHeading = targetGeometry.CalculateForwardVector(roadOffset - targetGeometry.Start);
			Vector3 referenceLinePosition = targetGeometry.CalculatePoint(roadOffset - targetGeometry.Start, height);
			Vector3 leftDir = Vector3.Cross(currentHeading, Vector3.up).normalized;

			float insideWidth;
			float outsideWidth;

			if (laneID < 0)
			{
				leftDir = -leftDir;
				insideWidth = -(float)upperT;
				outsideWidth = -(float)lowerT;
			}
			else
			{
				insideWidth = (float)lowerT;
				outsideWidth = (float)upperT;
			}
			float centerWidth = 0.5f * (insideWidth + outsideWidth);

			LaneSnapshotPoints currentSnapshot;
			currentSnapshot.forward = currentHeading;
			currentSnapshot.insidePoint = referenceLinePosition + (leftDir * (insideWidth));
			currentSnapshot.centerPoint = referenceLinePosition + (leftDir * (centerWidth + lateralLaneOffset));
			currentSnapshot.outsidePoint = referenceLinePosition + (leftDir * (outsideWidth));

			return currentSnapshot;
		}

		/// <summary>
		/// Calculate world coordinates on a road at a given s and t offset wrt the road's reference line.
		/// </summary>
		/// <param name="sOffset"></param>
		/// <param name="tOffset"></param>
		/// <returns>Forward vector for the position in respect to a positive travel direction</returns>
		public Vector3 GetWorldCoordinatesAt(float sOffset, float tOffset)
		{
			Vector3 wCoordinate;
			Vector3 nah;
			GetWorldLocationAndDirectionAt(sOffset,tOffset,out wCoordinate,out nah);

			return wCoordinate;
		}

		public DoubleVector GetWorldCoordinatesAtD(double sOffset, double tOffset)
		{
			double xloc, yloc, zloc, t1, t2, t3;
			GetWorldLocationAndDirectionAt(sOffset,tOffset,out xloc, out yloc, out zloc, out t1, out t2, out t3);
			return new DoubleVector(xloc,yloc,zloc);
		}

		public DoubleVector GetWorldForwardAtD(double sOffset, double tOffset)
		{
			DoubleVector loc, fwd;
			GetWorldLocationAndDirectionAt(sOffset,tOffset,out loc, out fwd);
			return fwd;
		}

		public void GetWorldLocationAndDirectionAt(double sOffset, double tOffset, out double xLoc, out double yLoc, out double zLoc, out double xFwd, out double yFwd, out double zFwd)
		{
			AbstractGeometry targetGeometry = GetGeometryAtOffset(sOffset);

			//convert sOffset in road coordinates to sOffset in geometry coordinates
			double localOffset = sOffset - targetGeometry.Start;

			double height = 0;
			//Currently we put the entire road network onto the XZ plane, with height 0. Later on we'll need to evaluate the elevation-profile, therefore I left the 
			//lines doing that here for reference purpose:
			// {
			// 	var controller = GetHeightControllerAtSOffset(sOffset);
			// 	height = controller.CalculateValueD(sOffset - controller.SOffset);
			// }

			//TODO: make this respect LateralProfile
			//calculate the word coordinates at sOffset
			double xRef, yRef, zRef;

			targetGeometry.CalculatePoint(localOffset, height,out xRef, out yRef, out zRef);
			targetGeometry.CalculateForwardVector(localOffset, out xFwd, out yFwd, out zFwd);

			//cross product between road forward and world up and result normalized.
			double leftMag = Math.Sqrt(zFwd * zFwd + xFwd * xFwd);
			double xLeft = -zFwd/leftMag;
			double zLeft = xFwd/leftMag;

			xLoc = xRef + xLeft * tOffset;
			yLoc = yRef; //yLeft is 0
			zLoc = zRef + zLeft * tOffset;
		}

		public void GetWorldLocationAndDirectionAt(double sOffset, double tOffset, out DoubleVector location,
			out DoubleVector forward)
		{
			double xLoc, yLoc, zLoc, xFwd, yFwd, zFwd;
			GetWorldLocationAndDirectionAt(sOffset, tOffset, out xLoc, out yLoc, out zLoc, out xFwd, out yFwd, out zFwd);
			location = new DoubleVector(xLoc, yLoc, zLoc);
			forward = new DoubleVector(xFwd, yFwd, zFwd);
		}

		public void GetWorldLocationAndDirectionAt(double sOffset, double tOffset, out Vector3 location, out Vector3 forward)
		{
			DoubleVector loc, fwd;
			GetWorldLocationAndDirectionAt(sOffset,tOffset,out loc, out fwd);
			location = (Vector3) loc;
			forward = (Vector3) fwd;
		}

		public Vector3 GetForwardDirectionAt(double sOffset)
		{
			DoubleVector loc, fwd;
			GetWorldLocationAndDirectionAt(sOffset, 0 ,out loc, out fwd);
			return (Vector3) fwd;
		}

		/// <summary>
		/// Calcuĺate a factor that discribes the scaling between a tangent vector length at t=0 (reference line) and at t=x at a given S offset (x is the specified input parameter)
		/// It can be used to calculate the size of a step along the reference line that is necessary to move along a certain length at t offset. 
		/// </summary>
		/// <param name="sOffset"></param>
		/// <param name="tOffset"></param>
		/// <returns>Multiplication factor</returns>
		public float GetTangentSizeFactorAt(double sOffset, double tOffset)
		{
			//get two points left and right around the sOffset on the reference line (+/- 1m)
			float delta = 1f;
			float reflineOffset1 = (float)sOffset - delta;
			float reflineOffset2 = (float)sOffset + delta;

			//if we get out of bounds we move our window back to the closest valid position
			if (reflineOffset1 < 0) {
				reflineOffset1 = 0;
				reflineOffset2 = 2*delta;
			}
			float totalRoadLength = (float)length;
			if (reflineOffset2 > totalRoadLength) {
				reflineOffset1 = totalRoadLength - 2*delta;
				reflineOffset2 = totalRoadLength;
			}	

			Vector3 p1 = GetWorldCoordinatesAt (reflineOffset1, (float)tOffset);
			Vector3 p2 = GetWorldCoordinatesAt (reflineOffset2, (float)tOffset);
			float distanceT = Vector3.Distance (p1, p2);
			float distanceT0 = 2 * delta;

			float sizeFactor = distanceT0 / distanceT;
			return sizeFactor;
		}

		public void SetID(string id)
		{
			this.id = id;
		}

		private AbstractGeometry GetGeometryAtOffset(double offset)
		{
			System.Diagnostics.Debug.Assert(planViewSections.Length > 0,"planViewSections was empty. This is forbidden in OpenDRIVE.");
			//check that geometry is sorted
			System.Diagnostics.Debug.Assert((new Func<bool>(() =>
			{
				double lastS = double.NegativeInfinity;
				for (int i = 0; i < planViewSections.Length; ++i)
				{
					if (planViewSections[i].StartD < lastS)
						return false;
					lastS = planViewSections[i].Start;
				}
				return true;
			}))(),"planViewSections is not sorted by ascending s offset. This is forbidden in OpenDRIVE.");

			for (int i = planViewSections.Length - 1; i >= 0; --i)
			{
				if (planViewSections[i].StartD <= offset)
					return planViewSections[i];
			}
			return null;
		}


		/// <summary>
		/// Wraps AbstractGeometry in another class to reflect OpenDRIVE XML structure, where each geometry element has the form <geometry><Arc></Arc></geometry>
		/// </summary>
		public class GeometryWrapper
		{
			[XmlElement("arc", typeof(Arc))]
			[XmlElement("line", typeof(Line))]
			[XmlElement("paramPoly3", typeof(ParamPoly3))]
			[XmlElement("poly3", typeof(Poly3))]
			[XmlElement("spiral", typeof(Spiral))]
			public AbstractGeometry inner;

			[XmlAttribute("s")] public double s;

			[XmlAttribute("x")] public double x;

			[XmlAttribute("y")] public double y;

			[XmlAttribute("hdg")] public double hdg;

			[XmlAttribute("length")] public double length;
		}

		public class LanesDeserializer
		{
			[XmlElement("laneSection")] public LaneSection[] laneSections;
			[XmlElement("laneOffset")] public PolynomicControl[] laneOffsets;
		}
	}

	public class RoadType
	{
		private double sOffset = 0;
		private RoadTypes type = RoadTypes.UNKNOWN;
		private SpeedRecord speed;

		[XmlAttribute("s")]
		public double SOffset
		{
			get { return sOffset; }
			set { sOffset = value; }
		}

		[XmlAttribute("type")]
		public RoadTypes Type
		{
			get { return type; }
			set { type = value; }
		}

		[XmlElement("speed")]
		public SpeedRecord Speed
		{
			get { return speed; }
			set { speed = value; }
		}
	}

	public class SpeedRecord
	{
		private float max = float.PositiveInfinity;
		private SpeedUnit unit = SpeedUnit.MPS;

		[XmlIgnore]
		public float Max
		{
			get { return max; }
			set { max = value; }
		}

		[XmlAttribute("max")]
		public string MaxString
		{
			get
			{
				if (float.IsNaN(max))
					return ("undefined");
				if (float.IsInfinity(max))
					return ("no limit");
				return max.ToString(CultureInfo.InvariantCulture);
			}
			set
			{
				if(value.Contains("undefined"))
					max = float.NaN;
				else if (value.Contains("no limit"))
					max = float.PositiveInfinity;
				else
				{
					IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
					max = float.Parse(value, NumberStyles.Float, provider);
				}
			}
		}

		[XmlAttribute("unit")]
		public SpeedUnit Unit
		{
			get { return unit; }
			set { unit = value; }
		}
	}
}