﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;

namespace Visualizer.World.OpenDrive
{
	public class Utilities
	{
		public static PolynomicControl GetControlAtOffset(float sOffset, PolynomicControl[] controls)
		{
			int i;
			return GetControlAtOffset(sOffset, controls, out i);
		}

		public static PolynomicControl GetControlAtOffset(float sOffset, PolynomicControl[] controls, out int index)
		{
			for (int i = controls.Length - 1; i >= 0; --i)
			{
				if (controls[i].SOffset <= sOffset)
				{
					index = i;
					return controls[i];
				}
			}
			index = -1;
			return new PolynomicControl();
		}
	}
}