﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Visualizer.AgentHandling;
using Visualizer.TrafficSignHandling;
using Visualizer.CameraHandling;
using Visualizer.Trace;
using Visualizer.UI;
using Visualizer.Environment;
using Visualizer.EntityRepository;
using Visualizer.World.Environment;
using Visualizer.World.OpenDrive;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.Core
{
	/// <summary>
	/// Delegate that can be used anywhere in the visualizer to get notified when the simulationLoader completes the loading process
	/// </summary>
	public delegate void LoadingFinishedEventHandler(bool success);

	public interface ISimulationLoader{
		event LoadingFinishedEventHandler LoadingFinished;
		SimulationMetaData GetCurrentSimulationMetaData();
		void DirectLoadRun (string traceFile, string mapFile, int runID);
		void ReloadTraceFile();
		void ReloadSceneryFile();
		bool IsSimulationLoaded ();
		void Clear ();
	}

	/// <summary>
	/// Central class that manages the file loading process and other system relevant tasks
	/// </summary>
    public sealed class SimulationLoader : MonoBehaviour, ISimulationLoader
    {
		public event LoadingFinishedEventHandler LoadingFinished;

		private IAnimationController animationController;
		private IDemoController demoController;
		private IWeatherController weatherController;
		private ITraceFileLoader traceFileLoader;
		private IRoadNetworkRenderer roadRenderer;
		private ISampleContainer sampleContainer;
		private IViewModeHandler viewModeHandler;
		private ITimeAxisHandler timeAxisHandler;
		private IAgentManager agentManager;
		private ITrafficSignManager trafficSignManager;
		private IFocusCameraHandler focusCameraHandler;
		private ICameraManager cameraManager;
		private IRoadExplorer roadExplorer;
		private ICmdLineInterpreter cmdLineInterpreter;
		private IUIModeHandler uiModeManager;
		private IEntityRepositoryLoader entityRepositoryLoader;
		private IEntityRepository entityRepository;
		private IWindowController windowController;
		private ITerrainManipulator terrainManipulator;
		private IMainUI ui;

		private SimulationMetaData currentSim;
		private Map loadedMap = null;
		private bool loadingFromDisk = true;
		private Stopwatch stopwatch = new Stopwatch(true);

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ITerrainManipulator tManipulator, IRoadNetworkRenderer rnRenderer, ITrafficSignManager tssManager, IWindowController winController, IMainUI mUi, IRoadExplorer rExplorer, ICameraManager cManager, IWeatherController wController, ITraceFileLoader tLoader, IDemoController dController, IAnimationController aController, ISampleContainer sContainer, IViewModeHandler vmHandler, ITimeAxisHandler taHandler, IAgentManager aManager, IFocusCameraHandler acHandler, ICmdLineInterpreter cInterpreter, IUIModeHandler uimManager, IEntityRepositoryLoader erLoader, IEntityRepository eRepository){
			cameraManager = cManager;
			weatherController = wController;
			traceFileLoader = tLoader;
			demoController = dController;
			animationController = aController;
			sampleContainer = sContainer;
			viewModeHandler = vmHandler;
			timeAxisHandler = taHandler;
			agentManager = aManager;
			focusCameraHandler = acHandler;
			roadExplorer = rExplorer;
			cmdLineInterpreter = cInterpreter;
			uiModeManager = uimManager;
			entityRepositoryLoader = erLoader;			
			entityRepository = eRepository;
			windowController = winController;
			trafficSignManager = tssManager;
			roadRenderer = rnRenderer;
			terrainManipulator = tManipulator;
			ui = mUi;
		}

		/// <summary>
		/// Initialize the SimulationLoader
		/// </summary>
		private void Start(){
			ui.LoadRunRequest += new LoadRunEventHandler(OnLoadRunRequest);
			ui.LoadTraceFileRequest += new LoadTraceFileEventHandler(OnLoadTraceFileRequest);
			ui.LoadSceneryFileRequest += new LoadSceneryFileEventHandler(OnLoadSceneryFileRequest);
			ui.ReadRunListRequest += new ReadRunListEventHandler(OnReadRunListRequest);
			uiModeManager.UIModeChanged += new UIModeChangedEventHandler(OnUIModeChanged);

			currentSim = new SimulationMetaData();
			loadingFromDisk = true;
			AutoLoad();
		}

		/// <summary>
		/// Loads files from that are given in the command line
		/// </summary>
		private void AutoLoad() {
			string autoLoadFile;
			int autoLoadRunID;
			if (cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.TraceFile, out autoLoadFile)) {
				if (cmdLineInterpreter.GetCmdLineArgAsInt (CmdLineArgs.RunID, out autoLoadRunID)) {
					StartCoroutine (LoadTraceFile_ (autoLoadFile, autoLoadRunID));
				}
			} else if(cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.SceneryFile, out autoLoadFile)){
				StartCoroutine (LoadScenery_ (autoLoadFile, false));
			}
		}

		/// <summary>
		/// Event from the UI
		/// </summary>
		private void OnReadRunListRequest() {
			if(currentSim.TraceFile != "")
				StartCoroutine (ReadRuns_ (currentSim.TraceFile));
		}

		/// <summary>
		/// Event from the UI
		/// </summary>
		private void OnLoadTraceFileRequest(string filePath) {		
			StartCoroutine (ReadRuns_ (filePath));
		}

		/// <summary>
		/// Event from the UI
		/// </summary>
		private void OnLoadSceneryFileRequest(string filePath) {		
			StartCoroutine (LoadScenery_ (filePath));
		}

		/// <summary>
		/// Event from the UI
		/// </summary>
		private void OnLoadRunRequest(string traceFilePath, int runID){
			StartCoroutine (LoadTraceFile_ (traceFilePath, runID));
		}

		/// <summary>
		/// Event from the UIModeManager
		/// </summary>
		private void OnUIModeChanged(UIMode oldMode, UIMode newMode) {
			if(oldMode == UIMode.SimulationPlayer || oldMode == UIMode.SceneryViewer)
				Clear();
		}

		/// <summary>
		/// Reads the runs from the specified trace file. The function executes as a coroutine in order to avoid blocking the UI.
		/// </summary>
		private IEnumerator ReadRuns_(string traceFilePath){
			windowController.ShowLoadingMessage("Reading Runs...");
			yield return new WaitForSeconds(0.01f); //make sure that the UI can update the loading panel

			List<Run> runs = null;
			try{
				runs = traceFileLoader.ReadRuns (traceFilePath);
			}catch(Exception ex){
				HandleError ("Reading runs failed", ex);
				yield break;
			}

			windowController.HideLoadingMessage();
			yield return new WaitForSeconds (0.01f);

			if(runs.Count > 1){
				ui.ShowRunSelectorWnd(traceFilePath, runs);
				ui.HideRunSelectorMenuItem(false);
			}else{
				StartCoroutine (LoadTraceFile_ (traceFilePath, runs[0].ID));
				ui.HideRunSelectorMenuItem(true);
			}
				
		}

		/// <summary>
		/// Loads the given run in the given trace file. The function must be executed as a coroutine in order to avoid blocking the UI.
		/// </summary>
		private IEnumerator LoadTraceFile_(string traceFile, int runID, string sceneryFile = ""){
			stopwatch.Start();	
			//if the same trace file has already been loaded, skip loading (note that the user can force a reload with ReloadTraceFile)
			if (IsNewFile(currentSim.TraceFile, traceFile, currentSim.TraceFileModDate) || currentSim.RunID != runID) {
				ClearTrace ();

				if(loadingFromDisk){
					windowController.ShowLoadingMessage("Loading Trace File...");
					yield return new WaitForSeconds (0.1f);
					try {

						traceFileLoader.LoadRunFromDisk (traceFile, runID, false);
					} catch (Exception ex) {
						HandleError ("Loading trace file failed", ex);
						yield break;
					}

					windowController.ShowLoadingMessage("Loading Extended Trace File...");
					yield return new WaitForSeconds (0.1f);
					try {
						string traceFileExt = GetExtendedTraceFile(traceFile);
						if(traceFileExt != "") {
							traceFileLoader.LoadRunFromDisk (traceFileExt, runID, true);
							currentSim.TraceFileExtension = traceFileExt;
						}
					} catch (Exception ex) {
						HandleError ("Loading trace file extension failed", ex);
						yield break;
					}

					windowController.ShowLoadingMessage("Loading Entity Repository...");
					yield return new WaitForSeconds (0.01f);
					try {
						entityRepositoryLoader.LoadFromDisk(runID, Path.GetDirectoryName(traceFile));
					} catch (Exception ex) {
						HandleError ("Loading entity repository failed", ex);
						yield break;
					}
				}
				else {
					windowController.ShowLoadingMessage("Loading Trace File...");
					yield return new WaitForSeconds (0.01f);
					try {
						traceFileLoader.LoadRunFromStream (traceFile, runID);
					} catch (Exception ex) {
						HandleError ("Loading trace file failed", ex);
						yield break;
					}
				}

				currentSim.TraceFile = traceFile;
				currentSim.RunID = runID;
				currentSim.TraceFileModDate = File.GetLastWriteTime (traceFile).ToString();
			}

			stopwatch.Stop("Trace file loaded in #ms.");

			//execute the next step in the file loading chain
			sceneryFile = loadingFromDisk ? GetSceneryFileNameFromTraceFile(traceFile) : sceneryFile;
			yield return LoadScenery_ (sceneryFile);
		}

		/// <summary>
		/// Loads the OpenDrive file located at the provided file path. The function executes as a coroutine in order to avoid blocking the UI.
		/// </summary>
		private IEnumerator LoadScenery_(string sceneryFile, bool reload = false){
			stopwatch.Start();
			//if the same map file has already been loaded, skip loading (note that the user can force a reload with keyboard shortcut)
			if (IsNewFile(currentSim.SceneryFile, sceneryFile, currentSim.SceneryFileModDate) || reload) {
				if (currentSim.SceneryFile != "") {
					ClearScenery ();
				}
				windowController.ShowLoadingMessage("Loading Environment...");
				yield return new WaitForSeconds (0.1f);
				
				try {
					var factory = new Map.Factory (new MeshFactory ());
					loadedMap = factory.Create (sceneryFile, loadingFromDisk) as Map;
					roadRenderer.BuildRoad(loadedMap);
					currentSim.SceneryFile = sceneryFile;
					currentSim.SceneryFileModDate = File.GetLastWriteTime (sceneryFile).ToString();
					terrainManipulator.RescaleTexture();
				} catch (Exception ex) {
					HandleError ("Loading scenery failed", ex);
					yield break;
				}
			}
			stopwatch.Stop("Scenery loaded in #ms.");
			FinalizeLoading (reload);
		}

		/// <summary>
		/// Reloads the currently loaded trace file (with adjusted parameters). The function executes as a coroutine in order to avoid blocking the UI.
		/// </summary>
		private IEnumerator ReloadTraceFile_(){
			int focusedAgentIDBeforeReload = -1;
			if (focusCameraHandler.GetFocusedAgent () != null)
				focusedAgentIDBeforeReload = focusCameraHandler.GetFocusedAgent ().ID;

			bool playingBeforeReload = animationController.IsPlaying();
			int timeStampBeforeReload = animationController.StopPlayback ();

			bool isDemoModeActiveBeforeReload = demoController.IsDemoRunning();
			if (isDemoModeActiveBeforeReload)
				demoController.ToggleDemoMode ();

			ViewMode viewModeBeforeReload = viewModeHandler.GetViewMode ();

			string traceFile = currentSim.TraceFile;
			int runID = currentSim.RunID;

			ClearTrace ();

			if(loadingFromDisk){
				windowController.ShowLoadingMessage("Reloading Trace File...");
				yield return new WaitForSeconds (0.4f);
				try {
					traceFileLoader.LoadRunFromDisk (traceFile, runID, false);
				} catch (Exception ex) {
					HandleError ("Reloading trace file failed", ex);
					yield break;
				}

				windowController.ShowLoadingMessage("Reloading Extended Trace File...");
				yield return new WaitForSeconds (0.01f);
				try {
					string traceFileExt = GetExtendedTraceFile(traceFile);
					if(traceFileExt != "")
						traceFileLoader.LoadRunFromDisk (traceFileExt, runID, true);
				} catch (Exception ex) {
					HandleError ("Reloading trace file extension failed", ex);
					yield break;
				}

				windowController.ShowLoadingMessage("Reloading Entity Repository...");
				yield return new WaitForSeconds (0.01f);
				try {
					entityRepositoryLoader.LoadFromDisk(runID, Path.GetDirectoryName(traceFile));
				} catch (Exception ex) {
					HandleError ("Reloading entity repository failed", ex);
					yield break;
				}
			}
			else {
				windowController.ShowLoadingMessage("Reloading Trace File...");
				yield return new WaitForSeconds (0.01f);
				try {
					traceFileLoader.LoadRunFromStream (traceFile, runID);
				} catch (Exception ex) {
					HandleError ("Reloading trace file failed", ex);
					yield break;
				}
			}

			currentSim.TraceFile = traceFile;
			currentSim.RunID = runID;
			currentSim.TraceFileModDate = File.GetLastWriteTime (traceFile).ToString();
			ui.Init(uiModeManager.GetMode(), currentSim);

			animationController.JumpToTimeStamp (timeStampBeforeReload);
			focusCameraHandler.FocusCamera (focusedAgentIDBeforeReload);
			viewModeHandler.SetViewMode (viewModeBeforeReload);
			if(playingBeforeReload)
				animationController.StartPlayback();

			if (isDemoModeActiveBeforeReload)
				demoController.ToggleDemoMode ();

			windowController.ShowLoadingMessage("Reloading finished.", 1f);
		}

		/// <summary>
		/// Direct loading takes the content of the files as input param instead of file names
		/// </summary>
		public void DirectLoadRun(string traceFile, string sceneryFile, int runID) {
			loadingFromDisk = false;
			StartCoroutine (LoadTraceFile_ (traceFile, runID, sceneryFile));
		}
		
		/// <summary>
		/// Reads scenery file name from the the trace file
		/// </summary>
		private string GetSceneryFileNameFromTraceFile(string traceFile) {
			string sceneryFile = traceFileLoader.ReadSceneryFileName (traceFile);
			if (!Helpers.IsAbsolutePath (sceneryFile)) {
				sceneryFile = Path.Combine(Path.GetDirectoryName(traceFile), sceneryFile);
			}
			return sceneryFile;
		}

		/// <summary>
		/// Gets the path of the extended simulation output, or returns empty if there is no extension
		/// </summary>
		private string GetExtendedTraceFile(string traceFile){
			string traceFileFolder = Path.GetDirectoryName(traceFile);
			string traceFileExtension = Path.Combine(traceFileFolder, "DriverOutput\\extendedDriverSimulationOutput.xml");
			if(File.Exists(traceFileExtension)){
				return traceFileExtension;
			} else {
				return "";
			}
		}

		/// <summary>
		/// Checks if the two given files differ. It also checks the file's time stamp to determine if the file has changed since it was loaded last time. 
		/// </summary>
		private bool IsNewFile(string oldFile, string newFile, string oldFileModDate){
			if (oldFile != newFile) {
				return true;
			} else if (loadingFromDisk) {
				return (oldFileModDate != File.GetLastWriteTime (newFile).ToString());
			}
			return true;
		}
		
		/// <summary>
		/// Public interface to force a reload the currently loaded trace file.
		/// </summary>
		public void ReloadTraceFile(){
			StartCoroutine (ReloadTraceFile_ ());
		}

		/// <summary>
		/// Public interface to force a reload of the currently loaded scenery file.
		/// </summary>
		public void ReloadSceneryFile(){
			StartCoroutine (LoadScenery_ (currentSim.SceneryFile, true));
		}

		/// <summary>
		/// Determines whether the system is ready for playback.
		/// </summary>
		public bool IsSimulationLoaded(){
			return (currentSim.TraceFile != "") && (currentSim.SceneryFile != "");
		}

		/// <summary>
		/// Public interface to get information about the currently loaded simulation
		/// </summary>
		public SimulationMetaData GetCurrentSimulationMetaData() {
			return currentSim;
		}

		/// <summary>
		/// Clears all the loaded data resetting the simulation loading state.
		/// </summary>
		public void Clear()
		{
			ClearTrace ();
			ClearScenery ();
		}

		/// <summary>
		/// Clears the loaded trace data.
		/// </summary>
		private void ClearTrace()
		{
			cameraManager.Clear ();
			focusCameraHandler.Clear ();
			animationController.Clear ();
			sampleContainer.Clear ();
			agentManager.Clear ();
			trafficSignManager.Clear();
			weatherController.Clear ();
			timeAxisHandler.Clear ();	
			entityRepository.Clear();
			ui.Clear();

			if (demoController.IsDemoRunning())
				demoController.ToggleDemoMode ();

			currentSim.TraceFile = "";
			currentSim.TraceFileExtension ="";
			currentSim.RunID = -2;

			Resources.UnloadUnusedAssets ();
			traceFileLoader.Init();
		}

		/// <summary>
		/// Clears all the loaded data resetting the simulation loading state.
		/// </summary>
		private void ClearScenery()
		{
			roadExplorer.Clear();
			GameObject.Destroy (roadRenderer.GetRootObject ());
			loadedMap = null;

			currentSim.SceneryFile = "";

			Resources.UnloadUnusedAssets ();
		}

		/// <summary>
		/// Performs operations that need to be executed after the loading chain has completed
		/// </summary>
		private void FinalizeLoading(bool reload){
			cameraManager.Init ();
			
			if(uiModeManager.GetMode() == UIMode.SimulationPlayer)
				animationController.JumpToTimeStamp(sampleContainer.GetFirstTimeStamp());
			
			ui.Init(uiModeManager.GetMode(), currentSim);

			if (!reload)
				ResetCamera();
			
			ApplyCmdLineParams ();

			//Wait a bit before closing the LoadingPanel
			windowController.ShowLoadingMessage("Loading Finished.", 1.5f);

			FireLoadingFinishedEvent (true);
		}

		/// <summary>
		/// Resets the camera position to default
		/// </summary>
		private void ResetCamera(){
			//if no agent is currently attached, attach to ego
			if (focusCameraHandler.GetFocusedAgent () == null)
				focusCameraHandler.FocusOnEgo ();

			//if attach to ego could not be done (e.g. there is no ego or not visible currently), we move the camera to the default position 
			if (focusCameraHandler.GetFocusedAgent () == null)
				cameraManager.SetDefaultPosition (loadedMap);

			viewModeHandler.Init ();
			if(uiModeManager.GetMode() == UIMode.SceneryViewer){
				cameraManager.SwitchViewMode(ViewMode.LookDown);
				viewModeHandler.SetViewMode(ViewMode.LookDown);
			} else {
				cameraManager.SwitchViewMode(ViewMode.Perspective);
				viewModeHandler.SetViewMode(ViewMode.Perspective);
			}
		}

		/// <summary>
		/// Applies the parameters that the user specified in the command line
		/// </summary>
		private void ApplyCmdLineParams(){
			if (uiModeManager.GetMode() == UIMode.SimulationPlayer){
				int timeStamp;
				if (cmdLineInterpreter.GetCmdLineArgAsInt (CmdLineArgs.TimeStamp, out timeStamp))
					animationController.JumpToTimeStamp (timeStamp);

				int agentID;
				if (cmdLineInterpreter.GetCmdLineArgAsInt (CmdLineArgs.AgentID, out agentID))
				if (agentManager.GetAgentIDs ().Contains (agentID))
					focusCameraHandler.FocusCamera (agentID);

				List<SimulationEvent> timeMarks;
				if (cmdLineInterpreter.GetTimeMarks (out timeMarks)) {
					foreach (SimulationEvent timeMark in timeMarks)
						sampleContainer.AddEvent (timeMark);
					timeAxisHandler.ReloadTimeMarks ();
				}
			}
		}

		/// <summary>
		/// Inform all subscribers that the loading has finished
		/// </summary>
		private void FireLoadingFinishedEvent(bool success){
			if (LoadingFinished != null)
				LoadingFinished (success);
		}

		/// <summary>
		/// Handle exceptions
		/// </summary>
		private void HandleError(string errorMessage, Exception ex){
			Clear ();
			uiModeManager.SwitchMode(UIMode.Unknown);
			windowController.ShowErrorMessage(errorMessage, ex.Message /*+ ex.StackTrace*/);
			FireLoadingFinishedEvent (false);
		}

        /// <summary>
        /// Called before the application is closed.
        /// </summary>
        private void OnApplicationQuit()
        {
			//After setting the terrain size in run mode we must reset it now as this setting stays also after the running mode finished
			//And if the terrain size is large, the next build takes extremely long or even hangs up 
			if(Application.isEditor)
				Terrain.activeTerrain.terrainData.size = new Vector3 (1000f, 0f, 1000f);

			Clear ();
        }
    }
}