﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Microsoft.Win32;
using UnityEngine;
using Zenject;

namespace Visualizer.Core
{
	public interface ICmdLineInterpreter{
		void Init (string cmdLine);
		bool GetTimeMarks (out List<SimulationEvent> timeMarks);
		bool GetCmdLineArgAsStr (string key, out string value);
		bool GetCmdLineArgAsInt (string key, out int value);
		bool GetCmdLineArgAsFloat (string key, out float value);
	}

	/// <summary>
	/// Interpreted command line arguments
	/// </summary>
	public static class CmdLineArgs
	{
		public const string TraceFile = "tracefile"; //!<The trace file that should be opened directly on application start in simviewer mode
		public const string SceneryFile = "sceneryfile"; //!<The scenery file that should be opened directly on application start in sceneryviewer mode
		public const string RunID = "runid"; //!<The run ID in the trace file that should be opened directly on application start
		public const string TimeStamp = "timestamp"; //!<The time stamp to which the visualizer should jumt directly on application start
		public const string AgentID = "agentid"; //!<The ID of the agent, to which the camera should be attached directly on application start
		public const string SystemMode = "sysmode"; //!<The system mode (currently supported values: "simplayer", "sensorpositioner", "sceneryviewer")
		public const string VehicleModel = "vehiclemodel"; //!<The vehicle model in sensorpositioner mode			
	}

	/// <summary>
	/// Reads the parameters passed to the application as command line arguments. The expected format of the parameters is URI Query String. For the list of available parameters, see class CmdLineArgs.
	/// Command line examples: 
	/// OpenPASS_Visualizer.exe startparams?sysmode=simplayer&tracefile=c:/testfiles/simulationoutput.xml&runID=0
	/// OpenPASS_Visualizer.exe startparams?sysmode=sensorpositioner&vehiclemodel=car_oldtimer
	/// OpenPASS_Visualizer.exe startparams?sysmode=sceneryviewer&sceneryfile=c:/testfiles/scenery.xodr
	/// </summary>
	public class CmdLineInterpreter : ICmdLineInterpreter {

		private NameValueCollection cmdLineArgs;
		private ISampleContainer sampleContainer;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ISampleContainer sContainer){
			sampleContainer = sContainer;

			Init (System.Environment.CommandLine);
		}

		/// <summary>
		/// Constructor of the CmdLineInterpreter
		/// </summary>
		public void Init(string cmdLine){
			if(Application.platform == RuntimePlatform.WindowsPlayer)
				RegisterAppToURIScheme ();

			string unEscapedCmdLine = System.Uri.UnescapeDataString (cmdLine);

			cmdLineArgs = ParseQueryString(unEscapedCmdLine);
		}
			
		/// <summary>
		/// Registers the application to URI scheme. Works only on Windows.
		/// <remarks>
		/// If the application is registered to URI scheme, it can be started from a web browser via an URI. The application key is "openpassvis".
		/// Usage example:
		/// openpassvis:\\startparams?tracefile=c:\\temp\\simulationoutput.xml&runID=5 
		/// See https://msdn.microsoft.com/en-us/library/aa767914 for more details on the mechanism.
		/// </remarks>
		/// </summary>
		private void RegisterAppToURIScheme(){
			try{
				string registryRoot = "HKEY_CLASSES_ROOT\\openpassvis";
				string uriSchemeKey = "URL Protocol";
				string commandPath = "\\shell\\open\\command";
				string defaultIconPath = "\\DefaultIcon";
				string appPath = System.Diagnostics.Process.GetCurrentProcess ().MainModule.FileName;
					Microsoft.Win32.Registry.SetValue (registryRoot, uriSchemeKey, "");
					Microsoft.Win32.Registry.SetValue (registryRoot + defaultIconPath, "", appPath + ",1");
					Microsoft.Win32.Registry.SetValue (registryRoot + commandPath, "", "\"" + appPath + "\" \"%1\"");
				Debug.Log("Succesfully registered application to URI scheme.");
			}
			catch(Exception ex){
				Debug.LogError ("Application could not be registered to URI scheme: " + ex.Message);
			}
		}

		/// <summary>
		/// Reads the list of time marks from the command line - currently not supported
		/// </summary>
		public bool GetTimeMarks(out List<SimulationEvent> timeMarks){
			timeMarks = new List<SimulationEvent> ();
			return true;
		}

		/// <summary>
		/// Reads the value of the given key from the command line as string 
		/// </summary>
		/// <returns><c>true</c>, if the key was found and could be interpreted, <c>false</c> otherwise.</returns>
		public bool GetCmdLineArgAsStr(string key, out string value){
			value = cmdLineArgs.Get (key);
			if (value == null)
				return false;
			else
				return true;
		}
	
		/// <summary>
		/// Reads the value of the given key from the command line as integer 
		/// </summary>
		/// <returns><c>true</c>, if the key was found and could be interpreted, <c>false</c> otherwise.</returns>
		public bool GetCmdLineArgAsInt(string key, out int value){
			value = -1;
			string valueStr = cmdLineArgs.Get (key);
			if (valueStr == null)
				return false;
			else
				return int.TryParse (valueStr, out value);
		}

		/// <summary>
		/// Reads the value of the given key from the command line as float 
		/// </summary>
		/// <returns><c>true</c>, if the key was found and could be interpreted, <c>false</c> otherwise.</returns>
		public bool GetCmdLineArgAsFloat(string key, out float value){
			value = -1f;
			string valueStr = cmdLineArgs.Get (key);
			if (valueStr == null) {
				return false;
			} else {
				IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
				return float.TryParse (valueStr, NumberStyles.Float, provider, out value);
			}
		}

		/// <summary>
		/// Parses the query string of the given url into a collection of key-value pairs. Replaces HttpUtility.ParseQueryString. 
		/// Based on: http://densom.blogspot.co.at/2009/08/how-to-parse-query-string-without-using.html
		/// </summary>
		private NameValueCollection ParseQueryString(string url)
		{
			NameValueCollection nvc = new NameValueCollection ();

			if (!url.Contains ("?")) {
				//no valid arguments
				return nvc;
			}

			// remove anything other than query string from url
			url = url.Substring (url.IndexOf ('?') + 1);

			foreach (string vp in Regex.Split(url, "&")) {
				string[] singlePair = Regex.Split (vp, "=");
				string key = singlePair [0];
				string value = string.Empty;
				if (singlePair.Length == 2) {
					value = singlePair [1];
				} 
				nvc.Add (key, value);
				Debug.Log ("Parsed CmdLinArg [" + key + "]: [" + value + "]");
			}
			return nvc;
		}
	}
}