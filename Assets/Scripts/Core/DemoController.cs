﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Visualizer.UI;
using Visualizer.Utilities;
using Visualizer.CameraHandling;
using Visualizer.Core;
using Visualizer.AgentHandling;
using Zenject;

public interface IDemoController{
	void ToggleDemoMode ();
	bool IsDemoRunning ();
}

/// <summary>
/// Controls the presentation mode, in which 
/// - the simulation is running endlessly, 
/// - the camera keeps turning slowly
/// - the camera jumps every 60s to the next agent
/// - the ViewMode changes ever 30s
/// </summary>
public class DemoController : MonoBehaviour, IDemoController {

	private IViewModeHandler viewModeHandler;
	private IAgentManager agentManager;
	private IAnimationController animationController;
	private ICameraManager cameraManager;		
	private ISimulationLoader simulationLoader;
	private IFocusCameraHandler focusCameraHandler;
	private IWindowController windowController;

	private bool demoIsRunning = false;
	private float viewModeSwitchPeriod = 20f;
	private float agentSwitchPeriod = 30f;
	private float timeSinceLastViewModeSwitch = 0f;
	private float timeSinceLastAgentSwitch = 0f;

	/// <summary>
	/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
	/// </summary>
	[Inject]
	public void Construct(IWindowController wController, ICameraManager cManager, IAnimationController aController, ISimulationLoader sManager, IViewModeHandler vmHandler, IAgentManager aManager, IFocusCameraHandler acHandler){
		cameraManager = cManager;
		viewModeHandler = vmHandler;
		animationController = aController;
		simulationLoader = sManager;
		agentManager = aManager;
		focusCameraHandler = acHandler;
		windowController = wController;
	}

	/// <summary>
	/// MonoBehaviour function that is called once per frame
	/// </summary>
	void Update () {
		if (!simulationLoader.IsSimulationLoaded())
			return;
		
		if (Input.GetKeyDown (KeyCode.Alpha1) && Input.GetKey(KeyCode.LeftAlt))
			ToggleDemoMode ();

		if (demoIsRunning) {
			timeSinceLastViewModeSwitch += Time.deltaTime;
			if (timeSinceLastViewModeSwitch > viewModeSwitchPeriod) {
				SwitchToNextViewMode ();
			}

			timeSinceLastAgentSwitch += Time.deltaTime;
			if (timeSinceLastAgentSwitch > agentSwitchPeriod) {
				SwitchToNextAgent ();
			}

			Agent focusedAgent = focusCameraHandler.GetFocusedAgent ();
			if (focusedAgent == null || 
				focusedAgent != null && focusedAgent.IsHidden()){
				SwitchToNextAgent ();
			}
		}
	}

	/// <summary>
	/// Turns the demo mode on/off.
	/// </summary>
	public void ToggleDemoMode(){
		demoIsRunning = !demoIsRunning;

		string screenText = demoIsRunning ? "Demo mode ON" : "Demo mode OFF";
		windowController.ShowLoadingMessage(screenText, 1.5f);

		animationController.SwitchRepeatedPlaybackMode(demoIsRunning);
		cameraManager.EnableDemoMode (demoIsRunning);

		if (demoIsRunning && focusCameraHandler.GetFocusedAgent () == null) {
			SwitchToNextAgent ();
			timeSinceLastViewModeSwitch = 0f;
			timeSinceLastAgentSwitch = 0f;
		}

		// this.InvokeDelayed(() => ui.HideLoadingMessage(0.1f), 1.5f);
		// windowController.HideLoadingMessage(1.5f);
	}

	/// <summary>
	/// Get the demo controller state
	/// </summary>
	public bool IsDemoRunning(){
		return demoIsRunning;
	}

	/// <summary>
	/// Get the next view mode and switch to it
	/// </summary>
	private void SwitchToNextViewMode(){
		ViewMode viewMode = viewModeHandler.GetViewMode ();

		switch (viewMode) {
		case ViewMode.LookDown:
			viewMode = ViewMode.Perspective;
			break;
		case ViewMode.Perspective:
			viewMode = ViewMode.Driver;
			break;
		case ViewMode.Driver:
			viewMode = ViewMode.LookDown;
			break;
		}
			
		viewModeHandler.SetViewMode (viewMode);
		timeSinceLastViewModeSwitch = 0f;
	}

	/// <summary>
	/// Get the next agent in the list of agents and attach the camera to it
	/// </summary>
	private void SwitchToNextAgent(){
		int currentFocusedAgentIdx = 0;
		List<int> agentIDs = agentManager.GetAgentIDs ();
		Agent currentFocusedAgent = focusCameraHandler.GetFocusedAgent ();
		if(currentFocusedAgent != null)
			currentFocusedAgentIdx = agentIDs.IndexOf(currentFocusedAgent.ID);
	
		currentFocusedAgentIdx++;
		if(currentFocusedAgentIdx >= agentIDs.Count)
			currentFocusedAgentIdx = 0;

		focusCameraHandler.FocusCamera (agentIDs [currentFocusedAgentIdx]);
		timeSinceLastAgentSwitch = 0f;
	}

}
