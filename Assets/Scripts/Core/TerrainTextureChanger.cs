/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
 
using UnityEngine;
using Zenject;

namespace Visualizer.Core
{
    public interface ITerrainManipulator{
        void RescaleTexture();
        void ToggleTexture ();
	}

    /// <summary>
    /// This class manipulates terrain appearance
    /// </summary>
    public class TerrainManipulator : MonoBehaviour, ITerrainManipulator
    {
        private IAppStorage appStorage;
        private Material realisticTerrain;
        private Material abstractTerrain;
        private string currentTerrainTexture;
        private float gridSize = 10f;

        private static class TerrainTexture {
            public static readonly string Abstract = "abstract";
            public static readonly string Realistic = "realistic";
        }

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAppStorage aStorage){
            appStorage = aStorage;
		}

		/// <summary>
		/// Initializes the TerrainManipulator
		/// </summary>
        public void Start(){
            realisticTerrain = Terrain.activeTerrain.materialTemplate;
            abstractTerrain = Resources.Load ("Materials/AbstractTerrain") as Material;
            string terrainTexture = appStorage.Read(AppStorageKey.TerrainTexture, TerrainTexture.Realistic);
            SetTexture(terrainTexture);
        }

		/// <summary>
		/// Rescale the abstract terrain's grid
		/// </summary>
        public void RescaleTexture(){
            Vector3 terrainSize = Terrain.activeTerrain.terrainData.size;
            abstractTerrain.mainTextureScale = new Vector2(terrainSize.x/gridSize, terrainSize.z/gridSize);
        }

		/// <summary>
		/// Toggle between abstract terrain and realistic terrain texture
		/// </summary>
        public void ToggleTexture(){
            if(currentTerrainTexture == TerrainTexture.Abstract)
                SetTexture(TerrainTexture.Realistic);
            else
                SetTexture(TerrainTexture.Abstract);
        }

		/// <summary>
		/// Toggle between abstract terrain and realistic terrain
		/// </summary>
        private void SetTexture(string terrainTexture){
            if(Terrain.activeTerrain == null) 
                return;

            if(terrainTexture == TerrainTexture.Abstract){
                Terrain.activeTerrain.materialTemplate = realisticTerrain;
                appStorage.Write(AppStorageKey.TerrainTexture, TerrainTexture.Abstract);
            }
            else {
                Terrain.activeTerrain.materialTemplate = abstractTerrain;
                appStorage.Write(AppStorageKey.TerrainTexture, TerrainTexture.Realistic);
            }     
            currentTerrainTexture = terrainTexture;       
            RescaleTexture();
        }
    }
}