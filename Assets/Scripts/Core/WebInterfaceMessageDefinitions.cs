﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.UI;
using Zenject;

namespace Visualizer.Core
{

	// ############ WebApp -> Visualizer Messages

	[Serializable]
	public class WebApp2VisMsg_LoadRun
	{
		public string traceFileContent;
		public string sceneryFileContent;
		public int runID;
	}

	[Serializable]
	public class WebApp2VisMsg_LoadSensor
	{
		public string vehicleModel;
		public SensorPosition sensorPosition;
	}

	[Serializable]
	public class WebApp2VisMsg_EnableFrameUpdate
	{
		public bool enable;
	}
		
	// ############ Visualizer -> WebApp Messages

	[Serializable]
	public abstract class Vis2WebAppMsg
	{
		public string messageId;
	}

	[Serializable]
	public class Vis2WebAppMsg_Ready : Vis2WebAppMsg
	{
		public Vis2WebAppMsg_Ready() {
			messageId = "VisualizerReady";
		}
	}

	[Serializable]
	public class Vis2WebAppMsg_LoadRunFinished : Vis2WebAppMsg
	{
		public Vis2WebAppMsg_LoadRunFinished() {
			messageId = "LoadRunFinished";
		}
	}

	[Serializable]
	public class Vis2WebAppMsg_LoadSensorFinished : Vis2WebAppMsg
	{
		public Vis2WebAppMsg_LoadSensorFinished() {
			messageId = "LoadSensorFinished";
		}
	}

	[Serializable]
	public class Vis2WebAppMsg_SaveSensorPosition : Vis2WebAppMsg
	{
		public SensorPosition sensorPosition;
		public Vis2WebAppMsg_SaveSensorPosition() {
			messageId = "SaveSensorPosition";
			sensorPosition = new SensorPosition();
		}
	}
}

