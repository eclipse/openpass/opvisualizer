/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using UnityEngine.UIElements;
using Zenject;
using Visualizer.Environment;
using Visualizer.Trace;
using Visualizer.EntityRepository;
using Visualizer.Core;
using Visualizer.UI;
using Visualizer.Utilities;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.TrafficSignHandling;
using Visualizer.World.Environment;

public class ApplicationInstaller : MonoInstaller<ApplicationInstaller>
{
    public override void InstallBindings()
    {
		Container.Bind<IMainUI> ().To<MainUI>().FromComponentInHierarchy().AsSingle();
		Container.Bind<IFileBrowserWndHandler>().To<FileBrowserWndHandler>().AsSingle();		
		Container.Bind<IFocusAgentSelectorWndHandler>().To<FocusAgentSelectorWndHandler>().AsSingle();
		Container.Bind<IRunSelectorWndHandler>().To<RunSelectorWndHandler>().AsSingle();
		Container.Bind<IRoadHighlightModeUIHandler>().To<RoadHighlightModeUIHandler>().AsSingle();
		Container.Bind<IDisplayOptionsWndHandler>().To<DisplayOptionsWndHandler>().AsSingle();	
		Container.Bind<IStaticAgentInfoWndHandler>().To<StaticAgentInfoWndHandler>().AsSingle();		
		Container.Bind<IFocusFrameRenderer>().To<FocusFrameRenderer>().AsSingle();		
		Container.Bind<IFileHelper>().To<FileHelper>().AsSingle();
		Container.Bind<IUnityIO>().To<UnityIO>().AsSingle();

		Container.Bind<IPanelResizer>().To<PanelResizer>().AsSingle();
		Container.Bind<IDriverPerceptionManager>().To<DriverPerceptionManager>().AsSingle();
		Container.Bind<IDisplayOptionsProvider>().To<DisplayOptionsProvider>().AsSingle();

		Container.Bind<IAppStorage>().To<AppStorage>().AsSingle();
		Container.Bind<IAgentFactory>().To<AgentFactory>().AsSingle();
		Container.Bind<IAgentManager>().To<AgentManager>().AsSingle();
		Container.Bind<IRoadNetworkRenderer>().To<RoadNetworkRenderer>().AsSingle();
		Container.Bind<IRoadObjectRenderer>().To<RoadObjectRenderer>().AsSingle();
		Container.Bind<IRoadSignalRenderer>().To<RoadSignalRenderer>().AsSingle();
		Container.Bind<ITrafficSignManager>().To<TrafficSignManager>().AsSingle();
		Container.Bind<ITrafficSignStateProvider>().To<TrafficSignStateProvider>().AsSingle();
		Container.Bind<IViewModeHandler>().To<ViewModeHandler>().AsSingle();
		Container.Bind<IPlaybackSpeedHandler>().To<PlaybackSpeedHandler>().AsSingle();
		Container.Bind<ITraceFileLoader>().To<TraceFileLoader>().AsSingle();
		Container.Bind<IJobHandler>().To<JobHandler>().AsSingle();
		Container.Bind<ISampleContainer>().To<SampleContainer>().AsSingle();
		Container.Bind<ICmdLineInterpreter>().To<CmdLineInterpreter>().AsSingle();
		Container.Bind<IEntityRepository>().To<EntityRepository>().AsSingle();
		Container.Bind<IEntityRepositoryLoader>().To<EntityRepositoryLoader>().AsSingle();
		Container.Bind<ISampleAttributeMapper>().To<SampleAttributeMapper>().AsSingle();

		Container.Bind<IAppVersion>().To<AppVersion>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<IUIModeHandler>().To<UIModeHandler>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<ICameraManager> ().To<CameraManager>().FromComponentInHierarchy().AsSingle();
		Container.Bind<ITimeAxisHandler>().To<TimeAxisHandler>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<IWeatherController>().To<WeatherController>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<IDemoController>().To<DemoController>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<ISimulationLoader>().To<SimulationLoader>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<ISensorPositioner>().To<SensorPositioner>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<IAnimationController>().To<AnimationController>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<IFocusCameraHandler>().To<FocusCameraHandler>().FromNewComponentOnNewGameObject ().AsSingle();
		Container.Bind<IRoadExplorer>().To<RoadExplorer>().FromNewComponentOnNewGameObject().AsSingle();
		Container.Bind<IAgentPanelManager>().To<AgentPanelManager>().FromNewComponentOnNewGameObject().AsSingle();
		Container.Bind<IWindowController>().To<WindowController>().FromNewComponentOnNewGameObject().AsSingle();
		Container.Bind<ITerrainManipulator>().To<TerrainManipulator>().FromNewComponentOnNewGameObject().AsSingle();
		
		Container.Bind<IWebInterface>().To<WebInterface>().FromNewComponentOnNewGameObject ().AsSingle().NonLazy ();
		Container.Bind<IMouseEventHandler>().To<MouseEventHandler>().FromNewComponentOnNewGameObject ().AsSingle().NonLazy ();
		Container.Bind<IKeyboardEventHandler>().To<KeyboardEventHandler>().FromNewComponentOnNewGameObject ().AsSingle().NonLazy ();
    
		Container.Bind<UIDocument>().FromComponentInHierarchy().AsSingle();
	}
}