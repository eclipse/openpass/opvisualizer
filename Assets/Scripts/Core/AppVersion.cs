﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Reflection;
using UnityEngine.UI;
using Zenject;

public interface IAppVersion{
	string VersionNumber {get;}
}

/// <summary>
/// Provides the application version number. 
/// Structure: major.minor.patch
/// </summary>
public class AppVersion : MonoBehaviour, IAppVersion
{
	public string VersionNumber { get => "3.00.0"; }

	void Start (){
		Debug.Log (string.Format ("Currently running version is {0}", VersionNumber));
	}
}