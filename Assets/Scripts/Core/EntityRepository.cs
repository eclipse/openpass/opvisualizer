﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Utilities;
using Visualizer.AgentHandling;

namespace Visualizer.EntityRepository
{
	public class Entity {
		public int id = 0;
		public string secondaryId = "";
		public string group = "";
		public string version ="";
		public string name ="";
		public string type ="";
		public string subtype ="";         
	}

	public interface IEntityRepository{
		void Clear ();
		void AddEntity (Entity entity);
		Entity GetEntityByOpenPassID(int id);
		List<Entity> GetEntityByOpenDriveID(string id);
	}

	/// <summary>
	/// A single instance class that handles entities that are loaded from the entity repository file.
	/// </summary>
	public class EntityRepository : IEntityRepository {

		private Dictionary<int, Entity> entities = new Dictionary<int, Entity>();

		/// <summary>
		/// Clear this instance.
		/// </summary>
		public void Clear(){
			entities.Clear ();
		}

		/// <summary>
		/// Adds an entity (all entity attributes) to the reository
		/// </summary>
		public void AddEntity (Entity entity){
			if (IsValid (entity))
				entities.Add(entity.id, entity);
			else
				Debug.LogWarning("Could not add entry to internal entity repository. ID " + entity.id + " already exists.");
		}

		/// <summary>
		/// Returns all entities with the given secondary id.
		/// </summary>
		public List<Entity> GetEntityByOpenDriveID(string id){
			List<Entity> list = new List<Entity>();
			if (id != "") {
				foreach (KeyValuePair<int, Entity> entity in entities)  
					if(entity.Value.secondaryId == id)
						list.Add(entity.Value);
			}
			return list;
		}

		/// <summary>
		/// Returns an entity (all entity attributes) with the given primary id, or null if not found
		/// </summary>
		public Entity GetEntityByOpenPassID(int id){
			if (entities.ContainsKey(id)){
				return entities[id];
			}
			return null;
		}

		/// <summary>
		/// Determines whether the given entity is valid
		/// </summary>
		private bool IsValid(Entity entity){
			if (entities.ContainsKey(entity.id)){
				return false;
			}
			return true;
		}
	}
}