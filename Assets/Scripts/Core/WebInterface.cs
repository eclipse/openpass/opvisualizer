﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.UI;
using Zenject;

namespace Visualizer.Core
{
	public interface IWebInterface{
	}

	/// <summary>
	/// This class implemets the communication interface to a web application, 
	/// where opVisualizer can be embedded, if built as WebGL app 
	/// </summary>
	public sealed class WebInterface : MonoBehaviour, IWebInterface 
	{
		private ISimulationLoader simulationLoader;
		private ISensorPositioner sensorPositioner;
		private IUIModeHandler uiModeManager;
		private Camera mainCamera;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ISimulationLoader simLoader, ISensorPositioner sensPositioner, IUIModeHandler uimManager){
			simulationLoader = simLoader;
			sensorPositioner = sensPositioner;
			uiModeManager = uimManager;
			simulationLoader.LoadingFinished += new LoadingFinishedEventHandler(OnLoadingRunFinished);
			sensorPositioner.SaveSensorPosition += new SaveSensorPositionEventHandler(OnSaveSensorPosition);
		}

		/// <summary>
		/// Prepare application for web mode
		/// </summary>
		public void Start() {
			if (Application.platform == RuntimePlatform.WebGLPlayer) {
				mainCamera = Camera.main;
				Application.runInBackground = true;
				Send_Vis2WebApp(new Vis2WebAppMsg_Ready());
			}
		}

		/// <summary>
		/// This function is called from the embedding webpage when a specific run should be loaded
		/// The input parameter is a JSON serialized message content that must have the same structure as the LoadRunMessage class
		/// </summary>
		public void WebApp2Vis_LoadRun (string serializedMessage) {
			Debug.Log ("Received LoadRun message in Visualizer");
			WebApp2VisMsg_LoadRun loadRunMessage = JsonUtility.FromJson<WebApp2VisMsg_LoadRun>(serializedMessage);
			uiModeManager.SwitchMode (UIMode.SimulationPlayer);
			sensorPositioner.Clear ();
			simulationLoader.DirectLoadRun (loadRunMessage.traceFileContent, loadRunMessage.sceneryFileContent, loadRunMessage.runID);
		}

		/// <summary>
		/// This function is called from the embedding webpage when the containing website is minimized/maximized 
		/// The input parameter is a JSON serialized message content that must have the same structure as the EnableFrameUpdateMessage class
		/// </summary>
		public void WebApp2Vis_EnableFrameUpdate (string serializedMessage) {
			Debug.Log ("Received EnableFrameUpdate message in Visualizer");
			WebApp2VisMsg_EnableFrameUpdate message = JsonUtility.FromJson<WebApp2VisMsg_EnableFrameUpdate>(serializedMessage);
			Application.runInBackground = message.enable;
			Time.timeScale = message.enable ? 1 : 0;
			mainCamera.enabled = message.enable;
		}

		/// <summary>
		/// This function is called from the embedding webpage when a specific sensor of a vehicle should be loaded
		/// The input parameter is a JSON serialized message content that must have the same structure as the LoadSensorMessage class
		/// </summary>
		public void WebApp2Vis_LoadSensor (string serializedMessage) {
			Debug.Log ("Received LoadSensor message in Visualizer:" + serializedMessage);
			WebApp2VisMsg_LoadSensor loadSensorMessage = JsonUtility.FromJson<WebApp2VisMsg_LoadSensor>(serializedMessage);
			uiModeManager.SwitchMode (UIMode.SensorPositioner);
			simulationLoader.Clear ();
			sensorPositioner.LoadSensor (loadSensorMessage.vehicleModel, loadSensorMessage.sensorPosition);
			Send_Vis2WebApp (new Vis2WebAppMsg_LoadSensorFinished ());
		}

		/// <summary>
		/// This function sends the new sensor positionions that the user specified, to the embedding website
		/// </summary>
		public void OnSaveSensorPosition(SensorPosition sensorPos) {
			Vis2WebAppMsg_SaveSensorPosition message = new Vis2WebAppMsg_SaveSensorPosition ();
			message.sensorPosition = sensorPos;
			Send_Vis2WebApp (message);
		}

		/// <summary>
		/// This event is fired, when the simulation loader has finished loading a simulation
		/// </summary>
		private void OnLoadingRunFinished (bool success) {
			Send_Vis2WebApp (new Vis2WebAppMsg_LoadRunFinished ());
		}

		/// <summary>
		/// Sends a message to the embedding website. Input parameter must be a valid Vis2WebAppMsg message  
		/// </summary>
		private void Send_Vis2WebApp(Vis2WebAppMsg message) {
			if (Application.platform == RuntimePlatform.WebGLPlayer) {
				Debug.Log ("Sending message " + message.messageId + " from visualizer to embedding site");
				string serializedMessage = JsonUtility.ToJson (message);
	
				// TODO:
				// We are calling the 'Vis2WebAppMsg' function here in an external index.html. This used to work with
				// Application.ExternalCall but by now the method is deprecated. It needs refactoring: 
				// the js code must be part of the Unity project as a .jslib extension. 
				// As we are currently not using the embedded WebGL version of the opVisualizer, 
				// I commented out this line for now.
				// 
				// The Unity documentation describes the new, recommended way how to call JavaScript functions from Unity script:
				// https://docs.unity3d.com/2021.2/Documentation/Manual/webgl-interactingwithbrowserscripting.html 

				//Application.ExternalCall ("Vis2WebAppMsg", serializedMessage);
			}
		}
	}
}

