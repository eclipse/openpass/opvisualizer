﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;

namespace Visualizer.Core
{
	/// <summary>
	/// Represents the basic event in the simulation.
	/// </summary>
	public class SimulationEvent {
		public int timeStamp = 0;
		public string eventType;
		public Dictionary<string, string> eventParameters;
		public List<long> triggeringEntities;
		public List<long> affectedEntities;
	}
}