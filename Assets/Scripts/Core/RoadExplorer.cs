/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.World.Environment;
using Visualizer.Utilities;
using Visualizer.UI;
using Visualizer.EntityRepository;
using System;
using System.Linq;
using System.Collections.Generic;
using Zenject;

namespace Visualizer.Core
{
    public interface IRoadExplorer{
		void HighlightTransform (Vector3 mousePos);
		void DisplayMousePos (Vector3 mousePos, bool valid);
		void Clear ();
		void SetHighlightMode(RoadHighlightMode mode);
	}

	public enum RoadHighlightMode { Road = 0, Lane, LaneSegment };

	/// <summary>
	/// The RoadExplorer highlights objects/road parts/signals that the user clicks 
	/// and displays various information related to them.
	/// </summary>
    public class RoadExplorer : MonoBehaviour, IRoadExplorer {

		private class HighlightedTransform {
			public Transform transformRef;
			public Color origColor;
			public float origMetallicValue;
			public Vector3 origPosition;
			public float animationVel;
			public float metallicValue;
		}
		private RoadHighlightMode highlightMode;
		private Color highlightColor = new Color(1f, 1f, 0f);
		private float highlightMetallicValue = 0.5f;
		private Vector2 cursorRoadPos = Vector2.zero;
		private List<HighlightedTransform> highlightedTransforms = new List<HighlightedTransform>();
		private LaneSegmentInfo clickedRoadSegmentInfo = null;
		private VisualElement highlightInfoPanel;
		private VisualElement roadHighlightModeDropdown;
		private Label highlightInfoLabel;
		private Label mousePosXLabel;
		private Label mousePosYLabel;
		private Vector3 lastClickPos;
		private IEntityRepository entityRepository;
		TerrainCollider terrainCollider;
		private IMainUI ui;

		//Drilled objects repsesent those road segments that have been hit by the mouse. 
		//They loose their collider on click, to enable the user to "drill" deeper below the hit collider.
		//This is useful if multiple roads overlap under the mouse (e.g. in junctions)
		//We store a reference to these objects to be able to restore the colliders, once the mouse is moved 
		private List<GameObject> drilledObjects = new List<GameObject>();

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IEntityRepository eRepository, IMainUI mUi){
			entityRepository = eRepository;
			ui = mUi;
		}

		/// <summary>
		/// MonoBehaviour function that is called on creation of the DemoController
		/// </summary>
		void Start () {
			mousePosXLabel = ui.GetVisualElement("mousepos-x-lbl") as Label;
			mousePosYLabel = ui.GetVisualElement("mousepos-y-lbl") as Label;
			highlightInfoPanel = ui.GetVisualElement("highlight-info-panel");
			highlightInfoLabel = ui.GetVisualElement("highlight-info-lbl") as Label;
			roadHighlightModeDropdown = ui.GetVisualElement("road-highlight-mode-dd");
			terrainCollider = Terrain.activeTerrain.GetComponent<TerrainCollider>();
			ClearHighlight();
		}	

		/// <summary>
		/// Switches the highlighting mode
		/// </summary>
		public void SetHighlightMode(RoadHighlightMode mode) {
			highlightMode = mode;
			ClearHighlight();
		}

        /// <summary>
		/// Clears the RoadExplorer.
		/// </summary>
		public void Clear(){
			ClearHighlight();
			ClearDrilling();
		}

		/// <summary>
		/// Marks any road objects/signals/lanes under the given mouse position
		/// </summary>
		public void HighlightTransform (Vector3 mousePosition){
			try {				
				//first of all, reset any previously highlighted transforms
				ClearHighlight();

				if( lastClickPos != mousePosition )
					ClearDrilling();

				EnableDrilledObjectColliders(false);

				RaycastHit mouseHit;
				Ray ray = Camera.main.ScreenPointToRay (mousePosition);
				if (Physics.Raycast (ray, out mouseHit)) {
					GameObject hitObject = mouseHit.collider.gameObject;
					if (hitObject.tag == "LaneSection"){
						DrillObject(hitObject);
						switch (highlightMode) {
							case RoadHighlightMode.Road:
								Transform[] allLaneSections = AllChildrenWithTag(hitObject.transform.parent, "LaneSection");
								Highlight(allLaneSections);
							break;
							case RoadHighlightMode.Lane:
								Transform[] laneSections = AllChildrenWithName(hitObject.transform.parent, hitObject.name);
								Highlight(laneSections);
							break;
							case RoadHighlightMode.LaneSegment:
								Highlight(hitObject.transform);
							break;
						}
						clickedRoadSegmentInfo = hitObject.GetComponent<LaneSegmentInfo> ();
						DisplayMousePos(mousePosition, true); //Display highlight info incl. mouse pos
						roadHighlightModeDropdown.style.display = DisplayStyle.Flex;	
					}
					else {
						if (hitObject.tag == "RoadObject" || hitObject.tag == "TrafficSign"){
							Transform[] allHitObjChildren = AllChildrenWithRenderer(hitObject.transform);
							Highlight(allHitObjChildren);
							DisplayHighlightInfo(GetObjectInfoText(hitObject.GetComponent<RoadObjectInfo> ()));
							roadHighlightModeDropdown.style.display = DisplayStyle.None;
						}
					}
				}
				lastClickPos = mousePosition;

				EnableDrilledObjectColliders(true);
			}
			catch (Exception ex){
				Debug.LogError ("Error while highlighting road section: " + ex.Message);
			}
        }

		/// <summary>
		/// Add a road segment to the drilled objects list.
		/// </summary>
		private void DrillObject(GameObject hitObject) {
			drilledObjects.Add(hitObject);
		}

		/// <summary>
		/// Enable/disable colliders of drilled objects.
		/// </summary>
		private void EnableDrilledObjectColliders(bool enable) {
			foreach (GameObject obj in drilledObjects)
				if(obj)
					obj.GetComponent<MeshCollider>().enabled = enable;
		}

		/// <summary>
		/// Clears the list of drilled objects.
		/// </summary>
		private void ClearDrilling() {
			EnableDrilledObjectColliders(true);
			drilledObjects.Clear();
		}

		/// <summary>
		/// Creates a user friendly info text about the currently highlighted road segment.
		/// This will be displayed in the highlight info box on the screen.
		/// </summary>
		private string GetRoadInfoText(LaneSegmentInfo laneSegmentInfo, Vector2 cursorRoadPos, bool roadPosvalid) {
			string text = "";
			if(laneSegmentInfo != null){
				text = "<b>Highlighted Road " + laneSegmentInfo.metaData.roadID + "</b>";
				if (highlightMode == RoadHighlightMode.Lane || highlightMode == RoadHighlightMode.LaneSegment) {
					text += "\n" + "Lane " + laneSegmentInfo.metaData.laneID;
				}
				if(roadPosvalid){
					text += "\n" + "S: " + Math.Round(cursorRoadPos.x, 2).ToString("F2")  + "m";
					text += "\n" + "T: " + Math.Round(cursorRoadPos.y, 2).ToString("F2")  + "m";
				} else {
					text += "\n" + "S: -";
					text += "\n" + "T: -";
				}
			}
			return text;
		}

		/// <summary>
		/// Creates a user friendly info text about the currently highlighted road object/signal.
		/// This will be displayed in the highlight info box on the screen.
		/// </summary>
		private string GetObjectInfoText(RoadObjectInfo roadObjectInfo) {
			string text = "";
			if(roadObjectInfo != null){
				List<Entity> entities = entityRepository.GetEntityByOpenDriveID(roadObjectInfo.data.id);
				
				text += "<b>Highlighted";
				if(roadObjectInfo.data.name != "") {
					text += " " + roadObjectInfo.data.name;
					if(roadObjectInfo.data.repeatId > -1)
						text += " (" + roadObjectInfo.data.repeatId + ")";
				}
				text += "</b>";
				if(roadObjectInfo.data.id != "") {
					text += "\n" + "OpenDRIVE ID: " + roadObjectInfo.data.id;
					if(roadObjectInfo.data.repeatId > -1 && roadObjectInfo.data.name == "")
						text += " (" + roadObjectInfo.data.repeatId + ")";
				}
				if (entities.Count > 0)
					text += "\n" + "OpenPASS ID: " + entities[0].id;
				if (entities.Count > 1)
					text += " - " + entities.Last().id;				
				if(roadObjectInfo.data.type != "")
					text += "\n" + "Type: " + roadObjectInfo.data.type;
				if(roadObjectInfo.data.subType != "")
					text += "\n" + "SubType: " + roadObjectInfo.data.subType;

				text += "\n" + "S: " + Math.Round(roadObjectInfo.data.s, 2);
				text += "\n" + "T: " + Math.Round(roadObjectInfo.data.t, 2);
				if(roadObjectInfo.data.width != 0)
					text += "\n" + "Width: " + Math.Round(roadObjectInfo.data.width, 2);
				if(roadObjectInfo.data.height != 0)
					text += "\n" + "Height: " + Math.Round(roadObjectInfo.data.height, 2);
				if(roadObjectInfo.data.length != 0)
					text += "\n" + "Length: " + Math.Round(roadObjectInfo.data.length, 2);
			}
			return text;
		}

		/// <summary>
		/// Shows the highlight info box with the given text
		/// </summary>
		private void DisplayHighlightInfo(string highlightInfoText) {
			highlightInfoLabel.text = highlightInfoText;
			highlightInfoPanel.transform.scale = UIScale.Up;
		}

		/// <summary>
		/// Highlights the given transform by setting its color to highligh color and
		/// increasing the height of the transform slightly to ensure it is on top of other 
		/// eventually overlapping objects
		/// </summary>
		private void Highlight(Transform transform) {
			MeshRenderer renderer = transform.gameObject.GetComponent<MeshRenderer> ();
			if (renderer != null){
				StoreHighlightedTransformState(transform);
				renderer.material.SetColor ("_Color", highlightColor);
				transform.position = new Vector3(transform.position.x, transform.position.y+0.01f, transform.position.z);
			}
		}

		/// <summary>
		/// Highlights the given array of transforms
		/// </summary>
		private void Highlight(Transform[] transforms) {
			foreach (Transform transform in transforms) {
				Highlight(transform);
			}
		}

		/// <summary>
		/// Clears any highlighting of any objects
		/// </summary>
		private void ClearHighlight(){
			if (highlightedTransforms.Count != 0 ) {
				foreach (HighlightedTransform highlightedTransform in highlightedTransforms) {
					RestoreHighlightedTransformState(highlightedTransform);
				}
				highlightedTransforms.Clear();
			}
			if (highlightInfoPanel != null)
				highlightInfoPanel.transform.scale = UIScale.DownX;

			clickedRoadSegmentInfo = null;
		}

		/// <summary>
		/// MonoBehaviour function that is called once per frame
		/// </summary>
		void Update()
		{
			foreach (HighlightedTransform highlightedTransform in highlightedTransforms) {
				MeshRenderer renderer = highlightedTransform.transformRef.gameObject.GetComponent<MeshRenderer> ();
				highlightedTransform.metallicValue = Mathf.SmoothDamp(highlightedTransform.metallicValue, highlightMetallicValue, ref highlightedTransform.animationVel, 0.3f);
				renderer.material.SetFloat("_Metallic", highlightedTransform.metallicValue);
			}
		}
 
 		/// <summary>
		/// MonoBehaviour function that is called once per frame
		/// </summary>
		public void DisplayMousePos(Vector3 mousePosition, bool valid) {
			Vector3 worldPosition = Vector3.zero;
			if(valid){
				Ray ray = Camera.main.ScreenPointToRay(mousePosition);
				RaycastHit mouseHit;
				if(terrainCollider.Raycast(ray, out mouseHit, 1000))
					worldPosition = mouseHit.point;
				else
					valid = false;
			}

			DisplayMouseWorldCoordinates(worldPosition, valid);
			DisplayMouseRoadCoordinates(worldPosition, mousePosition, valid);
		}

 		/// <summary>
		/// Displays the world coordinates unter the cursor
		/// </summary>
		private void DisplayMouseWorldCoordinates(Vector3 worldPosition, bool valid) {
			if (valid){
				mousePosXLabel.text = "X: " + Math.Round(worldPosition.x, 2).ToString("F2")  + "m";
				mousePosYLabel.text = "Y: " + Math.Round(worldPosition.z, 2).ToString("F2")  + "m";
			}
			else {
				mousePosXLabel.text = "X: -";
				mousePosYLabel.text = "Y: -";
			}
		}

 		/// <summary>
		/// Displays the road coordinates unter the cursor (if over a highlighted road segment)
		/// </summary>
		private void DisplayMouseRoadCoordinates(Vector3 worldPosition, Vector3 mousePosition, bool valid) {
			if(highlightedTransforms.Count == 0 || highlightedTransforms[0].transformRef.gameObject.tag != "LaneSection")
			 	return;

			Vector2 mouseCoordinates_road = Vector2.zero;
			bool roadPosValid = false;
			if(valid) {
				Ray ray = Camera.main.ScreenPointToRay(mousePosition);
				RaycastHit mouseHit;
				if (Physics.Raycast(ray, out mouseHit)){
					MeshCollider meshCollider = mouseHit.collider as MeshCollider;
					if (meshCollider != null && meshCollider.sharedMesh != null){
						Transform hitTransform = mouseHit.collider.transform;
						if (highlightedTransforms.Find(t => t.transformRef == hitTransform) != null){
							Mesh mesh = meshCollider.sharedMesh;
							Vector3[] vertices = mesh.vertices;
							int[] triangles = mesh.triangles;
							Vector3 p0_world = vertices[triangles[mouseHit.triangleIndex * 3 + 0]];
							Vector3 p1_world = vertices[triangles[mouseHit.triangleIndex * 3 + 1]];
							Vector3 p2_world = vertices[triangles[mouseHit.triangleIndex * 3 + 2]];
							roadPosValid = CalcRoadCoordinates(worldPosition, p0_world, p1_world, p2_world, out mouseCoordinates_road);
						}
					}
				}
			}
			DisplayHighlightInfo(GetRoadInfoText(clickedRoadSegmentInfo, mouseCoordinates_road, roadPosValid));
		}

		/// <summary>
		/// Calculate the road position at the given world position (q).
		/// The function expects p0, p1 and p2 to be world coordinates in a road segment, 
		/// to which we stored the corresponding road coordinates while road creation.  
		/// So we have three points for which we know the world coordinates as well as the road coordinates.
		/// We choose two of them (p1 and p2). By calculating their distance to query point (world coord system)
		/// we get two values that we then use in road coordinate system as radius of the two circles that 
		/// center in p1 and p2. The intersection of the two circles is the query point in the road coord system.
		/// </summary>
		private bool CalcRoadCoordinates(Vector3 q_w, Vector3 p0_w, Vector3 p1_w, Vector3 p2_w, out Vector2 q_r) {
			
			Vector2 p0_r, p1_r, p2_r;
			q_r = Vector2.zero;

			if(!GetRoadCoordinatesAt(p0_w, out p0_r))
				return false;

			if(!GetRoadCoordinatesAt(p1_w, out p1_r))
				return false;

			if(!GetRoadCoordinatesAt(p2_w, out p2_r))
				return false;

			float d_qp1 = Vector3.Distance(q_w, p1_w);
			float d_qp2 = Vector3.Distance(q_w, p2_w);

			Vector2 i1, i2;
			if(!Helpers.FindCircleIntersections(p1_r, d_qp1, p2_r, d_qp2, out i1, out i2))
				return false;

			if(Vector2.Distance(i1, p0_r) < Vector2.Distance(i2, p0_r))
				q_r = i1;
			else 
				q_r = i2;

			return true;
		}

		/// <summary>
		/// Searches for the given world coordinates in all highlighted object's road coordinates map
		/// that we stored at road creation for each vertex in the LaneSegmentInfo of the meshes.
		/// If the coordinate is not found, the function returns a zero vector
		/// </summary>
		private bool GetRoadCoordinatesAt(Vector3 worldCoordinates, out Vector2 roadCoordinates) {
			roadCoordinates = Vector2.zero;
			foreach (HighlightedTransform hTransform in highlightedTransforms) {
				LaneSegmentInfo laneSegmentInfo = hTransform.transformRef.gameObject.GetComponent<LaneSegmentInfo>();
				if(laneSegmentInfo != null && laneSegmentInfo.roadCoordinates.ContainsKey(worldCoordinates)) {
					roadCoordinates = laneSegmentInfo.roadCoordinates[worldCoordinates];
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Stores the state of the given transform to the list of highlighted transforms
		/// in order to be able to revert the transform attributes when removing highlighing
		/// </summary>
		private void StoreHighlightedTransformState(Transform transform) {
			HighlightedTransform highlightedTransform = new HighlightedTransform();
			highlightedTransform.transformRef = transform;	
			highlightedTransform.origColor = transform.gameObject.GetComponent<MeshRenderer> ().material.GetColor("_Color");
			highlightedTransform.origMetallicValue = transform.gameObject.GetComponent<MeshRenderer> ().material.GetFloat("_Metallic");
			highlightedTransform.origPosition = transform.position;
			highlightedTransforms.Add(highlightedTransform);
		}

		/// <summary>
		/// Revert the transform attributes to original state as it used to be before highlighting
		/// </summary>
		private void RestoreHighlightedTransformState(HighlightedTransform highlightedTransform) {
			if(highlightedTransform.transformRef != null) {
				MeshRenderer renderer = highlightedTransform.transformRef.gameObject.GetComponent<MeshRenderer> ();
				renderer.material.SetColor("_Color", highlightedTransform.origColor);	
				renderer.material.SetFloat("_Metallic", highlightedTransform.origMetallicValue);
				highlightedTransform.transformRef.position = highlightedTransform.origPosition;
			}
		}

		/// <summary>
		/// Returns all children of the given transform with the given name
		/// </summary>
		private Transform[] AllChildrenWithName(Transform parent, string name)
		{
			return parent.GetComponentsInChildren<Transform>().Where(t => t.name == name).ToArray();
		}

		/// <summary>
		/// Returns all children of the given transform with the given tag
		/// </summary>
		private Transform[] AllChildrenWithTag(Transform parent, string tag)
		{
			return parent.GetComponentsInChildren<Transform>().Where(t => t.tag == tag).ToArray();
		}

		/// <summary>
		/// Returns all children of the given transform that have a mesh renderer attached
		/// </summary>
		private Transform[] AllChildrenWithRenderer(Transform parent)
		{
			return parent.GetComponentsInChildren<Transform>().Where(t => t.gameObject.GetComponent<MeshRenderer>() != null).ToArray();
		}
    }
}
