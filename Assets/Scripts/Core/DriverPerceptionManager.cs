﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Visualizer.UI;
using Visualizer.Core;
using Visualizer.Trace;
using Visualizer.Utilities;
using Visualizer.AgentHandling;
using UnityEngine;
using Zenject;

namespace Visualizer.Core
{
	public delegate void DriverPerceptionChangeHandler(string loadedFile, string error);

	public class DriverPerceptionSampleAttributes : SampleAttributes
	{
		public override string XPosition => "Driver.Perception.PosX";
		public override string YPosition => "Driver.Perception.PosY";
	}

	public interface IDriverPerceptionManager{
		event DriverPerceptionChangeHandler DriverPerceptionLoaded;
		bool HasDriverPerception(int agentID);
		void ShowDriverPerception(int agentID);
		void UpdatePerceivedWorld(int timeStamp);
		void SetAnimationInterval (float animationInterval);
		List<string> GetAvailableAgentAttributes();
		int GetActiveAgentID ();
		Dictionary<string, Agent> GetAllPerceivedAgents();
		void Clear();	
	}

	/// <summary>
	/// Central class that manages the loading and serving of driver perceptions of agents (the world from the drivers perspective)
	/// </summary>
    public sealed class DriverPerceptionManager : IDriverPerceptionManager
    {
		public event DriverPerceptionChangeHandler DriverPerceptionLoaded;

		/// <summary>
		/// An agent's driver perception 
		/// </summary>		
		public class DriverPerception {
			public bool active = false;
			public Dictionary<int, Agent> perceivedAgents = null; //<!This list holds a reference to all perceived agents 
			public SampleContainer perceivedWorldSamples = null;	//<!This is the container of the perceived world samples 
		}

		private ISimulationLoader simulationLoader;
		private ISampleContainer coreSampleContainer;
		private IAgentManager agentManager;
		private IAgentFactory agentFactory;	
		private ISampleAttributeMapper sampleAttributeMapper;	

		private Dictionary<int, DriverPerception> driverPerceptions = new Dictionary<int, DriverPerception>(); //<!This list holds the driver perceptions of agents. The key is the agent ID

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ISampleAttributeMapper saMapper, ISampleContainer sContainer, ISimulationLoader sLoader, IAgentFactory aFactory, IAgentManager aManager){
			agentManager = aManager;
			simulationLoader = sLoader;
			agentFactory = aFactory;
			coreSampleContainer = sContainer;
			sampleAttributeMapper = saMapper;
		}

		/// <summary>
		/// Resets all internal data in the DriverPerceptionManager
		/// </summary>
		public void Clear() {
			foreach(DriverPerception driverPerception in driverPerceptions.Values){
				if (driverPerception.perceivedAgents != null) 
					foreach (int aID in driverPerception.perceivedAgents.Keys)
						GameObject.Destroy (driverPerception.perceivedAgents [aID].gameObject);					

				driverPerception.perceivedAgents = null;
			}
			driverPerceptions.Clear();
		}	
		
		/// <summary>
		/// Checks if the given agent has a driver perception file 
		/// </summary>
		public bool HasDriverPerception(int agentID) {
			string fileName = GetDriverPerceptionFileName(agentID);
			return File.Exists(fileName);
		}

		/// <summary>
		/// Loads the driver perception of the given agent, if not already loaded and displays its perceived world
		/// while hiding all other agent's driver perception 
		/// </summary>
		public void ShowDriverPerception(int agentID) {
			string error = "";
			if(HasDriverPerception(agentID)){
				if (!driverPerceptions.ContainsKey(agentID))
					error = LoadDriverPerception(agentID);
				else
					ActivatePerceivedWorld(agentID);
			} else {
				error = "Expected file: "+ GetDriverPerceptionFileName(agentID);
			}

			if(DriverPerceptionLoaded != null)
				DriverPerceptionLoaded(GetDriverPerceptionFileName(agentID), error);
		}

		/// <summary>
		/// Gets the available attributes of the given perceived agent 
		/// </summary>
		public List<string> GetAvailableAgentAttributes() {
			foreach(DriverPerception driverPerception in driverPerceptions.Values){
				if (driverPerception.active){
					return driverPerception.perceivedWorldSamples.AgentAttributes;
				}
			}
			return new List<string>();
		}

		/// <summary>
		/// Loads the driver perception of the given agent. This function might run long, depending on the size of the file. 
		/// It triggers the event DriverPerceptionLoaded if it finished running.
		/// </summary>
		private string LoadDriverPerception(int agentID) {
			string error = "";
			string driverPerceptionFile = GetDriverPerceptionFileName(agentID);
			
			try {
				SampleContainer sampleContainer = new SampleContainer();
				CyclicsParserCSV cyclicsParser = new CyclicsParserCSV(driverPerceptionFile);
				sampleContainer.AgentAttributes = cyclicsParser.GetAgentAttributesFromHeader();
				cyclicsParser.ParseAllSamples(sampleContainer, new DriverPerceptionSampleAttributes(), sampleAttributeMapper);

				List<int> agentIDs = cyclicsParser.GetAgentIDsFromHeader();

				List<AgentProperties> agentProps = new List<AgentProperties>();
				foreach (int id in agentIDs){
					AgentProperties agentProp = new AgentProperties();
					agentProp.ID = id;
					agentProp.VehicleModelName = "perceived_vehicle";
					agentProps.Add(agentProp);
				}

				DriverPerception driverPerception = new DriverPerception();
				driverPerception.perceivedAgents = SpawnPerceivedAgents (agentProps);
				driverPerception.perceivedWorldSamples = sampleContainer;
				driverPerceptions.Add(agentID, driverPerception);

				ActivatePerceivedWorld(agentID);
			} catch (Exception ex) {
				error = ex.Message;
				Clear ();
			}
			return error;
		}

		/// <summary>
		/// Builds the driver perception file name for the given agent, based on the currently loaded trace file path and run id
		/// </summary>
		private string GetDriverPerceptionFileName(int agentID) {
			SimulationMetaData simInfo = simulationLoader.GetCurrentSimulationMetaData();
			return Helpers.GetFileFolder(simInfo.TraceFile) + "\\DriverOutput\\SimRun_" + simInfo.RunID + "\\" + agentID + ".csv";
		}

		/// <summary>
		/// Creates the perceived agents. It copies the size of the real agent
		/// </summary>
		/// <param name="agentProps">List of agent properties</param>
		private Dictionary<int, Agent> SpawnPerceivedAgents (List<AgentProperties> agentProps){
			Dictionary<int, Agent> perceivedAgents = new Dictionary<int, Agent>();

			foreach (AgentProperties agentProp in agentProps) {
				Agent realAgent = agentManager.GetAgent(agentProp.ID);
				if(realAgent != null)
					agentProp.VehicleInfo = realAgent.StaticAgentInfo.VehicleInfo;
				Agent perceivedAgent = agentFactory.SpawnAgent(agentProp, false);
				perceivedAgent.SetTag("PerceivedAgent");
				perceivedAgents.Add (agentProp.ID, perceivedAgent);
			}
			return perceivedAgents;
		}

		/// <summary>
		/// Sets the percieved vehicles of the given agent's driver perception active and all other driver perceptions inactive.
		/// </summary>
		private void ActivatePerceivedWorld (int agentID){
			foreach(KeyValuePair<int, DriverPerception> driverPerception in driverPerceptions){
				driverPerception.Value.active = (driverPerception.Key == agentID);
				foreach (Agent agent in driverPerception.Value.perceivedAgents.Values) {
					agent.gameObject.SetActive(driverPerception.Value.active);
				}
			}
		}

		/// <summary>
		/// Returns the agent's ID whose driver perception is currently displayed. Returns -1 if no driver perception is active.
		/// </summary>
		public int GetActiveAgentID (){
			foreach(KeyValuePair<int, DriverPerception> driverPerception in driverPerceptions){
				if (driverPerception.Value.active)
					return driverPerception.Key;
			}
			return -1;
		}

		/// <summary>
		/// Returns all agents that have been loaded into the scene until now (also inactive ones).
		/// </summary>
		public Dictionary<string, Agent> GetAllPerceivedAgents() {
			Dictionary<string, Agent> allPerceivedAgents = new Dictionary<string, Agent>();
			foreach(KeyValuePair<int, DriverPerception> driverPerception in driverPerceptions){
				foreach (Agent agent in driverPerception.Value.perceivedAgents.Values) {
					string key = driverPerception.Key + " -> " + agent.ID; 
					allPerceivedAgents.Add(key, agent);
				}
			}
			return allPerceivedAgents;
		}

		/// <summary>
		/// Update all perceived agents of any active driver perception
		/// It pulls a sample from the perceived world's SampleContainer and passes them to the perceived agents. 
		/// It is called every time the AnimationController triggers a new frame update.
		/// Note that if a sample does not exist at the given timestamp
		/// the percieved agents are removed from the world for this time stamp
		/// </summary>
		/// <param name="timeStamp">Time stamp in ms</param>
		public void UpdatePerceivedWorld(int timeStamp){
			foreach(DriverPerception driverPerception in driverPerceptions.Values){
				AgentSample[] agentSamples = driverPerception.perceivedWorldSamples.GetSample(timeStamp);
				if (agentSamples == null)
					continue;
				
				if (agentSamples.Length > 0) {
					for (int agentIdx = 0; agentIdx < agentSamples.Length; agentIdx++) {
						AgentSample agentSample = agentSamples [agentIdx];
						Agent perceivedAgent = driverPerception.perceivedAgents[agentSample.AgentID];
						if (perceivedAgent != null) {
							perceivedAgent.SourcePosition = perceivedAgent.TargetPosition;
							perceivedAgent.TargetPosition = new Vector3 (agentSample.XPosition, 0f, agentSample.YPosition);
							perceivedAgent.SourceYawAngle = perceivedAgent.TargetYawAngle;
							perceivedAgent.TargetYawAngle = agentSample.YawAngle; 
							perceivedAgent.SourceRollAngle = perceivedAgent.TargetRollAngle;
							perceivedAgent.TargetRollAngle = agentSample.RollAngle;
							perceivedAgent.LastUpdate = Time.time;
							perceivedAgent.OptionalAttributes = agentSample.OptionalAttributes;
						}
					}
				} else {
					foreach(Agent agent in driverPerception.perceivedAgents.Values){
						agent.TargetPosition = Vector3.zero;
						agent.LastUpdate = Time.time;
					}
				}
			}
		}

		/// <summary>
		/// Passes the animation interval to the perceived agents. Used for agent movement interpolation
		/// </summary>
		/// <param name="animationInterval">Animation interval.</param>
		public void SetAnimationInterval (float animationInterval) {
			foreach(DriverPerception driverPerception in driverPerceptions.Values)
				foreach(Agent perceivedAgent in driverPerception.perceivedAgents.Values)
						perceivedAgent.AnimationInterval = animationInterval;
		}
    }
}