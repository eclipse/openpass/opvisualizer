﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Visualizer.Utilities;
using Visualizer.AgentHandling;

namespace Visualizer.Core
{
	public interface ISampleContainer{
		List<string> AgentAttributes { get; set; }
		int SamplingTime { get; set; }
		float CurrentSampleParsingTime { get; set; }
		void Clear ();
		void AddSample (int timeStamp, AgentSample[] sample);
		AgentSample[] GetSample (int timeStamp);
		void AddEvent (SimulationEvent simEvent);
		List<SimulationEvent> GetEvents ();
		int GetSampleCount ();
		int GetFirstTimeStamp ();
		int GetLastTimeStamp();
		int TimeStampToSampleIdx (int timeStamp);
		int SampleIdxToTimeStamp (int idx);
	}

	/// <summary>
	/// A single instance class that handles samples that are loaded from the trace file.
	/// </summary>
	public class SampleContainer : ISampleContainer {

		private ConcurrentDictionary<int,AgentSample[]> samples = new ConcurrentDictionary<int, AgentSample[]>();
		private List<SimulationEvent> events = new List<SimulationEvent>();
		private int firstTimeStamp = -1;
		private int lastTimeStamp = -1;

		public List<string> AgentAttributes { get; set; } = new List<string>();
		public int SamplingTime { get; set; } = -1;
		public float CurrentSampleParsingTime { get; set; } = 0;

		/// <summary>
		/// Clear this instance.
		/// </summary>
		public void Clear(){
			events.Clear ();
			samples.Clear();
			firstTimeStamp = -1;
			lastTimeStamp = -1;	
			SamplingTime = -1;
			AgentAttributes.Clear();
		}

		/// <summary>
		/// Adds a sample (all agent samples) at the specified point in time to the internal sample array
		/// </summary>
		public void AddSample(int timeStamp, AgentSample[] sample){
			if(samples == null)
				throw new System.InvalidOperationException("SampleContainer is not initialized");

			samples.AddOrUpdate(timeStamp, sample, (ts, s) => sample);
		}

		/// <summary>
		/// Returns a sample (array of all agent samples) at the specified point in time. Think of this piece of info as a snapshot of the world.
		/// </summary>
		public AgentSample[] GetSample(int timeStamp){
			if (samples != null && samples.ContainsKey(timeStamp))
				return samples[timeStamp];
			
			return null;
		}

		/// <summary>
		/// Adds a given simulation event to the internal list of events
		/// and makes sure, the list is kept sorted based on the event time stamps  
		/// </summary>
		public void AddEvent(SimulationEvent simEvent){
			if(events == null)
				throw new System.InvalidOperationException("Events list in SampleContainer is not initialized");

			events.Add(simEvent);
			events = events.OrderBy(e => e.timeStamp).ToList();
		}

		/// <summary>
		/// Gets the list of simulation events.
		/// </summary>
		public List<SimulationEvent> GetEvents(){
			return events;
		}

		/// <summary>
		/// Returns the number of samples that the sample container stores
		/// </summary>
		public int GetSampleCount(){
			if (samples == null)
				return 0;

			return samples.Count;
		}

		/// <summary>
		/// Returns the first time stamp that the sample container stores
		/// Note that we cache the result. 
		/// </summary>
		public int GetFirstTimeStamp(){
			if(samples != null && samples.Count > 0 && firstTimeStamp == -1) {
				firstTimeStamp = samples.Keys.Min();
			}
			return firstTimeStamp;
		}

		/// <summary>
		/// Returns the last time stamp that the sample container stores
		/// Note that we cache the result. 
		/// </summary>
		public int GetLastTimeStamp(){
			if(samples != null && samples.Count > 0 && lastTimeStamp == -1) {
				lastTimeStamp = samples.Keys.Max();
			}
			return lastTimeStamp;
		}

		/// <summary>
		/// Converts a time stamp to sample index.
		/// </summary>
		public int TimeStampToSampleIdx(int timeStamp){
			int idx = 0;
			if (SamplingTime != 0)
				idx = (int)((timeStamp - GetFirstTimeStamp()) / SamplingTime);
			return idx;
		}

		/// <summary>
		/// Converts a sample index to a time stamp.
		/// </summary>
		public int SampleIdxToTimeStamp(int idx){
			int timeStamp = idx * SamplingTime;
			return timeStamp + GetFirstTimeStamp();
		}
	}
}