﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using Visualizer.Utilities;
using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.UI;
using Visualizer.Environment;
using Visualizer.World.OpenDrive;
using Visualizer.World.Environment;
using Zenject;

namespace Visualizer.Core
{
	/// <summary>
	/// Method called when the loading has finished
	/// </summary>
	public delegate void SaveSensorPositionEventHandler(SensorPosition newSensorPosition);
	public delegate void CancelSensorPositioningEventHandler();

	public interface ISensorPositioner{
		event SaveSensorPositionEventHandler SaveSensorPosition;
		event CancelSensorPositioningEventHandler CancelSensorPositioning;
		void LoadSensor(string vehicleType, SensorPosition sensorPosition);
		void UpdateSensorVisualPosition ();
		void SaveSensor ();
		void CancelPositioning();
		void Clear ();
	}

	/// <summary>
	/// Structure thata describes the position of a sensor 
	/// </summary>
	[Serializable]
	public class SensorPosition
	{
		public string name;
		public float longitudinal;
		public float lateral;
		public float height;
		public float yaw;
		public float pitch;
		public float roll;
	}

	/// <summary>
	/// This class manages the sensor loading process
	/// </summary>
    public sealed class SensorPositioner : MonoBehaviour, ISensorPositioner
    {
		public event SaveSensorPositionEventHandler SaveSensorPosition;
		public event CancelSensorPositioningEventHandler CancelSensorPositioning;

		private IViewModeHandler viewModeHandler;
		private IFocusCameraHandler focusCameraHandler;
		private ICameraManager cameraManager;
		private IAgentFactory agentFactory;
		private ICmdLineInterpreter cmdLineInterpreter;
		private IMainUI ui;
		private IWindowController windowController;
		private IRoadNetworkRenderer roadRenderer;
		private IUIModeHandler uiModeManager;
		private IUnityIO unityIO;
		private ITerrainManipulator terrainManipulator;
		private Agent vehicle;

		private VisualElement actionBtnGroup;
		private Label sensorNameLbl;
		private Label vehicleModelLbl;
		private TextField sensorNameField;
		private TextField lateralPosField;
		private TextField longitudinalPosField;
		private TextField heightField;
		private TextField yawField;
		private TextField pitchField;
		private TextField rollField;
		private DropdownField vehicleModelDropdown;

		private List<string> vehicleModels = new List<string>();
		private Map loadedMap = null;
		private bool initialized = false;
		private readonly int repeatDelay = 400;
		private readonly int repeatInterval = 10;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ITerrainManipulator tManipulator, IUnityIO uIO, IUIModeHandler uimManager, IRoadNetworkRenderer rRenderer, IWindowController wController, IMainUI uiMain, ICameraManager cManager, IAgentFactory aFactory, IViewModeHandler vmHandler, IFocusCameraHandler acHandler, ICmdLineInterpreter cInterpreter){
			agentFactory = aFactory;
			cameraManager = cManager;
			viewModeHandler = vmHandler;
			focusCameraHandler = acHandler;
			cmdLineInterpreter = cInterpreter;
			ui = uiMain;
			windowController = wController;
			roadRenderer = rRenderer;
			uiModeManager = uimManager;
			terrainManipulator = tManipulator;
			unityIO = uIO;
		}

		/// <summary>
		/// Initialize the SensorPositioner
		/// </summary>
		private void Start(){
			VisualElement sensorPositionerContainer = ui.GetVisualElement("sensor-positioner-container");
            var template = Resources.Load<VisualTreeAsset>("UI/SensorPositionerPanel");
            VisualElement panel = template.CloneTree();
			sensorPositionerContainer.Add(panel);

			VisualElement longitudinalController = panel.Q<VisualElement>("longitudinal-controller");
			VisualElement lateralController = panel.Q<VisualElement>("lateral-controller");
			VisualElement heightController = panel.Q<VisualElement>("height-controller");
			VisualElement yawController = panel.Q<VisualElement>("yaw-controller");
			VisualElement pitchController = panel.Q<VisualElement>("pitch-controller");
			VisualElement rollController = panel.Q<VisualElement>("roll-controller");
			
			sensorNameLbl = panel.Q<Label>("sensor-name-lbl");
			vehicleModelLbl = panel.Q<Label>("vehicle-model-lbl");
			sensorNameField = panel.Q<TextField>("sensor-name-field");
			vehicleModelDropdown = panel.Q<DropdownField>("vehicle-model-dd");
			longitudinalPosField = longitudinalController.Q<TextField>("value-field");
			lateralPosField = lateralController.Q<TextField>("value-field");
			heightField = heightController.Q<TextField>("value-field");
			yawField = yawController.Q<TextField>("value-field");
			pitchField = pitchController.Q<TextField>("value-field"); 
			rollField = rollController.Q<TextField>("value-field");
			actionBtnGroup = panel.Q<VisualElement>("action-btn-group");	

			longitudinalPosField.RegisterCallback<ChangeEvent<string>>(ev => OnSensorParamChanged());
			lateralPosField.RegisterCallback<ChangeEvent<string>>(ev => OnSensorParamChanged());
			heightField.RegisterCallback<ChangeEvent<string>>(ev => OnSensorParamChanged());
			yawField.RegisterCallback<ChangeEvent<string>>(ev => OnSensorParamChanged());
			pitchField.RegisterCallback<ChangeEvent<string>>(ev => OnSensorParamChanged());
			rollField.RegisterCallback<ChangeEvent<string>>(ev => OnSensorParamChanged());
			vehicleModelDropdown.RegisterCallback<ChangeEvent<string>>(ev => VehicleModelDropdownChanged(ev.newValue));
			panel.Q<Button>("save-btn").RegisterCallback<ClickEvent>(ev => OnClickSaveSensor());

			panel.Q<VisualElement>("longitudinal-controller").Q<Button>("decrease-btn").RegisterCallback<ClickEvent>(ev => OnDecreaseSensorParam(longitudinalPosField));
			longitudinalController.Q<Button>("increase-btn").clickable = new Clickable(() => OnIncreaseSensorParam(longitudinalPosField), repeatDelay, repeatInterval);
			longitudinalController.Q<Button>("decrease-btn").clickable = new Clickable(() => OnDecreaseSensorParam(longitudinalPosField), repeatDelay, repeatInterval);
			lateralController.Q<Button>("increase-btn").clickable = new Clickable(() => OnIncreaseSensorParam(lateralPosField), repeatDelay, repeatInterval);
			lateralController.Q<Button>("decrease-btn").clickable = new Clickable(() => OnDecreaseSensorParam(lateralPosField), repeatDelay, repeatInterval);
			heightController.Q<Button>("increase-btn").clickable = new Clickable(() => OnIncreaseSensorParam(heightField), repeatDelay, repeatInterval);
			heightController.Q<Button>("decrease-btn").clickable = new Clickable(() => OnDecreaseSensorParam(heightField), repeatDelay, repeatInterval);
			yawController.Q<Button>("increase-btn").clickable = new Clickable(() => OnIncreaseSensorParam(yawField), repeatDelay, repeatInterval);
			yawController.Q<Button>("decrease-btn").clickable = new Clickable(() => OnDecreaseSensorParam(yawField), repeatDelay, repeatInterval);
			pitchController.Q<Button>("increase-btn").clickable = new Clickable(() => OnIncreaseSensorParam(pitchField), repeatDelay, repeatInterval);
			pitchController.Q<Button>("decrease-btn").clickable = new Clickable(() => OnDecreaseSensorParam(pitchField), repeatDelay, repeatInterval);
			rollController.Q<Button>("increase-btn").clickable = new Clickable(() => OnIncreaseSensorParam(rollField), repeatDelay, repeatInterval);
			rollController.Q<Button>("decrease-btn").clickable = new Clickable(() => OnDecreaseSensorParam(rollField), repeatDelay, repeatInterval);

			uiModeManager.UIModeChanged += new UIModeChangedEventHandler(OnUIModeChanged);

			vehicleModels = unityIO.GetAssetNames("Vehicle");
			vehicleModelDropdown.choices = vehicleModels;

			EnableEmbeddedMode(Application.platform == RuntimePlatform.WebGLPlayer);

			//we use Invoke to make sure that all modules have already initialized, when we execute this function 
			Invoke ("ReadCmdLine", 1);
		}

		/// <summary>
		/// Hides/shows UI controls that are used for the web mode.
		/// In embedded mode the vehicle model is passed as parameter, the sensor position has to have a name and
		/// we pass the position as back to the parent application if the user clicks on save. In standalone mode
		/// the we only show the sensor positioner but there is no further interaction with the data
		/// </summary>
		/// <param name="value">Value.</param>
		private void EnableEmbeddedMode(bool enable){
			actionBtnGroup.style.display = enable ? DisplayStyle.Flex : DisplayStyle.None;
			sensorNameLbl.style.display = enable ? DisplayStyle.Flex : DisplayStyle.None;
			sensorNameField.style.display = enable ? DisplayStyle.Flex : DisplayStyle.None;
			vehicleModelLbl.style.display = enable ? DisplayStyle.None : DisplayStyle.Flex;
			vehicleModelDropdown.style.display = enable ? DisplayStyle.None : DisplayStyle.Flex;
		}

		/// <summary>
		/// Event from the UIModeManager
		/// </summary>
		private void OnUIModeChanged(UIMode oldMode, UIMode newMode) {
			if(oldMode == UIMode.SensorPositioner)
				Clear();

			if(newMode == UIMode.SensorPositioner) {
				if(vehicleModels.Count() == 0)
					return;

				string currentVehicle = vehicleModelDropdown.value;
				if(currentVehicle != null)
					LoadSensor (currentVehicle, GetSensorPosition());
				else 
					vehicleModelDropdown.value = vehicleModels[0];
			}
		}

		/// <summary>
		/// Callback that is fired after the user changed the vehicle model dropdown
		/// </summary>
		/// <param name="value">Value.</param>
		private void VehicleModelDropdownChanged(string modelName){
			SensorPosition sensorPos = initialized ? GetSensorPosition() : CreateDefaultSensorPosition();
			LoadSensor (modelName.ToLower (), sensorPos);
		}

		/// <summary>
		/// Reads the command line params and directly loads the sensor if params match 
		/// Example command line: "C:\\OpenPASS_Visualizer.exe startparams?sysmode=sensorpositioner&vehiclemodel=car_oldtimer"
		/// </summary>
		private void ReadCmdLine(){
			string sysMode;
			string vehicleModel;
			if (cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.SystemMode, out sysMode)) {
				if (sysMode.ToLower () == "sensorpositioner") {
					if (cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.VehicleModel, out vehicleModel)) {
						SensorPosition sensorPos = CreateDefaultSensorPosition();
						LoadSensor (vehicleModel.ToLower (), sensorPos);
					}
				}
			}
		}

		private SensorPosition CreateDefaultSensorPosition() {
			SensorPosition sensorPosition = new SensorPosition ();
			sensorPosition.name = "New Sensor";
			sensorPosition.height = 2f;
			return sensorPosition;
		}

		/// <summary>
		/// The main public function that sets up the sensor positioning mode 
		/// </summary>
		public void LoadSensor(string vehicleType, SensorPosition sensorPosition){
			Clear ();

			SpawnVehicle (vehicleType);
			FillUIFields (sensorPosition);
			UpdateSensorVisualPosition ();
			LoadTestRoad();

			cameraManager.FocusCamera (vehicle);
			cameraManager.SwitchViewMode (ViewMode.Sensor);
			initialized = true;
		}

		/// <summary>
		/// Generate a simple straight test road
		/// </summary>
		private void LoadTestRoad(){
			var factory = new Map.Factory (new MeshFactory ());
			loadedMap = factory.Create (TestRoads.SimpleStraight, false) as Map;
			roadRenderer.BuildRoad(loadedMap);
			terrainManipulator.RescaleTexture();
		}

		/// <summary>
		/// Create the specified vehicle and adds a dummy sensor with the specified parameters
		/// </summary>
		private void SpawnVehicle(string vehicleType){
			AgentProperties agentProps = new AgentProperties ();
			SensorAttributes sensorAttributes = new SensorAttributes ();
			List<SensorAttributes> sensors = new List<SensorAttributes>();

			//set static sensor attributes
			sensorAttributes.OpeningAngleH = 0.52f; //30°
			sensorAttributes.DetectionRange = 3;
			sensorAttributes.Model = "SensorPositioner";
			sensors.Add (sensorAttributes);

			agentProps.VehicleInfo = new VehicleAttributes {
				Width = 0f,
				Height = 0f,
				Length = 0f,
				PivotOffset = 0f
			};
			agentProps.VehicleModelName = vehicleType;
			agentProps.Sensors = sensors;
			agentProps.ID = 0;

			//come baby come baby baby come come
			vehicle = agentFactory.SpawnAgent (agentProps, true, false);

			//position of vehicle cannot be 0,0,0 because that would hide the vehicle
			vehicle.TargetPosition = new Vector3 (0.1f, 0f, 0f);
			vehicle.ShowDisplayOption ("Visualized.SensorRange", true);
			vehicle.AreaOfInterest = -1;
		}

		/// <summary>
		/// Writes the sensor parameters into the text boxes
		/// </summary>
		private void FillUIFields(SensorPosition sensorPosition){
			sensorNameField.value = sensorPosition.name;
			longitudinalPosField.value = sensorPosition.longitudinal.ToString ("F2");
			lateralPosField.value = sensorPosition.lateral.ToString ("F2");
			heightField.value = sensorPosition.height.ToString ("F2");
			yawField.value = sensorPosition.yaw.ToString ("F2");
			pitchField.value = sensorPosition.pitch.ToString ("F2");
			rollField.value = sensorPosition.roll.ToString ("F2");
		}

		/// <summary>
		/// Reads the sensor parameters from the text boxes and returns them as SensorPosition object
		/// </summary>
		private SensorPosition GetSensorPosition() {
			SensorPosition sensorPosition= new SensorPosition ();
			sensorPosition.name = sensorNameField.value;
			sensorPosition.longitudinal = ToFloat(longitudinalPosField.value); 
			sensorPosition.lateral = ToFloat(lateralPosField.value); 
			sensorPosition.height = ToFloat(heightField.value); 
			sensorPosition.pitch = ToFloat(pitchField.value);
			sensorPosition.roll = ToFloat(rollField.value);
			sensorPosition.yaw = ToFloat(yawField.value);
			return sensorPosition;
		}

		/// <summary>
		/// Triggers the update of the sensor in the spawned vehicle
		/// </summary>
		public void UpdateSensorVisualPosition() {
			if (vehicle == null)
				return;

			SensorPosition sPos = GetSensorPosition ();

			SensorAttributes sensorAttributes = new SensorAttributes ();
			sensorAttributes.MountingPosition = new Vector3 (sPos.longitudinal, sPos.height, sPos.lateral); 
			sensorAttributes.OrientationYaw = sPos.yaw;
			sensorAttributes.OrientationPitch = sPos.pitch;
			sensorAttributes.OrientationRoll =sPos.roll;
			List<SensorAttributes> sensors = new List<SensorAttributes>();
			sensors.Add (sensorAttributes);

			vehicle.UpdateSensorAttributes (sensors);			
		}

		/// <summary>
		/// Fires an event with the current sensor position
		/// </summary>
		public void SaveSensor() {
			if (SaveSensorPosition != null)
				SaveSensorPosition (GetSensorPosition ());
		}

		/// <summary>
		/// Fires an event that the user would like to cancel the sensor adjustment
		/// </summary>
		public void CancelPositioning() {
			if (CancelSensorPositioning != null)
				CancelSensorPositioning ();
		}

		/// <summary>
		/// Helper function to convert string to float - if invalid, output is 0
		/// </summary>
		private float ToFloat(string str) {
			float res = 0;
			str = str.Replace(",", ".");
			IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
			float.TryParse (str, NumberStyles.Float, provider, out res);
			return res;
		}

		/// <summary>
		/// Clears all the loaded data
		/// </summary>
		public void Clear(){
			windowController.HideErrorMessage();
			ClearVehicle();
			ClearRoad();
		}

		/// <summary>
		/// Removes the vehicle from the scene
		/// </summary>
		public void ClearVehicle(){
			if (vehicle != null) {
				GameObject.Destroy (vehicle.gameObject);
				Resources.UnloadUnusedAssets ();
				vehicle = null;
			}
		}

		/// <summary>
		/// Removes the testroad from the scene
		/// </summary>
		public void ClearRoad(){
			GameObject.Destroy (roadRenderer.GetRootObject ());
			Resources.UnloadUnusedAssets ();		
			loadedMap = null;
		}


		/// <summary>
		/// Increase the passed parameter field value by 0.01
		/// </summary>
		public void OnIncreaseSensorParam(TextField paramField){
			float value = ToFloat(paramField.value);
			value += 0.01f;
			paramField.value = value.ToString ("F2");
		}

		/// <summary>
		/// Decrease the passed parameter field value by 0.01
		/// </summary>
		public void OnDecreaseSensorParam(TextField paramField){
			float value = ToFloat(paramField.value);
			value -= 0.01f;
			paramField.value = value.ToString ("F2");
		}

		/// <summary>
		/// Event that is called, when one of the  DecreaseSensorParam call to SensorPositioner
		/// </summary>
		public void OnSensorParamChanged(){
			UpdateSensorVisualPosition ();
		}

		/// <summary>
		/// Event that is called, when the user clicks the save button in the sensor positioner
		/// </summary>
		public void OnClickSaveSensor(){
			SaveSensor();
		}

		/// <summary>
		/// Handle exceptions
		/// </summary>
		private void HandleError(string errorMessage, Exception ex){
			windowController.ShowErrorMessage(errorMessage, ex.Message);
			Clear ();
		}

        /// <summary>
        /// Called before the application is closed.
        /// </summary>
        private void OnApplicationQuit() {
			Clear ();
        }
    }
}