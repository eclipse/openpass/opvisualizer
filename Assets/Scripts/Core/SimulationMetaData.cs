/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
	
namespace Visualizer.Core
{
    public class SimulationMetaData {
		public string TraceFile { get; set; } = "";
        public string TraceFileExtension { get; set; } = "";
		public string SceneryFile { get; set; } = "";
		public string SceneryFileModDate { get; set; } = "";
        public string TraceFileModDate { get; set; } = "";
		public int RunID { get; set; } = -1;
		public string DriverPerceptionFile { get; set; } = "";
	}
}