﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Timers;
using Visualizer.Utilities;
using System;
using Visualizer.AgentHandling;
using Visualizer.TrafficSignHandling;
using Visualizer.Environment;
using Visualizer.UI;
using Zenject;

namespace Visualizer.Core
{
	public delegate void SampleUpdatedEventHandler(int timeStamp);

	public interface IAnimationController{
		event SampleUpdatedEventHandler SampleUpdated;
		event Action PlaybackStarted;
		event Action PlaybackStopped;
		event Action PlaybackPending;

		void Init ();
		void Clear ();
		bool IsPlaying ();
		void SwitchRepeatedPlaybackMode (bool enabled);
		void StartPlayback();
		int StopPlayback ();
		void JumpToTimeStamp (int timeStamp);
		void JumpToNextSample ();
		void JumpToPreviousSample ();
		void SetPlaybackSpeed (float speed);
	}

	/// <summary>
	/// Central class whose main responsibility is to trigger the agent updates in intervals that is defined by playback speed and sampling time.
	/// The animation logic is as follows: after a sampling time ellapses, the AnimationController loads the agent status data from the SampleContainer 
	/// at the specific timeStamp and passed it on to the the AgentManager. The AgentManager then distributes the data to the agents, 
	/// and the agents update themselfs.
	/// </summary>
	public class AnimationController : MonoBehaviour, IAnimationController {
		
		enum State { Playing, Pending, Stopped };

		public event SampleUpdatedEventHandler SampleUpdated;
		public event Action PlaybackStarted;
		public event Action PlaybackStopped;
		public event Action PlaybackPending;
		private State animState = State.Stopped; 
		private bool repeatPlaybackMode = false;

		private IWeatherController weatherController;
		private ISampleContainer sampleContainer;
		private IAgentManager agentManager;
		private ITrafficSignManager trafficSignManager;
		private IMainUI ui;
		private IDriverPerceptionManager driverPerceptionManager;
		private IJobHandler jobHandler;
		private int currentTimeStamp = 0; //the time stamp of the sample that is currently active on the screen
		private float playbackSpeed = 1f;
		private float animationInterval;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IJobHandler jHandler, ITrafficSignManager tsManager, IWeatherController wController, ISampleContainer sContainer, IAgentManager aManager, IMainUI mUi, IDriverPerceptionManager mmManager){
			weatherController = wController; 
			sampleContainer = sContainer;
			agentManager = aManager;
			driverPerceptionManager = mmManager;
			trafficSignManager = tsManager;
			jobHandler = jHandler;
			ui = mUi;
		}

		/// <summary>
		/// Initializes the AnimationController instance.
		/// </summary>
		public void Init(){
			currentTimeStamp = sampleContainer.GetFirstTimeStamp();
			RecalcAnimationInterval ();
			driverPerceptionManager.DriverPerceptionLoaded += new DriverPerceptionChangeHandler(OnDriverPerceptionLoaded);
		}

		/// <summary>
		/// Clears the AnimationController.
		/// </summary>
		public void Clear(){
			StopPlayback ();
			repeatPlaybackMode = false;
			currentTimeStamp = 0;
			playbackSpeed = 1f;
		}

		/// <summary>
		/// Determines whether the animation is currently running
		/// </summary>
		public bool IsPlaying(){
			return (animState != State.Stopped);
		}

		/// <summary>
		/// Turns the repeated playback mode on/off
		/// </summary>
		public void SwitchRepeatedPlaybackMode (bool enabled){
			repeatPlaybackMode = enabled;
		}

		/// <summary>
		/// 
		/// </summary>
		public void StopAnimation(){
			CancelInvoke ("Play");
			agentManager.StopAgentAnimation ();
			trafficSignManager.StopAnimation();
			weatherController.StopAnimation ();
		}

		/// <summary>
		/// 
		/// </summary>
		public void StartAnimation(){
			InvokeRepeating ("Play", animationInterval, animationInterval);
			agentManager.StartAgentAnimation (playbackSpeed);
			trafficSignManager.StartAnimation(playbackSpeed);
			weatherController.StartAnimation (playbackSpeed);
		}

		/// <summary>
		/// Starts the playback.
		/// </summary>
		public void StartPlayback(){
			if (animState != State.Playing) {
				StartAnimation();
				animState = State.Playing;
				if(PlaybackStarted != null)
					PlaybackStarted();
			}
		}

		/// <summary>
		/// Stops the playback.Returns the current time stamp
		/// </summary>
		public int StopPlayback(){
			if (animState != State.Stopped) {
				StopAnimation();
				animState = State.Stopped;
				if(PlaybackStopped != null)
					PlaybackStopped();
			}
			return currentTimeStamp;
		}

		/// <summary>
		/// Pauses the playback during loading
		/// </summary>
		private void InterruptPlayback(){
			if (animState == State.Playing) {
				StopAnimation();
				animState = State.Pending;
				if(PlaybackPending != null)
					PlaybackPending();
			}
		}

		/// <summary>
		/// Displays the sample at the specified time stamp
		/// </summary>
		public void JumpToTimeStamp(int timeStamp){
			DisplaySample (timeStamp);
		}

		/// <summary>
		/// Jumps to next sample.
		/// </summary>
		public void JumpToNextSample(){
			int nextTimeStamp = currentTimeStamp + sampleContainer.SamplingTime;
			if(nextTimeStamp <= sampleContainer.GetLastTimeStamp())
				DisplaySample(nextTimeStamp);	
		}

		/// <summary>
		/// Jumps to previous sample.
		/// </summary>
		public void JumpToPreviousSample(){
			int previousTimeStamp = currentTimeStamp - sampleContainer.SamplingTime;
			if(previousTimeStamp >= sampleContainer.GetFirstTimeStamp())
				DisplaySample (previousTimeStamp);	
		}

		/// <summary>
		/// Sets the playback speed. (1.0 = normal speed, 0.5 = half speed, 2.0 = double speed)
		/// </summary>
		public void SetPlaybackSpeed(float speed){
			playbackSpeed = speed;
			RecalcAnimationInterval ();
			if (animState == State.Playing) {
				StopPlayback ();
				StartPlayback ();
			}
		}

		/// <summary>
		/// Calculates the trigger interval depending on the sampling time and playback speed
		/// </summary>
		private void RecalcAnimationInterval(){
			animationInterval = sampleContainer.SamplingTime * (1/playbackSpeed) * 0.001f;
			agentManager.SetAnimationInterval (animationInterval);
			driverPerceptionManager.SetAnimationInterval (animationInterval);
		}

		/// <summary>
		/// Displays the next sample and stops if we get to the last sample.
		/// </summary>
		private void Play()
		{
			int nextTimeStamp = currentTimeStamp + sampleContainer.SamplingTime;
			//see if we are at the end of the trace file
			if (nextTimeStamp <= sampleContainer.GetLastTimeStamp ()) {
				DisplaySample (nextTimeStamp);
			} else {
				HandleEndOfSimulation ();
			}
		}

		/// <summary>
		/// Handles reaching the last sample
		/// </summary>
		private void HandleEndOfSimulation(){
			if (repeatPlaybackMode)
				currentTimeStamp = sampleContainer.GetFirstTimeStamp();
			else
				StopPlayback ();
		}

		/// <summary>
		/// Displays the sample at the specified timestamp
		/// Triggers background parsing, if a sample is not yet loaded
		/// </summary>
		private void DisplaySample(int timeStamp){
			AgentSample[] sample = sampleContainer.GetSample (timeStamp);
			if(sample == null) {
				RequestSample(timeStamp);
				if(animState == State.Playing)
					InterruptPlayback();
				return;
			} else if (animState == State.Pending) {
				StartPlayback();
			}

			agentManager.UpdateAgents (sample, timeStamp);
			trafficSignManager.UpdateSigns(timeStamp);
			driverPerceptionManager.UpdatePerceivedWorld (timeStamp);

			ui.RefreshPanelData();

			//fire an event -> update the GUI
			if (SampleUpdated != null)
				SampleUpdated (timeStamp);

			currentTimeStamp = timeStamp;

			jobHandler.LoadSampleRequestPreload(timeStamp);

			float sampleParsingTime = sampleContainer.CurrentSampleParsingTime;
			float workload = (sampleParsingTime / sampleContainer.SamplingTime*playbackSpeed) * 100f;
			ui.DisplayParserWorkload(workload);
		}

		/// <summary>
		/// 
		/// </summary>
		private void RequestSample(int timeStamp){
			//TODO: implement timeout mechanism
			jobHandler.LoadSampleRequest(timeStamp);
			Debug.LogWarning("Sample at timestamp " + timeStamp + " is not yet loaded. Async load requested, waiting...");
			this.InvokeDelayed(() => DisplaySample(timeStamp), 0.01f);
		}

		/// <summary>
		/// Event triggered, when an agent's driver perception has been loaded (triggered by the user asynchronously)
		/// </summary>
		private void OnDriverPerceptionLoaded(string fileName, string error) {
			driverPerceptionManager.SetAnimationInterval (animationInterval);
			driverPerceptionManager.UpdatePerceivedWorld (currentTimeStamp);
		}
	}
}