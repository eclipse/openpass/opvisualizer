﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Visualizer.UI;

namespace Visualizer.Core
{
	public interface IAppStorage {
		List<string> ReadDisplayOptions (string configKey);
		void WriteDisplayOptions(string configKey, List<DisplayOption> displayOptions);
		string Read(string configKey, string defaultValue = "");
		int ReadInt(string configKey, int defaultValue = 0);
		bool ReadBool(string configKey, bool defaultValue = false);
		void Write(string configKey, string value);
    }

	/// <summary>
	/// List of keys that are used to identify various settings in AppStorage
	/// </summary>
	public static class AppStorageKey
	{
		public const string LastSelectedPath = "LastSelectedPath";
		public const string SelectedNonFocusAgentDisplayOptions = "SelectedAgentDisplayOptions";
		public const string SelectedFocusAgentDisplayOptions = "SelectedFocusAgentDisplayOptions";
		public const string SelectedPerceivedAgentDisplayOptions = "SelectedPerceivedAgentDisplayOptions";
		public const string InfoPanelWidth = "InfoPanelWidth";
		public const string InfoPanelDividerPos = "InfoPanelDeviderPos";
		public const string FocusPanelDividerPos = "FocusPanelDeviderPos";
		public const string FocusPanelWidth = "FocusPanelWidth";
		public const string FocusPanelHeight = "FocusPanelHeight";
		public const string ShowCursorPosition = "ShowCursorPosition";
		public const string TerrainTexture = "TerrainTexture";
	}

	/// <summary>
	/// Stores and accesses application information that should consist between application runs. 
	/// Using Unity's built PlayerPrefs mechanism, this storage system is plattform independent.
	/// </summary>
    public class AppStorage: IAppStorage
    {

		public List<string> ReadDisplayOptions (string configKey) {
			if (!PlayerPrefs.HasKey (configKey))
				return new List<string>();
	
			string serializedItemList = PlayerPrefs.GetString (configKey);
			return DeserializeCSVList (serializedItemList);
		}

		public void WriteDisplayOptions(string configKey, List<DisplayOption> displayOptions) {
			List<string> selectedKeys = new List<string>();
			foreach(DisplayOption option in displayOptions)
                if(option.Enabled)
					selectedKeys.Add(option.Key);

			PlayerPrefs.SetString(configKey, SerializeCSVList(selectedKeys));
			PlayerPrefs.Save ();
		}

		public string Read(string configKey, string defaultValue = ""){
			string value = defaultValue;
			if (PlayerPrefs.HasKey (configKey))
				value = PlayerPrefs.GetString (configKey);
			return value;
		}

		public int ReadInt(string configKey, int defaultValue = 0){
			string value = Read(configKey, defaultValue.ToString());
			int valueInt;
			bool success = int.TryParse(value, out valueInt);
			return success ? valueInt : defaultValue;
		}

		public bool ReadBool(string configKey, bool defaultValue = false){
			string value = Read(configKey, defaultValue.ToString());
			bool valueBool;
			bool success = bool.TryParse(value, out valueBool);
			return success ? valueBool : defaultValue;
		}

		public void Write(string configKey, string value){
			PlayerPrefs.SetString(configKey, value);
			PlayerPrefs.Save ();
		}

		private List<string> DeserializeCSVList(string serializedList){
			return new List<string> (serializedList.Split (','));
		}

		private string SerializeCSVList(List<string> list){
			return string.Join (",", list.ToArray ());;
		}
	}
}