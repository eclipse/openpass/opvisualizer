﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Visualizer.Utilities;
using Visualizer.Core;
using Visualizer.AgentHandling;
using Visualizer.Environment;
using Visualizer.TrafficSignHandling;
using Zenject;

namespace Visualizer.Trace
{
	/// <summary>
	/// Version constants 
	/// </summary>
	static class Version
	{
		public const string SupportedSchema = "0.3.1";
	}

	/// <summary>
	/// Xml tags that the TraceFileLoader reads.
	/// </summary>
	static class XmlTags
	{
		public const string SimulationOutput = "SimulationOutput";
		public const string SchemaVersion = "SchemaVersion";
		public const string SceneryFile = "SceneryFile";
		public const string RunId = "RunId";
		public const string RunResult = "RunResult";
		public const string Sample = "Sample";
		public const string CyclicsFile = "CyclicsFile";
		public const string Cyclics = "Cyclics";
		public const string Agents = "Agents";
		public const string Agent = "Agent";
		public const string ID = "Id";
		public const string AgentTypeGroupName = "AgentTypeGroupName";
		public const string VehicleModelType = "VehicleModelType";
		public const string VehicleAttributes = "VehicleAttributes";
		public const string VehicleWidth = "Width";
		public const string VehicleHeight = "Height";
		public const string VehicleLength = "Length";
		public const string VehiclePivotOffset = "LongitudinalPivotOffset";
		public const string Header = "Header";
		public const string Time = "Time";
		public const string EventType = "Name"; //changed in schema version 0.2.1!
		public const string Environment = "Environment";
		public const string Weather = "Weather";
		public const string WeatherZone = "WeatherZone";
		public const string VisibilityDistance = "VisibilityDistance";
		public const string TriggeringEntities = "TriggeringEntities";
		public const string AffectedEntities = "AffectedEntities";
		public const string Entity = "Entity";
		public const string Condition = "Condition";
		public const string CollisionEvent = "Collision";
		public const string Key = "Key";
		public const string Value = "Value";
		public const string Event = "Event";
		public const string EventParameter = "Parameter"; //changed in schema version 0.3.0!
		public const string X = "X";
		public const string Y = "Y";
		public const string SizeX = "SizeX";
		public const string SizeY = "SizeY";
		public const string Sensor = "Sensor";
		public const string SensorModel = "Type";
		public const string SensorName = "Name";
		public const string SensorOrientationYaw = "OrientationYaw";
		public const string SensorOrientationPitch = "OrientationPitch";
		public const string SensorOrientationRoll = "OrientationRoll";
		public const string SensorOpeningAngleH = "OpeningAngleH";
		public const string SensorOpeningAngleV = "OpeningAngleV";
		public const string SensorDetectionRange = "Range";
		public const string SensorMountingPosLongitudinal = "MountingPosLongitudinal";
		public const string SensorMountingPosLateral = "MountingPosLateral";
		public const string SensorMountingPosHeight = "MountingPosHeight";
	}

	public interface ITraceFileLoader {
		void Init();
		void LoadSample(int timeStamp);
		bool LoadRunFromDisk (string filePath, int runID, bool extendedTrace, bool validateHeader = true);
		bool LoadRunFromStream (string fileContent, int runID);
		List<Run> ReadRuns (string fileName);
		string ReadSceneryFileName (string traceFilePath);
	}

	/// <summary>
	/// Loads an xml trace file into the internal data structure
	/// </summary>
	public class TraceFileLoader : ITraceFileLoader {

		private IWeatherController weatherController;
		private ISampleContainer sampleContainer;
		private IAgentManager agentManager;
		private ITrafficSignManager trafficSignManager;
		private ITrafficSignStateProvider trafficSignStateProvider;
		private ISampleAttributeMapper sampleAttributeMapper;
		private IJobHandler jobHandler;
		private const string defaultSceneryFile = "./sceneryConfiguration.xml";
		private XmlReaderSettings readerSettings = new XmlReaderSettings() { CloseInput = true };
		CyclicsParserBase cyclicsParser = null;		
		CyclicsParserBase cyclicsParserExt = null;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IJobHandler jHandler, ISampleAttributeMapper saMapper, ITrafficSignStateProvider tssProvider, ITrafficSignManager tsManager, IWeatherController wController, ISampleContainer sContainer, IAgentManager aManager){
			weatherController = wController;
			sampleContainer = sContainer;
			agentManager = aManager;
			trafficSignManager = tsManager;
			trafficSignStateProvider = tssProvider;
			sampleAttributeMapper = saMapper;
			jobHandler = jHandler;
		}

		/// <summary>
		/// Initializes the instance
		/// </summary>
		public void Init(){
			sampleAttributeMapper.Init();
			jobHandler.Init();
			cyclicsParser = null;		
			cyclicsParserExt = null;
		}

		/// <summary>
		/// Reads a single run from the given trace file path.
		/// </summary>
		public bool LoadRunFromDisk(string filePath, int runID, bool extendedTrace, bool validateHeader = true){
			using (XmlReader xmlReader = XmlReader.Create(filePath, readerSettings))
			{
				return LoadRun (xmlReader, runID, filePath, extendedTrace, validateHeader);
			}
		}

		/// <summary>
		/// Reads a single run from the given trace file content.
		/// </summary>
		public bool LoadRunFromStream(string fileContent, int runID){
			using (XmlReader xmlReader = XmlReader.Create(new StringReader (fileContent), readerSettings))
			{
				return LoadRun (xmlReader, runID);
			}
		}

		/// <summary>
		/// Reads the content of the SceneryFile root element in the trace file
		/// </summary>
		public string ReadSceneryFileName(string traceFilePath) {
			using (XmlReader xmlReader = XmlReader.Create(traceFilePath, readerSettings))
			{
				while (xmlReader.Read ()) {
					if (xmlReader.IsStartElement () && xmlReader.Name.ToString () == XmlTags.SceneryFile)
						return xmlReader.ReadElementContentAsString();
				}
				Debug.LogWarning ("Tag '" + XmlTags.SceneryFile + "' not found in the trace file. Assuming default scenery file '" + defaultSceneryFile + "'.");
				return defaultSceneryFile;
			}
		}

		/// <summary>
		/// Reads a single run from the given trace file.
		/// </summary>
		private bool LoadRun(XmlReader xmlReader, int runID, string traceFilePath = "", bool extendedTrace = false, bool validateHeader = true){
			IXmlLineInfo xli = (IXmlLineInfo)xmlReader;
			while(xmlReader.Read())
			{
				if (xmlReader.IsStartElement())
				{
					switch (xmlReader.Name.ToString())
					{
					case XmlTags.SimulationOutput:
						string schemaVersion = xmlReader.GetAttribute (XmlTags.SchemaVersion);
						if (schemaVersion == "" || schemaVersion == null) {
							Debug.LogWarning ("No schema version found in trace file - will try to parse trace file as schema version " + Version.SupportedSchema);
						} else if (schemaVersion != Version.SupportedSchema) {
							throw new ArgumentException ("Trace file schema version " + schemaVersion + " is not supported. Supported schema version is " + Version.SupportedSchema); 
						}
						break;
					case XmlTags.RunResult:
						string id = xmlReader.GetAttribute (XmlTags.RunId);
						if (id == null)
							throw new ArgumentException ("Invalid trace file. Missing Tag '" + XmlTags.RunId + "' (Line " + xli.LineNumber + ")"); 
						if (Convert.ToInt64 (id) == runID) {
							XElement runResult = XElement.Load (xmlReader.ReadSubtree ());
							ReadRunResult (runResult, traceFilePath, extendedTrace, validateHeader);
							return true;
						}
						break;
					}
				}	           
			}
			throw new Exception("Run ID '" + runID + "' not found in the trace file."); 
		}

		/// <summary>
		/// Reads the list of runs in the specified file
		/// </summary>
		public List<Run> ReadRuns(string fileName){
			using (XmlReader xmlReader = XmlReader.Create(fileName, readerSettings))
			{
				IXmlLineInfo xli = (IXmlLineInfo)xmlReader;
				List<Run> runs = new List<Run> ();
				int eventCount = 0;
				int runID = -1;
				while(xmlReader.Read())
				{
					if (xmlReader.IsStartElement())
					{
						switch (xmlReader.Name.ToString())
						{
						case XmlTags.RunResult:
							if (runID != -1)
								runs.Add (new Run(runID, eventCount));

							if(!int.TryParse(xmlReader.GetAttribute (XmlTags.RunId), out runID))
								throw new ArgumentException ("Invalid trace file. Missing Tag '" + XmlTags.RunId + "' (Line " + xli.LineNumber + ")"); 

							eventCount = 0;
							break;
						case XmlTags.Event:
							
							eventCount++;
							break;
						}
					}           
				}

				//add the last run also
				if (runID != -1)
					runs.Add (new Run(runID, eventCount));
				
				return runs;
			}
		}

		/// <summary>
		/// Reads the RunResult section of the trace file. This is the main section with the simulation data.
		/// Note that at this point we only initialize the lazy parsing, that is prepare for the  
		/// </summary>
		private void ReadRunResult(XElement runResult, string traceFilePath, bool extendedTrace, bool validateHeader){
			EnvironmentInfo environment = ReadEnvironment (runResult);
			List<AgentProperties> agentProps = ReadAgents(runResult, environment);

			//if extendedTrace is set, we are merging multiple trace files
			//In this case we do not need to spawn agents again, just add the samples to the existing ones in the container
			if(!extendedTrace)
				agentManager.SpawnAgents (agentProps);
			else
				agentManager.ExtendStaticAgentProps (agentProps);

			InitCyclicsParsing (runResult, agentProps, traceFilePath, extendedTrace, validateHeader);	

			ReadEvents (runResult);
		}
			
		/// <summary>
		/// Reads the Agent section of the trace file into a list of AgentProperties structure. 
		/// AgentProperties contains the static agent attributes that are partially
		/// interpreted (see e.g. VehicleModelName or VehicleInfo) and partially isn't (see NonInterpretedProps)
		/// </summary>
		private List<AgentProperties> ReadAgents(XElement runResult, EnvironmentInfo environment){
			List<AgentProperties> agentProps = new List<AgentProperties> ();

			//read the agents from the XML
			IEnumerable<XElement> xmlAgents = 
				from el in runResult.Descendants (XmlTags.Agent)
				select el;

			//add info to the list of agent properties
			foreach (XElement xmlAgent in xmlAgents) {
				AgentProperties agentProp = new AgentProperties ();

				agentProp.TypeGroupName = GetXmlAttrStr(xmlAgent, XmlTags.AgentTypeGroupName, true);		
				agentProp.ID = (int)GetXmlAttrLong(xmlAgent, XmlTags.ID);
				agentProp.VehicleModelName = GetXmlAttrStr(xmlAgent, XmlTags.VehicleModelType, true);

				XElement xmlVehicleAttr = xmlAgent.Element (XmlTags.VehicleAttributes);
				if(xmlVehicleAttr != null){
					agentProp.VehicleInfo = new VehicleAttributes {
						Width = GetXmlAttrFloat(xmlVehicleAttr, XmlTags.VehicleWidth, 0f, 4f, true),
						Height = GetXmlAttrFloat(xmlVehicleAttr, XmlTags.VehicleHeight, 0f, 5f, true),
						Length = GetXmlAttrFloat(xmlVehicleAttr, XmlTags.VehicleLength, 0f, 20f, true),
						PivotOffset = GetXmlAttrFloat(xmlVehicleAttr, XmlTags.VehiclePivotOffset, -20f, 20f, true)
					};
				}
				agentProp.Sensors = ReadAgentSensors(xmlAgent);
				agentProp.DriverViewDistance = environment.VisibilityDistance;
				agentProp.NonInterpretedProps = ReadAllStaticAgentProps(xmlAgent);
				agentProps.Add (agentProp);
			}

			return agentProps;
		}

		/// <summary>
		/// Reads the Agent section of the trace file.
		/// It dumps all attributes to a KeyValue-List, where the key contains the full hierarchy in the form of namespaces.
		/// Example: Agent.Sensors.Sensor 2.Range. When a list of elements contain multiple elements with the same child name
		/// a counter is added to the name space level. See "Sensor 2" in the above example. 
		/// </summary>
		private List<GenericListItem> ReadAllStaticAgentProps(XElement xmlAgent){
			List<GenericListItem> allAttributes = new List<GenericListItem>();
			ReadXMLAttributesRecursive(xmlAgent, allAttributes, "");
			return allAttributes;
		}

		/// <summary>
		/// Reads all attributes from the given XML-Element without knowing the file structure.
		/// </summary>
		private void ReadXMLAttributesRecursive(XElement xElement, List<GenericListItem> attributes, string currentNameSpace)
		{
			foreach (var xAttribute in xElement.Attributes()){
				string key = AppendNameToNameSpace(currentNameSpace, xAttribute.Name.ToString());
				GenericListItem item = new GenericListItem(key, xAttribute.Value);
				attributes.Add(item);
			}
			int identicalElemCounter = 0;
			foreach (var childElement in xElement.Elements()){
				string nameSpace = AppendNameToNameSpace(currentNameSpace, childElement.Name.LocalName);
				if(HasIdentialChildElements(xElement.Elements(), childElement.Name.LocalName))
					nameSpace += " " + (++identicalElemCounter).ToString();
				ReadXMLAttributesRecursive(childElement, attributes, nameSpace);
			}
		}

		/// <summary>
		/// Returns true if the given list of XML-Element contians multiple children with the same name.
		/// </summary>
		private bool HasIdentialChildElements(IEnumerable<XElement> elements, string childName){
			int count = elements.Where(e => e.Name.LocalName == childName)
						.Select(e => e)
						.Count();
			return count > 1;
		}

		/// <summary>
		/// Appends a string to the end of the given namespace.
		/// </summary>
		private string AppendNameToNameSpace(string origNameSpace, string name) {
			string newNameSpace = origNameSpace;
			if(newNameSpace.Length > 0)
				newNameSpace += ".";
			newNameSpace += name;
			return newNameSpace;
		}

		/// <summary>
		/// Reads the Run Statistic info from the trace file.
		/// </summary>
		/// <param name="runResult">The 'RunResult' XMLTag</param>
		private EnvironmentInfo ReadEnvironment(XElement runResult){
			ReadWeather (runResult);

			EnvironmentInfo environment = new EnvironmentInfo ();
			try{
				string visibilityDistance = (string) (from el in runResult.Descendants (XmlTags.VisibilityDistance) select el).First ();
				IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
				environment.VisibilityDistance = float.Parse (visibilityDistance, NumberStyles.Float, provider);
			} catch (Exception ex){
				Debug.LogWarning ("Invalid/No visibility distance in trace file. Details: " + ex.Message);
			}
			return environment;
		}

		/// <summary>
		/// Reads the sensors of an agent. Returns a list of sensor attributes.
		/// </summary>
		/// <param name="xmlAgent">The 'Agent' XMLTag</param>
		private List<SensorAttributes> ReadAgentSensors(XElement xmlAgent){
			List<SensorAttributes> sensors = new List<SensorAttributes>();

			//read the agents from the XML
			IEnumerable<XElement> xmlSensors = 
				from sensor in xmlAgent.Descendants (XmlTags.Sensor)
				select sensor;

			foreach (XElement xmlSensor in xmlSensors) { 
				SensorAttributes sensor = new SensorAttributes ();
				sensor.Model = xmlSensor.Attribute (XmlTags.SensorModel).Value.Trim ();
				if (xmlSensor.Attribute (XmlTags.SensorName) != null)
					sensor.Name = xmlSensor.Attribute (XmlTags.SensorName).Value.Trim ();
			
				float mountingPosX = GetXmlAttrFloat(xmlSensor, XmlTags.SensorMountingPosLongitudinal, -20, 20);
				float mountingPosY = GetXmlAttrFloat(xmlSensor, XmlTags.SensorMountingPosHeight, 0, 8);
				float mountingPosZ = GetXmlAttrFloat(xmlSensor, XmlTags.SensorMountingPosLateral, -5, 5);
				sensor.MountingPosition = new Vector3 (mountingPosX, mountingPosY, mountingPosZ);
				sensor.OpeningAngleH = GetXmlAttrFloat(xmlSensor, XmlTags.SensorOpeningAngleH, 0, 2*(float)Math.PI, true);
				sensor.OpeningAngleV = GetXmlAttrFloat(xmlSensor, XmlTags.SensorOpeningAngleV, 0, (float)Math.PI, true);
				sensor.DetectionRange = GetXmlAttrFloat(xmlSensor, XmlTags.SensorDetectionRange, 0, 1000);
				sensor.OrientationPitch = GetXmlAttrFloat(xmlSensor, XmlTags.SensorOrientationPitch, -(float)Math.PI, (float)Math.PI);
				sensor.OrientationYaw = GetXmlAttrFloat(xmlSensor, XmlTags.SensorOrientationYaw, -(float)Math.PI, (float)Math.PI);
				sensor.OrientationRoll = GetXmlAttrFloat(xmlSensor, XmlTags.SensorOrientationRoll, -(float)Math.PI, (float)Math.PI);

				sensors.Add (sensor);
			}
			return sensors;
		}

		/// <summary>
		/// Prepares the simulation data reader for lazy parsing.
		/// </summary>
		/// <param name="runResult">The 'RunResult' XMLTag</param>
		public void InitCyclicsParsing(XElement runResult, List<AgentProperties> agentProps, string traceFilePath, bool extendedTrace, bool validateHeader){
			if(extendedTrace && runResult.Elements(XmlTags.Cyclics).Any() == false)
				return;

			CyclicsParserBase cParser;
			bool isSampleFileExternal = runResult.Elements(XmlTags.Cyclics).Elements(XmlTags.CyclicsFile).Any();

			if(isSampleFileExternal){
				string cyclicsFilePath = GetCyclicsFileFromRunResult(runResult, traceFilePath);
				cParser = new CyclicsParserCSV(cyclicsFilePath);
			}
			else{
				cParser = new CyclicsParserXML(runResult);
			}
			
			if(validateHeader)
				cParser.ValidateHeader(agentProps);
			
			sampleContainer.AgentAttributes.AddRange(cParser.GetAgentAttributesFromHeader());
			if(!extendedTrace){
				cyclicsParser = cParser;
				sampleContainer.SamplingTime = cParser.GetSamplingTime();
			} else {
				cyclicsParserExt = cParser;
			}

			/// Load the first and the last timestamp to assure that
			/// sampleContainer.GetFirstTimeStamp() and sampleContainer.GetLastTimeStamp()
			/// deliver correct results
			int firstTimeStamp = cyclicsParser.GetFirstTimeStamp();
			int lastTimeStamp = cyclicsParser.GetLastTimeStamp();
			jobHandler.LoadSampleRequest(firstTimeStamp, true);
			jobHandler.LoadSampleRequest(lastTimeStamp, true); 

		}

		/// <summary>
		/// Reads a single sample at the given timestamp into the sample container.
		/// </summary>
		public void LoadSample(int timeStamp){
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			if(cyclicsParser != null)
				cyclicsParser.ParseSample(timeStamp, sampleContainer, new SampleAttributes(), sampleAttributeMapper);
			if(cyclicsParserExt != null)
				cyclicsParserExt.ExtendSample(timeStamp, sampleContainer, sampleAttributeMapper);
			sampleContainer.CurrentSampleParsingTime = stopwatch.Stop();
		}


        /// <summary>
		/// Composes the file path of the CSV file based on the trace file's path
		/// </summary>
		private string GetCyclicsFileFromRunResult(XElement runResult, string traceFilePath){
			string csvSampleFileName = runResult.Element (XmlTags.Cyclics).Element (XmlTags.CyclicsFile).Value;
			string filePath = Path.GetDirectoryName(traceFilePath) + "/" + csvSampleFileName;
            
            if (!File.Exists(filePath))
				throw new Exception ("The cyclics file " + filePath + " could not be found.");

            return filePath;
        }

		/// <summary>
		/// Reads the content of the 'Accidents' tag. These will become the time marks on the time axis
		/// </summary>
		/// <param name="runResult">The RunResult XMLTag</param>
		private void ReadEvents(XElement runResult){
			//read the accidents from the XML to a list
			IEnumerable<XElement> events =  
				from el in runResult.Descendants(XmlTags.Event)  
				select el;

			//add events to SampleContainer with the data we read from the XML
			foreach (XElement eventElement in events) {

				SimulationEvent simEvent = new SimulationEvent();
				simEvent.eventType = GetXmlAttrStr(eventElement, XmlTags.EventType);
				simEvent.timeStamp = (int)GetXmlAttrLong(eventElement, XmlTags.Time, 0);
				simEvent.eventParameters = ReadEventParameters(eventElement);
				simEvent.triggeringEntities = ReadEventEntities (eventElement.Element(XmlTags.TriggeringEntities));
				simEvent.affectedEntities = ReadEventEntities (eventElement.Element(XmlTags.AffectedEntities));

				if(simEvent.eventType == "TrafficLight") {
					trafficSignStateProvider.AddEvent(simEvent);
				} else {
					MarkCollidingAgents (simEvent);
					sampleContainer.AddEvent(simEvent);
				}
			}


		}

		/// <summary>
		/// Reads the triggering or affected entities of an event
		/// </summary>
		private List<long> ReadEventEntities(XElement entitiesElement){

			IEnumerable<XElement> entitiesElements =  
				from el in entitiesElement.Descendants(XmlTags.Entity) 
				select el;

			List<long> entities = new List<long>();
			foreach (XElement el in entitiesElements) {
				entities.Add(GetXmlAttrLong(el, XmlTags.ID, 0));
			}
			return entities;
		}

		/// <summary>
		/// Returns the EventParameters of an event in the trace file as dictionary of strings.
		/// </summary>
		/// <param name="eventElement">The 'Event' XMLTag</param>
		private Dictionary<string, string> ReadEventParameters(XElement eventElement){
			//read the eventparameters of an event
			IEnumerable<XElement> eventParameterElements =  
				from el in eventElement.Descendants(XmlTags.EventParameter) 
				select el;

			Dictionary<string, string> eventParameters = new Dictionary<string, string>();
			//let AgentManager mark the ego agents
			foreach (XElement el in eventParameterElements) {
				try{
					string key = el.Attribute (XmlTags.Key).Value.Trim();
					string value = el.Attribute (XmlTags.Value).Value.Trim ();
					eventParameters.Add (key, value);
				}
				catch(Exception ex){
					throw new Exception("Invalid EventParamters Tag." + ex.Message);
				}
			}

			return eventParameters;
		}

		/// <summary>
		/// Sets the crash sample index of all agents that are involved in this event
		/// </summary>
		private void MarkCollidingAgents(SimulationEvent simEvent){
			if (simEvent.eventType == XmlTags.CollisionEvent) {
				foreach (int agentId in simEvent.triggeringEntities) {
					agentManager.UpdateCrashTimeStamp (agentId, simEvent.timeStamp);
				}

				foreach (int agentId in simEvent.affectedEntities) 
				{
					agentManager.UpdateCrashTimeStamp (agentId, simEvent.timeStamp);
				}
			}
		}

		/// <summary>
		/// Reads the weather info from the trace file.
		/// </summary>
		/// <param name="runResult">The 'RunResult' XMLTag</param>
		private void ReadWeather(XElement runResult){
			//WeatherController weatherController = GameObject.Find ("WeatherController").GetComponent<WeatherController>();

			//read the default weather
			try{
				string globalWeather = runResult.Element (XmlTags.Environment).Element (XmlTags.Weather).Attribute (XmlTags.Condition).Value;
				WeatherCondition weather = (WeatherCondition) Enum.Parse (typeof(WeatherCondition), globalWeather);
				weatherController.SetGlobalWeather (weather);
			} catch (Exception ex){
				Debug.LogWarning ("Invalid/No weather info in trace file. Details: " + ex.Message);
			}

			//read the weather zones from the XML to a list
			IEnumerable<XElement> weatherZones =  
				from el in runResult.Descendants(XmlTags.WeatherZone) 
				select el;

			//pass the zones to WeatherController
			foreach (XElement el in weatherZones) {
				try{
					WeatherZone zone = new WeatherZone ();
					IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
					float x = float.Parse (el.Attribute (XmlTags.X).Value, NumberStyles.Float, provider);
					float y = float.Parse (el.Attribute (XmlTags.Y).Value, NumberStyles.Float, provider);
					float sizeX = float.Parse (el.Attribute (XmlTags.SizeX).Value, NumberStyles.Float, provider);
					float sizeY = float.Parse (el.Attribute (XmlTags.SizeY).Value, NumberStyles.Float, provider);
					string condition = el.Attribute (XmlTags.Condition).Value;
					zone.Area = new Rect (x, y, sizeX, sizeY);
					zone.Condition = (WeatherCondition) Enum.Parse (typeof(WeatherCondition), condition);
					weatherController.AddWeatherSection (zone);
				} catch (Exception ex){
					//display a warning but continue loading
					Debug.LogWarning ("Invalid weather zone info in trace file. Details: " + ex.Message);
				}
			}
		}

		/// <summary>
		/// Reads the value of the given xml tag attribute as float.
		/// </summary>
		private float GetXmlAttrFloat(XElement xmlTag, string attrName, float min = -999999999999, float max = 999999999999, bool optional = false){
			float value = 0;

			if (xmlTag.Attribute (attrName) == null) {
				if (optional)
					return value;
				else
					throw new Exception ("Missing attribute " + attrName + " in tag " + xmlTag.Name);
			}

			IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
			if (!float.TryParse (xmlTag.Attribute (attrName).Value.Trim (), NumberStyles.Float, provider, out value))
				throw new Exception ("Invalid value in attribute " + attrName + " (" + xmlTag.Attribute (attrName).Value + ")");

			if (value > max || value < min)
				throw new Exception ("Value of attribute '" + attrName + "' is out of range. Parsed value is " + value.ToString() + ", but it should be in the range [" + min.ToString() + "]...[" + max.ToString() + "]");

			return value;
		}

		/// <summary>
		/// Reads the value of the given xml tag attribute as int.
		/// </summary>
		private long GetXmlAttrLong(XElement xmlTag, string attrName, long min = -999999999999, long max = 999999999999, bool optional = false){
			long value = 0;

			if (xmlTag.Attribute (attrName) == null) {
				if (optional)
					return value;
				else
					throw new Exception ("Missing attribute " + attrName + " in tag " + xmlTag.Name);
			}

			IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
			if (!long.TryParse (xmlTag.Attribute (attrName).Value.Trim (), NumberStyles.Integer, provider, out value))
				throw new Exception ("Invalid value in attribute " + attrName + " (" + xmlTag.Attribute (attrName).Value + ")");

			if (value > max || value < min)
				throw new Exception ("Value of attribute '" + attrName + "' is out of range. Parsed value is " + value.ToString() + ", but it should be in the range [" + min.ToString() + "]...[" + max.ToString() + "]");

			return value;
		}

		/// <summary>
		/// Reads the value of the given xml tag attribute as string or throws an exception if the attribute is not found.
		/// </summary>
		private string GetXmlAttrStr(XElement xmlTag, string attrName, bool optional = false){
			string value = "";
			if (xmlTag.Attribute (attrName) == null) {
				if (optional)
					return value;
				else
					throw new Exception ("Missing attribute '" + attrName + "' in tag '" + xmlTag.Name + "'");
			}
			return xmlTag.Attribute (attrName).Value.Trim ();
		}
	}
}