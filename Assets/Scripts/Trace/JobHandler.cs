﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Visualizer.Utilities;
using Visualizer.Trace;
using Zenject;

namespace Visualizer.Core
{
	public interface IJobHandler{
		void Init();
		void Clear();
		void LoadSampleRequest(int timeStamp, bool forceLoad = false);
		void LoadSampleRequestPreload(int timeStamp);
	}

	/// <summary>
	/// A single instance class that handles samples that are loaded asynchronously from the trace file.
	/// </summary>
	public class JobHandler : IJobHandler {

		private ConcurrentQueue<int> loadSampleRequestQueue = new ConcurrentQueue<int>();
		private Thread jobThread = null;
		private ISampleContainer sampleContainer;
		private ITraceFileLoader traceFileLoader;
		private const int bufferSize = 10; //the number of samples to preload in advance

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ITraceFileLoader tfLoader, ISampleContainer sContainer){
			traceFileLoader = tfLoader;
			sampleContainer = sContainer;
		}

		/// <summary>
		/// Clear this instance.
		/// </summary>
		public void Clear(){
			loadSampleRequestQueue.Clear ();
			if(jobThread != null)
				jobThread.Abort();
		}

		/// <summary>
		/// Initializes the job handler.
		/// </summary>
		public void Init(){
			Clear ();
		
			jobThread = new Thread ( () => ProcessBackgroundLoad() );
			jobThread.IsBackground = true;
  			jobThread.Start();
		}


		/// <summary>
		/// Check if the loading of a specific sample has already been requested.
		/// </summary>
		private bool IsSampleLoading(int timeStamp){
			return (loadSampleRequestQueue.Contains(timeStamp) || sampleContainer.GetSample(timeStamp) != null);
		}

		/// <summary>
		/// Public interface to request the loading of a sample from the trace file at the given time stamp.
		/// Can be called from another thread without blocking it. The loading process is asyncronous. 
		/// </summary>
		public void LoadSampleRequest(int timeStamp, bool forceLoad = false){	
			if(!IsSampleLoading(timeStamp) || forceLoad)
				loadSampleRequestQueue.Enqueue(timeStamp);
		}

		/// <summary>
		/// Public interface to request the loading a multiple samples ahaed 
		/// The number of preloaded samples is determined by BUFFERSIZE.
		/// Can be called from another thread without blocking it. The loading process is asyncronous.  
		/// </summary>
		public void LoadSampleRequestPreload(int timeStamp){
			for (int offset = 0; offset<=bufferSize; offset++) {
				int timeStampToLoad = timeStamp + offset*sampleContainer.SamplingTime;
				if(timeStampToLoad <= sampleContainer.GetLastTimeStamp() && timeStampToLoad >= sampleContainer.GetFirstTimeStamp())
					if(!IsSampleLoading(timeStampToLoad))
						loadSampleRequestQueue.Enqueue(timeStampToLoad);
			}

		}

		/// <summary>
		/// Background thread that pulls loading jobs from the job queue and executes them
		/// </summary>
		private void ProcessBackgroundLoad(){
			do {
				int timeStamp;
				if (loadSampleRequestQueue.TryPeek(out timeStamp)){
					try {
						traceFileLoader.LoadSample(timeStamp);
					}catch (Exception ex) {
						Debug.LogError ("Error loading sample " + timeStamp + ": " + ex.Message);
					}
					loadSampleRequestQueue.TryDequeue(out timeStamp);
				} else {
					Thread.Sleep(1);
				}
			} while(true);
		}
	}
}