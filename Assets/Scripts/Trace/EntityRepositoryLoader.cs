﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Visualizer.Trace;
using Zenject;

namespace Visualizer.EntityRepository
{
	/// <summary>
	/// Version constants 
	/// </summary>
	static class Version
	{
		public const string SupportedSchema = "0.3.0";
	}


	public interface IEntityRepositoryLoader {
		void LoadFromDisk (int runID, string basePath);
		void LoadFromStream (string fileContent);
	}

	/// <summary>
	/// Loads an xml trace file into the internal data structure
	/// </summary>
	public class EntityRepositoryLoader : IEntityRepositoryLoader {

		private IEntityRepository entityRepository;
		private const string peristentRepositoryFile = "/Repository_Persistent.csv";
		private const string runRepositoryFile = "/Repository_Run_#.csv";

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IEntityRepository eRepository){
			entityRepository = eRepository;
		}

		/// <summary>
		/// Loads the content of the global and run specific repository CSV files into the internal entityRepository.
		/// </summary>
		public void LoadFromDisk(int runID, string basePath){
			//read persistent repository into the entity repo
			string globalFile = basePath + peristentRepositoryFile;
			if (File.Exists(globalFile)) {
				List<string> CSVList = FileReader.ReadAllLinesSafe(globalFile);
				ReadRepo(CSVList);
			}

			//read run specific repository into the entity repo
			string runFile = basePath + runRepositoryFile.Replace("#", runID.ToString("D3"));
			if (File.Exists(runFile)) {
				List<string> CSVList = FileReader.ReadAllLinesSafe(runFile);
				ReadRepo(CSVList);
			}
		}

		/// <summary>
		/// Reads the repository for a specific run from the given file content.
		/// </summary>
		public void LoadFromStream(string fileContent){
			//To be implemented
		}

		/// <summary>
		/// Reads the content of the given repository and fills it into the internal entityRepository.
		/// <param name="CSVList">String array that contains the csv formatted entity data</param>
		/// </summary>
		private void ReadRepo(List<string> CSVList){

		    //the first line of the csv is the header (sample attributes)
			List<string> header = ReadHeader (CSVList.First()); 
			
			//remove the header from the list
			CSVList.RemoveAt(0);

			//fill EntityRepository with the data
			foreach (string entityCSV in CSVList) {

				//create a sample parser that will help us to get the values fast from a line of the entity repo 
				SampleParser csvParser = new SampleParser (header, entityCSV, ';');
				Entity entity = new Entity();

				entity.id = csvParser.GetAttributeAsInt (-1, "id");
				entity.secondaryId = csvParser.GetAttributeAsStr (-1, "secondary id");
				entity.group = csvParser.GetAttributeAsStr (-1, "group");
				entity.version = csvParser.GetAttributeAsStr (-1, "version");
				entity.name = csvParser.GetAttributeAsStr (-1, "name");
				entity.type = csvParser.GetAttributeAsStr (-1, "type");
				entity.subtype = csvParser.GetAttributeAsStr (-1, "subtype");

				entityRepository.AddEntity (entity);
			}
		}
        
		/// <summary>
		/// Reads the repository header
		/// </summary>
		private List<string> ReadHeader(string headerLine){
			return headerLine.Split (';').Where(s => !string.IsNullOrWhiteSpace(s)).Select (x => x.Trim ()).ToList<string> ();
		}	
	}
}