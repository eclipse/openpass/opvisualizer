/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Xml;
using System.Xml.Linq;
using Visualizer.AgentHandling;
using Visualizer.Core;

namespace Visualizer.Trace
{
	/// <summary>
	/// Helper class that can read the cyclics of the XML trace file. The cyclics are internal part of the trace file.
	/// </summary>
	public class CyclicsParserXML : CyclicsParserBase {

		/// <summary>
		/// Initializes a new instance of the class and reads the cyclics XML tag into simple string lists 
		/// This step is fairly performant, in opposition to parsing the samples that is executed later, asynchronously on demand
		/// </summary>
		public CyclicsParserXML(XElement runResult) {
			ReadHeader(runResult);
			ReadSamples(runResult);
		}

        /// <summary>
		/// Reads the cyclics header and fills it into an internal list (headerItems)
		/// Note that we add here the timestamp header item
		/// </summary>
		private void ReadHeader(XElement runResult) {
			string headerLine = GetHeaderLineFromRunResult(runResult);
			headerItems = ParseHeader (headerLine); 
			headerItems.Insert(0, "Timestep");
		} 


        /// <summary>
		/// Reads the cyclics data from the trace file and fills it to a simple string list.
		/// Note that we add here the timestamp to the csv-list that we read from the XML attribute "Time".
		/// </summary>
		private void ReadSamples(XElement runResult){

			//read the samples from the XML to a list
			IEnumerable<XElement> xmlSamples = 
				from el in runResult.Descendants (XmlTags.Sample)
				select el;

			//Convert the samples to a string list, so that we can use the generic ParseSamples function.
			foreach(XElement xmlSample in xmlSamples) {
				string csvSample = GetTimeStampXML(xmlSample) + ", " + xmlSample.Value;
				csvSamples.Add(csvSample);
			}
		}

        /// <summary>
		/// Reads the CSV formatted header line from the xml runresult structure
		/// </summary>
		private string GetHeaderLineFromRunResult(XElement rResult){
			string headerLine = (string)
				(from el in rResult.Descendants (XmlTags.Header)
					select el).First (); 

            return headerLine;
        }

		/// <summary>
		/// Reads the 'Time' attribute of a 'Sample' XMLTag
		/// </summary>
		/// <returns>The time stamp in milliseconds</returns>
		private int GetTimeStampXML(XElement sample){
			int timeStamp;
			string time = sample.Attribute (XmlTags.Time).Value;
			if (!int.TryParse (time, out timeStamp))
				throw new InvalidOperationException ("Invalid time stamp '" + time + "'");
			return timeStamp;
		}
	}
}