﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;

namespace Visualizer.Trace
{
	public class Run
	{
		public int ID { get; set; }
		public int EventCount { get; set; }

		public Run(int id, int eventCount){
			this.ID = id;
			this.EventCount = eventCount;
		}
	}
}

