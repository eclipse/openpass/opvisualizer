﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;

namespace Visualizer.Trace
{
	/// <summary>
	/// The attributes of an agent that the visualizer needs to display a simulation, excluding noninterpreted agent info panel elements
	/// </summary>
	public class SampleAttributes
	{
		public virtual string XPosition => "XPosition";
		public virtual string YPosition => "YPosition";
		public virtual string YawAngle => "YawAngle";
		public virtual string RollAngle => "RollAngle";
		public virtual string BrakeLight => "BrakeLight";
		public virtual string AOI => "AOI";
		public virtual string TurnSignal => "IndicatorState";
	}

	/// <summary>
	/// Helper class that can read an attribute value from a sample based on the attribute name specified in the header
	/// </summary>
	public class SampleParser {

		private Dictionary<string, string> sample;
		private char delimiter;

		/// <summary>
		/// Initializes a new instance of the SampleParser class that can be used to read values from a single sample (single timestamp).
		/// Creates a dictionary where key is the attribute's key in the header and value is the attributes value.
		/// </summary>
		/// <param name="attrKeys">The list of attribute keys (the header) as string list.</param>
		/// <param name="attrValues">The comma separated list of values in a sample</param>
		public SampleParser(List<string> attrKeys, string attrValues, char delimiterChar = ','){
			List<string> values = attrValues.Split(delimiterChar).Select(x => x.Trim()).ToList<string>();
			delimiter = delimiterChar;
			sample = Enumerable.Range(0, values.Count).ToDictionary(i => attrKeys[i], i => values[i]);
		}

		/// <summary>
		/// Returns the timestamp of the given sample
		/// </summary>
		public int GetTimeStamp() {
			return GetAttributeAsInt(-1, "Timestep");
		}
		
		/// <summary>
		/// Returns the sample as a CSV formatted list
		/// </summary>
		public string GetSampleAsCSV(){
			return string.Join(delimiter, sample.Select(attr => attr.Value));
		}

		/// <summary>
		/// Gets the given attribute of a given agent from the sample as float. If fails to convert the value to float, it returns default 0f.
		/// </summary>
		public float GetAttributeAsFloat(int agentID, string attrName){
			string attrValue = GetAttribute(agentID, attrName);

			float result = 0f;
			if (attrValue == string.Empty)
				return result;

			IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");	
			if(!float.TryParse (attrValue, NumberStyles.Float, provider, out result))
				Debug.LogWarning("Could not convert " + attrName + " to float!");
			
			return result;
		}

		/// <summary>
		/// Gets the given attribute of a given agent from the sample as integer. If fails to convert the value to int, it returns default 0.
		/// </summary>
		public int GetAttributeAsInt(int agentID, string attrName){
			string attrValue = GetAttribute(agentID, attrName);

			int result = 0;
			if (attrValue == string.Empty)
				return result;

			if(!int.TryParse (attrValue, out result))
				Debug.LogWarning("Could not convert " + attrName + " to int!");

			return result;
		}

		/// <summary>
		/// Gets the given attribute of a given agent from the sample as boolean.
		/// </summary>
		public bool GetAttributeAsBool(int agentID, string attrName){
			string attrValueStr = GetAttribute(agentID, attrName).Trim ();

			if (attrValueStr == "0" || 
				attrValueStr == "false" || 
				attrValueStr == "no")
				return false;
			else
				return true;
		}

		/// <summary>
		/// Gets the given attribute of a given agent from the sample as string.
		/// </summary>
		public string GetAttributeAsStr(int agentID, string attrName){
			return GetAttribute(agentID, attrName);
		}

		/// <summary>
		/// Internal function to get the given attribute of a given agent from the sample. 
		/// If agentID is valid (not -1), the expected attribute syntax is: 'AgentID:AttributeName'. 
		/// If the given attribute name or agentID is invalid, it returns an empty string.
		/// </summary>
		private string GetAttribute(int agentID, string attrName){
			string key;
			if(agentID == -1)
				key = attrName;
			else
				key = agentID + ":" + attrName;

			if(sample.ContainsKey(key))
				return sample[key];
			else
				return string.Empty;
		}

		/// <summary>
		/// Internal function to set the given attribute of a given agent in the sample. 
		/// If agentID is valid (not -1), the expected attribute syntax is: 'AgentID:AttributeName'. 
		/// If the given attribute name or agentID is invalid, it does nothing.
		/// </summary>
		public void SetAttribute(string value, int agentID, string attrName){
			string key;
			if(agentID == -1)
				key = attrName;
			else
				key = agentID + ":" + attrName;

			if(sample.ContainsKey(key))
				sample[key] = value;
		}

		/// <summary>
		/// Checks if the attribute exists in the sample
		/// </summary>
		public bool HasAttribute(int agentID, string attrName){
			string key = agentID + ":" + attrName;
			return sample.ContainsKey(key);
		}
	}
}