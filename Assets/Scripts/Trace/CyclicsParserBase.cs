/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System;
using System.IO;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Visualizer.AgentHandling;
using Visualizer.Core;
using Visualizer.Utilities;
		
namespace Visualizer.Trace
{
	/// <summary>
	/// Base class with common CSV and XML import functionality
	/// </summary>
	public abstract class CyclicsParserBase {

		protected List<string> headerItems = new List<string>();
		protected List<string> csvSamples = new List<string>();
		List<int> agentIDs = new List<int>();
		
        /// <summary>
		/// Finds a sample in the csv formatted sample array with the given time stamp
		/// Note that the time stamp must first column of the csv sample 
		/// </summary>
		public string FindSample(int timeStamp){
			string timeStampStr = timeStamp.ToString();
			string csvSample = csvSamples.Find(s => s.StartsWith(timeStampStr + ","));
			return csvSample;
		}

        /// <summary>
		/// Reads all cyclics data from the csv formatted sample array and fills it to the given SampleContainer.
		/// </summary>
		/// <param name="samples">String array that contains the csv formatted samples</param>
		public void ParseAllSamples(ISampleContainer sampleContainer, SampleAttributes sampleAttributes, ISampleAttributeMapper attributeMapper){
			foreach (string csvSample in csvSamples)
				ParseSample(csvSample, sampleContainer, sampleAttributes, attributeMapper);
		}

        /// <summary>
		/// Reads the cyclics data at a specific time stamp from the csv formatted sample array and adds it to the given SampleContainer.
		/// </summary>
		public void ParseSample(int timeStamp, ISampleContainer sampleContainer, SampleAttributes sampleAttributes, ISampleAttributeMapper attributeMapper){
			string csvSample = FindSample(timeStamp);
			if (csvSample == null) {
				Debug.LogWarning("Could not parse sample at timestamp " + timeStamp + ", as it is not found in trace file.");
				sampleContainer.AddSample (timeStamp, new AgentSample[0]);
			} else {
				ParseSample(csvSample, sampleContainer, sampleAttributes, attributeMapper);
			}
		}

		/// <summary>
		/// Reads the cyclics data from the csv formatted sample array and fill it to the given SampleContainer.
		/// It initializes the container.
		/// </summary>
		private void ParseSample(string csvSample, ISampleContainer sampleContainer, SampleAttributes sampleAttributes, ISampleAttributeMapper attributeMapper){	

			List<int> agentIDs = GetAgentIDsFromHeader();
			AgentSample[] sample = new AgentSample[agentIDs.Count];
			SampleParser sampleParser = new SampleParser (headerItems, csvSample);

			int agentIdx = 0;
			foreach(int agentID in agentIDs){
				AgentSample agentSample = new AgentSample ();
				agentSample.AgentID = agentID;
				agentSample.XPosition = sampleParser.GetAttributeAsFloat (agentID, sampleAttributes.XPosition);
				agentSample.YPosition = sampleParser.GetAttributeAsFloat (agentID, sampleAttributes.YPosition);
				agentSample.YawAngle = sampleParser.GetAttributeAsFloat (agentID, sampleAttributes.YawAngle) * -Mathf.Rad2Deg; 	//convert from radians to degrees	
				agentSample.RollAngle = sampleParser.GetAttributeAsFloat (agentID, sampleAttributes.RollAngle) * -Mathf.Rad2Deg; 	//convert from radians to degrees	
				agentSample.BrakeLight = sampleParser.GetAttributeAsBool (agentID, sampleAttributes.BrakeLight);
				agentSample.AOI = sampleParser.GetAttributeAsInt (agentID, sampleAttributes.AOI);
				agentSample.TurnSignal = sampleParser.GetAttributeAsInt (agentID, sampleAttributes.TurnSignal);
				//Read here all attributes from the trace file that are not relevant for graphical display (they are only going to be displayed in the agent info panel)
				foreach (string itemName in sampleContainer.AgentAttributes) {
					string itemValue = sampleParser.GetAttributeAsStr(agentID, itemName);
					if(attributeMapper != null)
						itemValue = attributeMapper.MapValue(itemName, itemValue);
					agentSample.OptionalAttributes.Add(itemName, itemValue);
				}

				sample [agentIdx] = agentSample;
				agentIdx++;
			}
			int timeStamp = sampleParser.GetTimeStamp();
			sampleContainer.AddSample (timeStamp, sample);
		}

        /// <summary>
		/// Appends cyclics data from the sample array to the given SampleContainer that already contains data at the given timestamp.
		/// This can be used to merge sample data from multiple trace files. 
		/// The data is added as optional attribute to the agent sample
		/// </summary>
		public void ExtendSample(int timeStamp, ISampleContainer sampleContainer, ISampleAttributeMapper attributeMapper){
			string csvSample = FindSample(timeStamp);
			if (csvSample != "")
				ExtendSample(csvSample, sampleContainer, attributeMapper);
		}

        /// <summary>
		/// Appends cyclics data from the given csv formatted sample to the given SampleContainer.
		/// </summary>
		/// <param name="samples">String array that contains the csv formatted samples</param>
		private void ExtendSample(string csvSample, ISampleContainer sampleContainer, ISampleAttributeMapper attributeMapper){
			
			//create a sample parser that will help us to get the values fast from the current sample
			SampleParser sampleParser = new SampleParser (headerItems, csvSample);
			int timeStamp = sampleParser.GetTimeStamp();
			AgentSample[] existingSample = sampleContainer.GetSample (timeStamp);
			if(existingSample == null) {
				Debug.LogWarning("Could not extend sample container at timestamp " + timeStamp + ", as there is no existing data at that point");
				return;
			}
			foreach(AgentSample agentSample in existingSample){
				foreach (string itemName in sampleContainer.AgentAttributes) {
					if(sampleParser.HasAttribute(agentSample.AgentID, itemName)){
						string itemValue = sampleParser.GetAttributeAsStr(agentSample.AgentID, itemName);
						if(attributeMapper != null)
							itemValue = attributeMapper.MapValue(itemName, itemValue);
						if(!agentSample.OptionalAttributes.ContainsKey(itemName))
							agentSample.OptionalAttributes.Add(itemName, itemValue);
						else
							agentSample.OptionalAttributes[itemName] = itemValue;
					}
				}
			}
		}

        /// <summary>
		/// Parses the agent IDs from the header and checks the agent IDs against the passed list of AgentProperties
		/// </summary>
		public void ValidateHeader(List<AgentProperties> agents){
			List<int> agentIDs = GetAgentIDsFromHeader();
			foreach (int agentID in agentIDs) {
				bool foundID = false;
				foreach (AgentProperties agent in agents) {
					if (agent.ID == agentID) {
						foundID = true;
						break;
					}
				}
				if(!foundID)
					throw new Exception ("Agent IDs in header do not match the list of agents in '" + XmlTags.Agents + "' Tag. Unknown agent ID '" + agentID + "' in header.");
			}
		}

		/// <summary>
		/// Read the time stamp from the first csv sample
		/// </summary>
		public int GetFirstTimeStamp(){
			SampleParser sampleParser = new SampleParser (headerItems, csvSamples.First());
			return sampleParser.GetTimeStamp();
		}

		/// <summary>
		/// Read the time stamp from the last csv sample
		/// </summary>
		public int GetLastTimeStamp(){
			SampleParser sampleParser = new SampleParser (headerItems, csvSamples.Last());
			return sampleParser.GetTimeStamp();
		}

		/// <summary>
		/// Determine the sampling time based on the first and second time stamp
		/// TODO: change request to simulationOutput: sampling time should be explicitly defined as XML attrib
		/// </summary>
		public int GetSamplingTime(){
			if(csvSamples.Count < 2) {
				Debug.LogError("Not enough samples to determine sampling time");
				return -1;
			}
			SampleParser firstSample = new SampleParser (headerItems, csvSamples[0]);
			SampleParser secondSample = new SampleParser (headerItems, csvSamples[1]);
			return secondSample.GetTimeStamp() - firstSample.GetTimeStamp();
		}

		/// <summary>
		/// Read the agent ids from the header line
		/// </summary>
		public List<int> GetAgentIDsFromHeader(){
			List<int> aIDs = new List<int>();
			foreach(string headerItem in headerItems){
				if(headerItem != "Timestep") {
					int agentID = ParseAgentID(headerItem);
					if (!aIDs.Contains(agentID))
						aIDs.Add(agentID);
				}
			}
			return aIDs;
		}

		/// <summary>
		/// Reads the list of attributes from the header that the agents contain and returns them as a list of string.
		/// </summary>
		public List<string> GetAgentAttributesFromHeader(){
			List<string> agentAttributes = new List<string> ();
			foreach (string attribute in headerItems) {
				string agentAttribute = ParseAgentAttribute (attribute);
				if (agentAttribute != "" && !agentAttributes.Contains (agentAttribute))
					agentAttributes.Add (agentAttribute);
			}
			return agentAttributes;
		}

        /// <summary>
		/// Parse the cyclics header from string into a list
		/// </summary>
		protected List<string> ParseHeader(string headerLine){
			List<string> hItems = new List<string>();
			hItems = headerLine.Split (',').Select (x => x.Trim ()).ToList<string> ();
			RemoveLeadingZeros(hItems);
			return hItems;
		}


       /// <summary>
		/// Removes leading zeros from agent ids in the header.
		/// </summary>
		protected void RemoveLeadingZeros(List<string> hItems){
			for( int i = 0; i < hItems.Count; ++i ) {
				hItems[i] = hItems[i].TrimStart('0'); 
				if(hItems[i][0] == ':'){
					//Fix AgentID "0"
					hItems[i] = hItems[i].Insert( 0, "0" );
				}
			}
		}

        /// <summary>
		/// Validates and parses the header entry and reads the ID into an integer (expected format: 'AgentID:AttributeName').
		/// </summary>
		protected int ParseAgentID(string attribute){
			int colonIdx = attribute.IndexOf(":");
			if (colonIdx == -1)
				throw new Exception ("Invalid agent header format (missing ':')");

			string agentIDstr = attribute.Substring (0, colonIdx);

			int aID;
			if(!int.TryParse(attribute.Substring (0, colonIdx), out aID))
				throw new Exception ("Invalid agent ID in header: '" + agentIDstr + "' (attribute: '" + attribute + "')");

			return aID;
		}

		/// <summary>
		/// Parses a single agent attribute (expected format: 'AgentID:AttributeName').
		/// If the input parameter does not match the expected format, it is not an agent attribute -> we return an empty string 
		/// </summary>
		private string ParseAgentAttribute(string attribute){
			int colonIdx = attribute.IndexOf(":");
			if (colonIdx == -1)
				return "";
			return attribute.Substring (colonIdx+1, attribute.Length-colonIdx-1);
		}
	}
}