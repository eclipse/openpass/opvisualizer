/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Visualizer.AgentHandling;
using Visualizer.Core;

namespace Visualizer.Trace
{
	/// <summary>
	/// Helper class that can read the cyclics of a CSV cyclics file that is referenced by the trace file.
	/// </summary>
	public class CyclicsParserCSV : CyclicsParserBase{

		/// <summary>
		/// Initializes a new instance of the class and reads the file into simple string lists 
		/// This step is fairly performant, in opposition to parsing the samples that is executed later, asynchronously on demand
		/// </summary>
		public CyclicsParserCSV(string cyclicsFilePath) {
			ReadHeader(cyclicsFilePath);
			ReadSamples(cyclicsFilePath);
		}

        /// <summary>
		/// Reads the cyclics header and fills it into an internal list (headerItems)
		/// </summary>
		private void ReadHeader(string cyclicsFilePath) {
			string headerLine = FileReader.ReadFirstLinesSafe(cyclicsFilePath);
			headerItems = ParseHeader (headerLine); 
		} 
		
        /// <summary>
		/// Reads the cyclics data from a CSV file that is referenced in the trace file and fills it into a simple string list.
		/// </summary>
		private void ReadSamples(string cyclicsFilePath){
			csvSamples = FileReader.ReadAllLinesSafe(cyclicsFilePath);
			//remove the header from the list
			csvSamples.RemoveAt(0);
		}
	}
}