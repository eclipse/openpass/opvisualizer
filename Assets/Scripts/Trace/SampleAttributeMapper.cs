﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.Trace
{
	public interface ISampleAttributeMapper {
		void Init();
		string MapValue(string attrName, string attrValue);
	}

	/// <summary>
	/// Helper class that can map sample attribute values based on the predifened dictionaries
	/// </summary>
	public class SampleAttributeMapper : ISampleAttributeMapper {
		private class AttributeMapsFileContent {
			public List<AttributeMap> AttributeMapList { get; set; }
		}

		private class AttributeMap {
			public string AttributeName { get; set; }
			public Dictionary<string, string> ValueMap { get; set; }
		}

		private IFileHelper fileHelper;
		private AttributeMapsFileContent maps = null;
		private string sampleAttrMapsFileName = "SampleAttrMaps.json";

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IFileHelper fHelper){
			fileHelper = fHelper;
		}

        /// <summary>
		/// Loads the sample attribute map file
		/// </summary>
		public void Init () {
			string fileName = fileHelper.GetAppDataPath() + "\\Config\\" + sampleAttrMapsFileName;

			if(!fileHelper.FileExists(fileName))
				return;

			try {
            	string jsonContent = fileHelper.ReadFile(fileName);
				LoadMap(jsonContent);
			} catch (Exception ex) {
				throw new Exception("Loading SampleAttributeMap file " + fileName + " failed: " + ex.Message);
			}
		}

		/// <summary>
		/// Loads the json formatted list of attribute maps
		/// </summary>
		private void LoadMap(string jsonContent){
			maps = JsonConvert.DeserializeObject<AttributeMapsFileContent>(jsonContent);
		}

		/// <summary>
		/// Core mapping function
		/// </summary>
		public string MapValue(string attrName, string attrValue){
			if (maps != null && maps.AttributeMapList != null){
				AttributeMap attrMap = maps.AttributeMapList.Find(m => m.AttributeName == attrName);
				if(attrMap != null) {
					if(attrMap.ValueMap.ContainsKey(attrValue)) {
						attrValue = attrMap.ValueMap[attrValue];
					}
				}
			}
			return attrValue;
		}
	}
}