/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Collections.Generic;

namespace Visualizer.Trace
{
	/// <summary>
	/// This class provides extended file reading functionality 
	/// </summary>
	public static class FileReader {

        /// <summary>
        /// This function reads the given file even if it is opened in another application for writing. 
        /// The regular .NET implementation of ReadAllLines throws an exception in this case.
        /// </summary>
        public static List<string> ReadAllLinesSafe(String path)
        {
            using (var csv = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(csv))
            {
                List<string> file = new List<string>();
                while (!sr.EndOfStream)
                {
                    file.Add(sr.ReadLine());
                }
                return file;
            }
        }

        /// <summary>
        /// This function reads the first line from the given file even if it is opened in another application for writing. 
        /// The regular .NET implementation of ReadAllLines throws an exception in this case.
        /// </summary>
        public static string ReadFirstLinesSafe(String path)
        {
            using (var csv = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(csv))
            {
                return sr.ReadLine();
            }
        }
    }
}