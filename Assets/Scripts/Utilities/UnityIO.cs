﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.EventSystems;
using Zenject;

namespace Visualizer.Utilities
{

	public interface IUnityIO{
		bool GetKeyDown(KeyCode key);
		bool GetKey(KeyCode key);
		bool GetMouseButton(int button);
		bool GetMouseButtonUp(int button);
		bool GetMouseButtonDown(int button);
		Vector3 GetMousePos();
		bool IsMouseOverUI();
		GameObject GetGameObjectAtScreenPoint(Vector2 screenPos);
		GameObject LoadAsset(string assetKey);
		List<string> GetAssetNames(string assetLabel);
	}

	/// <summary>
	/// This class is a wrapper for unity core IO functions that can be mocked for testing
	/// </summary>
	public class UnityIO : IUnityIO {

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){
		}

		/// <summary>
		/// Returns true during the frame the user starts pressing down the key
		/// </summary>
		public bool GetKeyDown(KeyCode key){
			 return Input.GetKeyDown(key);
		}

		/// <summary>
		/// Returns weather the given key is pressed
		/// </summary>
		public bool GetKey(KeyCode key){
			 return Input.GetKey(key);
		}

		/// <summary>
		/// Returns true if the given mouse button is pressed
		/// </summary>
		public bool GetMouseButton(int button){
			return Input.GetMouseButton (button);
		}

		/// <summary>
		/// Returns true during the frame the user starts pressing the given mouse button
		/// </summary>
		public bool GetMouseButtonDown(int button){
			return Input.GetMouseButtonDown (button);
		}

		/// <summary>
		/// Returns true during the frame the user releases the given mouse button
		/// </summary>
		public bool GetMouseButtonUp(int button){
			return Input.GetMouseButtonUp (button);
		}

		/// <summary>
		/// Returns the current mouse position
		/// </summary>
		public Vector3 GetMousePos(){
			return Input.mousePosition;
		}

		/// <summary>
		/// Returns wheter the mouse cursor is currently over a UI element
		/// </summary>
		public bool IsMouseOverUI(){
			return EventSystem.current.IsPointerOverGameObject();
		}
		
		/// <summary>
		/// Try to get the GameObject that is the closest to the camera at the given screen coordinate.
		/// If no object is found, null is returned.
		/// </summary>
		public GameObject GetGameObjectAtScreenPoint(Vector2 screenPos) {
			RaycastHit rayCastHit;
			Ray ray = Camera.main.ScreenPointToRay (screenPos);
			if (Physics.Raycast (ray, out rayCastHit, 1000f))
				return rayCastHit.collider.gameObject;
			else
				return null;
		}

		/// <summary>
		/// Loads an addressable resource by the given asset key snchronously.
		/// If the resource is not found, null is returned
		/// </summary>
		public GameObject LoadAsset(string assetKey) {
			GameObject asset = null;
			try {
				AsyncOperationHandle<GameObject> opHandle = Addressables.LoadAssetAsync<GameObject>(assetKey);
				opHandle.WaitForCompletion();

				if(opHandle.Status == AsyncOperationStatus.Succeeded)
					asset = opHandle.Result;
				else
					Addressables.Release(opHandle);
			} catch {
				// the async operation logs the error
			}
			return asset;
		}

		/// <summary>
		/// Retruns a list of all available addressable assets with the given label
		/// </summary>
		public List<string> GetAssetNames(string assetLabel) {
			List<string> assetNames = new List<string>(); 
			IList<IResourceLocation> assetLocations = new List<IResourceLocation>();

			try {
				AsyncOperationHandle<IList<IResourceLocation>> opHandle = Addressables.LoadResourceLocationsAsync(assetLabel);
				opHandle.WaitForCompletion();

				if(opHandle.Status == AsyncOperationStatus.Succeeded)
					assetLocations = opHandle.Result;
				else
					Addressables.Release(opHandle);
			} catch {
				// the async operation logs the error
			}

			foreach (var location in assetLocations)
				assetNames.Add(location.PrimaryKey);

			return assetNames;
		}
	}
}