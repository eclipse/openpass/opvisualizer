/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
    
namespace Visualizer.Utilities {    

    public class GenericListItem {
        public string Key {get; set;} = "";
        public string Label { get; set; } = "";
        public string Value {get; set;} = "";
        public bool Enabled { get; set; } = true;
        
         /// <summary>
        /// Constructs a GenericListItem 
        /// </summary>
        public GenericListItem(string key, string label, string value, bool enabled){
            Key = key;
            Label = label;
            Value = value;
            Enabled = enabled;
        }

        /// <summary>
        /// Alternative costructurs with default values 
        /// </summary>
        public GenericListItem(string key, string value) : this(key, key, value, true) {}
        public GenericListItem(string key, bool enabled) : this(key, key, "", enabled) {}
        public GenericListItem(string key, string label, bool enabled) : this(key, label, "", enabled) {}
    }
}