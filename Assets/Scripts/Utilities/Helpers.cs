/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Collections;
using UnityEngine;
using System.Globalization;

namespace Visualizer.Utilities
{
    /// <summary>
	/// General algorithms and helpers that don't fit into any other class
	/// </summary>
	public static class Helpers
	{
		/// <summary>
		/// Extracts the folder name of the given file path
		/// </summary>
		public static string GetFileFolder(string file) {
			int lastSlashIdx = file.LastIndexOf ("\\");
			if (lastSlashIdx == -1) {
				lastSlashIdx = file.LastIndexOf ("/");
			}
			if (lastSlashIdx == -1) {
				lastSlashIdx = 0;
			}
			string folder = file.Substring (0, lastSlashIdx);
			return folder;
		}

        /// <summary>
        /// Checks if the given path is absolute or relative
        /// </summary>
        public static bool IsAbsolutePath(string path) {
            return Path.IsPathRooted (path) && !Path.GetPathRoot (path).Equals (Path.DirectorySeparatorChar.ToString (), StringComparison.Ordinal);
        }

        /// <summary>
        /// Executes a function (with parameters) after the given delay
		/// It returns the enumarator that can be used to cancel the execution with CancelDelayedInvocation
        /// </summary>
		public static IEnumerator InvokeDelayed(this MonoBehaviour mb, Action f, float delay){
			IEnumerator invocation = InvokeRoutine(f, delay);
			mb.StartCoroutine(invocation);
			return invocation;
		}
	
		private static IEnumerator InvokeRoutine(System.Action func, float delay){
			yield return new WaitForSeconds(delay);
			func();
		}

        /// <summary>
        /// Executes a function (with parameters) after the given delay
        /// </summary>
		public static void CancelDelayedInvocation(this MonoBehaviour mb, IEnumerator invocation){
			mb.StopCoroutine(invocation);
		}

		/// <summary>
		/// Removes CRLF and TAB characters from the giben text (can be used for labels that contain a path with "...\n...", "...\t..." or "...\r...")
		/// </summary>
        public static string SanitizeScreenText(string text) {
            return text.Replace(@"\n", @"\\n").Replace(@"\t", @"\\t").Replace(@"\r", @"\\r");
        }

		/// <summary>
		/// Convert string to float
		/// </summary>
		public static float Str2Float(string str) {
			float flt = 0;
			IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");	
			float.TryParse (str, NumberStyles.Float, provider, out flt);
			return flt;
		} 

        /// <summary>
		/// Checks if the given point lies in the triangle defined by p1, 2 and p3.
		/// </summary>
        public static bool IsPointInTriangle(Vector2 queryPoint, Vector2 p1, Vector2 p2, Vector2 p3) {
            double d1, d2, d3;
            bool has_neg, has_pos;

			d1 = (queryPoint.x - p2.x) * (p1.y - p2.y) - (p1.x - p2.x) * (queryPoint.y - p2.y);
			d2 = (queryPoint.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (queryPoint.y - p3.y);
			d3 = (queryPoint.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (queryPoint.y - p1.y);

            has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
            has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

            return !(has_neg && has_pos);
        }

		/// <summary>
		/// Find the points where the two circles intersect.
		/// c1 and r1 represent the center coordinate and the radius of the first circlie
		/// c2 and r2 represent the center coordinate and the radius of the second circlie
		/// returns false if the circles do not intersect otherwise true 
		/// </summary>
		public static bool FindCircleIntersections(Vector2 c1, float r1, Vector2 c2, float r2, out Vector2 intersection1, out Vector2 intersection2) {
			intersection1 = Vector2.zero;
			intersection2 = Vector2.zero;

			// Find the distance between the centers.
			double dx = c1.x - c2.x;
			double dy = c1.y - c2.y;
			double dist = Math.Sqrt(dx * dx + dy * dy);

			if (Math.Abs(dist - (r1 + r2)) < 0.00001)
			{
				intersection1 = Vector2.Lerp(c1, c2, r1 / (r1 + r2));
				intersection2 = intersection1;
				return true;
			}
		
			// See how many solutions there are.
			if (dist > r1 + r2){
				// No solutions, the circles are too far apart.
				return false;
			}
			else if (dist < Math.Abs(r1 - r2)){
				// No solutions, one circle contains the other.
				return false;
			}
			else if ((dist == 0) && (r1 == r2)){
				// No solutions, the circles coincide.
				return false;
			}
			else {
				// Find a and h.
				double a = (r1 * r1 - r2 * r2 + dist * dist) / (2 * dist);
				double h = Math.Sqrt(r1 * r1 - a * a);
	
				// Find P2.
				double cx2 = c1.x + a * (c2.x - c1.x) / dist;
				double cy2 = c1.y + a * (c2.y - c1.y) / dist;
	
				// Get the points P3.
				intersection1 = new Vector2(
					(float)(cx2 + h * (c2.y - c1.y) / dist),
					(float)(cy2 - h * (c2.x - c1.x) / dist));
				intersection2 = new Vector2(
					(float)(cx2 - h * (c2.y - c1.y) / dist),
					(float)(cy2 + h * (c2.x - c1.x) / dist));
				
				return true;
			}
		}
    }
}