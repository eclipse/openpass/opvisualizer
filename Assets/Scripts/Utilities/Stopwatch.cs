/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
    
using UnityEngine;
namespace Visualizer.Utilities {    

    public class Stopwatch {
        private System.Diagnostics.Stopwatch stopwatch = null;
        private bool active = false;

        /// <summary>
        /// Constructs a Stopwatch instance 
        /// </summary>
        public Stopwatch(bool enabled = true){
            active = enabled;
            if(active)
                stopwatch = new System.Diagnostics.Stopwatch();
        }

        /// <summary>
        /// Starts the timer 
        /// </summary>
        public void Start(){
            if(active == false || stopwatch == null)
                return;
            stopwatch.Reset();
            stopwatch.Start();
        }

        /// <summary>
        /// Stops the timer and displays a debug message. 
        /// Use character '#' in the message as a placeholder for the time value.
        /// </summary>
        public float Stop(string message = ""){
            if(active == false || stopwatch == null)
                return 0;
            stopwatch.Stop();
            if(message != "") {
                message = message.Replace("#", stopwatch.ElapsedMilliseconds.ToString());
                Debug.Log(message);
            }
            return stopwatch.ElapsedMilliseconds;
        }
    }
}