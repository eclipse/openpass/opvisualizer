﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Zenject;

namespace Visualizer.Utilities
{

	/// <summary>
	/// Class that represents the data source of a file list entry.
	/// </summary>
	public class FileListEntry {
		public string Name { get; set; }
		public FileType Type { get; set; }
		public bool Active { get; set; }
		
		public FileListEntry(string name, FileType type, bool active){
			Name = name;
			Type = type;
			Active = active;
		}
	}

	/// <summary>
	/// FileTypes that should be differentiated in the file list. Can be extended.
	/// </summary>
	public enum FileType {
		File,
		Folder
	}

	public interface IFileHelper{
		bool FileExists(string fileName);
		bool DirectoryExists(string path);
		string ReadFile(string fileName);
		string GetAppDataPath();
		List<string> GetSystemDrives();
		List<FileListEntry> ReadDirectoryContents(string folder, string selectionPattern);
	}

	/// <summary>
	/// Helper functions that can be mocked for testing
	/// </summary>
	public class FileHelper : IFileHelper{

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){
		}

		/// <summary>
		/// Returns weather the given path is valid
		/// </summary>
		public bool DirectoryExists(string path) {
			 return Directory.Exists(path);
		}

		/// <summary>
		/// Returns weather the given file (incl path) exists
		/// </summary>
		public bool FileExists(string fileName) {
			 return File.Exists(fileName);
		}

		/// <summary>
		/// Reads the content of the given file
		/// </summary>
		public string ReadFile(string fileName) {
			 return File.ReadAllText(fileName);
		}

		/// <summary>
		/// Returns the Data folder of the application
		/// </summary>
		public string GetAppDataPath() {
			 return Application.dataPath;
		}

		/// <summary>
		/// Returns the list of drives that are mounted in the system
		/// </summary>
		public List<string> GetSystemDrives() {
			 return Directory.GetLogicalDrives().ToList();
		}

		/// <summary>
		/// Reads the content of the given folder into a list of FileListEntries
		/// </summary>
		public List<FileListEntry> ReadDirectoryContents(string folder, string selectionPattern) {
			List<FileListEntry> fileList = new List<FileListEntry>();
			string[] directories = Directory.GetDirectories(folder);
			for (int i = 0; i < directories.Length; ++i) {
				string folderName = directories[i].Substring(directories[i].LastIndexOf(Path.DirectorySeparatorChar) + 1);
				fileList.Add(new FileListEntry(folderName, FileType.Folder, true));
			}
			string[] files = Directory.GetFiles(folder);
			Array.Sort(files);
			for (int i = 0; i < files.Length; ++i) {
				string fileName = Path.GetFileName(files[i]);
				bool active = fileName.EndsWith(selectionPattern);
				fileList.Add(new FileListEntry(fileName, FileType.File, active));
			}
			return fileList;
		}
	}
}