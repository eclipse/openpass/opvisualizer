/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine.UIElements;

namespace Visualizer.UI
{
	public class TooltipManipulator : Manipulator
	{
		private Tooltip tooltip;

		public TooltipManipulator(Tooltip tooltip)
		{
			this.tooltip = tooltip;
		}

		protected override void RegisterCallbacksOnTarget()
		{
			target.RegisterCallback<MouseEnterEvent>(MouseIn);
			target.RegisterCallback<MouseOutEvent>(MouseOut);
		}

		protected override void UnregisterCallbacksFromTarget()
		{
			target.UnregisterCallback<MouseEnterEvent>(MouseIn);
			target.UnregisterCallback<MouseOutEvent>(MouseOut);
		}

		private void MouseIn(MouseEnterEvent e)
		{
			tooltip.Show(target);
			tooltip.RegisterCallback<GeometryChangedEvent>(OnGeometryChanged);
		}

		private void MouseOut(MouseOutEvent e)
		{
			tooltip.Close();
			tooltip.UnregisterCallback<GeometryChangedEvent>(OnGeometryChanged);
		}

		private void OnGeometryChanged(GeometryChangedEvent evt)
		{
			//After the label changes, we must recalculate the tooltip position 
			if (evt.oldRect.size != evt.newRect.size)
				tooltip.Show(target);
		}

		// ============================================================================================================
	}
}