﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Visualizer.Core;
using Visualizer.Utilities;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Zenject;

namespace Visualizer.UI
{
	public interface IKeyboardEventHandler{
		bool EnableHandling { get; set; }
	}

	public class KeyboardEventHandler : MonoBehaviour, IKeyboardEventHandler
	{
		private ISimulationLoader simulationLoader;
		private IAnimationController animationController;
		private IAgentManager agentManager;
		private IFocusCameraHandler focusCameraHandler;
		private IUnityIO unityIO;
		private IUIModeHandler uiModeHandler;
		private ITerrainManipulator terrainManipulator;
		private IMainUI ui;

		float timeSinceKeyDown = 0;
		float timeSinceLastSpoolStep = 0;
		float normalSpoolPeriod = 0.1f;
		float highSpeedSpoolPeriod = 0.02f;
		float lowSpeedSpoolingBegin = 0.3f;
		float highSpeedSpoolingBegin = 3f;

		public bool EnableHandling { get; set; }

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ITerrainManipulator tManipulator, IUIModeHandler uimHandler, IMainUI mUi, IUnityIO uIO, IAnimationController aController, ISimulationLoader sLoader, IAgentManager aManager, IFocusCameraHandler aCHandler){
			animationController = aController;
			simulationLoader = sLoader;
			agentManager = aManager;
			focusCameraHandler = aCHandler;
			unityIO = uIO;
			uiModeHandler = uimHandler;
			ui = mUi;
			terrainManipulator = tManipulator;
			
			EnableHandling = true;
		}

		/// <summary>
		/// MonoBehaviour function that is called once per frame
		/// </summary>
		public void Update(){
			if (EnableHandling)
				HandleKeyboardEvents ();
		}

		/// <summary>
		/// Handles the keyboard events.
		/// </summary>
		private void HandleKeyboardEvents(){
			if (uiModeHandler.GetMode() == UIMode.SimulationPlayer && simulationLoader.IsSimulationLoaded()) {
				//Single press left arrow  = reverse one sample
				if (unityIO.GetKeyDown (KeyCode.LeftArrow)) {
					timeSinceKeyDown = 0;
					timeSinceLastSpoolStep = 0;
					animationController.JumpToPreviousSample ();
				}

				//Single press right arrow = one sample forward
				if (unityIO.GetKeyDown (KeyCode.RightArrow)) {
					timeSinceKeyDown = 0;
					timeSinceLastSpoolStep = 0;
					animationController.JumpToNextSample ();
				}

				//Single press down arrow = change camera focus to next agent
				if (unityIO.GetKeyDown (KeyCode.DownArrow))
					HandleFocusCameraToNext (true);

				//Single press down arrow = change camera focus to previous agent
				if (unityIO.GetKeyDown (KeyCode.UpArrow))
					HandleFocusCameraToNext (false);

				//Hold down left arrow = spool reverse 
				if (unityIO.GetKey (KeyCode.LeftArrow))
					HandleAnimationSpooling (KeyCode.LeftArrow);

				//Hold down right arrow = spool forward
				if (unityIO.GetKey (KeyCode.RightArrow))
					HandleAnimationSpooling (KeyCode.RightArrow);

				//Space = toggle start stop playback
				if (unityIO.GetKeyDown (KeyCode.Space)) {
					if (animationController.IsPlaying())
						animationController.StopPlayback ();
					else
						animationController.StartPlayback ();
				}

				//Reload current trace file
				if (unityIO.GetKeyDown (KeyCode.Alpha2) && unityIO.GetKey(KeyCode.LeftAlt))
					simulationLoader.ReloadTraceFile ();
			}

			if (uiModeHandler.GetMode() == UIMode.SimulationPlayer ||
				uiModeHandler.GetMode() == UIMode.SceneryViewer) {
				//Reload current scenery file
				if (unityIO.GetKeyDown (KeyCode.Alpha3) && unityIO.GetKey(KeyCode.LeftAlt))
					simulationLoader.ReloadSceneryFile ();

				//Toggle High/Low Quality
				if (unityIO.GetKeyDown (KeyCode.Alpha4) && unityIO.GetKey(KeyCode.LeftAlt))
					SwitchDisplayQuality ();

				//Toggle Cursor Position display
				if (unityIO.GetKeyDown (KeyCode.Alpha5) && unityIO.GetKey(KeyCode.LeftAlt))
					ui.ToggleCursorPosition ();

				//Toggle Terrain type
				if (unityIO.GetKeyDown (KeyCode.Alpha6) && unityIO.GetKey(KeyCode.LeftAlt))
					terrainManipulator.ToggleTexture();
			}
		}

		/// <summary>
		/// Spools the animaton first slowly, then, if called repeatedly, after a while faster (while the user is holding down a key)
		/// </summary>
		/// <param name="keyCode">The key that the user is holding down.</param>
		private void HandleAnimationSpooling(KeyCode keyCode){
			timeSinceKeyDown += Time.deltaTime;
			timeSinceLastSpoolStep += Time.deltaTime;

			float spoolPeriod = lowSpeedSpoolingBegin;
			if (timeSinceKeyDown > lowSpeedSpoolingBegin)
				spoolPeriod = normalSpoolPeriod;
			if (timeSinceKeyDown > highSpeedSpoolingBegin)
				spoolPeriod = highSpeedSpoolPeriod;

			if (timeSinceLastSpoolStep > spoolPeriod) {
				switch (keyCode) {
				case KeyCode.LeftArrow:
					animationController.JumpToPreviousSample ();
					break;
				case KeyCode.RightArrow:
					animationController.JumpToNextSample ();
					break;
				}
				timeSinceLastSpoolStep = 0;
			}
		}

		/// <summary>
		/// Activates the next quality level. It loops through all levels, if reached the last level it begins from the beginning.
		/// </summary>
		private void SwitchDisplayQuality() {
			int currentLevel = QualitySettings.GetQualityLevel();
			if (currentLevel == QualitySettings.names.Length-1)
				QualitySettings.DecreaseLevel ();
			else
				QualitySettings.IncreaseLevel ();

		}

		/// <summary>
		/// Attaches the camera to the next or previous available agent that is not hidden
		/// </summary>
		/// <param name="focusToNext">If true, the next agent is selected, if false, the previous.</param>
		private void HandleFocusCameraToNext(bool focusToNext){
			List<int> agentIDs = agentManager.GetAgentIDs ();

			int agentListIdx = 0;
			Agent focusAgent = focusCameraHandler.GetFocusedAgent ();
			if(focusAgent != null)
				agentListIdx = agentIDs.IndexOf(focusAgent.ID);

			for(int retries = 0; retries < agentIDs.Count; retries++){
				agentListIdx = focusToNext ? (agentListIdx+1) : (agentListIdx-1);

				if( agentListIdx < 0 ) 
					agentListIdx = agentIDs.Count - 1; //reached the beginning of list -> start again from the end

				if( agentListIdx > agentIDs.Count - 1) 
					agentListIdx = 0; //reached the end of list -> start again from the beginning

				Agent nextAgent = agentManager.GetAgent (agentIDs[agentListIdx], false);
				if(nextAgent != null && !nextAgent.IsHidden()){
					focusCameraHandler.FocusCamera (nextAgent.ID);
					break;
				}
			}
		}
	}
}

