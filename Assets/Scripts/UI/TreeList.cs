/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.Utilities;
using Visualizer.Core;

namespace Visualizer.UI {

    public class TreeListItem : GenericListItem {
        public int Level { get; set; } = 0;
        public bool Group { get; set; } = false;
        public TreeListItem(string key, string label, string value, int level, bool group, bool enabled) : base(key, label, value, enabled) {
            Level = level;
            Group = group;
        }
    }

    /// <summary>
    /// List wrapper that handles TreeListItems
    /// </summary>
    public class TreeList : List<TreeListItem>
    {
        private string rootNs = "";

        /// <summary>
        /// Reads the given list of DisplayOptions with namespace to list of TreeListItems
        /// </summary>
        public void CreateFromDisplayOptions(List<DisplayOption> options, string rootNamespace = "") {
            List<GenericListItem> items = options.ConvertAll(o => new GenericListItem(o.Key, o.Label, "", o.Enabled));
            CreateFromGenericList(items, rootNamespace);
        }

        /// <summary>
        /// Reads the given list of GenericListItems with namespace to list of TreeListItems
        /// </summary>
        public void CreateFromGenericList(List<GenericListItem> items, string rootNamespace = "") {
            if (rootNamespace != "")
                AddRootNamespace(items, rootNamespace);

            base.Clear();
            string lastPath = "";
            foreach (GenericListItem item in items){
                int newLevel = item.Key.Count(f => (f == '.'));
                string newPath = GetFullPathFromNameSpace(item.Key, newLevel);
                if(lastPath != newPath){
                    //Detected path change.
                    int nextLevel = FindCommonParentLevel(newPath, lastPath, newLevel);
                    while(nextLevel < newLevel){
                        //now, add group levels to tree until we reach the new level
                        string groupName = GetGroupNameFromPath(item.Key, nextLevel);
                        base.Add(new TreeListItem(groupName, groupName, "", nextLevel, true, true)); 
                        nextLevel++;   
                        lastPath = GetFullPathFromNameSpace(item.Key, nextLevel);
                    }
                }
                //add a new item to the tree
                string label = GetLabelFromNamespace(item.Label);
                base.Add(new TreeListItem(item.Key, label, item.Value, newLevel, false, item.Enabled));
            }
        }

        /// <summary>
        /// Retreives the tree list as a list of DisplayOptions
        /// </summary>
        public List<DisplayOption> ToDisplayOptionList() {
            List<DisplayOption> options = this.Where(treeItem => !treeItem.Group)
                                            .Select(i => new DisplayOption(i.Key, i.Label, i.Enabled))
                                            .Select(i => {i.Key = RemoveRootNamespace(i.Key); return i;})
                                            .ToList();
            return options;
        }

        /// <summary>
        /// Starting at root level, test, how deep in the hierarchy the two given paths are identical. 
        /// Returns the common parent level
        /// </summary>
        private int FindCommonParentLevel (string path1, string path2, int maxLevel){
            int testLevel = 0;
            while(testLevel < maxLevel){
                string testPath1 = GetFullPathFromNameSpace(path1, testLevel);
                string testPath2 = GetFullPathFromNameSpace(path2, testLevel);                    
                if(testPath1 == testPath2)
                    testLevel++;   
                else
                    // Found common parent
                    break;
            }
            return testLevel;
        }

        /// <summary>
        /// Extracts the part of the path that corresponds to the given level
        /// </summary>
        private string GetGroupNameFromPath(string path, int level){
            int beginGroupName = GetIdxOfNthCharInString(path, '.', level)+1;
            int endGroupName = GetIdxOfNthCharInString(path, '.', level+1);
            if(endGroupName == -1)
                endGroupName = path.Length;
            string groupName = path.Substring(beginGroupName, endGroupName-beginGroupName);
            return groupName;
        }

        /// <summary>
        /// Finds the Nth character in the given string
        /// </summary>
        private int GetIdxOfNthCharInString(string str, char ch, int n)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++) {
                if (str[i] == ch){
                    count++;
                    if (count == n)
                        return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Creates a label from the given namespace by removing the path
        /// </summary>
        /// <param name="nameSpace">A string that contains the full path incl. label.</param>
        private string GetLabelFromNamespace(string nameSpace){
            int lastDotPos = nameSpace.LastIndexOf('.');
            string label = nameSpace;
            if (lastDotPos > -1) {
                label = nameSpace.Substring(lastDotPos + 1);
            }
            return label;
        }

        /// <summary>
        /// Gets the full path from the given key (the namespace in front of the actual key name)
        /// </summary>
        /// <param name="key">A string that contains the attributes full name incl. namespace.</param>
        /// <param name="level">The level of the namespace (count of groups), we are interested in.</param>
        private string GetFullPathFromNameSpace(string key, int level){                     
            int endPath = GetIdxOfNthCharInString(key, '.', level+1);
            string path;
            if(endPath == -1)
                path = key;
            else 
                path = key.Substring(0, endPath);
            return path;
        }

        /// <summary>
        /// Adds the given parameter group to keys that do not have any namespace
        /// </summary>
        private void AddRootNamespace(List<GenericListItem> items, string rootNamespace){
            foreach (GenericListItem item in items){
                bool hasGroup = (item.Key.IndexOf('.') > -1);
                if(!hasGroup)
                    item.Key = rootNamespace + "." + item.Key;
            }
            rootNs = rootNamespace;
        }

        /// <summary>
        /// Removes the root namespace from all keys of the given DisplayOptions
        /// </summary>
        private string RemoveRootNamespace(string key){
            if(rootNs != "")
                return key.Replace(rootNs + ".", "");
            else
                return key;
        }
    }
}