﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;
using Visualizer.CameraHandling;
using Zenject;

namespace Visualizer.UI
{
	public interface IViewModeHandler{
		void Init();
		void Refresh ();
		void SetViewMode (ViewMode vMode);
		void EnableDriverViewMode (bool enable);
		ViewMode GetViewMode ();
	}

	/// <summary>
	/// Handles the ViewMode dropdown UI element
	/// </summary>
	public class ViewModeHandler : IViewModeHandler {

		private DropdownField dropdown;
		private ViewMode defaultViewMode = ViewMode.Perspective;
		private ICameraManager cameraManager;
		private IMainUI ui;

		private Dictionary<string, ViewMode> dropdownItems = new Dictionary<string, ViewMode>(){
			{ "Look Down", ViewMode.LookDown },
			{ "Perspective", ViewMode.Perspective }
		};

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ICameraManager cManager, IMainUI mUi){
			cameraManager = cManager;
			ui = mUi;
		}

		/// <summary>
		/// Initial set up of the dropdown
		/// </summary>
		public void Init(){
			dropdown = ui.GetVisualElement("view-mode-dd") as DropdownField;
			if(dropdown != null) {
				dropdown.choices = dropdownItems.Keys.ToList();
				dropdown.RegisterCallback<ChangeEvent<string>>(ev => DropdownValueChanged(ev.newValue));
			}
		}

		/// <summary>
		/// Clear and refill the list of items from the dropdownItems dictionary
		/// </summary>
		public void Refresh(){
			if (dropdown == null)
				return;
			
			ViewMode origMode = GetViewMode();
			dropdown.choices = dropdownItems.Keys.ToList();
			SetViewMode(origMode);
		}

		/// <summary>
		/// Public interface to change the view mode.
		/// </summary>
		public void SetViewMode(ViewMode vMode){
			if (dropdown == null)
				return;	

			string label = FindDropdownItem(vMode);
			if(label != "")
				dropdown.value = label;
			else
				dropdown.value = FindDropdownItem(defaultViewMode);
		}

		/// <summary>
		/// Gets the currently selected view mode.
		/// </summary>
		public ViewMode GetViewMode(){
			if (dropdown == null || dropdown.value == null )
				return defaultViewMode;	
			return dropdownItems [dropdown.value];
		}

		/// <summary>
		/// Sets the default ViewMode.
		/// </summary>
		private void SetDefault(){
			SetViewMode(defaultViewMode);
		}

		/// <summary>
		/// Called, when the Dropdown selection has changed.
		/// </summary>
		private void DropdownValueChanged(string value){
			ViewMode newViewMode = dropdownItems [value];
			cameraManager.SwitchViewMode (newViewMode);
		}

		/// <summary>
		/// Adds/removes the 'Driver' ViewMode to/from the list of dropdownitems.
		/// </summary>
		public void EnableDriverViewMode(bool enable){
			if (dropdown == null)
				return;

			string driverViewModeKey = "Driver";

			if (enable) {
				if (!dropdownItems.ContainsKey (driverViewModeKey))
					dropdownItems.Add (driverViewModeKey, ViewMode.Driver);
			}
			else{
				if (dropdownItems.ContainsKey (driverViewModeKey)) {
					//when the the Driver ViewMode is active while removing the option, we must first switch to default view
					if(GetViewMode() == ViewMode.Driver)
						SetDefault (); 
					dropdownItems.Remove (driverViewModeKey);
				}
			}
			Refresh ();
		}

		/// <summary>
		/// Finds the item (string value) of the given ViewMode in the dropdown option list. Returns an empty string if the given option is not found.
		/// </summary>
		private string FindDropdownItem(ViewMode vMode){
			return dropdownItems.FirstOrDefault (x => x.Value == vMode).Key;
		}
	}
}
