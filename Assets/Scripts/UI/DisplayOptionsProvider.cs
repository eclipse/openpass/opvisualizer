/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Core;
using Visualizer.Utilities;
using Visualizer.Trace;
using Zenject;

namespace Visualizer.UI {

	public delegate void DisplayOptionsChangedEventHandler();

    public interface IDisplayOptionsProvider {
        event DisplayOptionsChangedEventHandler DisplayOptionsChanged;
        List<DisplayOption> Get(AgentPanelType type);
        void Update(AgentPanelType type, List<DisplayOption> displayOptions);
        void Clear();
    }

    public class DisplayOptionsProvider : IDisplayOptionsProvider
    {
        public event DisplayOptionsChangedEventHandler DisplayOptionsChanged;
        private List<DisplayOption> nonFocusAgentDO = null;
        private List<DisplayOption> focusAgentDO = null;
        private List<DisplayOption> perceivedAgentDO = null;

        private IAgentManager agentManager;
        private IDriverPerceptionManager driverPerceptionManager;
        private ISampleContainer agentSampleContainer;
        private IAppStorage appStorage;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAppStorage aStorage, IAgentManager aManager, IDriverPerceptionManager mmManager, ISampleContainer sContainer){    
            agentManager = aManager;
            driverPerceptionManager = mmManager;
            agentSampleContainer = sContainer;
            appStorage = aStorage;
		}

		/// <summary>
		/// Clears all internal data 
		/// </summary>
        public void Clear() {
            if (focusAgentDO != null)
                focusAgentDO.Clear();
            if (nonFocusAgentDO != null)
                nonFocusAgentDO.Clear();
            if (perceivedAgentDO != null)
                perceivedAgentDO.Clear();
            focusAgentDO = null;
            nonFocusAgentDO = null;
            perceivedAgentDO = null;
        }

		/// <summary>
		/// Returns a list of display options for the given agent panel type. 
        /// Note that this function implements lazy loading so the first time it is called, it builds the display options  
		/// </summary>
        public List<DisplayOption> Get(AgentPanelType type) {
            switch(type) {
                case AgentPanelType.FocusAgent:
                    if(focusAgentDO != null)
                        return focusAgentDO;
                    else 
                        return BuildFocusAgentDO();
                case AgentPanelType.NonFocusAgent:
                    if(nonFocusAgentDO != null)
                        return nonFocusAgentDO;
                    else 
                        return BuildNonFocusAgentDO();
                case AgentPanelType.PerceivedAgent:
                    if(perceivedAgentDO != null)
                        return perceivedAgentDO;
                    else 
                        return BuildPerceivedAgentDO();
            }
            return new List<DisplayOption>(); //should not get here...
        }

		/// <summary>
		/// Stores the given list of display options for the given agent panel type in the internal list and in the app storage.
        /// It also informs the AgentManager that the DisplayOptions changed and fires the DisplayOptionChanged event. 
        /// This function cannot be called before the DisplayOptions list has been created (after a Get())
		/// </summary>
        public void Update(AgentPanelType type, List<DisplayOption> displayOptions) {
            if((type == AgentPanelType.FocusAgent && focusAgentDO == null) ||
                (type == AgentPanelType.NonFocusAgent && nonFocusAgentDO == null) ||
                (type == AgentPanelType.PerceivedAgent && perceivedAgentDO == null)) {
                Debug.LogWarning("Trying to call Update before display option list has been created. Update skipped.");
                return;
            }

            switch(type) {
                case AgentPanelType.FocusAgent:
                    UpdateFocusAgentDO(displayOptions);
                break;
                case AgentPanelType.NonFocusAgent:
                    UpdateNonFocusAgentDO(displayOptions);
                break;
                case AgentPanelType.PerceivedAgent:
                    UpdatePerceivedAgentDO(displayOptions);
                break;
            }
            
            agentManager.UpdateDisplayOptionVisibility(Get(AgentPanelType.FocusAgent), Get(AgentPanelType.NonFocusAgent));

            if(DisplayOptionsChanged != null)
                DisplayOptionsChanged();
        }

		/// <summary>
		/// Builds the list of display options for the focus agent panel.
        /// It merges floating display options, visualized options and all noninterpreted sample attribute names that the trace file contains
		/// </summary>
        private List<DisplayOption> BuildFocusAgentDO() {
            string configKey = AppStorageKey.SelectedFocusAgentDisplayOptions;
            List<string> selectedKeys = appStorage.ReadDisplayOptions (configKey); 
            List<string> allKeys = new List<string>();
            string[] floatingPanelDisplayOptions = {"Floating.ID", "Floating.Tacho"};
            string[] visualizedDisplayOptions = {"Visualized.AOIArrow", "Visualized.VisibilityDistance", "Visualized.SensorRange"};
            allKeys.AddRange(floatingPanelDisplayOptions);
            allKeys.AddRange(visualizedDisplayOptions);
            allKeys.AddRange(agentSampleContainer.AgentAttributes);
            List<DisplayOption> displayOptions = CreateDisplayOptions(allKeys, selectedKeys);
            AddVisibilityDistanceToDisplayOptionLabel(displayOptions);
            focusAgentDO = displayOptions;
            return displayOptions;
        }

		/// <summary>
		/// Builds the list of display options for the nonfocus agent panel.
        /// It merges floating display options, visualized options and all noninterpreted sample attribute names that the trace file contains
		/// </summary>
        private List<DisplayOption> BuildNonFocusAgentDO() {
            string configKey = AppStorageKey.SelectedNonFocusAgentDisplayOptions;
            List<string> selectedKeys = appStorage.ReadDisplayOptions (configKey); 
            List<string> allKeys = new List<string>();
            string[] floatingPanelDisplayOptions = {"Floating.ID", "Floating.Tacho"};
            string[] visualizedDisplayOptions = {"Visualized.AOIArrow", "Visualized.SensorRange"};
            allKeys.AddRange(floatingPanelDisplayOptions);
            allKeys.AddRange(visualizedDisplayOptions);
            allKeys.AddRange(agentSampleContainer.AgentAttributes);
            List<DisplayOption> displayOptions = CreateDisplayOptions(allKeys, selectedKeys);
            nonFocusAgentDO = displayOptions;
            return displayOptions;
        }

		/// <summary>
		/// Builds the list of display options for the focus agent panel.
        /// It merges floating display options and all noninterpreted sample attribute names that the driver perception file contains
		/// </summary>
        private List<DisplayOption> BuildPerceivedAgentDO() {
            string configKey = AppStorageKey.SelectedPerceivedAgentDisplayOptions;
            List<string> selectedKeys = appStorage.ReadDisplayOptions (configKey);
            List<string> allKeys = new List<string>();
            allKeys.Add("Floating.ID");
            allKeys.AddRange(driverPerceptionManager.GetAvailableAgentAttributes());
            List<DisplayOption> displayOptions = CreateDisplayOptions(allKeys, selectedKeys); 
            perceivedAgentDO = displayOptions;           
            return displayOptions;
        }

		/// <summary>
		/// Stores the given list of display options for non focus agent panel in the internal list and in the app storage.
		/// </summary>
        private void UpdateNonFocusAgentDO(List<DisplayOption> displayOptions) {
            nonFocusAgentDO = displayOptions;
            string configKey = AppStorageKey.SelectedNonFocusAgentDisplayOptions;
            appStorage.WriteDisplayOptions (configKey, displayOptions);
        }

		/// <summary>
		/// Stores the given list of display options for perceived agent panel in the internal list and in the app storage.
		/// </summary>
        private void UpdatePerceivedAgentDO(List<DisplayOption> displayOptions) {
            perceivedAgentDO = displayOptions;
            string configKey = AppStorageKey.SelectedPerceivedAgentDisplayOptions;
            appStorage.WriteDisplayOptions (configKey, displayOptions);
        }

		/// <summary>
		/// Stores the given list of display options for focus agent panel in the internal list and in the app storage.
		/// </summary>
        private void UpdateFocusAgentDO(List<DisplayOption> displayOptions) {
            focusAgentDO = displayOptions;
            string configKey = AppStorageKey.SelectedFocusAgentDisplayOptions;
            appStorage.WriteDisplayOptions (configKey, displayOptions);
        }

		/// <summary>
		/// Creates a list of display options from the two given string lists while setting the enabled attribute of the options
        /// Note that if there are no options selected, we set all options to enabled (we suppose, its the first time 
        /// the user runs the application, so the appstorage does not contain the the selected display options yet)
		/// </summary>
        private List<DisplayOption> CreateDisplayOptions(List<string> allKeys, List<string> selectedKeys) {
            List<DisplayOption> options = new List<DisplayOption>();
            foreach(string key in allKeys) {
                bool enabled = (selectedKeys.Count > 0) ? selectedKeys.Contains(key) : true;
                options.Add(new DisplayOption(key, Translator.GetLabel(key), enabled));
            }
            return options;
        }

        /// <summary>
		/// Extend the label of the Visibility Distance display option with the distance value
        /// Note that this value is currently global, so it doesn't matter, from which agent we read it (we take the first one)
        /// </summary>
        private void AddVisibilityDistanceToDisplayOptionLabel(List<DisplayOption> displayOptions) {
            DisplayOption visibilityDistanceOption = displayOptions.Find(d => d.Key == "Visualized.VisibilityDistance");
            List<int> agentIDs = agentManager.GetAgentIDs();
            if (agentIDs.Count > 0){
                float visibilityDistance = agentManager.GetAgent(agentIDs.First()).DriverViewDistance;
                visibilityDistanceOption.Label += " (" + visibilityDistance + "m)";
            }
        }
    }
}