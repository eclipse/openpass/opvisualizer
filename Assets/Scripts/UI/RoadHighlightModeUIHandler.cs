﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;
using Visualizer.Core;
using Zenject;

namespace Visualizer.UI
{
	public interface IRoadHighlightModeUIHandler{
		void Init (VisualElement root);
	}

	/// <summary>
	/// Handles the user's choice of road highlight mode (road, lane, lane section)
	/// </summary>
	public class RoadHighlightModeUIHandler : IRoadHighlightModeUIHandler{

		private DropdownField dropdown;
		private IRoadExplorer roadExplorer;

		private readonly Dictionary<string, RoadHighlightMode> HighlightModes = new Dictionary<string, RoadHighlightMode>(){
			{ "Road", RoadHighlightMode.Road },
			{ "Lane", RoadHighlightMode.Lane },
			{ "Lane Section", RoadHighlightMode.LaneSegment }
		};

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IRoadExplorer rExplorer){
			roadExplorer = rExplorer;
		}

		/// <summary>
		/// Initialize the handler
		/// </summary>
		public void Init (VisualElement root){
			dropdown = root.Q<DropdownField>("road-highlight-mode-dd");

			if (dropdown != null){
				dropdown.tooltip = "T:Road Highlight Mode";
				dropdown.AddManipulator(new TooltipManipulator(root.Q<VisualElement>("tooltip") as Tooltip));
				dropdown.choices = HighlightModes.Keys.ToList ();
				dropdown.RegisterCallback<ChangeEvent<string>>(ev => DropdownValueChanged(ev.newValue));
				dropdown.SetValueWithoutNotify("Road");
			}
		}

		/// <summary>
		/// Callback that is fired after the user changed the dropdown value
		/// </summary>
		/// <param name="value">Value.</param>
		private void DropdownValueChanged(string value){
			RoadHighlightMode newHighlightMode = HighlightModes [value];
			roadExplorer.SetHighlightMode(newHighlightMode);
		}
	}
}
