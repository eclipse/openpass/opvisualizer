/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;

namespace Visualizer.UI {

    public class FloatingAgentPanelHandler
    {
        private VisualElement panel;
        private VisualElement agentId;
        private Label agentIdLabel;
        private Label velocityLabel;
        private VisualElement tachoPointer;
        private VisualElement tachoColor;
        private VisualElement tachoBackground;
        private VisualElement perceiveIcon;
        private Agent agentRef;
        private float velocity1;
		private float velocity2;
        private float yOffset;
		private Vector2 currentPanelPos = Vector2.zero;
        private IFormatProvider formatProvider;
        private ICameraManager cameraManager;
        private IViewModeHandler viewModeHandler;

        private enum PanelDisplay { 
            Hide, 
            Show 
        };
        private PanelDisplay displayMode; 

        private bool tachoVisible = false;
        private bool panelOutOfView = false;
        private bool agentIdVisible = false;
        private readonly string accSampleKey = "AccelerationEgo";
        private readonly string velocitySampleKey = "VelocityEgo";
		private float minVel = 0f;
		private float maxVel = 250f;
		private float minVelAngle = -45f;
		private float maxVelAngle = 215;
		private float maxAcc = 7f;

        public float CameraDistance { get; private set; } = 0f;

        public FloatingAgentPanelHandler(VisualElement vElement, List<DisplayOption> displayOptions, Agent agent, string label, AgentPanelType type, ICameraManager cManager, IViewModeHandler vmHandler) {
            panel = vElement;
            agentRef = agent;
            cameraManager = cManager;
            viewModeHandler = vmHandler;
            agentIdLabel = panel.Q<Label>("agent-id-lbl");
            agentId = panel.Q<VisualElement>("agent-id");
            velocityLabel = panel.Q<Label>("velocity-lbl");
            perceiveIcon = panel.Q<VisualElement>("perceive-icon");
            tachoPointer = panel.Q<VisualElement>("tacho-pntr-base");
            tachoColor = panel.Q<VisualElement>("tacho-color");
            tachoBackground = panel.Q<VisualElement>("tacho-background");
            agentIdLabel.text = label;
            formatProvider = CultureInfo.CreateSpecificCulture("en-US");
            bool isPerceivedAgent = type == AgentPanelType.PerceivedAgent;
            yOffset = isPerceivedAgent ? -40f : 40f;
            perceiveIcon.style.display = isPerceivedAgent ? DisplayStyle.Flex : DisplayStyle.None;
            agentId.style.paddingLeft = isPerceivedAgent ? 10f : 3f;
            agentId.style.paddingRight = isPerceivedAgent ? 10f : 3f;

            UpdateVisibility(displayOptions);
        }


        /// <summary>
		/// Turns id or tacho on or off 
		/// </summary>
        public void UpdateVisibility(List<DisplayOption> displayOptions) {
            DisplayOption optID = displayOptions.FirstOrDefault(o => o.Key == "Floating.ID");
            if(optID != null)
                agentIdVisible = optID.Enabled;

            bool tachoDataAvailable = false;
            if(agentRef != null && agentRef.OptionalAttributes != null)
                tachoDataAvailable = (agentRef.OptionalAttributes.ContainsKey(accSampleKey) && agentRef.OptionalAttributes.ContainsKey(velocitySampleKey));

            DisplayOption optTacho = displayOptions.FirstOrDefault(o => o.Key == "Floating.Tacho");
            if(optTacho != null)
                tachoVisible = tachoDataAvailable && optTacho.Enabled;

            ShowComponents();
        }

        /// <summary>
		/// Show/hide tacho 
		/// </summary>
		private void ShowComponents(){
            velocityLabel.visible = tachoVisible && !panelOutOfView;
            tachoPointer.visible = tachoVisible && !panelOutOfView;
            tachoBackground.visible = tachoVisible && !panelOutOfView;
            tachoColor.visible = tachoVisible && !panelOutOfView;
            agentId.visible = agentIdVisible;
        }

		/// <summary>
		/// Calculate the current info panel position based on the agents position and update the tacho
		/// </summary>
		public void UpdateFloatingPanel(){
            if(!tachoVisible && !agentIdVisible)
                return;

			Rect agentBoundingFrame = CalculateAgentBoundingFrame ();
            float scaleFactor = CalculateScaleFactor();

            Vector2 panelPos = Vector2.zero;               
            if(displayMode != PanelDisplay.Hide){
                panelPos = CalculatePanelPos(agentBoundingFrame, scaleFactor);
                CameraDistance = Vector3.Distance(agentRef.TargetPosition, cameraManager.GetCameraPos());            
            }
            SetPanelPos(panelPos);
            SetPanelScale(scaleFactor);

            if(tachoVisible){
                UpdateTacho();
                ShowComponents();
            }
		}

		/// <summary>
		/// Calculates the agent bounding rectangle in 2D screen space.
		/// </summary>
		private Rect CalculateAgentBoundingFrame(){
            displayMode = PanelDisplay.Show;

			if (Camera.main == null || agentRef.IsHidden()) {
                displayMode = PanelDisplay.Hide;
				return Rect.zero;
            }

			//If the agent is not visible to the camera, but not behind it, we display the panel small
			Vector3 viewPos = Camera.main.WorldToViewportPoint (agentRef.TargetPosition);

            //If the agent is behind the camera, we hide the panel
			if (viewPos.x < -1.5f || viewPos.x > 1.5f || viewPos.y < -1.5f || viewPos.y > 1.5f || viewPos.z < 0f)
                displayMode = PanelDisplay.Hide;

			Vector3[] boundingBox = agentRef.GetBoundingBox ();

			//Get the vertices in UI space
			for (int i = 0; i < boundingBox.Length; i++) {
				boundingBox [i] = Camera.main.WorldToScreenPoint (boundingBox [i]);
				boundingBox [i].y = Screen.height - boundingBox [i].y;
			}

			//Calculate the min and max positions
			Vector3 min = boundingBox [0];
			Vector3 max = boundingBox [0];
			for (int i = 1; i < boundingBox.Length; i++) {
				min = Vector3.Min (min, boundingBox [i]);
				max = Vector3.Max (max, boundingBox [i]);
			}

			//Construct a rect of the min and max positions and apply the margin
			return Rect.MinMaxRect (min.x, min.y, max.x, max.y);
        }

        private Vector2 CalculatePanelPos(Rect agentBoundingFrame, float scaleFactor){
            Vector2 targetPanelPos = new Vector2 ();

            float panelWidth = panel.resolvedStyle.width;
            float panelHeight = panel.resolvedStyle.height;
			targetPanelPos.x = agentBoundingFrame.center.x - panelWidth / 2;
			targetPanelPos.y = agentBoundingFrame.y - panelHeight - (yOffset * scaleFactor);

            panelOutOfView = targetPanelPos.x < 0f ||
                                targetPanelPos.y < -20f ||
                                targetPanelPos.x > Screen.width-panelWidth ||
                                targetPanelPos.y > Screen.height-panelHeight;

            targetPanelPos.x = Mathf.Clamp(targetPanelPos.x, 0, Screen.width-panelWidth);
            targetPanelPos.y = Mathf.Clamp(targetPanelPos.y, 0, Screen.height-panelHeight);
            
            bool smoothMove = Vector2.Distance(currentPanelPos, targetPanelPos) < 200f; 
            if(smoothMove) {
                currentPanelPos.x = Mathf.SmoothDamp (currentPanelPos.x, targetPanelPos.x, ref velocity1, 0.15f);
                currentPanelPos.y = Mathf.SmoothDamp (currentPanelPos.y, targetPanelPos.y, ref velocity2, 0.15f);
            } else {
                currentPanelPos.x = targetPanelPos.x;
                currentPanelPos.y = targetPanelPos.y;
            }

            return currentPanelPos;
		}

        private void SetPanelPos(Vector2 panelPos) {
            panel.style.left = panelPos.x;
            panel.style.top = panelPos.y;
        }

        private void SetPanelScale(float scaleFactor) {
            panel.transform.scale = new Vector3(scaleFactor, scaleFactor, 1f);
        }

		/// <summary>
		/// Calculate a factor between 0 and 1 that inversely correlates to the distance between the agent and the camera.
        /// The calcualted number is used to scale the panel.
        /// minDist is the minimal camera distance at (and below) which the panel will have full size (scale 1)
        /// maxDist is the maximal camera distance at (and above) which the panel will be invisible (scale 0)
		/// </summary>
        private float CalculateScaleFactor() {
            if (displayMode == PanelDisplay.Hide)
                return 0f;

            float minDist = 10f;
            float maxDist = viewModeHandler.GetViewMode() == ViewMode.LookDown ? 3000f : 300f;

            float range = maxDist - minDist;
            float factor = 1 - (CameraDistance - minDist)/range;
            factor = Mathf.Clamp(factor, 0f, 1f);
            
            return factor;
        }

		/// <summary>
		/// Update the label background according to speed and acceleration
		/// </summary>
        private void UpdateTacho() {
            UpdateTachoAcc();
            UpdateTachoVel();
        }

		/// <summary>
		/// Update the background color according to the acceleration
		/// </summary>
        private void UpdateTachoAcc() {
            float acc = 0f;
            if(agentRef != null && agentRef.OptionalAttributes != null && agentRef.OptionalAttributes.ContainsKey(accSampleKey))
                float.TryParse (agentRef.OptionalAttributes[accSampleKey], NumberStyles.Float, formatProvider, out acc); 
            float accStrength = Mathf.Abs(acc / maxAcc); 
			Color bckgColor = Color.gray;
			if (acc >= 0f)
				bckgColor = new Color (0f, 1f, 0f, accStrength);
			else
				bckgColor = new Color (1f, 0f, 0f, accStrength);
			tachoColor.style.unityBackgroundImageTintColor = bckgColor;
        }

        /// <summary>
		/// Update the velocity label according to the current velocity
		/// </summary>
        private void UpdateTachoVel() {
            float velocity = 0f;
            if(agentRef != null && agentRef.OptionalAttributes != null && agentRef.OptionalAttributes.ContainsKey(velocitySampleKey))
                float.TryParse (agentRef.OptionalAttributes [velocitySampleKey], NumberStyles.Float, formatProvider, out velocity); 
			velocity *= ((60f * 60f) / 1000f); //convert from m/s to km/h
			velocityLabel.text = string.Format ("{0:0}", velocity);

			float totalAngle = Mathf.Abs(maxVelAngle - minVelAngle);
			float totalSpeed = Mathf.Abs(maxVel - minVel);
			float speedFraction = velocity / totalSpeed;
			float angle = minVelAngle + (speedFraction * totalAngle);
            tachoPointer.transform.rotation = Quaternion.Euler(0f, 0f, angle);
        }

        public VisualElement GetRootVisualElement() {
            return panel;
        }
    }
}