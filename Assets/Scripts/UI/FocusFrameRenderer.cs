﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;
using Visualizer.Utilities;
using Visualizer.AgentHandling;
using Visualizer.UI;
using Zenject;

namespace Visualizer.CameraHandling
{

		/// <summary>
	/// Delegate that can be used to get notified when the focus agent moves out of the camera view
	/// </summary>
	public delegate void FocusAgentOutOfViewWarningEventHandler(bool flashSemaphore);

	public interface IFocusFrameRenderer{
		event FocusAgentOutOfViewWarningEventHandler FocusAgentOutOfViewWarning;
		void ShowFrame (Agent agent);
		void UpdateFrame ();
		void Init (VisualElement root);
		void Clear ();
	}

	/// <summary>
	/// Handles the attachment of the camera to agents
	/// </summary>
	public class FocusFrameRenderer : IFocusFrameRenderer {
		public event FocusAgentOutOfViewWarningEventHandler FocusAgentOutOfViewWarning;
        private VisualElement focusFrame = null;
		private Agent focusedAgent = null;
		private float changeFocusMoment;
		private Rect fullScreenFocusFrame;
		private float smoothingVelX, smoothingVelY, smoothingVelW, smoothingVelH;
		private bool agentOutOfViewWarning = false;
		private bool flashSemaphore = false;
		private float flashMoment;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){
		}

		public void Init(VisualElement root){
			focusFrame = root.Q<VisualElement>("focus-frame");
			flashMoment = Time.time;
		}
			
		/// <summary>
		/// Clear this instance.
		/// </summary>
		public void Clear(){
			focusedAgent = null;
		}

		public void ShowFrame (Agent agent) {
			focusedAgent = agent;
			changeFocusMoment = Time.time;
		}

		/// <summary>
		/// Called for rendering the frame.
		/// </summary>
		public void UpdateFrame () {
			float now = Time.time;
			float fadeOutTime = 7f;
			float timeEllapsedSinceAttach = now - changeFocusMoment;
			if (timeEllapsedSinceAttach < fadeOutTime) {
				float opacity = Mathf.Lerp (1f, 0f, timeEllapsedSinceAttach/fadeOutTime);
				RenderFocusFrame (opacity);	
			}

			if(agentOutOfViewWarning){
				if(now - flashMoment > 0.1f) {
					flashSemaphore = !flashSemaphore;	
					focusFrame.EnableInClassList("highlighted-focus-frame", flashSemaphore);
					flashMoment = now;
					if(focusedAgent != null && FocusAgentOutOfViewWarning != null)
						FocusAgentOutOfViewWarning(flashSemaphore);
				}
			} else if(flashSemaphore){
				focusFrame.EnableInClassList("highlighted-focus-frame", false);
				if(FocusAgentOutOfViewWarning != null)
					FocusAgentOutOfViewWarning(false);
				flashSemaphore = false;
			}
		}

		/// <summary>
		/// Draws the focus frame around the attached agent.
		/// </summary>
		private void RenderFocusFrame(float opacity){
			Rect targetBoundingRect;
			if (focusedAgent != null) {
				targetBoundingRect = CalculateAgentBoundingFrame ();
			} else {
				//full screen frame
				targetBoundingRect = new Rect (0, 0, Screen.width, Screen.height);
				opacity = 0f;
			}

			float margin = 100f;
			agentOutOfViewWarning = targetBoundingRect.xMax < margin ||
									targetBoundingRect.xMin > Screen.width-margin ||
									targetBoundingRect.yMin > Screen.height-margin ||
									targetBoundingRect.yMax < margin;

			if(targetBoundingRect == Rect.zero) {
				focusFrame.style.opacity = 0f;
			} else {
				focusFrame.style.left = Mathf.SmoothDamp (focusFrame.resolvedStyle.left, targetBoundingRect.x, ref smoothingVelX, 0.1f);
				focusFrame.style.top = Mathf.SmoothDamp (focusFrame.resolvedStyle.top, targetBoundingRect.y, ref smoothingVelY, 0.1f);
				focusFrame.style.width = Mathf.SmoothDamp (focusFrame.resolvedStyle.width, targetBoundingRect.width, ref smoothingVelW, 0.1f);
				focusFrame.style.height = Mathf.SmoothDamp (focusFrame.resolvedStyle.height, targetBoundingRect.height, ref smoothingVelH, 0.1f);
				focusFrame.style.opacity = opacity;
			}
		}
			
		/// <summary>
		/// Returns the agent bounding rectangle in 2D screen space.
		/// </summary>
		private Rect CalculateAgentBoundingFrame(){
			if (Camera.main == null)
				return Rect.zero;
			
			Vector3[] boundingBox = focusedAgent.GetBoundingBox();

			//If the agent is not visible to the camera, we hide the frame
			Vector3 viewPos = Camera.main.WorldToViewportPoint (boundingBox[0]);
			if (viewPos.x < -2.5f || viewPos.x > 2.5f || viewPos.y < -2.5f || viewPos.y > 2.5f || viewPos.z < 3f) {
				return Rect.zero;
			}
				
			//Get the vertices in GUI space
			for (int i = 0; i < boundingBox.Length; i++) {
				boundingBox [i] = Camera.main.WorldToScreenPoint (boundingBox [i]);
				boundingBox [i].y = Screen.height - boundingBox [i].y;
			}

			//Calculate the min and max positions
			Vector3 min = boundingBox [0];
			Vector3 max = boundingBox [0];
			for (int i = 1; i < boundingBox.Length; i++) {
				min = Vector3.Min (min, boundingBox [i]);
				max = Vector3.Max (max, boundingBox [i]);
			}

			//Construct a rect of the min and max positions
			Rect rect = Rect.MinMaxRect (min.x, min.y, max.x, max.y);

			//limit rect tot screen dimensions
			float margin = 20f;
			rect.xMax = Mathf.Max(rect.xMax, margin);
			rect.yMax = Mathf.Max(rect.yMax, margin);
			rect.xMin = Mathf.Min(rect.xMin, Screen.width-margin);
			rect.yMin = Mathf.Min(rect.yMin, Screen.height-margin);

			return rect;
		}
	}
}