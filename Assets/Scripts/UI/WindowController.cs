/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Core;
using Visualizer.Utilities;
using Visualizer.Trace;
using Zenject;

namespace Visualizer.UI {

    public interface IWindowController {
        void Init(VisualElement root);
        VisualElement OpenWindow(string resourceName, string title, string subtitle, int width, int height = 0);
        void CloseWindow();
        void ShowLoadingMessage(string msgText, float autoHideDelay = 0f);
        void HideLoadingMessage(float delay = 0f);
        void ShowErrorMessage(string msgText, string details);
        void HideErrorMessage();
        void Clear();
    }

    public static class UIScale {
        public static readonly Vector3 DownXY = new Vector3(0f, 0f, 1f);
        public static readonly Vector3 DownX = new Vector3(0f, 1f, 1f);
        public static readonly Vector3 Up = new Vector3(1f, 1f, 1f);
    }

    public class WindowController : MonoBehaviour, IWindowController
    {
        VisualElement windowContainer;
        VisualElement loadingMessagePanel;
        Label loadingMessage;
        VisualElement errorMessagePanel;
        Label errorMessage;
        Label errorMessageDetails;
        IKeyboardEventHandler keyboardEventHandler;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IKeyboardEventHandler keHandler){
            keyboardEventHandler = keHandler;
		}

        /// <summary>
		/// Clears the window controller
		/// </summary>
        public void Clear() {
            CloseWindow();
            HideErrorMessage();
        }

        /// <summary>
		/// Initializes the window controller
		/// </summary>
        public void Init(VisualElement root) {
            windowContainer = root.Q<VisualElement>("window-container");
            loadingMessagePanel = root.Q<VisualElement>("loading-message-panel");
            loadingMessage = loadingMessagePanel.Q<Label>("message-lbl");
            errorMessagePanel = root.Q<VisualElement>("error-message-panel");
            errorMessagePanel.Q<Button>("close-btn").RegisterCallback<ClickEvent>(ev => HideErrorMessage());
            errorMessage = errorMessagePanel.Q<Label>("message-lbl");
            errorMessageDetails = errorMessagePanel.Q<Label>("details-lbl"); 
            CloseWindow();
            HideLoadingMessage();
            HideErrorMessage();
        }

        /// <summary>
		/// Loads the given uxml resource and displays it in windowContainer
		/// </summary>
        public VisualElement OpenWindow(string resourceName, string title, string subtitle, int width, int height = 0) {
            if (IsWindowOpened())
                CloseWindow();

            var template = Resources.Load<VisualTreeAsset>(resourceName);
            VisualElement window = template.CloneTree();

            windowContainer.Clear();
            windowContainer.Add(window);

            window.Q<Button>("close-btn").RegisterCallback<ClickEvent>(ev => CloseWindow());
            window.Q<Label>("window-title").text = title;
            window.Q<Label>("window-subtitle").text = subtitle;

            int maxHeight = Screen.height - 200;
            int maxWidth = Screen.width - 200;
            height = Math.Min(maxHeight, height);
            width = Math.Min(maxWidth, width);
            windowContainer.style.height = (height > 0) ? height : maxHeight;
            windowContainer.style.width = width;
            windowContainer.transform.scale = UIScale.Up;
            windowContainer.style.opacity = 1;

            keyboardEventHandler.EnableHandling = false;
            return window;
        }

        /// <summary>
		/// Hides the currently opened window
		/// </summary>
        public void CloseWindow() {
            keyboardEventHandler.EnableHandling = true;
            windowContainer.transform.scale = UIScale.DownX;
            windowContainer.style.opacity = 0;
         }

        /// <summary>
		/// Returns whether a window is currently shown
		/// </summary>
        private bool IsWindowOpened() {
            return windowContainer.transform.scale == UIScale.Up;
        }

        /// <summary>
		/// Displays the info message box with the given text
		/// </summary>
        public void ShowLoadingMessage(string msgText, float autoHideDelay = 0f) {
            HideErrorMessage();
            loadingMessage.text = Helpers.SanitizeScreenText(msgText);
            loadingMessagePanel.EnableInClassList("smooth-scale", false);
            ScaleLoadingMessage(UIScale.Up);
            if(autoHideDelay > 0f)
                HideLoadingMessage(autoHideDelay);
        }

        /// <summary>
		/// Hides the info message box after the given delay in ms
		/// </summary>
        public void HideLoadingMessage(float delay = 0f) {
            if (delay > 0)
                loadingMessagePanel.EnableInClassList("smooth-scale", true);
            this.InvokeDelayed(() => ScaleLoadingMessage(UIScale.DownXY), delay);
        }

        /// <summary>
		/// Scales the loading message box up/down
		/// </summary>
        private void ScaleLoadingMessage(Vector3 scale) {
            loadingMessagePanel.transform.scale = scale;
        }

        /// <summary>
		/// Displays the error message box with the given text
		/// </summary>
        public void ShowErrorMessage(string msgText, string details) {
            HideLoadingMessage();
            errorMessage.text = Helpers.SanitizeScreenText(msgText);
            errorMessageDetails.text = Helpers.SanitizeScreenText(details);
            errorMessagePanel.transform.scale = UIScale.Up;
        }

        /// <summary>
		/// Hides the error message box
		/// </summary>
        public void HideErrorMessage() {
            errorMessagePanel.transform.scale = UIScale.DownX;
        }
    }
}