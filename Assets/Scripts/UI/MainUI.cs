/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Core;
using Visualizer.Utilities;
using Visualizer.Trace;
using Zenject;

namespace Visualizer.UI {

	/// <summary>
	/// Delegates that are executed at various user triggered UI events
	/// </summary>
	public delegate void LoadTraceFileEventHandler(string path);
	public delegate void LoadSceneryFileEventHandler(string path);
    public delegate void LoadRunEventHandler(string traceFilePath, int runID);
    public delegate void ReadRunListEventHandler();
    public delegate void SwitchUIModeRequestEventHandler(UIMode newMode);

    public interface IMainUI {
        event LoadTraceFileEventHandler LoadTraceFileRequest;
        event LoadSceneryFileEventHandler LoadSceneryFileRequest;
        event LoadRunEventHandler LoadRunRequest;
        event ReadRunListEventHandler ReadRunListRequest;
        event SwitchUIModeRequestEventHandler SwitchUIModeRequest;
        void Init(UIMode mode, SimulationMetaData simInfo);
        void RefreshPanelData();
        void ShowAgentPopupMenu(Agent agent, Vector3 popupPos);
        void ShowPerceivedAgentPopupMenu(Agent agent, Vector3 popupPos);
        void ShowFocusAgentPanel(Agent agent);
        void ShowNonFocusAgentPanel(Agent agent);
        void ShowPerceivedAgentPanel(Agent agent);
        void ShowDriverPerception(Agent agent);      
        void ToggleCursorPosition();
        void DisplayParserWorkload(float workload);
        void HidePopupMenu();
        VisualElement GetVisualElement(string name);
        void ShowRunSelectorWnd(string traceFile, List<Run> runs);
        void HideRunSelectorMenuItem(bool hide);
        void HideTooltip();
        void Clear();
    }

	/// <summary>
	/// The main UI is the central class that handles the 2D controls on the screen. It coordinates several
    /// specific UI handler classes.
	/// </summary>
    public class MainUI : MonoBehaviour, IMainUI
    {
        VisualElement root;
        VisualElement mainMenu;
        VisualElement agentMenu;
        VisualElement windowContainer;
        VisualElement mousePosPanel;
        Label agentMenuLabel;
        Label parserWorkloadLabel;
        Label simulationInfoIcon;
        Button dynamicAgentInfoMI;
        Button staticAgentInfoMI;
        Button driverPerceptionMI;
        Button perceivedAgentInfoMI;
        Button startStopBtn;

        Agent clickedAgent = null;
        int origDropdownFieldPos = 0; //temporary workaround
        SimulationMetaData simulationMetaData;

        private IDriverPerceptionManager driverPerceptionManager;
        private IAnimationController animationController;
        private IAgentManager agentManager;
        private IFileBrowserWndHandler fileBrowserWndHandler;
        private IRunSelectorWndHandler runSelectorWndHandler;
        private IFocusAgentSelectorWndHandler focusAgentSelectorWndHandler;
        private IAppVersion appVersion;
        private IRoadHighlightModeUIHandler roadHighlightModeUIHandler;
        private IViewModeHandler viewModeHandler;
        private IFocusCameraHandler focusCameraHandler;
        private IFocusFrameRenderer focusFrameRenderer;
        private IPlaybackSpeedHandler playbackSpeedHandler;
        private ITimeAxisHandler timeAxisHandler; 
        private IAgentPanelManager agentPanelManager;
        private IDisplayOptionsProvider displayOptionsProvider;
        private IWindowController windowController;
        private IDisplayOptionsWndHandler displayOptionsWndHandler;
        private IStaticAgentInfoWndHandler staticAgentInfoWndHandler;
        private IAppStorage appStorage;
        private UIDocument uiDocument;

        public event LoadTraceFileEventHandler LoadTraceFileRequest;
        public event LoadSceneryFileEventHandler LoadSceneryFileRequest;
        public event LoadRunEventHandler LoadRunRequest;
        public event ReadRunListEventHandler ReadRunListRequest;
        public event SwitchUIModeRequestEventHandler SwitchUIModeRequest;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(UIDocument uiDoc, IAppStorage aStorage, IFocusFrameRenderer ffRenderer, IStaticAgentInfoWndHandler saiHandler, IDisplayOptionsWndHandler dolHandler, IWindowController wController, IDisplayOptionsProvider doProvider, IAgentPanelManager aipManager, IFocusCameraHandler acHandler, IPlaybackSpeedHandler psHandler, ITimeAxisHandler taHandler, IFocusAgentSelectorWndHandler fasHandler, IViewModeHandler vmHandler, IRoadHighlightModeUIHandler rhmHandler, IAppVersion aVersion, IRunSelectorWndHandler rsHandler, IFileBrowserWndHandler fbHandler, IAgentManager aManager, IDriverPerceptionManager mmManager, IAnimationController aController){
            driverPerceptionManager = mmManager;
            animationController = aController;
            agentManager = aManager;
            fileBrowserWndHandler = fbHandler;
            runSelectorWndHandler = rsHandler;
            focusAgentSelectorWndHandler = fasHandler;
            appVersion = aVersion;
            roadHighlightModeUIHandler = rhmHandler;
            viewModeHandler = vmHandler;
            focusCameraHandler = acHandler;
            playbackSpeedHandler = psHandler;
            timeAxisHandler = taHandler;
            agentPanelManager = aipManager;
            displayOptionsProvider = doProvider;
            windowController = wController;
            displayOptionsWndHandler = dolHandler;
            staticAgentInfoWndHandler = saiHandler;
            focusFrameRenderer = ffRenderer;
            appStorage = aStorage;
            uiDocument = uiDoc;
		}

        /// <summary>
        /// Clears the UI
        /// </summary>
        public void Clear() {
            HidePopupMenu();
            driverPerceptionManager.Clear();
            windowController.Clear();
            agentPanelManager.Clear();
            displayOptionsProvider.Clear();
            clickedAgent = null;
        }

        /// <summary>
        /// Query a visual element from the visual tree with the given name
        /// </summary>
        public VisualElement GetVisualElement(string name) {
            return root.Q<VisualElement>(name);
        }

        /// <summary>
        /// Monobehaviour routine executed as soon the application launches
        /// </summary>
        void OnEnable() {
            Startup();
        }

        /// <summary>
        /// Monobehaviour routine executed as soon the application launches
        /// </summary>
        public void Startup() {
            root = uiDocument.rootVisualElement;
            AddRootStylesheet();

            windowContainer = root.Q<VisualElement>("window-container");
            mainMenu = root.Q<VisualElement>("main-menu");
            agentMenu = root.Q<VisualElement>("agent-menu");
            simulationInfoIcon = root.Q<Label>("simulation-info-icon");
            agentMenuLabel = agentMenu.Q<Label>("agent-id");
            dynamicAgentInfoMI = agentMenu.Q<Button>("dynamic-agent-info-menuitem");
            staticAgentInfoMI = agentMenu.Q<Button>("static-agent-info-menuitem");
            driverPerceptionMI = agentMenu.Q<Button>("driver-perception-menuitem");
            perceivedAgentInfoMI = agentMenu.Q<Button>("perceived-agent-info-menuitem");
            startStopBtn = root.Q<Button>("start-stop-btn");
            parserWorkloadLabel = root.Q<Label>("parser-workload-lbl");
            mousePosPanel = root.Q<VisualElement>("mousepos-panel");

            Button mainMenuBtn = root.Q<Button>("main-menu-btn");
            Button globalDisplayOptionsBtn = root.Q<Button>("global-display-options-btn");
            Button loadTraceMI = root.Q<Button>("load-trace-menu-item");
            Button loadSceneryMI = root.Q<Button>("load-scenery-menu-item");
            Button loadRunMI = root.Q<Button>("load-run-menu-item");
            Button sensorPositionerMI = root.Q<Button>("sensor-positioner-menu-item");
            Button usageHintsMI = root.Q<Button>("usage-hints-menu-item");
            Button versionInfoMI = root.Q<Button>("version-info-menu-item");
            Button stepBackBtn = root.Q<Button>("step-back-btn");
            Button stepForwardBtn = root.Q<Button>("step-forward-btn");
            Button closeMousePosBtn = root.Q<Button>("mousepos-close-btn");

            startStopBtn.RegisterCallback<ClickEvent>(ev => OnClickStartPlaybackBtn());
            stepBackBtn.RegisterCallback<ClickEvent>(ev => OnClickStepBackButton());
            stepForwardBtn.RegisterCallback<ClickEvent>(ev => OnClickStepForwardButton());
            mainMenuBtn.RegisterCallback<ClickEvent>(ev => OnClickMainMenuBtn());
            globalDisplayOptionsBtn.RegisterCallback<ClickEvent>(ev => OnClickGlobalDisplayOptionsBtn());
            loadTraceMI.RegisterCallback<ClickEvent>(ev => OnClickLoadTrace());
            loadSceneryMI.RegisterCallback<ClickEvent>(ev => OnClickLoadScenery());
            loadRunMI.RegisterCallback<ClickEvent>(ev => OnClickLoadRun());
            sensorPositionerMI.RegisterCallback<ClickEvent>(ev => OnClickSensorPositioner());
            usageHintsMI.RegisterCallback<ClickEvent>(ev => OnClickUsageHints());
            versionInfoMI.RegisterCallback<ClickEvent>(ev => OnClickVersionInfo());
            closeMousePosBtn.RegisterCallback<ClickEvent>(ev => OnClickCloseMousePosBtn());

			animationController.PlaybackStarted += new Action(OnPlaybackStarted);
			animationController.PlaybackStopped += new Action(OnPlaybackStopped);
			animationController.PlaybackPending += new Action(OnPlaybackPending);
		    driverPerceptionManager.DriverPerceptionLoaded += new DriverPerceptionChangeHandler(OnDriverPerceptionLoaded);
            mainMenu.visible = false;   
            agentMenu.visible = false;   
 
            windowController.Init(root);
            agentPanelManager.Init(root);
            roadHighlightModeUIHandler.Init(root);
            focusFrameRenderer.Init(root);
            AddTooltipHandler(simulationInfoIcon);
            AddTooltipHandler(parserWorkloadLabel);

            fileBrowserWndHandler.AddTooltipRequest += new AddTooltipRequestEventHandler(AddTooltipHandler);
            fileBrowserWndHandler.HideTooltipRequest += new HideTooltipRequestEventHandler(HideTooltip);
        }

        /// <summary>
        /// Monobehaviour routine executed once per frame
        /// </summary>
        private void Update() {
            agentPanelManager.UpdateFloatingPanelPositions();
            UpdateDropdownPopupPosition();
            focusFrameRenderer.UpdateFrame();
        }

        /// <summary>
		/// Init is called after the file load completes. Execute here code that needs information from the currently loaded file. 
		/// </summary>
		public void Init (UIMode mode, SimulationMetaData simInfo) {
            agentPanelManager.ClearFloatingPanels();
			switch (mode) {
                case UIMode.SimulationPlayer:
                	animationController.Init ();
                    viewModeHandler.Init (); 
                    focusCameraHandler.Init ();
                    playbackSpeedHandler.Init ();
                    timeAxisHandler.Init (root);
                    agentPanelManager.CreateFloatingPanels();
                break;
            }
            simulationMetaData = simInfo;
            ShowCursorPosition(appStorage.ReadBool(AppStorageKey.ShowCursorPosition, true));
            SetSimulationInfoIconTooltip(simInfo);
		}

        /// <summary>
		/// Sets the tooltip of the manipulator to the given visual element
		/// </summary>
        private void SetSimulationInfoIconTooltip(SimulationMetaData simInfo) {
            string tooltip = "";
            tooltip += "<b>Currently loaded simulation</b>";
            if (simInfo.TraceFile != "")
                tooltip += "<br><br><b>Trace File:</b><br>" + simInfo.TraceFile;
            if (simInfo.TraceFileExtension != "")
                tooltip += "<br><br><b>Trace File Extension:</b><br>" + simInfo.TraceFileExtension;
            if (simInfo.SceneryFile != "")
                tooltip += "<br><br><b>Scenery File:</b><br>" + simInfo.SceneryFile;
            if (simInfo.DriverPerceptionFile != "")
                tooltip += "<br><br><b>Driver Perception File:</b><br>" + simInfo.DriverPerceptionFile;
            if (simInfo.RunID > -1)
                tooltip += "<br><br><b>Run:</b><br>" + simInfo.RunID.ToString();
            simulationInfoIcon.tooltip = Helpers.SanitizeScreenText(tooltip);
        }

        
        /// <summary>
		/// Adds a tooltip manipulator to the given visual element
		/// </summary>
        private void AddTooltipHandler(VisualElement elem) {
            elem.AddManipulator(new TooltipManipulator(root.Q<VisualElement>("tooltip") as Tooltip));
        }

        /// <summary>
		/// Force closes the tooltip
		/// </summary>
        public void HideTooltip() {
            Tooltip tooltip = root.Q<VisualElement>("tooltip") as Tooltip;
            tooltip.Close();
        }

        /// <summary>
		/// Hides/shows the load run menu item
		/// </summary>
        public void HideRunSelectorMenuItem(bool hide) {
            root.Q<VisualElement>("load-run-menu-item").style.display = hide ? DisplayStyle.None : DisplayStyle.Flex;
        }

        /// <summary>
		/// Hides/shows the cursor position display field
		/// </summary>
        private void ShowCursorPosition(bool show){
            mousePosPanel.EnableInClassList("closed-mousepos-panel", !show);
            appStorage.Write(AppStorageKey.ShowCursorPosition, show.ToString());
        }   

        /// <summary>
		/// Toggles the cursor position display field
		/// </summary>
        public void ToggleCursorPosition() {
           bool show = appStorage.ReadBool(AppStorageKey.ShowCursorPosition, true);
           show =! show;
           ShowCursorPosition(show);
        }

        /// <summary>
        /// Event that is called, when the DriverPerceptionManager finished loading.
        /// </summary>
        private void OnClickCloseMousePosBtn() {
            ShowCursorPosition(false);
        }

        /// <summary>
        /// Event that is called, when the DriverPerceptionManager finished loading.
        /// </summary>
        private void OnDriverPerceptionLoaded(string fileName, string error) {
            if(error != "") {
                windowController.ShowErrorMessage("Loading driver perception failed", error);
                return;
            } 

            agentPanelManager.CreatePerceivedAgentFloatingPanels();
            simulationMetaData.DriverPerceptionFile = fileName; 
            SetSimulationInfoIconTooltip(simulationMetaData);
            windowController.ShowLoadingMessage("Driver Perception loaded.", 1f);
        }

        /// <summary>
        /// Event that is called, when the user clicks the main menu icon.
        /// </summary>
		private void OnClickMainMenuBtn(){
            // With the delayed invocation we make sure that the global click handling is executed first
            // (see MouseEventhandler::HandleLeftMouseUpGlobal that hides popup menus) and only after the code here
            this.InvokeDelayed(() => mainMenu.visible = true, 0f);
		}

        /// <summary>
        /// Event that is called, when the user clicks the global (non-focus agent) display options button.
        /// </summary>
        private void OnClickGlobalDisplayOptionsBtn() {
            ShowNonFocusAgentDisplayOptionsWnd();
        }

        /// <summary>
        /// Event that is called, when the user changed the visibility of the focus agent's options.
        /// </summary>
        private void OnFocusAgentDisplayOptionListChanged(List<DisplayOption> displayOptions) {
            displayOptionsProvider.Update(AgentPanelType.FocusAgent, displayOptions);
        }
        
        /// <summary>
        /// Event that is called, when the user changed the visibility of the non-focus agent's options.
        /// </summary>
        private void OnNonFocusAgentDisplayOptionListChanged(List<DisplayOption> displayOptions) {
            displayOptionsProvider.Update(AgentPanelType.NonFocusAgent, displayOptions);
        }

        /// <summary>
        /// Event that is called, when the user changed the visibility of the perceived agent's options.
        /// </summary>
        private void OnPerceivedAgentDisplayOptionListChanged(List<DisplayOption> displayOptions) {
            displayOptionsProvider.Update(AgentPanelType.PerceivedAgent, displayOptions);
        }

        /// <summary>
        /// Event that is called, when the user clicks the load trace file menu item
        /// </summary>
		private void OnClickLoadTrace(){
            ShowTraceFileBrowserWnd();
		}

        /// <summary>
        /// Event that is called, when the user clicks the load run menu item
        /// </summary>
        private void OnClickLoadRun(){
            if(ReadRunListRequest != null)
                ReadRunListRequest();
		}

        /// <summary>
        /// Event that is called, when the user clicks the sensor positioner menu item
        /// </summary>
		private void OnClickSensorPositioner(){
            if(SwitchUIModeRequest != null)
                SwitchUIModeRequest(UIMode.SensorPositioner);
		}

        /// <summary>
        /// Event that is called, when the user clicks the load scenery file menu item
        /// </summary>
		private void OnClickLoadScenery(){
            ShowSceneryFileBrowserWnd();
		}

        /// <summary>
        /// Event that is called, when the user clicks the usage hints menu item
        /// </summary>
        private void OnClickUsageHints() {
            ShowUsageHintsWnd();
        }

        /// <summary>
        /// Event that is called, when the user clicks the verison info menu item
        /// </summary>
        private void OnClickVersionInfo() {
            ShowVersionInfoWnd();
        }

        /// <summary>
        /// Event that is called, when the user clicks the start/stop playback button
        /// </summary>
		private void OnClickStartPlaybackBtn(){		
			if (animationController.IsPlaying())
				animationController.StopPlayback ();
			else
				animationController.StartPlayback ();
		}

        /// <summary>
        /// Event that is called, when the user clicks the step forward button
        /// </summary>
		private void OnClickStepForwardButton(){
			animationController.JumpToNextSample ();
		}

        /// <summary>
        /// Event that is called, when the user clicks the step backward button
        /// </summary>
		private void OnClickStepBackButton(){
			animationController.JumpToPreviousSample ();
		}

        /// <summary>
        /// Event that is called, when the playback has been started
        /// </summary>
		private void OnPlaybackStarted(){
			UpdatePlayButton (true);
		}

        /// <summary>
        /// Event that is called, when the playback has been stopped
        /// </summary>
		private void OnPlaybackStopped(){
			UpdatePlayButton (false);
		}

        /// <summary>
        /// Event that is called, when the playback had to be paused during background loading
        /// </summary>
		private void OnPlaybackPending(){
			startStopBtn.AddToClassList("red-tint");
		}

        /// <summary>
        /// Event that is called, when the user has selected a trace file in the file selector window
        /// If the user cancelled the selection, path is an empty string
        /// </summary>
        private void OnTraceFileSelected(string path) {
            windowController.CloseWindow();
            if (path == "")
                return;

            if(SwitchUIModeRequest != null)
                SwitchUIModeRequest(UIMode.SimulationPlayer);
            if(LoadTraceFileRequest != null)
                LoadTraceFileRequest(path);
        }

        /// <summary>
        /// Event that is called, when the user has selected a scenery file in the file selector window
        /// If the user cancelled the selection, path is an empty string
        /// </summary>
        private void OnSceneryFileSelected(string path) {
            windowController.CloseWindow();
            if (path == "")
                return;
            if(SwitchUIModeRequest != null)
                SwitchUIModeRequest(UIMode.SceneryViewer);
            if(LoadSceneryFileRequest != null)
                LoadSceneryFileRequest(path);
        }

        /// <summary>
        /// Event that is called, when the user has selected a run in the run selector window
        /// If the user cancelled the selection, -1 is returned as runID
        /// </summary>
        private void OnRunSelected(string tracePath, int runID) {
            windowController.CloseWindow();
            if (runID == -1)
                return;

            if(SwitchUIModeRequest != null)
                SwitchUIModeRequest(UIMode.SimulationPlayer);
            if(LoadRunRequest != null)
                LoadRunRequest(tracePath, runID);
        }

        /// <summary>
        /// Event that is called, when the user has selected an agent in the focus agent selector window
        /// </summary>
        private void OnFocusAgentSelected() {
            windowController.CloseWindow();
        }
        
        /// <summary>
        /// Updates the icon of the playback button
        /// </summary>
		private void UpdatePlayButton(bool playing){
            startStopBtn.AddToClassList(playing ? "pause-image" : "play-image");
            startStopBtn.RemoveFromClassList(playing ? "play-image" : "pause-image");
            startStopBtn.RemoveFromClassList("red-tint");
            // if(playbackPendingInvocation != null)
			// 	 this.CancelDelayedInvocation(playbackPendingInvocation);
			// playbackPendingInvocation = this.InvokeDelayed(() => startStopBtn.RemoveFromClassList("red-tint"), 0.1f);

		}
        
                /// <summary>
        /// Initializes and displays the agent context menu at the given position for the given agent
        /// </summary>
        public void DisplayParserWorkload(float workload) {
            parserWorkloadLabel.text = workload + "%";
        }

        /// <summary>
        /// Triggers an update of the data displayed in the agent panels (focus, docked and floating).
        /// The panels pull their data directly from their agent reference
        /// </summary>
        public void RefreshPanelData(){
            agentPanelManager.RefreshPanels();
        }
        
        /// <summary>
        /// Initializes and displays the agent context menu at the given position for the given agent
        /// </summary>
        public void ShowAgentPopupMenu(Agent agent, Vector3 popupPos) {
            clickedAgent = agent;
            agentMenuLabel.text = "Agent " + clickedAgent.ID;
            dynamicAgentInfoMI.RegisterCallback<ClickEvent>(ev => ShowNonFocusAgentPanel(clickedAgent));
            staticAgentInfoMI.RegisterCallback<ClickEvent>(ev => ShowStaticAgentInfoWnd(clickedAgent));
            perceivedAgentInfoMI.style.display = DisplayStyle.None;
            dynamicAgentInfoMI.style.display = DisplayStyle.Flex;
            staticAgentInfoMI.style.display = DisplayStyle.Flex;
            driverPerceptionMI.style.display = DisplayStyle.None;
            if(driverPerceptionManager.HasDriverPerception(clickedAgent.ID)){
                driverPerceptionMI.style.display = DisplayStyle.Flex;
                driverPerceptionMI.RegisterCallback<ClickEvent>(ev => ShowDriverPerception(clickedAgent));
            }
            ShowPopupMenu(popupPos);
        }
        
        /// <summary>
        /// Initializes and displays the perceived agent context menu at the given position for the given perceived agent
        /// </summary>
        public void ShowPerceivedAgentPopupMenu(Agent agent, Vector3 popupPos) {
            clickedAgent = agent;
            agentMenuLabel.text = "Perceived Agent " + driverPerceptionManager.GetActiveAgentID() + " -> " + clickedAgent.ID;
            perceivedAgentInfoMI.style.display = DisplayStyle.Flex;
            dynamicAgentInfoMI.style.display = DisplayStyle.None;
            staticAgentInfoMI.style.display = DisplayStyle.None;
            driverPerceptionMI.style.display = DisplayStyle.None;
            perceivedAgentInfoMI.RegisterCallback<ClickEvent>(ev => ShowPerceivedAgentPanel(clickedAgent));
            ShowPopupMenu(popupPos);
        }

        /// <summary>
		/// Displays the popup menu at the given position, limiting the popup to screen area
        /// </summary>
        private void ShowPopupMenu(Vector3 popupPos) {
            int maxLeft = Screen.width - (int)agentMenu.resolvedStyle.width;
            int maxTop = Screen.height - (int)agentMenu.resolvedStyle.height;
			agentMenu.style.left = Math.Min(popupPos.x, maxLeft);
			agentMenu.style.top = Math.Min(-popupPos.y, maxTop);
            agentMenu.visible = true;
            agentMenu.BringToFront();
        }

        /// <summary>
        /// Uninitializes and hides all menus
        /// </summary>
        public void HidePopupMenu() {
            agentMenu.visible = false;
            mainMenu.visible = false;
            dynamicAgentInfoMI.UnregisterCallback<ClickEvent>(ev => ShowNonFocusAgentPanel(clickedAgent));
            staticAgentInfoMI.UnregisterCallback<ClickEvent>(ev => ShowStaticAgentInfoWnd(clickedAgent));
            driverPerceptionMI.UnregisterCallback<ClickEvent>(ev => ShowDriverPerception(clickedAgent));
            perceivedAgentInfoMI.UnregisterCallback<ClickEvent>(ev => ShowPerceivedAgentPanel(clickedAgent));
            clickedAgent = null;
        }

        /// <summary>
		/// Adds a panel for the given non-focus agent in the dock
        /// </summary>
        public void ShowNonFocusAgentPanel(Agent agent) {
            List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
            VisualElement visualInfoPanel = agentPanelManager.ShowNonFocusAgentPanel(agent, displayOptions);
            visualInfoPanel?.Q<Button>("configure-btn").RegisterCallback<ClickEvent>(ev => ShowNonFocusAgentDisplayOptionsWnd());
        }
        
        /// <summary>
		/// Adds a panel for the given perceived agent in the dock
        /// </summary>
        public void ShowPerceivedAgentPanel(Agent agent) {
            List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
            VisualElement visualInfoPanel = agentPanelManager.ShowPerceivedAgentPanel(agent, displayOptions);
            visualInfoPanel?.Q<Button>("configure-btn").RegisterCallback<ClickEvent>(ev => ShowPerceivedAgentDisplayOptionsWnd());
        }
        
        /// <summary>
		/// Shows the given agent in the focus agent panel (lower-left of the screen)
        /// If the given agent is null, the focus agent panel gets minimized and shows no agent 
        /// </summary>
        public void ShowFocusAgentPanel(Agent agent) {
            List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);
            VisualElement visualInfoPanel = agentPanelManager.ShowFocusAgentPanel(agent, displayOptions);
            visualInfoPanel?.Q<Button>("configure-btn").RegisterCallback<ClickEvent>(ev => ShowFocusAgentDisplayOptionsWnd());
            visualInfoPanel?.Q<Button>("switch-focus-btn").RegisterCallback<ClickEvent>(ev => ShowFocusAgentSelectorWnd());
        }
        
        /// <summary>
		/// Loads the driver perception file (perceived world) of the given agent 
        /// </summary>
        public void ShowDriverPerception(Agent agent) {
            if(agent.gameObject.tag != "Agent")
                return;

            agentPanelManager.ClearPerceivedAgentPanels();
            displayOptionsProvider.Clear();

            windowController.ShowLoadingMessage("Loading driver perception...");
            //as the loading is a potentionally long running task, we delay it a bit to make sure the loading message gets displayed
            this.InvokeDelayed(() => driverPerceptionManager.ShowDriverPerception(agent.ID), 0.1f);
        }
        
        /// <summary>
		/// Configures and displays the static agent info window of the given agent
        /// </summary>
         private void ShowStaticAgentInfoWnd(Agent agent) {
            if(agent.gameObject.tag != "Agent")
                return;

            int windowWidth = 400;
            VisualElement window = windowController.OpenWindow("UI/StaticAgentInfoWindow", "Static Agent Info", "", windowWidth);
            staticAgentInfoWndHandler.Init(agent.ID, agent.StaticAgentInfo.NonInterpretedProps, window);
        }
        
        /// <summary>
		/// Configures and displays the non-focus agent display options window, where the user can choose the parameters that
        /// all non-focus agent panels should display
        /// </summary>
        private void ShowNonFocusAgentDisplayOptionsWnd() {
            int windowWidth = 400;
            VisualElement window = windowController.OpenWindow("UI/DisplayOptionsWindow", "Display Options", "(all non-focus agents)", windowWidth);
            List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
            displayOptionsWndHandler.Init (displayOptions, window.Q<ListView>());
            displayOptionsWndHandler.ListChanged += new DisplayOptionListChangedHandler(OnNonFocusAgentDisplayOptionListChanged);
         }
        
        /// <summary>
		/// Configures and displays the perceived agent display options window, where the user can choose the parameters that
        /// all perceived agent panels should display
        /// </summary>
        private void ShowPerceivedAgentDisplayOptionsWnd() {
            int windowWidth = 400;
            VisualElement window = windowController.OpenWindow("UI/DisplayOptionsWindow", "Display Options", "(all perceived agents)", windowWidth);
            List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
            displayOptionsWndHandler.Init (displayOptions, window.Q<ListView>());
            displayOptionsWndHandler.ListChanged += new DisplayOptionListChangedHandler(OnPerceivedAgentDisplayOptionListChanged);
         }

        /// <summary>
		/// Configures and displays the focus agent display options window, where the user can choose the parameters that
        /// the focus-agent panel should display
        /// </summary>
        private void ShowFocusAgentDisplayOptionsWnd() {
            int windowWidth = 400;
            VisualElement window = windowController.OpenWindow("UI/DisplayOptionsWindow", "Display Options", "(focus agent)", windowWidth);
            List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);
            displayOptionsWndHandler.Init (displayOptions, window.Q<ListView>());
            displayOptionsWndHandler.ListChanged += new DisplayOptionListChangedHandler(OnFocusAgentDisplayOptionListChanged);
         }

        /// <summary>
		/// Displays the trace file browser window
        /// </summary>
        private void ShowTraceFileBrowserWnd() {
            int windowWidth = 700;
            VisualElement window = windowController.OpenWindow("UI/FileBrowserWindow", "Choose a trace file", "(.xml)", windowWidth);
            fileBrowserWndHandler.Init (window, "xml", windowWidth); 
            fileBrowserWndHandler.FileSelected += new FileChosenEventHandler(OnTraceFileSelected);
        }

        /// <summary>
		/// Displays the scenery file browser window
        /// </summary>
        private void ShowSceneryFileBrowserWnd() {
            int windowWidth = 700;
            VisualElement window = windowController.OpenWindow("UI/FileBrowserWindow", "Choose a scenery file", "(.xodr)", windowWidth);
            fileBrowserWndHandler.Init (window, "xodr", windowWidth);
            fileBrowserWndHandler.FileSelected += new FileChosenEventHandler(OnSceneryFileSelected);
        }

        /// <summary>
		/// Displays the run selector window
        /// </summary>
        public void ShowRunSelectorWnd(string traceFile, List<Run> runs) {
            int rowHeight = 30;
            int minWindowHeight = 150;
            int windowWidth = 300;
            int windowHeight = minWindowHeight + runs.Count*rowHeight;
            VisualElement window = windowController.OpenWindow("UI/RunSelectorWindow", "Select Run", "", windowWidth, windowHeight);
            runSelectorWndHandler.Init (window, traceFile, runs);
            runSelectorWndHandler.RunSelected += new RunSelectionEventHandler(OnRunSelected);
        }

        /// <summary>
		/// Displays the focus agent selector window
        /// </summary>
        private void ShowFocusAgentSelectorWnd() {
            int rowHeight = 30;
            int minWindowHeight = 150;
            int windowWidth = 300;
            int windowHeight = minWindowHeight + agentManager.GetAgentCount()*rowHeight;
            VisualElement window = windowController.OpenWindow("UI/FocusAgentSelectorWindow", "Change Focus Agent", "", windowWidth, windowHeight);
            window.Q<Button>("cancel-btn").RegisterCallback<ClickEvent>(ev => windowController.CloseWindow());
            focusAgentSelectorWndHandler.Init (window);
            focusAgentSelectorWndHandler.FocusAgentSelected += new FocusAgentChangeEventHandler(OnFocusAgentSelected);
        }

        /// <summary>
		/// Displays the usage hints window
        /// </summary>
        private void ShowUsageHintsWnd() {
            int windowWidth = 600;
            VisualElement window = windowController.OpenWindow("UI/UsageHintsWindow", "Usage Hints", "", windowWidth);
        }

        /// <summary>
		/// Displays the version info window
        /// </summary>
        private void ShowVersionInfoWnd() {
            int windowWidth = 400;
            int windowHeight = 250;
            VisualElement window = windowController.OpenWindow("UI/VersionInfoWindow", "Version Info", "", windowWidth, windowHeight);
            window.Q<Label>("version-lbl").text = "v" + appVersion.VersionNumber;
            window.Q<Label>("graphics-lbl").text = SystemInfo.graphicsDeviceName;            
        }

        /// <summary>
		/// This is a temporary workaround to move the popup of the dropdown above the dropdown if it's out of the screen.
        /// It is currently used for the playback speed dropdown that opens it's popup outside of the visible applicaton window.
        /// This workatound should be removed as soon the dropdown uielement can natively handle popups out of the screen
        /// (See Unity Issue 1365095)
        /// </summary>
		private void UpdateDropdownPopupPosition(){
			VisualElement dropdownPopup = root.parent.Q(className: "unity-base-dropdown__container-outer");
			if(dropdownPopup != null){
               if(origDropdownFieldPos > 0){
                    dropdownPopup.style.top = origDropdownFieldPos - 50 - dropdownPopup.resolvedStyle.height;
                    origDropdownFieldPos = 0;
                    dropdownPopup.style.opacity = 1;
                }
                if(dropdownPopup.resolvedStyle.height < 30){
                    origDropdownFieldPos = (int)dropdownPopup.resolvedStyle.top;
                    dropdownPopup.style.height = Screen.height;
                    dropdownPopup.style.top = 0;
                    dropdownPopup.style.opacity = 0;
                }
			}
		}

        /// <summary>
        /// As some of the style classes of UI.Main.css (e.g. .unity-base-dropdown...) 
        /// must be applied to the parent of the rootVisualElement (Panel Settings)
        /// we also add the stylesheet to that element
        /// </summary>
        private void AddRootStylesheet() {
            StyleSheet styleSheet = Resources.Load<StyleSheet>("UI/UI-Main");
            if (styleSheet != null)
                root.parent.styleSheets.Add(styleSheet);
            else
                Debug.LogWarning("UI/UI-Main.css could not be found."); 
        }
    }
}