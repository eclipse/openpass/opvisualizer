/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.Utilities;
using Visualizer.Core;
using Zenject;

namespace Visualizer.UI {

    public interface IStaticAgentInfoWndHandler{
        void Init(int agentID, List<GenericListItem> props, VisualElement window);
    }

    public class StaticAgentInfoWndHandler : IStaticAgentInfoWndHandler
    {
        private VisualElement root;
        private VisualElement divider;
        private ListView paramListView;
        bool isResizing = false;
        private TreeList treeList = new TreeList();

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){
		}

        /// <summary>
        /// This class gets a list of static agent properties and fills them into a ListView
        /// The key of the properties can have a namespace, which is displayed as hierarchy
        /// </summary>
        /// <param name="props">List of items that the list view should handle</param>
        public void Init(int agentID, List<GenericListItem> props, VisualElement window) {
            root = window;
            paramListView = root.Q<ListView>();
            divider = root.Q<VisualElement>("divider-bar");
            root.Q<Label>("window-title").text = "Agent " + agentID + " Static Info";
            divider.RegisterCallback<MouseDownEvent>(ev => DividerMouseDown(ev));
            divider.RegisterCallback<MouseUpEvent>(ev => HandleMouseUp(ev.mousePosition));
            divider.RegisterCallback<MouseMoveEvent>(ev => HandleMouseDrag(ev.mousePosition));
            paramListView.RegisterCallback<MouseUpEvent>(ev => HandleMouseUp(ev.mousePosition));
            paramListView.RegisterCallback<MouseMoveEvent>(ev => HandleMouseDrag(ev.mousePosition));
            InitListView(props);
        }

        /// <summary>
        /// Initialize the parameter list view
        /// </summary>
        private void InitListView(List<GenericListItem> props){
            RemoveId(props);
            treeList.CreateFromGenericList(props, "General");

            var visualItemTemplate = Resources.Load<VisualTreeAsset>("UI/StaticAgentInfoListItem");
            Func<VisualElement> makeItem = () => visualItemTemplate.Instantiate();
            Action<VisualElement, int> bindItem = (e, i) =>
            {
                e.Q<Label>("key-lbl").text = treeList[i].Label;
                e.Q<Label>("value-lbl").text = treeList[i].Value;
                e.Q<Label>("key-lbl").style.width = divider.style.left;
                e.Q<Label>("key-lbl").style.paddingLeft = 10 + treeList[i].Level * 20;
                if(treeList[i].Group == true)
                    e.Q<Label>("key-lbl").AddToClassList("list-group");
                else    
                    e.Q<Label>("key-lbl").RemoveFromClassList("list-group");
            };
            paramListView.makeItem = makeItem;
            paramListView.bindItem = bindItem;
            paramListView.itemsSource = treeList;
        }

        /// <summary>
        /// Removes all parameters whose key is "Id". The ID is displayed in the window title. When merging multiple files, this 
        /// </summary>
        private void RemoveId(List<GenericListItem> props){
            props.RemoveAll(x => x.Key == "Id");
        }

        /// <summary>
        /// Handles the mouse move event
        /// </summary>
		private void HandleMouseDrag(Vector2 mousePos){
            if(isResizing){
                divider.style.left = mousePos.x - root.worldBound.x;
                paramListView.Rebuild();
            }
		}

        /// <summary>
        /// Handles the mouse down event
        /// </summary>
        private void DividerMouseDown(MouseDownEvent ev) {
            isResizing = true;         
        }

        /// <summary>
        /// Handles the mouse up event
        /// </summary>
		private void HandleMouseUp(Vector2 mousePos){
            isResizing = false;  
        }
    }
}