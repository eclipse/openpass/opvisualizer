/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using Visualizer.AgentHandling;

namespace Visualizer.UI {
    
    /// <summary>
	/// An AgentPanelItem is the data source of an AgenInfoPanel param list item
	/// </summary>
    public class AgentPanelItem {
        public string Key { get; set; }
        public string Label { get; set; }
        public bool Visible { get; set; }
        private Agent _agentRef;
        private IFormatProvider formatProvider;

        /// <summary>
        /// Constructs an AgentPanelItem 
        /// </summary>
        public AgentPanelItem(string key, Agent aRef){
            Visible = true;
            Key = key;
            Label = GetLabelFromKey(key);
            formatProvider = CultureInfo.CreateSpecificCulture("en-US");
            _agentRef = aRef;
        }

        /// <summary>
        /// Pulls the value of the infopanel item from the agent't attribute map
        /// </summary>
        public string GetValue() {
            if (_agentRef != null && _agentRef.OptionalAttributes != null && _agentRef.OptionalAttributes.ContainsKey (Key))
                return FormatValue(_agentRef.OptionalAttributes[Key]);
            else
                return "-";
        }
        
        /// <summary>
        /// Round the passed value to 2 decimal places, if it's a float. If it isn't a float, it does nothing
        /// </summary>
        /// <param name="strVal">A string that contains a float.</param>
        private string FormatValue(string strVal){
            if (strVal.Contains (".")) {
                float floatVal;
                if (float.TryParse (strVal, NumberStyles.Float, formatProvider, out floatVal))
                    strVal = string.Format ("{0:0.00}", floatVal);
            }
            return strVal;
        }

        /// <summary>
        /// Creates a label from the given key by removing the namespace from the given key
        /// </summary>
        /// <param name="key">A string that contains the attributes full name incl. namespace.</param>
        private string GetLabelFromKey(string key){
            int lastDotPos = key.LastIndexOf('.');
            string label = key;
            if (lastDotPos > -1) {
                label = key.Substring(lastDotPos + 1);
            }
            return label;
        }

    }
}
