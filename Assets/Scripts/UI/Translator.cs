﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;

namespace Visualizer.UI
{
	/// <summary>
	/// Helper class that can map various keys to their displayed label 
	// (used currently only for a portion of the UI but can be extended in future for language translations)
	/// </summary>
	public static class Translator {

		private static readonly Dictionary<string, string> TextMap = new Dictionary<string, string>
		{
			{ "Floating.ID", "Floating.Agent ID" },
			{ "Floating.Tacho", "Floating.Speedometer" },
			{ "Visualized.AOIArrow", "Visualized.Driver's Area of Interest" },
			{ "Visualized.VisibilityDistance", "Visualized.Visibility Distance" },
			{ "Visualized.SensorRange", "Visualized.Sensor's Detection Range" }
		};
	
		/// <summary>
		/// Returns the translated text or the key, if no translation is found
		/// </summary>
		public static string GetLabel(string key){
			if(TextMap.ContainsKey(key))
				return TextMap [key];
			return key;
		}
	}
}