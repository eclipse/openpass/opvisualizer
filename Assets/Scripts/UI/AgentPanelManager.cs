/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.UI {

    public interface IAgentPanelManager {
        void Init(VisualElement rootVisualElement);
		void CreateFloatingPanels();
        void CreatePerceivedAgentFloatingPanels();
        VisualElement ShowFocusAgentPanel(Agent agent, List<DisplayOption> displayOptions);
        VisualElement ShowNonFocusAgentPanel(Agent agent, List<DisplayOption> displayOptions);
        VisualElement ShowPerceivedAgentPanel(Agent agent, List<DisplayOption> displayOptions);
        void RefreshPanels();
        void UpdateFloatingPanelPositions();    
        void ClearPerceivedAgentPanels();
        void ClearFloatingPanels();
        void Clear();
    }

    /// <summary>
    /// AgentPanelManager handles all agent panels on the screen (floating, focus, docked) that display dynamic textual agent info
    /// </summary>
    public class AgentPanelManager: MonoBehaviour, IAgentPanelManager
    {
        VisualElement root;
        VisualElement infoPanelContainer;
        VisualElement focusAgentPanelContainer;
        VisualElement floatingInfoPanelLayer;

        private IAgentManager agentManager;
        private IDisplayOptionsProvider displayOptionsProvider;
        private IPanelResizer panelResizer;
        private ICameraManager cameraManager;
        private IViewModeHandler viewModeHandler;
        private IDriverPerceptionManager driverPerceptionManager;
        private IAppStorage appStorage;
        private IFocusFrameRenderer focusFrameRenderer;
        private IFocusCameraHandler focusCameraHandler;
        
        //Each agent panel has it's own handler instance. The handlers are structured in these lists:
        private AgentPanelHandler focusAgentPanelHandler = null;
        private Dictionary<int, AgentPanelHandler> nonFocusAgentPanelHandlers = new Dictionary<int, AgentPanelHandler>();
        private SortedDictionary<string, FloatingAgentPanelHandler> floatingAgentPanelHandlers = new SortedDictionary<string, FloatingAgentPanelHandler>();
        private Dictionary<int, AgentPanelHandler> perceivedAgentPanelHandlers = new Dictionary<int, AgentPanelHandler>();

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IFocusCameraHandler fcHandler, IFocusFrameRenderer ffRenderer, IAppStorage aStorage, IDriverPerceptionManager mmManager, IDisplayOptionsProvider doProvider, IAgentManager aManager, IPanelResizer pResizer, ICameraManager cManager, IViewModeHandler vmHandler){
            agentManager = aManager;
            panelResizer = pResizer;
            displayOptionsProvider = doProvider;
            cameraManager = cManager;
            viewModeHandler = vmHandler;
            driverPerceptionManager = mmManager;
            appStorage = aStorage;
            focusFrameRenderer = ffRenderer;
            focusCameraHandler = fcHandler;
		}
        
        /// <summary>
        /// Closes all agent info panels
        /// </summary>
        public void Clear() {
            ClearNonFocusAgentPanels();
            ClearPerceivedAgentPanels();
            ClearFloatingPanels();
            MinimizeFocusAgentPanel();
        }

        /// <summary>
        /// Initializes the manager class
        /// </summary>
        public void Init(VisualElement rootVisualElement){
            root = rootVisualElement;
            infoPanelContainer = root.Q<VisualElement>("info-panel-container");
            focusAgentPanelContainer = root.Q<VisualElement>("focus-agent-info-panel-container");
            floatingInfoPanelLayer = root.Q<VisualElement>("floating-info-panel-layer");
            panelResizer.Init(root);
            displayOptionsProvider.DisplayOptionsChanged += new DisplayOptionsChangedEventHandler(OnDisplayOptionsChanged);
            focusFrameRenderer.FocusAgentOutOfViewWarning += new FocusAgentOutOfViewWarningEventHandler(OnFocusAgentOutOfViewWarning);
            ShowInfoPanelContainer(false); 
            MinimizeFocusAgentPanel();
        }

        /// <summary>
        /// Closes all perceived agent info panels
        /// </summary>
        public void ClearPerceivedAgentPanels(){
            List<int> perceivedAgentIDs = new List<int>(perceivedAgentPanelHandlers.Keys);
            foreach(int agentID in perceivedAgentIDs)
                ClosePerceivedAgentPanel(agentID);
        }

        /// <summary>
        /// Closes all non-focus agent info panels
        /// </summary>
        private void ClearNonFocusAgentPanels(){
            List<int> agentIDs = new List<int>(nonFocusAgentPanelHandlers.Keys);
            foreach(int agentID in agentIDs) 
                CloseNonFocusAgentPanel(agentID);
        }

        /// <summary>
        /// Closes all floating agent info panels
        /// </summary>
        public void ClearFloatingPanels(){
            List<string> floatingAgentPanelKeys = new List<string>(floatingAgentPanelHandlers.Keys);
            foreach(string agentKey in floatingAgentPanelKeys) 
                CloseFloatingAgentPanel(agentKey);
        }

        /// <summary>
        /// Creates for each agent in the currently loaded trace file a floating info panel
        /// </summary>
		public void CreateFloatingPanels(){
            floatingAgentPanelHandlers.Clear();
            List<int> agentIDs = agentManager.GetAgentIDs ();
			foreach(int agentID in agentIDs){
				Agent agent = agentManager.GetAgent(agentID);
                VisualElement visualInfoPanel = CreateFloatingPanel();
                List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
                FloatingAgentPanelHandler infoPanelHandler = new FloatingAgentPanelHandler(visualInfoPanel, displayOptions, agent, agent.ID.ToString(), AgentPanelType.NonFocusAgent, cameraManager, viewModeHandler);
                floatingAgentPanelHandlers.Add(agent.ID.ToString(), infoPanelHandler);
			}
		}

        /// <summary>
        /// Updates the position of each floating agent info panel, so that they always stay above their corresponding agent
        /// </summary>
		public void UpdateFloatingPanelPositions(){
            foreach(FloatingAgentPanelHandler infoPanelHandler in floatingAgentPanelHandlers.Values)
                infoPanelHandler.UpdateFloatingPanel();
            SortFloatingAgentPanels();
        }

        /// <summary>
        /// Sorts the floating info panel visual elements according to their corresponding agent's distance to the camera,
        //  so that panels belonging to agents that are closer to the camera are on top of panels that are farther.
        /// Note that FloatingAgentPanelHandler::UpdateFloatingPanel calculates the CameraDistance property of all panels. 
        /// </summary>
        private void SortFloatingAgentPanels() {
            foreach (KeyValuePair<string, FloatingAgentPanelHandler> handler in floatingAgentPanelHandlers.OrderBy(h => h.Value.CameraDistance))
                handler.Value.GetRootVisualElement().SendToBack();
		}

        /// <summary>
        /// Creates a floating agent info panel for each perceived agent
        /// </summary>
        public void CreatePerceivedAgentFloatingPanels() {
            Dictionary <string, Agent> perceivedAgents = driverPerceptionManager.GetAllPerceivedAgents();
            foreach(KeyValuePair<string, Agent> agentEntry in perceivedAgents) {
                if(!floatingAgentPanelHandlers.ContainsKey(agentEntry.Key)) {
                    VisualElement visualInfoPanel = CreateFloatingPanel();
                    List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
                    FloatingAgentPanelHandler infoPanelHandler = new FloatingAgentPanelHandler(visualInfoPanel, displayOptions, agentEntry.Value, agentEntry.Key, AgentPanelType.PerceivedAgent, cameraManager, viewModeHandler);
                    floatingAgentPanelHandlers.Add(agentEntry.Key, infoPanelHandler);
                }
            }
        }

        /// <summary>
        /// Creates a static AgentPanel VisualElement out of the uxml template and adds it to the InfoPanelContainer
        /// </summary>
        private VisualElement CreateNonFocusAgentPanel(){
            var template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.CloneTree();
            infoPanelContainer.Add(visualInfoPanel);
            ShowInfoPanelContainer(true);
            visualInfoPanel.SendToBack(); //<- ensure that the resizers are in front of the panel
            return visualInfoPanel;
        }

        /// <summary>
        /// Creates a floating AgentPanel VisualElement out of the uxml template and adds it to the ui root
        /// </summary>
        private VisualElement CreateFloatingPanel(){
            var template = Resources.Load<VisualTreeAsset>("UI/FloatingAgentPanel");
            VisualElement visualInfoPanel = template.CloneTree().Q<VisualElement>("panel");
            floatingInfoPanelLayer.Add(visualInfoPanel);
            visualInfoPanel.SendToBack();
            return visualInfoPanel;
        }

        /// <summary>
        /// Triggers an update of the data values displayed in the agent panels.
        /// The panels pull their data directly from their agent reference
        /// </summary>
        public void RefreshPanels(){
            foreach(AgentPanelHandler handler in nonFocusAgentPanelHandlers.Values)
                handler.RefreshValues();
            foreach(AgentPanelHandler handler in perceivedAgentPanelHandlers.Values)
                handler.RefreshValues();
            if(focusAgentPanelHandler != null)
                focusAgentPanelHandler.RefreshValues();
        }

        /// <summary>
        /// Creates, initialises and displays an agent panel for the given non-focus agent
        /// </summary>
        public VisualElement ShowNonFocusAgentPanel(Agent agent, List<DisplayOption> displayOptions) {
            if(nonFocusAgentPanelHandlers.ContainsKey(agent.ID))
                return null;

            VisualElement visualInfoPanel = CreateNonFocusAgentPanel();
            visualInfoPanel.Q<Button>("close-btn").RegisterCallback<ClickEvent>(ev => CloseNonFocusAgentPanel(agent.ID));
            string header = "Agent " + agent.ID;
            
            AgentPanelHandler infoPanelHandler = new AgentPanelHandler(visualInfoPanel, displayOptions, agent, header, AgentPanelType.NonFocusAgent, appStorage);
            nonFocusAgentPanelHandlers.Add(agent.ID, infoPanelHandler);
            return visualInfoPanel;
        }

        /// <summary>
        /// Creates, initialises and displays an agent panel for the given perceived agent
        /// </summary>
        public VisualElement ShowPerceivedAgentPanel(Agent agent, List<DisplayOption> displayOptions) {
            if(perceivedAgentPanelHandlers.ContainsKey(agent.ID))
                return null;

            VisualElement visualInfoPanel = CreateNonFocusAgentPanel();
            visualInfoPanel.Q<Button>("close-btn").RegisterCallback<ClickEvent>(ev => ClosePerceivedAgentPanel(agent.ID));
            string header = "Agent " + driverPerceptionManager.GetActiveAgentID() + " -> " + agent.ID;

            AgentPanelHandler infoPanelHandler = new AgentPanelHandler(visualInfoPanel, displayOptions, agent, header, AgentPanelType.PerceivedAgent, appStorage);
            perceivedAgentPanelHandlers.Add(agent.ID, infoPanelHandler);
            return visualInfoPanel;
        }
 
        /// <summary>
		/// Shows the given agent in the focus agent panel (lower-left of the screen)
        /// If the given agent is null, the focus agent panel gets minimized and shows no agent.
        /// The panel is only created once and is refilled with new content, if the focus agent changes.
        /// For the sake of cool visual effects, we first wait until the curently opened focus agent panel minimizes, 
        /// before we maximize it again with the new agent information
        /// </summary>
        public VisualElement ShowFocusAgentPanel(Agent agent, List<DisplayOption> displayOptions) {
            MinimizeFocusAgentPanel();
            this.InvokeDelayed(() => SwitchFocusAgentPanel(agent), 0.5f);
            
            if(focusAgentPanelHandler == null)
                return CreateFocusAgentPanel(displayOptions);
            else
                return null;
        }

        /// <summary>
		/// Creates the focus agent panel
        /// </summary>
        private VisualElement CreateFocusAgentPanel(List<DisplayOption> displayOptions) {
            var template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.CloneTree();
            focusAgentPanelContainer.Add(visualInfoPanel);
            focusAgentPanelContainer.style.width = appStorage.ReadInt(AppStorageKey.FocusPanelWidth, 250);
            visualInfoPanel.SendToBack(); //<- ensure that the resizers are in front of the panel
            visualInfoPanel.Q<Button>("minimize-btn").RegisterCallback<ClickEvent>(ev => MinimizeFocusAgentPanel());
            visualInfoPanel.Q<Button>("maximize-btn").RegisterCallback<ClickEvent>(ev => MaximizeFocusAgentPanel());
            visualInfoPanel.Q<Label>("agent-id-lbl").RegisterCallback<ClickEvent>(ev => ResetCameraToFocusAgent());
            focusAgentPanelHandler = new AgentPanelHandler(visualInfoPanel, displayOptions, null, "", AgentPanelType.FocusAgent, appStorage);
            return visualInfoPanel;
        }

        /// <summary>
		/// Minimizes the focus agent panel
        /// </summary>
        private void MinimizeFocusAgentPanel() {
            if(focusAgentPanelHandler != null){
                focusAgentPanelHandler.ChangeWindowMode(AgentPanelWindowMode.Minimized);
                focusAgentPanelContainer.style.height = focusAgentPanelHandler.GetMinimizedHeight();
            }
            else {
                focusAgentPanelContainer.style.height = 0;
            }
            panelResizer.EnableFocusPanelResizing(false);
        }

        /// <summary>
		/// Maximizes the focus agent panel
        /// </summary>
        private void MaximizeFocusAgentPanel() {
            focusAgentPanelContainer.style.height = appStorage.ReadInt(AppStorageKey.FocusPanelHeight, 300);
            focusAgentPanelHandler.ChangeWindowMode(AgentPanelWindowMode.Maximized);
            panelResizer.EnableFocusPanelResizing(true);
        }

        /// <summary>
		/// Moves the camera back to the focus agent (useful if the agent is out of view)
        /// </summary>
        private void ResetCameraToFocusAgent() {
            focusCameraHandler.ResetFocus();
        }

        /// <summary>
		/// Fills the focus agent panel with information of the given agent
        /// </summary>
        private void SwitchFocusAgentPanel(Agent agent){
            focusAgentPanelHandler.SwitchAgent(agent);
            if(agent != null)
                MaximizeFocusAgentPanel();

            UpdatePanelDisplayOptions();
            agentManager.UpdateDisplayOptionVisibility(displayOptionsProvider.Get(AgentPanelType.FocusAgent), displayOptionsProvider.Get(AgentPanelType.NonFocusAgent));
        }

        /// <summary>
		/// Opens/closes the agent panel dock in the right screen side
        /// </summary>
        private void ShowInfoPanelContainer(bool show) {
            if (show) {
                int width = appStorage.ReadInt(AppStorageKey.InfoPanelWidth, 300);
                width = Math.Max(width, 100); //TODO: can we do this with uss min-width?
                infoPanelContainer.style.width = width;
            } else {
                infoPanelContainer.style.width = 0;
            }
        }

        /// <summary>
		/// Closes the non-focus agent panel with the given agent ID
        /// </summary>
        private void CloseNonFocusAgentPanel(int agentID) {
            if(!nonFocusAgentPanelHandlers.ContainsKey(agentID))
                return;
        
            infoPanelContainer.Remove(nonFocusAgentPanelHandlers[agentID].GetRootVisualElement());
            nonFocusAgentPanelHandlers.Remove(agentID);    
            if(nonFocusAgentPanelHandlers.Count == 0 && perceivedAgentPanelHandlers.Count == 0)
                ShowInfoPanelContainer(false);
            Resources.UnloadUnusedAssets();
        }

        /// <summary>
		/// Closes the floating panel with the given key
        /// </summary>
        private void CloseFloatingAgentPanel(string agentKey) {
            if(!floatingAgentPanelHandlers.ContainsKey(agentKey))
                return;
        
            floatingInfoPanelLayer.Remove(floatingAgentPanelHandlers[agentKey].GetRootVisualElement());
            floatingAgentPanelHandlers.Remove(agentKey);    
            Resources.UnloadUnusedAssets();
        }

        /// <summary>
		/// Closes the perceived agent panel with the given agent ID
        /// </summary>
        private void ClosePerceivedAgentPanel(int agentID) {
            if(!perceivedAgentPanelHandlers.ContainsKey(agentID))
                return;
        
            infoPanelContainer.Remove(perceivedAgentPanelHandlers[agentID].GetRootVisualElement());
            perceivedAgentPanelHandlers.Remove(agentID);    
            if(nonFocusAgentPanelHandlers.Count == 0 && perceivedAgentPanelHandlers.Count == 0)
                ShowInfoPanelContainer(false);
            Resources.UnloadUnusedAssets();
        }

        /// <summary>
		/// Event that is called when the user changes the display options in the display options window
        /// (toggles the visibility of a parameter displayed in the agent panels)
        /// </summary>
        private void OnDisplayOptionsChanged() {
            UpdatePanelDisplayOptions();
        }

        /// <summary>
		/// Triggers an update of the parameter lists (data keys) of all agent panels. 
        /// Note that the data values are updated with RefreshPanels().
        /// </summary>
        private void UpdatePanelDisplayOptions() {
            string focusAgentID = focusAgentPanelHandler.GetAgentID().ToString();
            List<DisplayOption> focusAgentDisplayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);
            List<DisplayOption> nonFocusAgentDisplayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
            List<DisplayOption> perceivedAgentDisplayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
            
            foreach(KeyValuePair<string, FloatingAgentPanelHandler> handler in floatingAgentPanelHandlers)
                handler.Value.UpdateVisibility((handler.Key == focusAgentID) ? focusAgentDisplayOptions : nonFocusAgentDisplayOptions);

            foreach(AgentPanelHandler handler in nonFocusAgentPanelHandlers.Values)
                handler.UpdateDisplayOptions(nonFocusAgentDisplayOptions);

            foreach(AgentPanelHandler handler in perceivedAgentPanelHandlers.Values)
                handler.UpdateDisplayOptions(perceivedAgentDisplayOptions);

            focusAgentPanelHandler.UpdateDisplayOptions(focusAgentDisplayOptions);
        }

        /// <summary>
		/// This event is fired, when the currently focused agent moves out of the view (user moves the camera away)
		/// </summary>
		private void OnFocusAgentOutOfViewWarning (bool flashSemaphore) {
            if(focusAgentPanelHandler != null)
			    focusAgentPanelHandler.FlashTitle(flashSemaphore);
		}
    }
}