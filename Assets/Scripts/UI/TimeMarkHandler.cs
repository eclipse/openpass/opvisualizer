﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Zenject;

namespace Visualizer.UI
{
	/// <summary>
	/// The time mark is a colorful indicator of an event on the time axis. 
	/// An event is usually a collision between two agents. When the user clicks on a time mark, 
	/// the camera jumps to the event in time and space. A tooltip describes the event in text. 
	/// </summary>
	public class TimeMarkHandler {

		SimulationEvent simEvent;
		private IAnimationController animationController;
		private IFocusCameraHandler focusCameraHandler;
		private VisualElement visualTimeMark;
		private Vector2 collapsedPos = new Vector2(0f, 0f);
		private Vector2 expandedPos = new Vector2(0f, -35f);
		private bool showWires = false;

		public TimeMarkHandler(VisualElement root, VisualElement vTimeMark, IAnimationController aController, IFocusCameraHandler acHandler, float pixelMsFactor, SimulationEvent sEvent){
			animationController = aController;
			focusCameraHandler = acHandler;
			simEvent = sEvent;
			visualTimeMark = vTimeMark;

			//set tooltip text
			visualTimeMark.AddManipulator(new TooltipManipulator(root.Q<Tooltip>("tooltip")));
			visualTimeMark.tooltip = "T:" + CreateToolTipText(simEvent.timeStamp * 0.001f);

			//Turns the time marks yellow in case it is not a collision
			if(simEvent.eventType == "Collision" || simEvent.eventType == "ExtendedCollision")
				visualTimeMark.Q("image").AddToClassList("red-time-mark");
			else
				visualTimeMark.Q("image").AddToClassList("yellow-time-mark");

			//set the position of the mark
			SetPosition (pixelMsFactor);

			//add OnClick handler
			visualTimeMark.RegisterCallback<ClickEvent>(ev => OnClick());
			visualTimeMark.generateVisualContent += OnGenerateVisualContent;
		}

		/// <summary>
		/// Creates the text that is displayed above the time mark, based on the parameters of the SimulationEvent
		/// </summary>
		/// <param name="simEvent">The details of the event.</param>
		private string CreateToolTipText(float timeStamp){
			string triggeringIDs = "";
			foreach(int triggeringID in simEvent.triggeringEntities){
				if (triggeringIDs != "")
					triggeringIDs += ", ";
		 		triggeringIDs += triggeringID.ToString();
			}
			string affectedIDs = "";
			foreach(int affectedID in simEvent.affectedEntities){
				if (affectedIDs != "")
					affectedIDs += ", ";
		 		affectedIDs += affectedID.ToString();
			}
			string parameters = "";
			foreach(KeyValuePair<string, string> eventParam in simEvent.eventParameters){
		 		parameters += "\n" + eventParam.Key + ": " + eventParam.Value;
			}

			string tooltip;
			tooltip = "<b>" + simEvent.eventType + "</b>";
			tooltip +=  "\nTimestamp: " + string.Format ("{0:00.000}", timeStamp) + "s";
			if(triggeringIDs != "")
				tooltip +=  "\nTriggering Entity: " + triggeringIDs;			
			if(affectedIDs != "")
				tooltip +=  "\nAffected Entity: " + affectedIDs;
			tooltip += parameters;
			
			return tooltip;
		}

		/// <summary>
		/// Updates the position of a time mark based on the time axis unit (depends on the size of the time axis)
		/// </summary>
		/// <param name="pixelMsFactor">Describes, how many horizontal pixels on the time axis correspond to one millisecond.</param>
		private void SetPosition(float pixelMsFactor){
			float xPos = (simEvent.timeStamp * pixelMsFactor) - GetWidth()/2f;
			visualTimeMark.style.left = xPos;
			collapsedPos.x = xPos;
			collapsedPos.y = visualTimeMark.resolvedStyle.top;
		}

		/// <summary>
		/// Returns the time marks root visual element
		/// </summary>
		public VisualElement GetVisualElement(){
			return visualTimeMark;
		}

		/// <summary>
		/// Returns the time marks left position
		/// </summary>
		public float GetLeft(){
			return visualTimeMark.resolvedStyle.left;
		}

		/// <summary>
		/// Returns the time marks width
		/// Note: currently (2021.2.0b10) for some reason either visualTimeMark.resolvedStyle.width nor visualTimeMark.style.width works
		/// -> this value must correspond to the real width of the time mark!
		/// </summary>
		public float GetWidth(){
			return 15f;
		}
		
		/// <summary>
		/// Sets the time mark into the expanded state (used only if it belongs to a group)
		/// </summary>
		public void Expand(float xPos){
			visualTimeMark.RemoveFromClassList("time-mark-collapsed");
			visualTimeMark.AddToClassList("time-mark-expanded");
			expandedPos.x = xPos;
			visualTimeMark.style.top = expandedPos.y;
			visualTimeMark.style.left = expandedPos.x;
			visualTimeMark.pickingMode = PickingMode.Position;
			visualTimeMark.visible = true;
			showWires = true;
			visualTimeMark.MarkDirtyRepaint();
		}

		/// <summary>
		/// Sets the time mark into the collapsed state (used only if it belongs to a group)
		/// </summary>
		public void Collapse(){
			visualTimeMark.AddToClassList("time-mark-collapsed");
			visualTimeMark.RemoveFromClassList("time-mark-expanded");
			visualTimeMark.pickingMode = PickingMode.Ignore;
			visualTimeMark.style.left = collapsedPos.x;
			visualTimeMark.style.top = collapsedPos.y;
			visualTimeMark.visible = false;
			showWires = false;
			visualTimeMark.MarkDirtyRepaint();
		}

		/// <summary>
		/// When the user clicks on a time mark, the camera jumps to the event in time and space. 
		/// </summary>
		public void OnClick(){
			animationController.JumpToTimeStamp (simEvent.timeStamp);
			if(simEvent.triggeringEntities.Count > 0)
				focusCameraHandler.FocusCamera ((int)simEvent.triggeringEntities[0]);
		}

		/// <summary>
		/// Callback that is executed when the visual element's visual contents need to be (re)generated. 
		/// </summary>
		public void OnGenerateVisualContent(MeshGenerationContext context){
			if(showWires) {
				List<Vector2> points = new List<Vector2>();
				float timeMarkWidth = 15f;
				float timeMarkHeight = 23f;
				float wireWidth = collapsedPos.x - visualTimeMark.resolvedStyle.left;
				float wireHeight = collapsedPos.y - visualTimeMark.resolvedStyle.top ;

				points.Add(new Vector2(timeMarkWidth/2f, timeMarkHeight-3f));
				points.Add(new Vector2(timeMarkWidth/2f, timeMarkHeight+5f));
				points.Add(new Vector2(wireWidth+timeMarkWidth/2f, wireHeight + 3f));
				points.Add(new Vector2(wireWidth+timeMarkWidth/2f, wireHeight + timeMarkHeight));
				
				DrawWire(points, 2f, visualTimeMark.Q("image").resolvedStyle.unityBackgroundImageTintColor, context);
			}
		}

		/// <summary>
		/// Creates a mesh with a line, connecting the given list of points. 
		/// Must be executed from the generateVisualContent callback of the visual element
		/// Based on https://forum.unity.com/threads/draw-a-line-from-a-to-b.698618/
		/// </summary>
		private void DrawWire(List<Vector2> points, float thickness, Color color, MeshGenerationContext context)
        {
            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices = new List<ushort>();
 
            for (int i = 0; i < points.Count - 1; i++)
            {
                var pointA = points[i];
                var pointB = points[i + 1];
 
                float angle = Mathf.Atan2(pointB.y - pointA.y, pointB.x - pointA.x);
                float offsetX = thickness / 2 * Mathf.Sin(angle);
                float offsetY = thickness / 2 * Mathf.Cos(angle);
 
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x + offsetX, pointA.y - offsetY, Vertex.nearZ),
                    tint = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x + offsetX, pointB.y - offsetY, Vertex.nearZ),
                    tint = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x - offsetX, pointB.y + offsetY, Vertex.nearZ),
                    tint = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x - offsetX, pointB.y + offsetY, Vertex.nearZ),
                    tint = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x - offsetX, pointA.y + offsetY, Vertex.nearZ),
                    tint = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x + offsetX, pointA.y - offsetY, Vertex.nearZ),
                    tint = color
                });
 
                ushort indexOffset(int value) => (ushort)(value + (i * 6));
                indices.Add(indexOffset(0));
                indices.Add(indexOffset(1));
                indices.Add(indexOffset(2));
                indices.Add(indexOffset(3));
                indices.Add(indexOffset(4));
                indices.Add(indexOffset(5));
            }
 
            var mesh = context.Allocate(vertices.Count, indices.Count);
            mesh.SetAllVertices(vertices.ToArray());
            mesh.SetAllIndices(indices.ToArray());
        }
	}
}