/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;

namespace Visualizer.UI {

    public class DisplayOption {
        public string Key { get; set; }
        public string Label { get; set; }
        public bool Enabled { get; set; }
        
        /// <summary>
        /// Constructs a DisplayOption 
        /// </summary>
        public DisplayOption(string key, bool enabled){
            Key = key;
            Label = key;
            Enabled = enabled;
        }

        public DisplayOption(string key, string label, bool enabled){
            Key = key;
            Label = label;
            Enabled = enabled;
        }
    }
}