/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Core;
using Visualizer.Utilities;
using Visualizer.Trace;
using Zenject;

namespace Visualizer.UI {

    public interface IPanelResizer {
        void Init(VisualElement root);
        void HandleMouseUp(Vector2 mousePos);
        void HandleMouseDrag(Vector2 mousePos, bool swapY);
        void EnableFocusPanelResizing(bool enable);
    }

    public class PanelResizer : IPanelResizer
    {
        private enum ResizeMode { None, InfoPanelW, FocusPanelW, FocusPanelH };

        private ResizeMode mode;
        private VisualElement infoPanelContainer;
        private VisualElement focusPanelContainer;
        private VisualElement infoPanelResizerW;
        private VisualElement focusPanelResizerH;
        private VisualElement focusPanelResizerW;
        private IAppStorage appStorage;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAppStorage aStorage){
            appStorage = aStorage;
		}

        public void Init(VisualElement root)
        {
            infoPanelContainer = root.Q<VisualElement>("info-panel-container");
            focusPanelContainer = root.Q<VisualElement>("focus-agent-info-panel-container");
            infoPanelResizerW = infoPanelContainer.Q<VisualElement>("w-resizer-bar");
            focusPanelResizerW = focusPanelContainer.Q<VisualElement>("w-resizer-bar");
            focusPanelResizerH = focusPanelContainer.Q<VisualElement>("h-resizer-bar");
            infoPanelResizerW.RegisterCallback<MouseDownEvent>(ev => HandleMouseDown(ev, ResizeMode.InfoPanelW));
            infoPanelContainer.RegisterCallback<MouseUpEvent>(ev => HandleMouseUp(ev.mousePosition));
            infoPanelContainer.RegisterCallback<MouseMoveEvent>(ev => HandleMouseDrag(ev.mousePosition, true));
            focusPanelResizerH.RegisterCallback<MouseDownEvent>(ev => HandleMouseDown(ev, ResizeMode.FocusPanelH));
            focusPanelResizerW.RegisterCallback<MouseDownEvent>(ev => HandleMouseDown(ev, ResizeMode.FocusPanelW));
            focusPanelContainer.RegisterCallback<MouseUpEvent>(ev => HandleMouseUp(ev.mousePosition));
            focusPanelContainer.RegisterCallback<MouseMoveEvent>(ev => HandleMouseDrag(ev.mousePosition, true));
            mode = ResizeMode.None;
        }

        public void EnableFocusPanelResizing(bool enable){
            focusPanelResizerH.style.display = enable ? DisplayStyle.Flex : DisplayStyle.None;
            focusPanelResizerW.style.display = enable ? DisplayStyle.Flex : DisplayStyle.None;
        }

        private void HandleMouseDown(MouseDownEvent ev, ResizeMode m) {
            mode = m;
            switch(mode){
            case ResizeMode.InfoPanelW:
                infoPanelContainer.RemoveFromClassList("smoothed-move");
            break;
            case ResizeMode.FocusPanelH:
            case ResizeMode.FocusPanelW:
                focusPanelContainer.RemoveFromClassList("smoothed-move");
            break;
            }
        }

		public void HandleMouseDrag(Vector2 mousePos, bool swapY){
            if (swapY)
                mousePos.y =  Screen.height - mousePos.y;

            mousePos.x = Mathf.Clamp(mousePos.x, 30f, Screen.width-30f);
            mousePos.y = Mathf.Clamp(mousePos.y, 50f, Screen.height-50f);

            switch(mode){
            case ResizeMode.InfoPanelW:
                float infoPanelW = Screen.width - mousePos.x;  
                infoPanelContainer.style.width = Mathf.Max(infoPanelW, 150f);     
            break;
            case ResizeMode.FocusPanelH:
                float footerHeight = 85; //Workaround as resolvedStyle.height on "footer-area" is not working
                float focusPanelH = mousePos.y - focusPanelContainer.resolvedStyle.bottom - footerHeight; 
                focusPanelContainer.style.height = Mathf.Max(focusPanelH, 80f);        
            break;
            case ResizeMode.FocusPanelW:
                float focusPanelW = mousePos.x - focusPanelContainer.resolvedStyle.left;   
                focusPanelContainer.style.width = Mathf.Max(focusPanelW, 200f);
            break;
            }
		}

		public void HandleMouseUp(Vector2 mousePos){
            switch(mode){
            case ResizeMode.InfoPanelW:
                infoPanelContainer.AddToClassList("smoothed-move");
                appStorage.Write(AppStorageKey.InfoPanelWidth, infoPanelContainer.resolvedStyle.width.ToString());
            break;
            case ResizeMode.FocusPanelH:
                focusPanelContainer.AddToClassList("smoothed-move");
                appStorage.Write(AppStorageKey.FocusPanelHeight, focusPanelContainer.resolvedStyle.height.ToString());
            break;
            case ResizeMode.FocusPanelW:
                focusPanelContainer.AddToClassList("smoothed-move");
                appStorage.Write(AppStorageKey.FocusPanelWidth, focusPanelContainer.resolvedStyle.width.ToString());
            break;
            }
            mode = ResizeMode.None;
        }
   }
}