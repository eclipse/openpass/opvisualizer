/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.Core;
using Visualizer.AgentHandling;

namespace Visualizer.UI {

    public enum AgentPanelType { FocusAgent, NonFocusAgent, PerceivedAgent };
    public enum AgentPanelWindowMode { Minimized, Maximized };
    public class AgentPanelHandler
    {
        private VisualElement root;
        private Label headerLabel;
        private Button closeButton;
        private ListView paramListView;
        private VisualElement divider;
        private VisualElement titleBar;
        private VisualElement focusIcon;
        private VisualElement carIcon;
        private VisualElement truckIcon;
        private VisualElement pedestrianIcon;
        private VisualElement perceiveIcon;
        private VisualElement maximizeBtn;
        private VisualElement minimizeBtn;
        private VisualElement configureBtn;
        private VisualElement closeBtn;
        private VisualElement changeFocusBtn;
        private Agent agentRef;
        private AgentPanelType panelType;
        List<DisplayOption> displayOptions = new List<DisplayOption>();
        bool isResizing = false;
        private IAppStorage appStorage;
        
        private List<AgentPanelItem> dataItems = new List<AgentPanelItem>();

        /// <summary>
        /// A panel that displays live information about an agent in form of a key-value-list
        /// </summary>
        /// <param name="vElement">The visual element of the panel</param>
        /// <param name="dOptions">List of item keys that the InfoPanel displays</param>
        /// <param name="agent">Reference to the agent, whose info should be displayed</param>
        /// <param name="headerLabel">The text that should be displayed in the title bar</param>
        /// <param name="type">Type of the panel (regular agent, perceived agent, focus agent)</param>
        public AgentPanelHandler(VisualElement vElement, List<DisplayOption> dOptions, Agent agent, string header, AgentPanelType type, IAppStorage aStorage) {
            root = vElement;
            agentRef = agent;
            panelType = type;
            appStorage = aStorage;
            headerLabel = root.Q<Label>("agent-id-lbl");
            paramListView = root.Q<ListView>("param-list-view");
            divider = root.Q<VisualElement>("divider-bar");
            focusIcon = root.Q<VisualElement>("focus-icon");
            carIcon = root.Q<VisualElement>("car-icon");
            truckIcon = root.Q<VisualElement>("truck-icon");
            pedestrianIcon = root.Q<VisualElement>("pedestrian-icon");
            perceiveIcon = root.Q<VisualElement>("perceive-icon");
            changeFocusBtn = root.Q<VisualElement>("switch-focus-btn");
            configureBtn = root.Q<VisualElement>("configure-btn");
            minimizeBtn = root.Q<VisualElement>("minimize-btn");
            maximizeBtn = root.Q<VisualElement>("maximize-btn");
            closeBtn = root.Q<VisualElement>("close-btn");
            titleBar = root.Q<VisualElement>("titlebar");
            divider.RegisterCallback<MouseDownEvent>(ev => DividerMouseDown(ev));
            divider.RegisterCallback<MouseUpEvent>(ev => HandleMouseUp(ev.mousePosition));
            divider.RegisterCallback<MouseMoveEvent>(ev => HandleMouseDrag(ev.mousePosition));
            paramListView.RegisterCallback<MouseUpEvent>(ev => HandleMouseUp(ev.mousePosition));
            paramListView.RegisterCallback<MouseMoveEvent>(ev => HandleMouseDrag(ev.mousePosition));
            displayOptions = dOptions;

            InitHeader(header);
            InitListView();
            LoadDividerPos();
        }

        /// <summary>
        /// Set up the panel's title bar
        /// </summary>
        private void InitHeader(string label){
            focusIcon.style.display = DisplayStyle.None;
            perceiveIcon.style.display = DisplayStyle.None;
            changeFocusBtn.style.display = DisplayStyle.None;
            minimizeBtn.style.display = DisplayStyle.None;
            maximizeBtn.style.display = DisplayStyle.None;
            configureBtn.style.display = DisplayStyle.None;
            closeBtn.style.display = DisplayStyle.None;
            carIcon.style.display = DisplayStyle.None;
            truckIcon.style.display = DisplayStyle.None;
            pedestrianIcon.style.display = DisplayStyle.None;

            switch (panelType){
                case AgentPanelType.NonFocusAgent:
                    closeBtn.style.display = DisplayStyle.Flex;
                    VehicleModelType vehicleType = agentRef.StaticAgentInfo.VehicleModelType;
                    if(vehicleType == VehicleModelType.Unknown)
				        vehicleType = VehicleModelType.Car;
                    carIcon.style.display = (vehicleType == VehicleModelType.Car) ? DisplayStyle.Flex : DisplayStyle.None;
                    truckIcon.style.display = (vehicleType == VehicleModelType.Truck) ? DisplayStyle.Flex : DisplayStyle.None;
                    pedestrianIcon.style.display = (vehicleType == VehicleModelType.Pedestrian) ? DisplayStyle.Flex : DisplayStyle.None;
                break;
                case AgentPanelType.PerceivedAgent:
                    perceiveIcon.style.display = DisplayStyle.Flex;
                    closeBtn.style.display = DisplayStyle.Flex;
                    configureBtn.style.display = DisplayStyle.Flex;
                break;
                case AgentPanelType.FocusAgent:
                    focusIcon.style.display = DisplayStyle.Flex;
                    changeFocusBtn.style.display = DisplayStyle.Flex;
                    minimizeBtn.style.display = DisplayStyle.Flex;
                    configureBtn.style.display = DisplayStyle.Flex;
                break;
            }
            headerLabel.text = label;
        }


        /// <summary>
        /// Initialize the parameter list of the AgentPanel
        /// </summary>
        private void InitListView(){
            dataItems.Clear();

            if(displayOptions == null)
                return;

            displayOptions = displayOptions.Where(o => !o.Key.StartsWith("Floating.") && !o.Key.StartsWith("Visualized.")).ToList();
            
            foreach (DisplayOption option in displayOptions){
                if(option.Enabled)
                    dataItems.Add(new AgentPanelItem(option.Key, agentRef));
            }

            var visualItemTemplate = Resources.Load<VisualTreeAsset>("UI/AgentPanelItem");
            Func<VisualElement> makeItem = () => visualItemTemplate.Instantiate();
            Action<VisualElement, int> bindItem = (e, i) =>
            {
                e.Q<Label>("key-lbl").text = dataItems[i].Label;
                e.Q<Label>("value-lbl").text = dataItems[i].GetValue();
                e.Q<Label>("key-lbl").style.width = divider.style.left;
            };

            paramListView.makeItem = makeItem;
            paramListView.bindItem = bindItem;
            paramListView.itemsSource = dataItems;
        }

        /// <summary>
        /// Updates the panel's target agent reference
        /// </summary>
        public void SwitchAgent(Agent newAgent){
            if (newAgent == null){
                headerLabel.text = "None";
                maximizeBtn.style.display = DisplayStyle.None;
            }
            else {
                headerLabel.text = "Agent " + newAgent.ID;
            }
            
            agentRef = newAgent;
            InitListView();
        }

        /// <summary>
        /// Updates the value of all elements in the list.
        /// </summary>
        public void RefreshValues(){
            if(agentRef != null)
                paramListView.Rebuild();
        }


        /// <summary>
        /// Reinitializes the list view with the given list of item keys.
        /// </summary>
        public void UpdateDisplayOptions(List<DisplayOption> options){
            displayOptions = options;
            InitListView();
        }

        /// <summary>
        /// Handles the mouse move event
        /// </summary>
		public void HandleMouseDrag(Vector2 mousePos){
            if(isResizing){
                divider.style.left = mousePos.x - root.worldBound.x;
                paramListView.Rebuild();
            }
		}

        /// <summary>
        /// Handles the mouse down event
        /// </summary>
        private void DividerMouseDown(MouseDownEvent ev) {
            isResizing = true;         
        }

        /// <summary>
        /// Handles the mouse up event
        /// </summary>
		public void HandleMouseUp(Vector2 mousePos){
            SaveDividerPos();
            isResizing = false;  
        }

        /// <summary>
        /// Passes the divider position to the app storage for persistance
        /// </summary>
		public void SaveDividerPos(){
            string cfgKey = (panelType == AgentPanelType.FocusAgent) ? AppStorageKey.FocusPanelDividerPos : AppStorageKey.InfoPanelDividerPos;
            int value = (int)divider.style.left.value.value;
            appStorage.Write(cfgKey, value.ToString());
        }

        /// <summary>
        /// Reads the divider position from the app storage and sets the divider accordingly
        /// </summary>
		public void LoadDividerPos(){
            string cfgKey;
            int defaultPos;
            if(panelType == AgentPanelType.FocusAgent) {
                cfgKey = AppStorageKey.FocusPanelDividerPos;
                defaultPos = 120;
            } else {
                cfgKey = AppStorageKey.InfoPanelDividerPos;
                defaultPos = 180;
            }
            divider.style.left = appStorage.ReadInt(cfgKey, defaultPos);
        }

        /// <summary>
        /// Returns the panel's root visual element
        /// </summary>
        public VisualElement GetRootVisualElement() {
            return root;
        }

        /// <summary>
        /// Returns the panel's target agent ID, or -1 if invalid
        /// </summary>
        public int GetAgentID() {
            if(agentRef != null)
                return agentRef.ID;
            else
                return -1;
        }

        /// <summary>
        /// Returns the height of the panel's title bar
        /// </summary>
        public int GetMinimizedHeight() {
            return (int)titleBar.resolvedStyle.height;
        }

        /// <summary>
        /// Displays the minimize or maximize buttons
        /// </summary>
        public void ChangeWindowMode(AgentPanelWindowMode mode) {
            if(mode == AgentPanelWindowMode.Maximized){
                minimizeBtn.style.display = DisplayStyle.Flex;
                maximizeBtn.style.display = DisplayStyle.None;
                titleBar.AddToClassList("agent-info-panel-maximized-titlebar");
                titleBar.RemoveFromClassList("agent-info-panel-minimized-titlebar");
            } else {
                minimizeBtn.style.display = DisplayStyle.None;
                maximizeBtn.style.display = DisplayStyle.Flex;
                titleBar.AddToClassList("agent-info-panel-minimized-titlebar");
                titleBar.RemoveFromClassList("agent-info-panel-maximized-titlebar");
            }
        }

        /// <summary>
        /// Flashes the title of the panel to display warning (event fired once per flash to ensure sync flashing)
        /// </summary>
        public void FlashTitle(bool flashSemaphore) {
            focusIcon.EnableInClassList("agent-info-panel-titlebar-warning", flashSemaphore);
            headerLabel.EnableInClassList("agent-info-panel-titlebar-warning", flashSemaphore);
        }
    }
}