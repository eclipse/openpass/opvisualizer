/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Visualizer.AgentHandling;
using Visualizer.Core;
using Zenject;

namespace Visualizer.UI {

	public delegate void DisplayOptionListChangedHandler(List<DisplayOption> displayOptions);

	public interface IDisplayOptionsWndHandler{
		void Init(List<DisplayOption> options, ListView optionsListView);
		event DisplayOptionListChangedHandler ListChanged;
	}

    public class DisplayOptionsWndHandler : IDisplayOptionsWndHandler {
        
        public event DisplayOptionListChangedHandler ListChanged;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){
		}

        /// <summary>
        /// This class gets a list of DisplayOptions, fills them into a DisplyOptionListView and updates them
        /// </summary>
        /// <param name="options">List of DisplayOptions that the paramswitch list view should handle</param>
        public void Init(List<DisplayOption> options, ListView optionsListView)
        {
            TreeList treeList = new TreeList();
            treeList.CreateFromDisplayOptions(options, "Textual");

            var visualItemTemplate = Resources.Load<VisualTreeAsset>("UI/DisplayOptionListItem");
            Func<VisualElement> makeItem = () => 
            { 
                VisualElement listItem = visualItemTemplate.Instantiate();
                listItem.Q<Toggle>("checkbox").RegisterValueChangedCallback(evt => ValueChanged(evt, treeList));
                return listItem;
            };
            Action<VisualElement, int> bindItem = (e, i) =>
            {
                e.Q<Label>("key-lbl").text = treeList[i].Label;
                e.style.paddingLeft = 10 + treeList[i].Level * 10;
                e.Q<Toggle>("checkbox").userData = i;
                e.Q<Toggle>("checkbox").SetValueWithoutNotify(treeList[i].Enabled);
                if(treeList[i].Group){
                    e.Q<Label>("key-lbl").AddToClassList("list-group");
                    e.Q<Toggle>("checkbox").style.display = DisplayStyle.None;
                } else {    
                    e.Q<Label>("key-lbl").RemoveFromClassList("list-group");
                    e.Q<Toggle>("checkbox").style.display = DisplayStyle.Flex;
                }

            };
            optionsListView.makeItem = makeItem;
            optionsListView.bindItem = bindItem;
            optionsListView.itemsSource = treeList;
            ListChanged = null;
        }

        /// <summary>
        /// This event handler is called after a toggle item is clicked. 
        /// Note that we use the userData object of the toggle to pass the index of the list item from bindItem to this hook
        /// </summary>
        private void ValueChanged(ChangeEvent<bool> evt, TreeList treeList) {
            Toggle toggle = evt.target as Toggle;
            int i = (int)toggle.userData;
            treeList[i].Enabled = evt.newValue;
            if (ListChanged != null)
                ListChanged (treeList.ToDisplayOptionList());
        }
    }
}