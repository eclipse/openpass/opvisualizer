﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Visualizer.Core;
using Visualizer.Utilities;
using Visualizer.Trace;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Visualizer.UI
{
	public delegate void RunSelectionEventHandler(string traceFilePath, int runID);

	public interface IRunSelectorWndHandler{
		void Init(VisualElement window, string path, List<Run> runs);
		event RunSelectionEventHandler RunSelected;
	}

	/// <summary>
	/// Dialog where the user can choose a simulation run (part of the trace file)
	/// </summary>
	public class RunSelectorWndHandler: IRunSelectorWndHandler {

		public event RunSelectionEventHandler RunSelected;
		private VisualElement window;
		private ListView runListView;
		private Button okButton;
		private List<Run> dataSource = new List<Run>();
		DateTime lastClickTime;
		private string tracePath = "";

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){
		}

		/// <summary>
		/// Initializes the file browser dialog
		/// </summary>
		public void Init(VisualElement root, string path, List<Run> runs){
			window = root;
			tracePath = path;
			dataSource = runs;
			runListView = window.Q<ListView>();
			okButton = window.Q<Button>("ok-btn");
			okButton.RegisterCallback<ClickEvent>(ev => AcceptRunSelection());
			window.Q<Button>("close-btn").RegisterCallback<ClickEvent>(ev => CancelRunSelection());
			window.Q<Button>("cancel-btn").RegisterCallback<ClickEvent>(ev => CancelRunSelection());		
			okButton.SetEnabled(false);
			RunSelected = null;
			
			InitRunListView();
		}

		/// <summary>
		/// Fills the file list 
		/// </summary>
		private void InitRunListView() {
			var visualItemTemplate = Resources.Load<VisualTreeAsset>("UI/RunSelectorListItem");
            Func<VisualElement> makeItem = () => visualItemTemplate.Instantiate();
            
            Action<VisualElement, int> bindItem = (e, i) =>
            {
                e.Q<Label>("run-id-lbl").text = dataSource[i].ID.ToString();
                e.Q<Label>("run-info-lbl").text = "(" + dataSource[i].EventCount + " events)";
            };
            runListView.makeItem = makeItem;
            runListView.bindItem = bindItem;
            runListView.itemsSource = dataSource;
			runListView.onItemsChosen += OnDoubleClickDummy; //<- NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click in OnSelectionChange
			runListView.onSelectionChange += OnSelectionChange;
		}

		/// <summary>
		/// Callback invoked when the user double clicks a file or folder.
		/// </summary>
		private void OnDoubleClick(IEnumerable<object> selection) {
			if(selection.Count() == 0 || selection.First() == null){
				return;
			}

			AcceptRunSelection();
		}

		/// <summary>
		/// Callback invoked when the user double clicks a list entry.
		/// </summary>
		private void OnDoubleClickDummy(IEnumerable<object> selection) {
			//NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click in OnSelectionChange
			//Still, we must have an empty function registered for the onItemsChosen event, otherwise other strange behaviour shows up
		}

		/// <summary>
		/// Callback invoked when the user selects a run in the list
		/// </summary>
		private void OnSelectionChange(IEnumerable<object> selection) {	
			if(selection.Count() == 0 || selection.First() == null){
				return;
			}

			Run selectedRun = selection.First() as Run;
			okButton.SetEnabled(true);

			//NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click
			double timeSinceLastClick = (DateTime.UtcNow - lastClickTime).TotalMilliseconds;
			if(timeSinceLastClick < 300)
				OnDoubleClick(selection);
			lastClickTime = DateTime.UtcNow;	
		}

		/// <summary>
		/// Called when the user either double clicked a list entry or pressed OK of the dialog
		/// </summary>
		private void AcceptRunSelection() {
			if(runListView.selectedIndex < 0)
				return;

			Run selectedRun = dataSource[runListView.selectedIndex];
			if (selectedRun == null)
				return;
			
			if (RunSelected != null)
				RunSelected (tracePath, selectedRun.ID);
		}

		/// <summary>
		/// Called when the user either selected a run and pressed OK or canceled the dialog
		/// </summary>
		private void CancelRunSelection() {		
			if (RunSelected != null)
				RunSelected ("", -1);
		}
	}
}