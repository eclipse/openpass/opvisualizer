﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Visualizer.Core;
using Visualizer.Utilities;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Visualizer.UI
{
	public delegate void FileChosenEventHandler(string path);
	public delegate void AddTooltipRequestEventHandler(VisualElement element);
	public delegate void HideTooltipRequestEventHandler();

	public interface IFileBrowserWndHandler{
		void Init(VisualElement window, string selectionPattern, int windowWidth);
		event FileChosenEventHandler FileSelected;
		event AddTooltipRequestEventHandler AddTooltipRequest;
		event HideTooltipRequestEventHandler HideTooltipRequest;
	}

	/// <summary>
	/// Dialog where the user can browse for files
	/// </summary>
	public class FileBrowserWndHandler: IFileBrowserWndHandler {

		public event FileChosenEventHandler FileSelected;
		public event AddTooltipRequestEventHandler AddTooltipRequest;
		public event HideTooltipRequestEventHandler HideTooltipRequest;
		private VisualElement window;
		private ListView fileListView;
		private Button okButton;
		private Button exitFolderBtn;
		private Button directPathBtn;
		private VisualElement driveButtonContainer;
		private VisualElement pathButtonContainer;
		private TextField directPathTextBox;
		private List<FileListEntry> dataSource = new List<FileListEntry>();
		private string selectionPattern = "";
		private string currentFolder = "";
		private static readonly char pathSeparator = Path.DirectorySeparatorChar;
		DateTime lastClickTime;
		private int maxPathWidth = 0;
		private int maxDriveWidth = 0;
		private int longestFolderSize = 0;
		private int longestDriveSize = 0;

        private IAppStorage appStorage;
		private IFileHelper fileHelper;
	
		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAppStorage aStorage, IFileHelper fHelper){
			appStorage = aStorage;
			fileHelper = fHelper;
		}

		/// <summary>
		/// Initializes the file browser dialog
		/// </summary>
		public void Init(VisualElement root, string sPattern, int windowWidth){
			window = root;
			selectionPattern = sPattern;
			maxPathWidth = windowWidth - 20;
			maxDriveWidth = windowWidth;

			fileListView = window.Q<ListView>();
			okButton = window.Q<Button>("ok-btn");
			exitFolderBtn = window.Q<Button>("exit-folder-btn");
			directPathBtn = window.Q<Button>("direct-path-btn");
			okButton.RegisterCallback<ClickEvent>(ev => AcceptFileSelection());
			exitFolderBtn.RegisterCallback<ClickEvent>(ev => OnClickExitFolder());
			directPathBtn.RegisterCallback<ClickEvent>(ev => OnClickDirectPath());
			driveButtonContainer = window.Q<VisualElement>("drive-btn-container");
			pathButtonContainer = window.Q<VisualElement>("path-btn-container");
			directPathTextBox = window.Q<TextField>("direct-path-textbox");
			window.Q<Button>("close-btn").RegisterCallback<ClickEvent>(ev => CancelFileSelection());
			window.Q<Button>("cancel-btn").RegisterCallback<ClickEvent>(ev => CancelFileSelection());
			directPathTextBox.RegisterCallback<ChangeEvent<string>>(ev => ChangeFolder(ev.newValue));
			directPathTextBox.RegisterCallback<FocusOutEvent>(OnDirectPathTextBoxFocusOut);

			FileSelected = null;

			if(AddTooltipRequest != null){
				AddTooltipRequest(directPathBtn);
				AddTooltipRequest(exitFolderBtn);
			}

			InitFileListView();
			CreateDriveButtons();

			string initialPath = appStorage.Read(AppStorageKey.LastSelectedPath);
			if(!fileHelper.DirectoryExists(initialPath))
				initialPath = Directory.GetDirectoryRoot(fileHelper.GetAppDataPath());

			ChangeFolder(initialPath);
		}

		/// <summary>
		/// Fills the list of files into the list view
		/// </summary>
		private void ChangeFolder(string path) {
			if(Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor )
				if(!path.StartsWith(pathSeparator.ToString()))
					path = pathSeparator + path;

			if(!fileHelper.DirectoryExists(path)){
				Debug.Log("Could not find path " + path);
				return;
			}

			ShowDirectPathTextBox(false);

			dataSource = fileHelper.ReadDirectoryContents(path, selectionPattern);
			RefreshFileListView();

			okButton.SetEnabled(false);

			appStorage.Write(AppStorageKey.LastSelectedPath, path);
			currentFolder = path;

			if(HideTooltipRequest != null)
				HideTooltipRequest();

			CreatePathButtons();
		}

		/// <summary>
		/// Creates a button for each folder in the path
		/// </summary>
		private void CreatePathButtons(int maxLength = -1) {
			pathButtonContainer.Clear();
			pathButtonContainer.RegisterCallback<GeometryChangedEvent>(OnPathRedraw);
			
			longestFolderSize = 0;
			string[] stringSeparators = new string[] { pathSeparator.ToString() };
			List<string> pathParts = currentFolder.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();
			string path = "";
			foreach(string pathPart in pathParts){
				Button folderBtn = new Button();
				path += pathPart + pathSeparator;
				string buttonPath = path;
				folderBtn.RegisterCallback<ClickEvent>(ev => ChangeFolder(buttonPath));
				folderBtn.AddToClassList("path-btn");
				string displayedFolder = pathPart;
				bool elided = false;
				if (maxLength > -1 && pathPart.Length > maxLength){
					displayedFolder = pathPart.Substring(0, maxLength);
					folderBtn.tooltip = pathPart;
					elided = true;
					if(AddTooltipRequest != null)
						AddTooltipRequest(folderBtn);
				}
				longestFolderSize = Math.Max(longestFolderSize, displayedFolder.Length);
				folderBtn.text = displayedFolder + (elided ? "..." : "") + "\\";
				pathButtonContainer.Add(folderBtn);
			}
		}

		/// <summary>
		/// Creates a button for each logical drive
		/// </summary>
		private void CreateDriveButtons(int maxLength = -1) {
			driveButtonContainer.Clear();
			driveButtonContainer.RegisterCallback<GeometryChangedEvent>(OnDrivesRedraw);
			longestDriveSize = 0;
			List<string> driveNames = fileHelper.GetSystemDrives();
			foreach(string driveName in driveNames){
				Button driveBtn = new Button();
				driveBtn.RegisterCallback<ClickEvent>(ev => OnClickChangeDrive(driveName));
				driveBtn.AddToClassList("dialog-btn");
				driveBtn.AddToClassList("drive-btn");
				string displayedDrive = driveName;
				bool elided = false;
				if (maxLength > -1 && driveName.Length > maxLength){
					displayedDrive = driveName.Substring(0, maxLength);
					driveBtn.tooltip = driveName;
					elided = true;
					if(AddTooltipRequest != null)
						AddTooltipRequest(driveBtn);
				}
				longestDriveSize = Math.Max(longestDriveSize, displayedDrive.Length);
				driveBtn.text = displayedDrive + (elided ? "..." : "");
				driveButtonContainer.Add(driveBtn);
			}
		}

		/// <summary>
		/// Called as soon as the visual element is redrawn and the resolvedStyle contains correct values
		/// </summary>
		private void OnPathRedraw(GeometryChangedEvent evt)
		{
			if (pathButtonContainer.resolvedStyle.width > maxPathWidth) {
				CreatePathButtons(longestFolderSize - 1);
			}
		}

		/// <summary>
		/// Called as soon as the visual element is redrawn and the resolvedStyle contains correct values
		/// </summary>
		private void OnDrivesRedraw(GeometryChangedEvent evt)
		{
			if (driveButtonContainer.resolvedStyle.width > maxDriveWidth) {
				int nextLength = Math.Max(longestDriveSize-2, 2);
				CreateDriveButtons(nextLength);
			}
		}

		/// <summary>
		/// Fills the file list 
		/// </summary>
		private void InitFileListView() {
			var visualItemTemplate = Resources.Load<VisualTreeAsset>("UI/FileBrowserListItem");
            Func<VisualElement> makeItem = () => visualItemTemplate.Instantiate();
            
            Action<VisualElement, int> bindItem = (e, i) =>
            {
                Label fileLabel = e.Q<Label>("filename-lbl");
				fileLabel.text = dataSource[i].Name;
				SetListItemIcon(e, dataSource[i].Type);
				GrayNonMatchingItem(fileLabel, dataSource[i]);
            };
            fileListView.makeItem = makeItem;
            fileListView.bindItem = bindItem;
            fileListView.itemsSource = dataSource;
			fileListView.onItemsChosen += OnDoubleClickDummy; //<- NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click in OnSelectionChange
			fileListView.onSelectionChange += OnSelectionChange;
		}

		/// <summary>
		/// Fills the file list 
		/// </summary>
		private void RefreshFileListView() {
			fileListView.Clear();
			fileListView.itemsSource = dataSource;
			fileListView.ScrollToItem(0);
			fileListView.ClearSelection();
			fileListView.Rebuild();
		}


		/// <summary>
		/// Enables the file icon of a list item that corresponds to the given FileType.
		/// As bindItem is called for every list item, every time the user scrolls the list, we avoid 
		/// constantly reloading the icon and just switch the icon visibility  
		/// </summary>
		private void SetListItemIcon(VisualElement listItem, FileType type) {
			switch (type) {
				case FileType.File:
					listItem.Q<VisualElement>("icon-file").style.display = DisplayStyle.Flex;
					listItem.Q<VisualElement>("icon-folder").style.display = DisplayStyle.None;
				break;
				case FileType.Folder:
					listItem.Q<VisualElement>("icon-file").style.display = DisplayStyle.None;
					listItem.Q<VisualElement>("icon-folder").style.display = DisplayStyle.Flex;
				break;
			}
		}


		/// <summary>
		/// Colors the given listentry gray, if its disabled
		/// </summary>
		private void GrayNonMatchingItem(Label fileLabel, FileListEntry listEntry) {
			if (!listEntry.Active)
				fileLabel.AddToClassList("disabled-list-item");
			else
				fileLabel.RemoveFromClassList("disabled-list-item");
		}

		/// <summary>
		/// Callback invoked when the user clicks the exit folder button
		/// </summary>
		private void OnClickExitFolder() {
			int lastSlashPos = currentFolder.LastIndexOf (pathSeparator);
			if (lastSlashPos != -1 && (lastSlashPos != currentFolder.Length-1)) {
				string newFolder = currentFolder.Substring (0, lastSlashPos);
				if(newFolder.EndsWith(':'))
					newFolder += pathSeparator;
				ChangeFolder(newFolder);
			}
		}

		/// <summary>
		/// Callback invoked when the user clicks the direct path button
		/// </summary>
		private void OnClickDirectPath() {
			ShowDirectPathTextBox(true);
		}

		/// <summary>
		/// Event triggered if the direct path input text field looses focus 
		/// </summary>
		private void OnDirectPathTextBoxFocusOut(FocusOutEvent evt) {
			ShowDirectPathTextBox(false);
		}

		/// <summary>
		/// Displays/hides the direct path input text field and sets its value to the current folder
		/// </summary>
		private void ShowDirectPathTextBox(bool show) {
			pathButtonContainer.style.display = show ? DisplayStyle.None : DisplayStyle.Flex;
			directPathTextBox.style.display = show ? DisplayStyle.Flex : DisplayStyle.None;

			if(show){
				directPathTextBox.SetValueWithoutNotify(Helpers.SanitizeScreenText(currentFolder));							directPathTextBox.SelectAll();
				directPathTextBox.Focus();
				directPathTextBox.SelectAll();
			}
		}

		/// <summary>
		/// Callback invoked when the user double clicks a file or folder.
		/// </summary>
		private void OnDoubleClick(IEnumerable<object> selection) {
			if(selection.Count() == 0 || selection.First() == null){
				return;
			}

			FileListEntry selectedEntry = selection.First() as FileListEntry;
			if (selectedEntry.Active == false)
				return;

			if(selectedEntry.Type == FileType.Folder)
				ChangeFolder(Path.Combine(currentFolder, selectedEntry.Name));
			else
				AcceptFileSelection();
		}

		/// <summary>
		/// Callback invoked when the user double clicks a file or folder.
		/// </summary>
		private void OnDoubleClickDummy(IEnumerable<object> selection) {
			//NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click in OnSelectionChange
			//Still, we must have an empty function registered for the onItemsChosen event, otherwise other strange behaviour shows up
		}

		/// <summary>
		/// Callback invoked when the user selects a file or folder
		/// </summary>
		private void OnSelectionChange(IEnumerable<object> selection) {	
			if(selection.Count() == 0 || selection.First() == null){
				return;
			}

			FileListEntry selectedFile = selection.First() as FileListEntry;
			if (selectedFile.Type == FileType.Folder)
				okButton.SetEnabled(false);
			else
				okButton.SetEnabled(selectedFile.Active);

			//NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click
			double timeSinceLastClick = (DateTime.UtcNow - lastClickTime).TotalMilliseconds;
			if(timeSinceLastClick < 300)
				OnDoubleClick(selection);
			lastClickTime = DateTime.UtcNow;	
		}

		/// <summary>
		/// Callback invoked when the user selects a file or folder
		/// </summary>
		private void OnClickChangeDrive(string driveName) {		
			ChangeFolder(driveName);
		}

		/// <summary>
		/// Called when the user either double clicked a file or pressed OK of the dialog
		/// </summary>
		private void AcceptFileSelection() {
			if(fileListView.selectedIndex < 0)
				return;

			FileListEntry selectedEntry = dataSource[fileListView.selectedIndex];
			if (selectedEntry == null)
				return;
			
			string fileName = Path.Combine(currentFolder, selectedEntry.Name);
			if (FileSelected != null)
				FileSelected (fileName);
		}


		/// <summary>
		/// Called when the user either selected a file and pressed OK or canceled the dialog
		/// </summary>
		/// <param name="path">The file that the user chose</param>
		private void CancelFileSelection() {		
			if (FileSelected != null)
				FileSelected ("");
		}
	}
}