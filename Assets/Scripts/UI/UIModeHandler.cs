﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;
using Visualizer.Core;
using Visualizer.UI;

namespace Visualizer.UI
{
	public delegate void UIModeChangedEventHandler(UIMode oldMode, UIMode newMode);
	public interface IUIModeHandler{
        event UIModeChangedEventHandler UIModeChanged;
		void SwitchMode (UIMode mode);
		UIMode GetMode ();
	}

	public enum UIMode { Unknown, SimulationPlayer, SensorPositioner, SceneryViewer };

	public class UIModeHandler : MonoBehaviour, IUIModeHandler {

		private UIMode uiMode = UIMode.Unknown;
		private ICmdLineInterpreter cmdLineInterpreter;
		private IMainUI ui;
		public event UIModeChangedEventHandler UIModeChanged;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ICmdLineInterpreter cInterpreter, IMainUI mUi){
			cmdLineInterpreter = cInterpreter;
			ui = mUi;
		}

		/// <summary>
		/// Monobehaviour method, called right after instantiation of the class
		/// </summary>
		void Start () {
			ui.SwitchUIModeRequest += new SwitchUIModeRequestEventHandler(OnSwitchUIModeRequest);
			string mode;
			cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.SystemMode, out mode);
			mode = mode == null ? "" : mode;
			switch (mode.ToLower()) {
				case "sensorpositioner":
					SwitchMode (UIMode.SensorPositioner);
					break;
				case "simplayer":
					SwitchMode (UIMode.SimulationPlayer);
					break;
				case "sceneryviewer":
					SwitchMode (UIMode.SceneryViewer);
					break;
				default:
					SwitchMode(UIMode.Unknown);
					break;
			}
		}

		/// <summary>
		/// Event that is triggered when the user changes the UIMode manually
		/// </summary>
		private void OnSwitchUIModeRequest(UIMode newMode) {
			if (newMode != uiMode) {
				if(UIModeChanged != null)
					UIModeChanged(uiMode, newMode);		
				SwitchMode(newMode);
			}
		}

		/// <summary>
		/// Shows/hides UI elements according to the given UIMode
		/// </summary>
		public void SwitchMode(UIMode mode) {
			ShowElement("sensor-positioner-layer", false);
			ShowElement("global-display-options-btn", false);
			ShowElement("load-run-menu-item", false);
			ShowElement("view-mode-dd", false);
			ShowElement("footer-area", false);
			ShowElement("info-panel-container", false);
			ShowElement("focus-agent-info-panel-container", false);
			ShowElement("highlight-info-panel", false);
			ShowElement("floating-info-panel-layer", false);
			ShowElement("mousepos-panel", false);

			switch (mode) {
			case UIMode.SimulationPlayer:
				ShowElement("view-mode-dd", true);
				ShowElement("global-display-options-btn", true);
				ShowElement("footer-area", true);
				ShowElement("info-panel-container", true);
				ShowElement("focus-agent-info-panel-container", true);
				ShowElement("highlight-info-panel", true);
				ShowElement("floating-info-panel-layer", true);
				ShowElement("mousepos-panel", true);
				break;
			case UIMode.SensorPositioner:
				ShowElement("sensor-positioner-layer", true);
				break;
			case UIMode.SceneryViewer:
				ShowElement("view-mode-dd", true);
				ShowElement("highlight-info-panel", true);
				ShowElement("mousepos-panel", true);
				break;
			default:
			case UIMode.Unknown:
				// only burger menu visible
				break;
			}
			uiMode = mode;
		}

		/// <summary>
		/// Shows/hides the visual element with the given name
		/// </summary>
		private void ShowElement(string name, bool show) {
			VisualElement element = ui.GetVisualElement(name);
			if(element != null)
				element.style.display = show ? DisplayStyle.Flex : DisplayStyle.None;
		}	

		public UIMode GetMode() {
			return uiMode;
		}
	}
}