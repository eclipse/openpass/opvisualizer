﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.UI
{
	public interface ITimeAxisHandler{
		void Init(VisualElement root);
		void Clear ();
		void ReloadTimeMarks ();
	}

	public enum TimeMarkGroupState { Expanded, Collapsed };
	public class TimeMarkGroup {
		public List<TimeMarkHandler> Members { get; set; } = new List<TimeMarkHandler>();
		public TimeMarkGroupState State { get; set; } = TimeMarkGroupState.Collapsed;
		public VisualElement GroupMarker { get; set; } = null;
		public IEnumerator CollapseInvocation { get; set; } = null;
	}

	/// <summary>
	/// Handles the time axis
	/// </summary>
	public class TimeAxisHandler: MonoBehaviour, ITimeAxisHandler{

		private IAnimationController animationController;
		private ISampleContainer sampleContainer;
		private IFocusCameraHandler focusCameraHandler;

		//UI elements
		private Label currentPlaybackTime;
		private SliderInt timeAxis;
		private VisualElement visualTimeMarkContainer;
		private VisualElement elapsedTimeColorBar;
		private List<TimeMarkHandler> allTimeMarkHandlers = new List<TimeMarkHandler>();
		private List<TimeMarkGroup> timeMarkGroups = new List<TimeMarkGroup>();

		//Internal variables
		private float lastTimeAxisWidth = 0f;
		private bool initialized = false;
		private VisualElement rootVisualElem;
		private IEnumerator timeMarkRecreateInvocation = null;
		float minGroupMarkerWidth = 18f; // the minimum with of a group marker
		int maxGroupSize = 20; // the maximum number of time marks in a group

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAnimationController aController, ISampleContainer sContainer, IFocusCameraHandler acHandler){
			animationController = aController;
			sampleContainer = sContainer;
			focusCameraHandler = acHandler;
		}

		/// <summary>
		/// Initializes the time axis
		/// </summary>
		public void Init(VisualElement root){
			//subscribe for the event that is fired when the animation controller updates the current sample
			animationController.SampleUpdated += new SampleUpdatedEventHandler(OnUpdateSamplePointer);
			rootVisualElem = root;

			timeAxis = rootVisualElem.Q<SliderInt>("time-axis-slider");
			currentPlaybackTime = rootVisualElem.Q<Label>("playback-time-lbl");
			visualTimeMarkContainer = rootVisualElem.Q("time-mark-container");
			timeAxis.RegisterValueChangedCallback(ev => OnSliding());
			timeAxis.RegisterCallback<GeometryChangedEvent>(OnTimeAxisResize);

			int totalPlaybackTimeInMs = sampleContainer.GetLastTimeStamp() - sampleContainer.GetFirstTimeStamp();
			timeAxis.lowValue = 0;
			if (sampleContainer.SamplingTime == 0)
				timeAxis.highValue = 0;
			else
				timeAxis.highValue = totalPlaybackTimeInMs/sampleContainer.SamplingTime;

			CreateElapsedTimeColorBar();
			CreateTimeMarks ();

			UpdateTimeAxis (sampleContainer.GetFirstTimeStamp());
			UpdateTimeLabel (sampleContainer.GetFirstTimeStamp());

			initialized = true;
		}

		/// <summary>
		/// Clears the time axis.
		/// </summary>
		public void Clear(){
			RemoveTimeMarks ();
			timeAxis.UnregisterValueChangedCallback(ev => OnSliding());
			initialized = false;
		}

		/// <summary>
		/// Called as soon as the visual element of the time axis is redrawn and the resolvedStyle contains correct values
		/// </summary>
		private void OnTimeAxisResize(GeometryChangedEvent evt)
		{
			float currentTimeAxisWidth = timeAxis.resolvedStyle.width; 
			if (lastTimeAxisWidth != currentTimeAxisWidth) {
				ReloadTimeMarksLazy();
				lastTimeAxisWidth = currentTimeAxisWidth;
			}
		}
		
		/// <summary>
		/// Recreates all time marks and time mark groups after some delay. This function makes sure that the recreation only executes once, 
		/// even if it is triggered multiple times within the given delay 
		/// (when OnGeometryChange keeps firing while the user is resizing the app window).
		/// </summary>
		private void ReloadTimeMarksLazy() {
			if (!initialized)
				return;

			visualTimeMarkContainer.visible = false; //we hide all time marks for the time of recreation to avoid flashing
			if(timeMarkRecreateInvocation != null)
				 this.CancelDelayedInvocation(timeMarkRecreateInvocation);
			timeMarkRecreateInvocation = this.InvokeDelayed(() => ReloadTimeMarks(), 0.1f);
		}

		/// <summary>
		/// Recreates all time marks and time mark groups
		/// </summary>
		public void ReloadTimeMarks() {
			if (!initialized)
				return;

			RemoveTimeMarks ();
			CreateTimeMarks ();
			UpdateElapsedTimeColorBar();
			timeMarkRecreateInvocation = null;
		}

		/// <summary>
		/// Creates the time marks based on the events in the loaded trace file. 
		/// A time mark is a button on the time axis that the user can use to navigate to the event in space and time.
		/// </summary>
		private void CreateTimeMarks(){
			float pixelMsFactor = CalculatePixelMsFactor ();
			List<SimulationEvent> simEvents = sampleContainer.GetEvents ();
			foreach (SimulationEvent simEvent in simEvents) {
				var template = Resources.Load<VisualTreeAsset>("UI/TimeMark");
				VisualElement visualTimeMark = template.CloneTree().Q<Button>();
				visualTimeMarkContainer.Add(visualTimeMark);
				TimeMarkHandler timeMarkHandler = new TimeMarkHandler(rootVisualElem, visualTimeMark, animationController, focusCameraHandler, pixelMsFactor, simEvent);
				allTimeMarkHandlers.Add(timeMarkHandler);
			}

			//wait for the time marks to be drawn and only then execute the group creation  
			if(allTimeMarkHandlers.Count > 0)
				allTimeMarkHandlers[0].GetVisualElement().RegisterCallback<GeometryChangedEvent>(OnTimeMarksCreated);
		}

		/// <summary>
		/// Called as soon as the visual element of the time marks is redrawn and the resolvedStyle contains correct values
		/// </summary>
		private void OnTimeMarksCreated(GeometryChangedEvent evt)
		{
			if(allTimeMarkHandlers.Count > 0)
				allTimeMarkHandlers[0].GetVisualElement().UnregisterCallback<GeometryChangedEvent>(OnTimeMarksCreated);

			GroupTimeMarks();
		}

		/// <summary>
		/// Clears all time marks
		/// </summary>
		private void GroupTimeMarks(){
			if(!initialized)
				return;

			CreateTimeMarkGroups();
			CreateTimeMarkGroupMarkers();
			timeMarkRecreateInvocation = null;
			visualTimeMarkContainer.visible = true; //we hide all time marks for the time of recreation to avoid flashing
		}

		/// <summary>
		/// Creates the logical grouping of time markers, based on their position.
		/// If two time marks overlap, they are grouped
		/// </summary>
		private void CreateTimeMarkGroups() {
			TimeMarkGroup timeMarkGroup = null;
			for (int idx= 0; idx < allTimeMarkHandlers.Count-1; idx++) {
				TimeMarkHandler firstHandler = allTimeMarkHandlers[idx];
				TimeMarkHandler secondHandler = allTimeMarkHandlers[idx+1];
				if (secondHandler.GetLeft() < firstHandler.GetLeft() + firstHandler.GetWidth()) {
					if(timeMarkGroup == null){ //overlapping but not in a group yet -> create new group
						timeMarkGroup = new TimeMarkGroup();
						timeMarkGroup.Members.Add(firstHandler);
						timeMarkGroups.Add(timeMarkGroup);
					}
					timeMarkGroup.Members.Add(secondHandler); //overlapping -> add to last group
					if(timeMarkGroup.Members.Count >= maxGroupSize){
						timeMarkGroup = null; //max group size reached -> next group
						idx++; //the last mark has already been added to a group -> skip 
					}
				} else {
					timeMarkGroup = null; //not overlapping -> next group
				}
			}			
		}

		/// <summary>
		/// Creates the group marker visual elements and positions them to the middle of the group
		/// </summary>
		private void CreateTimeMarkGroupMarkers() {
			foreach (TimeMarkGroup timeMarkGroup in timeMarkGroups) {
				var template = Resources.Load<VisualTreeAsset>("UI/TimeMarkGroup");
				VisualElement groupMarker = template.CloneTree().Q<Button>();
				groupMarker.RegisterCallback<MouseEnterEvent>(ev => ExpandGroupedTimeMarks(timeMarkGroup, null));
				groupMarker.RegisterCallback<MouseLeaveEvent>(ev => CollapseGroupedTimeMarksIntent(timeMarkGroup));

				float minLeft = 9999f;
				float maxLeft = -9999f;
				bool redGroup = false;
				foreach (TimeMarkHandler timeMark in timeMarkGroup.Members) {
					float left = timeMark.GetLeft();
					minLeft = Math.Min(left, minLeft);
					maxLeft = Math.Max(left, maxLeft);
					if(timeMark.GetVisualElement().Q("image").ClassListContains("red-time-mark"))
						redGroup = true;
				}
				if (redGroup)
					groupMarker.AddToClassList("red-time-mark-group");
				else
					groupMarker.AddToClassList("yellow-time-mark-group");
				
				float totalGroupWidth = maxLeft - minLeft;
				float groupMarkerWidth = Math.Max(minGroupMarkerWidth, totalGroupWidth);
				float timeMarkerWidth = timeMarkGroup.Members[0].GetWidth();
				groupMarker.style.left = minLeft + (timeMarkerWidth/2f) + (totalGroupWidth/2f) - (groupMarkerWidth/2f);
				groupMarker.style.width = groupMarkerWidth;
				groupMarker.Q<Label>("member-count").text = timeMarkGroup.Members.Count.ToString();

				visualTimeMarkContainer.Add(groupMarker);
				timeMarkGroup.GroupMarker = groupMarker;

				//hide all grouped time marks as default
				CollapseGroupedTimeMarks(timeMarkGroup);
			}
		}

		/// <summary>
		/// Shows all time marks in the given group stacked in a column above the group marker. 
		/// </summary>
		private void ExpandGroupedTimeMarks(TimeMarkGroup timeMarkGroup, TimeMarkHandler hoveredTimeMark) {
			if(hoveredTimeMark != null)
				hoveredTimeMark.GetVisualElement().BringToFront();

			if(timeMarkGroup.CollapseInvocation != null) {
				this.CancelDelayedInvocation(timeMarkGroup.CollapseInvocation);
				timeMarkGroup.CollapseInvocation = null;
			}

			if (timeMarkGroup.State == TimeMarkGroupState.Expanded)
				return;

			float timeMarkerWidth = timeMarkGroup.Members[0].GetWidth();
			float expandedGroupWidth = timeMarkGroup.Members.Count*timeMarkerWidth;
			float groupMarkerWidth = timeMarkGroup.GroupMarker.resolvedStyle.width;
			float groupMarkerLeft = timeMarkGroup.GroupMarker.resolvedStyle.left;
			float expandedTimeMarkXPos = groupMarkerLeft + (groupMarkerWidth/2f) - expandedGroupWidth/2f;
			expandedTimeMarkXPos = Mathf.Clamp(expandedTimeMarkXPos, 0, timeAxis.resolvedStyle.width - expandedGroupWidth);
			foreach (TimeMarkHandler timeMark in timeMarkGroup.Members) {
				timeMark.Expand(expandedTimeMarkXPos);
				expandedTimeMarkXPos += timeMarkerWidth;
				timeMark.GetVisualElement().RegisterCallback<MouseEnterEvent>(ev => ExpandGroupedTimeMarks(timeMarkGroup, timeMark));
				timeMark.GetVisualElement().RegisterCallback<MouseLeaveEvent>(ev => CollapseGroupedTimeMarksIntent(timeMarkGroup));
			}
			timeMarkGroup.GroupMarker.style.opacity = 0;
			timeMarkGroup.GroupMarker.transform.scale = new Vector3(1f, 2f, 1f);
			timeMarkGroup.State = TimeMarkGroupState.Expanded; 
		}

		/// <summary>
		/// Intentiates the collapsing of the given time mark group with some ms delay. 
		/// In this delay time, the collapsing can be still cancelled (if the mouse enters to another time mark in the group)
		/// </summary>
		private void CollapseGroupedTimeMarksIntent(TimeMarkGroup timeMarkGroup) {
			if(timeMarkGroup.CollapseInvocation != null)
				this.CancelDelayedInvocation(timeMarkGroup.CollapseInvocation);
			timeMarkGroup.CollapseInvocation = this.InvokeDelayed(() => CollapseGroupedTimeMarks(timeMarkGroup), 0.2f);
		}

		/// <summary>
		/// Hides the time marks of the given time mark group
		/// </summary>
		private void CollapseGroupedTimeMarks(TimeMarkGroup timeMarkGroup) {
			foreach (TimeMarkHandler timeMark in timeMarkGroup.Members) {
				timeMark.Collapse();
				timeMark.GetVisualElement().UnregisterCallback<MouseEnterEvent>(ev => ExpandGroupedTimeMarks(timeMarkGroup, timeMark));
				timeMark.GetVisualElement().UnregisterCallback<MouseLeaveEvent>(ev => CollapseGroupedTimeMarksIntent(timeMarkGroup));
			}
			timeMarkGroup.State = TimeMarkGroupState.Collapsed;
			timeMarkGroup.GroupMarker.style.opacity = 1;
			timeMarkGroup.GroupMarker.transform.scale = new Vector3(1f, 1f, 1f);
			timeMarkGroup.CollapseInvocation = null;
		}

		/// <summary>
		/// Clears all time marks
		/// </summary>
		private void RemoveTimeMarks(){
			if(initialized){
				timeMarkGroups.Clear();
				allTimeMarkHandlers.Clear();
				visualTimeMarkContainer.Clear();
			}
		}
		
		/// <summary>
		/// Returns the width in pixels of one millisecond on the time axis
		/// </summary>
		private float CalculatePixelMsFactor(){
			if(sampleContainer.SamplingTime == 0)
				return 0f;

			int totalTime = sampleContainer.GetLastTimeStamp() - sampleContainer.GetFirstTimeStamp();
			int timeAxisSteps = totalTime/sampleContainer.SamplingTime;
			float timeAxisWidth = timeAxis.resolvedStyle.width;
			float unit = timeAxisWidth / (timeAxisSteps-1);
			return unit / sampleContainer.SamplingTime;
		}

		/// <summary>
		/// Triggered by the event handler when the user is scrolling (drags the mouse pointer over the time axis)
		/// </summary>
		private void OnSliding(){
			if (!initialized)
				return;
			int timeStamp = sampleContainer.SampleIdxToTimeStamp(timeAxis.value);
			animationController.JumpToTimeStamp(timeStamp);
			UpdateElapsedTimeColorBar();
		}

		/// <summary>
		/// Fired when the AnimationController updates the current sample. It updates the time label and the axis pointer
		/// </summary>
		public void OnUpdateSamplePointer(int timeStamp){
			UpdateTimeLabel (timeStamp);
			UpdateTimeAxis (timeStamp);
		}

		/// <summary>
		/// Updates the current time stamp and the total playback time label.
		/// </summary>
		private void UpdateTimeLabel (int timeStampInMs){
			float timeStampInS = timeStampInMs * 0.001f;	
			float lastTimeStampInS = sampleContainer.GetLastTimeStamp() * 0.001f;
			currentPlaybackTime.text = string.Format ("{0:00.000} / {1:00.000}", timeStampInS, lastTimeStampInS);
		}

		/// <summary>
		/// Updates the pointer of the time axis.
		/// </summary>
		private void UpdateTimeAxis (int timeStamp){
			int sliderValue = sampleContainer.TimeStampToSampleIdx(timeStamp);
			timeAxis.SetValueWithoutNotify(sliderValue);
			UpdateElapsedTimeColorBar();
		}

		/// <summary>
		/// Creates a rectangle that covers the left side of the time axis slider
		/// </summary>
		void CreateElapsedTimeColorBar(){
			if(timeAxis.Q("elapsed-time-color-bar") != null)
				return;

			VisualElement tracker = timeAxis.Q(className: Slider.trackerUssClassName);
			elapsedTimeColorBar = new VisualElement(){name = "elapsed-time-color-bar"};
			tracker.Add(elapsedTimeColorBar); //Adding it as a child means it will be drawn on top
			elapsedTimeColorBar.style.height = tracker.layout.height;
		}

		void UpdateElapsedTimeColorBar(){
			VisualElement dragger = timeAxis.Q(className: Slider.draggerUssClassName);
			elapsedTimeColorBar.style.width = dragger.transform.position.x;
		}
	}
}