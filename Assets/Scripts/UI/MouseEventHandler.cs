﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using Visualizer.Core;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Utilities;
using Zenject;

namespace Visualizer.UI
{
	public interface IMouseEventHandler{
	}

	public class MouseEventHandler : MonoBehaviour, IMouseEventHandler
	{
		private ISimulationLoader simulationLoader;
		private IFocusCameraHandler focusCameraHandler;
		private IRoadExplorer roadExplorer;
		private IMainUI ui;
		private IPanelResizer panelResizer;
		private IUIModeHandler uiModeHandler;
		private IUnityIO unityIO;

		//internal variables
		private Vector3 mouseDownPos = Vector3.zero;
		private float doubleClickStart = 0;
		private Vector3 lastMousePos = Vector3.zero;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IUnityIO uIO, IUIModeHandler uimHandler, IPanelResizer pResizer, ISimulationLoader sLoader, IFocusCameraHandler acHandler, IRoadExplorer rExplorer, IMainUI mUI){
			simulationLoader = sLoader;
			focusCameraHandler = acHandler;
			roadExplorer = rExplorer;
			panelResizer = pResizer;
			ui = mUI;
			uiModeHandler = uimHandler;
			unityIO = uIO;
		}

		/// <summary>
		/// MonoBehaviour function that is called once per frame
		/// </summary>
		public void Update(){
			HandleMouseEvents ();
		}

		/// <summary>
		/// Handles the mouse events.
		/// Note that the camera controllers have own mouse event handlers
		/// </summary>
		private void HandleMouseEvents(){
			bool mouseOverUI = unityIO.IsMouseOverUI();
				
			//Mouse cursor movement
			HandleMouseMove(mouseOverUI);

			//Left click begin
			if (unityIO.GetMouseButtonDown (0) && !mouseOverUI)
				HandleMouseDown();

			//Left mouse drag 
			if (unityIO.GetMouseButton(0) && !mouseOverUI)
				HandleDrag ();

			//Left click end 
			if (unityIO.GetMouseButtonUp(0) && !mouseOverUI)
				HandleLeftMouseUp ();

			//Left click anywhere even over UI elements 
			if (unityIO.GetMouseButtonUp(0))
				HandleLeftMouseUpGlobal ();

			//Right click begin
			if (unityIO.GetMouseButtonDown (1))
				HandleMouseDown ();

			//Right click end
			if (unityIO.GetMouseButtonUp (1))
				HandleRightMouseUp ();

			//Left double click
			if (unityIO.GetMouseButtonUp (0) && !mouseOverUI){
				if ((Time.time - doubleClickStart) < 0.3f){
					HandleDoubleClick();
					doubleClickStart = -1;
				}
				else{
					doubleClickStart = Time.time;
				}
			}
		}

		/// <summary>
		/// Handling cursor movement (only if position changes)
		/// </summary>
		private void HandleMouseMove (bool mouseOverUI) {
			Vector3 mousePos = unityIO.GetMousePos();
			if (lastMousePos == mousePos)
				return;

			roadExplorer.DisplayMousePos(mousePos, !mouseOverUI);
			lastMousePos = mousePos;
		}

		/// <summary>
		/// If the right click hits an agent, we display the agent popup menu.
		/// </summary>
		private void HandleRightMouseUp (){
			//if the mouse was moved between down and up, we don't display the popup menu
			if (mouseDownPos != unityIO.GetMousePos())
				return;

			Vector3 mousePos = unityIO.GetMousePos();
			Vector3 popupPos = mousePos;
			popupPos.y -= Screen.height + 30f;

			Agent hitAgent = GetAgentAt(mousePos);
			if(uiModeHandler.GetMode() == UIMode.SimulationPlayer && hitAgent != null) {
				if(hitAgent.gameObject.tag == "Agent")
					ui.ShowAgentPopupMenu(hitAgent, popupPos);
				if(hitAgent.gameObject.tag == "PerceivedAgent")
					ui.ShowPerceivedAgentPopupMenu(hitAgent, popupPos);
			}
		}

		/// <summary>
		/// Store the mouse position
		/// </summary>
		private void HandleMouseDown (){
			mouseDownPos = unityIO.GetMousePos();
		}

		/// <summary>
		/// Handle mouse movement while the left button is pressed
		/// </summary>
		private void HandleDrag (){
			panelResizer.HandleMouseDrag(unityIO.GetMousePos(), false);
		}

		/// <summary>
		/// Handles the left mouse button release event
		/// </summary>
		private void HandleLeftMouseUp ()	{
			Vector3 mousePos = unityIO.GetMousePos();

			panelResizer.HandleMouseUp(mousePos);

			if(mouseDownPos == mousePos && uiModeHandler.GetMode() != UIMode.SensorPositioner){
				roadExplorer.HighlightTransform(mousePos);
			}
			if(unityIO.GetKey(KeyCode.LeftControl)){
				Agent hitAgent = GetAgentAt(mousePos);
				if(hitAgent != null && uiModeHandler.GetMode() == UIMode.SimulationPlayer) {
					if( hitAgent.gameObject.tag == "Agent")
						ui.ShowNonFocusAgentPanel(hitAgent);
					if(hitAgent.gameObject.tag == "PerceivedAgent")
						ui.ShowPerceivedAgentPanel(hitAgent);
				}
			}
			if(unityIO.GetKey(KeyCode.LeftShift)){
				Agent hitAgent = GetAgentAt(mousePos);
				if(hitAgent != null && uiModeHandler.GetMode() == UIMode.SimulationPlayer){
					ui.ShowDriverPerception(hitAgent);
				}
			}
		}

		/// <summary>
		/// Handles the left mouse button release event over any UI element
		/// </summary>
		private void HandleLeftMouseUpGlobal ()	{
			ui.HidePopupMenu();
		}

		/// <summary>
		/// A double click on an agent attaches the camera to it. A double click elswhere detaches the camera, if attached to any agent
		/// </summary>
		private void HandleDoubleClick ()	{
			if(uiModeHandler.GetMode() != UIMode.SimulationPlayer)
				return;

			Agent hitAgent = GetAgentAt(unityIO.GetMousePos());
			if(hitAgent != null && hitAgent.tag == "Agent")
				focusCameraHandler.FocusCamera (hitAgent.ID);
			else
				focusCameraHandler.FocusCamera (-1);
		}

		/// <summary>
		/// Return the agent at the given position. The function returns null if no agent is at the position.
		/// </summary>
		private Agent GetAgentAt (Vector2 mousePos){
			GameObject hitObject = unityIO.GetGameObjectAtScreenPoint(mousePos);
			if (hitObject == null)
				return null;
			if (hitObject.tag == "Agent" || hitObject.tag == "PerceivedAgent") {
				Agent agent = hitObject.GetComponent<Agent> ();
				if (agent != null) {
					return agent;
				}
			}
			return null;
		}
	}
}

