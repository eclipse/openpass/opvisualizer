﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Utilities;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Visualizer.UI
{
	public delegate void FocusAgentChangeEventHandler();

	public interface IFocusAgentSelectorWndHandler{
		event FocusAgentChangeEventHandler FocusAgentSelected;
		void Init(VisualElement window);
	}

	/// <summary>
	/// Dialog where the user can choose the focus agent (to which the camera is attached)
	/// </summary>
	public class FocusAgentSelectorWndHandler: IFocusAgentSelectorWndHandler {

		private IAgentManager agentManager;
		private IFocusCameraHandler focusCameraHandler;
		private IAnimationController animationController;
		public event FocusAgentChangeEventHandler FocusAgentSelected;
		private ListView agentListView;
		private Button okButton;
		private Button closeButton;
		private List<GenericListItem> dataSource = new List<GenericListItem>();
		private SampleUpdatedEventHandler sampleUpdatedEventHandler = null;
		DateTime lastClickTime;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAgentManager aManager, IFocusCameraHandler acHandler, IAnimationController aController){
			agentManager = aManager;
			focusCameraHandler = acHandler;
			animationController = aController;
		}


		/// <summary>
		/// Initializes the file browser dialog
		/// </summary>
		public void Init(VisualElement window){
			agentListView = window.Q<ListView>();
			okButton = window.Q<Button>("ok-btn");
			closeButton = window.Q<Button>("close-btn");
			okButton.RegisterCallback<ClickEvent>(ev => AcceptSelection());
			closeButton.RegisterCallback<ClickEvent>(ev => CancelSelection());
			okButton.SetEnabled(false);
			FocusAgentSelected = null;
			if (sampleUpdatedEventHandler == null) {
				sampleUpdatedEventHandler = new SampleUpdatedEventHandler(OnSampleUpdated);
				animationController.SampleUpdated += sampleUpdatedEventHandler;
			}
			CreateListItems();
			InitListView();
		}

		/// <summary>
		/// Fills the file list 
		/// </summary>
		private void InitListView() {
			var visualItemTemplate = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorListItem");
            Func<VisualElement> makeItem = () => visualItemTemplate.Instantiate();

            Action<VisualElement, int> bindItem = (e, i) =>
            {
				e.Q<Label>("agent-lbl").text = dataSource[i].Label;
				e.Q<Label>("agent-sub-lbl").text = dataSource[i].Value;
				bool isFocusAgent = GetCurrentFocusAgentID() == dataSource[i].Key;
				bool isDisabled = !dataSource[i].Enabled;
				VehicleModelType vehicleType = GetVehicleModelType(dataSource[i].Key);
				e.Q<VisualElement>("icon-agent-car").style.display = (vehicleType == VehicleModelType.Car) ? DisplayStyle.Flex : DisplayStyle.None;
				e.Q<VisualElement>("icon-agent-truck").style.display = (vehicleType == VehicleModelType.Truck) ? DisplayStyle.Flex : DisplayStyle.None;
				e.Q<VisualElement>("icon-agent-pedestrian").style.display = (vehicleType == VehicleModelType.Pedestrian) ? DisplayStyle.Flex : DisplayStyle.None;
				e.Q<Label>("agent-lbl").EnableInClassList("selected-list-item", isFocusAgent);
				e.Q<Label>("agent-lbl").EnableInClassList("disabled-list-item", isDisabled);
				e.Q<Label>("agent-sub-lbl").EnableInClassList("disabled-list-item", isDisabled);
				e.Q<Label>("agent-sub-lbl").EnableInClassList("selected-list-item", isFocusAgent);
				e.Q<VisualElement>("icon-agent-car").EnableInClassList("selected-list-icon", isFocusAgent);
				e.Q<VisualElement>("icon-agent-car").EnableInClassList("disabled-list-icon", isDisabled);
				e.Q<VisualElement>("icon-agent-truck").EnableInClassList("selected-list-icon", isFocusAgent);
				e.Q<VisualElement>("icon-agent-truck").EnableInClassList("disabled-list-icon", isDisabled);
				e.Q<VisualElement>("icon-agent-pedestrian").EnableInClassList("selected-list-icon", isFocusAgent);
				e.Q<VisualElement>("icon-agent-pedestrian").EnableInClassList("disabled-list-icon", isDisabled);
            };
            agentListView.makeItem = makeItem;
            agentListView.bindItem = bindItem;
            agentListView.itemsSource = dataSource;
			agentListView.onItemsChosen += OnDoubleClickDummy; //<- NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click in OnSelectionChange
			agentListView.onSelectionChange += OnSelectionChange;
		}

		/// <summary>
		/// Creates the list items and adds their current crash status to their name.
		/// </summary>
		private void CreateListItems(){
			dataSource.Clear();
			dataSource.Add (new GenericListItem("-1", "None", true));
			List<int> agentIDs = agentManager.GetAgentIDs ();
			agentIDs.Sort ();
			foreach (int aID in agentIDs) {
				Agent agent = agentManager.GetAgent (aID);
				string label = "Agent " + agent.ID.ToString ();
				string subLabel = "";
				if (agent.StaticAgentInfo.TypeGroupName != "Common" && agent.StaticAgentInfo.TypeGroupName != "")
					subLabel += "(" + agent.StaticAgentInfo.TypeGroupName + ")";
				if (agent.CrashStatus == Crash.WillCrash ||
					agent.CrashStatus == Crash.HasCrashed)
					subLabel += " (Collision)";
				dataSource.Add (new GenericListItem(aID.ToString(), label, subLabel, !agent.IsHidden()));
			} 
		}

		/// <summary>
		/// Triggered when the AnimationController updates the current sample
		/// </summary>
		private void OnSampleUpdated(int sampleIdx){
			if(dataSource.Count > 0)
				UpdateList();
		}

		/// <summary>
		/// Updates the enabled flag of the list items based on the corresponding agents visibility
		/// </summary>
		private void UpdateList(){
			foreach (GenericListItem listItem in dataSource) {
				int agentID = -1;
				int.TryParse(listItem.Key, out agentID);
				Agent agent = agentManager.GetAgent(agentID, false);
				if(agent != null)
					listItem.Enabled = !agent.IsHidden();
			} 
			SetOKButtonVisibility();
			agentListView.Rebuild();
		}

		/// <summary>
		/// Returns the currently focused agent ID as string
		/// </summary>
		private string GetCurrentFocusAgentID() {
			Agent currentFocusAgent = focusCameraHandler.GetFocusedAgent();
			return currentFocusAgent == null ? "-1" : currentFocusAgent.ID.ToString();
		}

		/// <summary>
		/// Returns the type of the vehicle that is used to choose the icon for the given agent
		/// </summary>
		private VehicleModelType GetVehicleModelType(string agentIDStr) {
			int agentID = -1;
			int.TryParse(agentIDStr, out agentID);
			Agent agent = agentManager.GetAgent(agentID, false);
			VehicleModelType type = VehicleModelType.Unknown; 
			if (agent != null)
				type = agent.StaticAgentInfo.VehicleModelType;
			if(type == VehicleModelType.Unknown)
				type = VehicleModelType.Car;
			return type;
		}

		/// <summary>
		/// Callback invoked when the user double clicks an agent.
		/// </summary>
		private void OnDoubleClick(IEnumerable<object> selection) {
			if(selection.Count() == 0 || selection.First() == null){
				return;
			}
			AcceptSelection();
		}

		/// <summary>
		/// Callback invoked when the user double clicks a list entry.
		/// </summary>
		private void OnDoubleClickDummy(IEnumerable<object> selection) {
			//NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click in OnSelectionChange
			//Still, we must have an empty function registered for the onItemsChosen event, otherwise other strange behaviour shows up
		}

		/// <summary>
		/// Callback invoked when the user selects an agent in the list
		/// </summary>
		private void OnSelectionChange(IEnumerable<object> selection) {	
			if(selection.Count() == 0 || selection.First() == null){
				return;
			}

			SetOKButtonVisibility();

			//NOTE: onItemsChosen is broken currenly (Unity 2021.2beta) -> manually detecting double click
			double timeSinceLastClick = (DateTime.UtcNow - lastClickTime).TotalMilliseconds;
			if(timeSinceLastClick < 300)
				OnDoubleClick(selection);
			lastClickTime = DateTime.UtcNow;	
		}

		/// <summary>
		/// Sets the OK button active or inactive depending on the current selection
		/// </summary>
		private void SetOKButtonVisibility() {	
			if(agentListView.selectedIndex < 0)
				return;

			GenericListItem selectedItem = dataSource[agentListView.selectedIndex];
			bool isSelectionIsCurrentFocusAgent = (GetCurrentFocusAgentID() == selectedItem.Key);
			okButton.SetEnabled(!isSelectionIsCurrentFocusAgent && selectedItem.Enabled);
		}

		/// <summary>
		/// Called when the user either double clicked a list entry or pressed OK of the dialog
		/// </summary>
		private void AcceptSelection() {
			if(agentListView.selectedIndex < 0)
				return;

			GenericListItem selectedItem = dataSource[agentListView.selectedIndex];
			if (selectedItem == null || !selectedItem.Enabled)
				return;		
			
			if(FocusAgentSelected != null)
				FocusAgentSelected();

			int agentID;
			int.TryParse(selectedItem.Key, out agentID);
			
			focusCameraHandler.FocusCamera(agentID);
			dataSource.Clear();
		}

		/// <summary>
		/// Called when the user pressed the close button of the dialog
		/// </summary>
		private void CancelSelection() {
			dataSource.Clear();
		}
	}
}