﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UIElements;
using UnityEngine;
using Visualizer.Core;
using Zenject;

namespace Visualizer.UI
{
	public interface IPlaybackSpeedHandler{
		void Init ();
	}

	/// <summary>
	/// Handles the user's choice of playback speed
	/// </summary>
	public class PlaybackSpeedHandler : IPlaybackSpeedHandler{

		private DropdownField dropdown;
		private IAnimationController animationController;
		private IMainUI ui;

		private readonly Dictionary<string, float> dataSource = new Dictionary<string, float>(){
			{ "0.1x", 0.1f },
			{ "0.25x", 0.25f },
			{ "0.5x", 0.5f },
			{ "1x", 1f },
			{ "2x", 2f },
			{ "5x", 5f }
		};

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(IAnimationController aController, IMainUI mUi){
			animationController = aController;
			ui = mUi;
		}

		/// <summary>
		/// Initialize the handler
		/// </summary>
		public void Init (){
            dropdown = ui.GetVisualElement("playback-speed-dd") as DropdownField;

			if (dropdown != null){
				dropdown.choices = dataSource.Keys.ToList();
				dropdown.RegisterCallback<ChangeEvent<string>>(ev => DropdownValueChanged(ev.newValue));
				dropdown.value = "1x";
			}
		}

		/// <summary>
		/// Callback that is fired after the user changed the playback speed dropdown value
		/// </summary>
		/// <param name="value">Value.</param>
		private void DropdownValueChanged(string value){
			float newPlaybackSpeed = dataSource [value];
			animationController.SetPlaybackSpeed(newPlaybackSpeed);
		}
	}
}
