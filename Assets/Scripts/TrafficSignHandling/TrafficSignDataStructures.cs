﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using UnityEngine;

namespace Visualizer.TrafficSignHandling
{
	public class TrafficSignEvent {
		public string SignalID { get; set; }
		public int TimeStamp { get; set; }
		public string State { get; set; }
	}

	[Serializable]
	public enum LightBulbState {
		Unknown,
		On,
		Off,
		Flashing
	}

	/// <summary>
	/// A TrafficLightState links a given signal state (by name) to a list of light bulb rules.
	/// </summary>
	[Serializable]
	public class TrafficLightState {
		[SerializeField]
		[Tooltip("This name must match a traffic light state value that the OpenPASS Simulator is outputing in the trace file")]
		private string name;

		[SerializeField]
		public TrafficLightBulbRule[] BulbRules; 

		public string Name{
			get { return name; }
			set { name = value; }
		}
	}

	/// <summary>
	/// A light bulb rule links a light bulb state to a specific light bulb instance
	/// </summary>
	[Serializable]
	public class TrafficLightBulbRule {
		[SerializeField]
		[Tooltip("Drag here in Unity's Inspector the bulb GameObject that has a TrafficLightBulbController attached to it")]
		public TrafficLightBulbController BulbController;

		[SerializeField]
		[Tooltip("Choose here in Unity's Inspector the state that this bulb should have in this rule")]
		public LightBulbState BulbState; 
	}
}