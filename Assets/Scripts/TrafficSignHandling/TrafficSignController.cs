﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.TrafficSignHandling
{
	/// <summary>
	/// Abstract base class of all TrafficSignImplementations. 
	/// </summary>
	public abstract class TrafficSignController : MonoBehaviour {
		protected string currentState = "";
		public virtual string SignalID { get; set; }		
		public abstract void UpdateSign(string signState);
		public abstract void StartAnimation(float aSpeed);
		public abstract void StopAnimation();
	}
}