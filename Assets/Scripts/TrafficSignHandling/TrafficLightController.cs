﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.TrafficSignHandling
{
	/// <summary>
	/// </summary>
	public class TrafficLightController : TrafficSignController {

		#region editor options
			[SerializeField]
			[Tooltip("Add here in Unity's Inspector all the states that this traffic light type should know. Drag then to each traffic light state the bulb GameObjects (they must have a TrafficLightBulbController attached) and choose the desired bulb state from the dropdown.")]
			private TrafficLightState[] trafficLightStates;
		#endregion

		/// <summary>
		/// Translates the given signal state into light bulb states based on the rules provided in the trafficLightStates 
		/// of this instance. It updates all linked bulbs of this traffic light.
		/// If the given state is not a known state, it turns all lightbulbs to state unknown. 
		/// </summary>
		public override void UpdateSign(string signState){
			if(trafficLightStates == null || trafficLightStates.Count() == 0)
				return;

			if(currentState == signState)
				return;

			TrafficLightState newState = Array.Find(trafficLightStates, st => st.Name == signState);
			if (newState != null) {
				foreach(TrafficLightBulbRule bulbRule in newState.BulbRules)
					bulbRule.BulbController.ChangeState(bulbRule.BulbState);
			}
			else {
				TrafficLightState state = trafficLightStates.First();
				foreach(TrafficLightBulbRule bulbRule in state.BulbRules)
					bulbRule.BulbController.ChangeState(LightBulbState.Unknown, true);
			}
			currentState = signState;
		}

		/// <summary>
		/// Called when the user starts the playback. Force here a status update to start active animations.
		/// </summary>
		public override void StartAnimation(float aSpeed){
			if(trafficLightStates == null || trafficLightStates.Count() == 0)
				return;

			TrafficLightState state = trafficLightStates.First();
			foreach(TrafficLightBulbRule bulbRule in state.BulbRules)
				bulbRule.BulbController.StartAnimation(aSpeed);
		}

		/// <summary>
		/// Stops any running animations.
		/// </summary>
		public override void StopAnimation(){
			if(trafficLightStates == null || trafficLightStates.Count() == 0)
				return;

			TrafficLightState state = trafficLightStates.First();
			foreach(TrafficLightBulbRule bulbRule in state.BulbRules)
				bulbRule.BulbController.StopAnimation();
		}

		/// <summary>
		/// Sets the states of the traffic light
		/// Note that this function is used only for unit testing. 
		/// For application usage, use the Unity Editor to define the states. 
		/// </summary>
		public void SetTrafficLightStates(TrafficLightState[] states){
			trafficLightStates = states;
		}
	}
}