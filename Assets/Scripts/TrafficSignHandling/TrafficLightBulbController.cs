﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.TrafficSignHandling
{
	/// <summary>
	/// Controls a single traffic light bulb. This script must be attached to the 3D object as a component.
	/// The light bulb can be then linked to any TrafficLightController that translates signal states to bulb states
	/// </summary>
	public class TrafficLightBulbController : MonoBehaviour {

		private Renderer bulb = null;
		private LightBulbState currentState = LightBulbState.Unknown;
		private float animationSpeed = 1f;
		private bool isPlaybackRunning = false;


		/// <summary>
		/// Enables the given objects (light bulbs)
		/// </summary>
		public void ChangeState (LightBulbState bulbState, bool force = false) {
			if (bulb == null) {
				bulb = GetComponent<MeshRenderer>();
				if (bulb == null) {
					Debug.LogError("This controller script must be attached to a GameObject with a renderer");
					return;
				}
			}

			if(currentState == bulbState && !force)
				return;
			
			StopFlash();

			switch (bulbState) {
				case LightBulbState.Off:
				case LightBulbState.Unknown:
					bulb.enabled = false;
				break;
				case LightBulbState.On:
					bulb.enabled = true;
				break;
				case LightBulbState.Flashing:
					if(isPlaybackRunning)
						StartFlash();
					else 
						bulb.enabled = true;
				break;
			}
			currentState = bulbState;
		}

		/// <summary>
		/// Called when the user starts the playback. Force here a status update to start active animations.
		/// </summary>
		public void StartAnimation(float aSpeed){
			isPlaybackRunning = true;
			animationSpeed = aSpeed;
			ChangeState(currentState, true);
		}

		/// <summary>
		/// Stops any running animations.
		/// </summary>
		public void StopAnimation(){
			isPlaybackRunning = false;
			StopFlash();
		}

		/// <summary>
		/// Starts the flashing animation of the bulb
		/// </summary>
		private void StartFlash(){
			StartCoroutine(Flash());
		}

		/// <summary>
		/// Stops the flashing animation of the bulb
		/// </summary>
		private void StopFlash(){
			StopAllCoroutines();
		}

		/// <summary>
		/// The core of the flashing logic
		/// This function should be run as a couroutine
		/// It must be stopped from outside with StopCoroutine
		/// </summary>
		private IEnumerator Flash()
		{
			if (bulb == null)
				yield break;

			bool flasherState = false;
			while (true) {
				bulb.enabled = flasherState;
				yield return new WaitForSeconds((1 / animationSpeed) * 0.5f);
				flasherState =! flasherState;
			}
		}
	}
}