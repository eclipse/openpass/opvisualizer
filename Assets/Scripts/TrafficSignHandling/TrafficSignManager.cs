﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Utilities;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.UI;
using Visualizer.World.Environment;
using System;
using Zenject;

namespace Visualizer.TrafficSignHandling
{
	public interface ITrafficSignManager{
		void UpdateSigns (int timeStamp);
		void StartAnimation (float animationSpeed);
		void StopAnimation ();
		void Clear ();
	}

	/// <summary>
	/// Manages all dynamic traffic signs (like traffic lights) and their updates.
	/// </summary>
	public class TrafficSignManager : ITrafficSignManager{

		private ITrafficSignStateProvider trafficSignStateProvider;
		private IRoadSignalRenderer roadSignalRenderer;

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(ITrafficSignStateProvider tssProvider, IRoadSignalRenderer rsRenderer){
			trafficSignStateProvider = tssProvider;
			roadSignalRenderer = rsRenderer;
		}

		/// <summary>
		/// The core funtion of TrafficSignManager. It takes the state of all traffic signs and passes them to 
		/// the signal objects (scripts attached to the GameObject of the signals). 
		/// It is called every time the AnimationController triggers a sample update.
		/// </summary>
		/// <param name="timeStamp">Time stamp in ms</param>
		public void UpdateSigns (int timeStamp) {
			List<TrafficSignController> trafficSignControllers = roadSignalRenderer.GetSignalControllers();
			if(trafficSignControllers != null) {
				foreach (TrafficSignController ts in trafficSignControllers){
					string state = trafficSignStateProvider.GetState(ts.SignalID, timeStamp);
					ts.UpdateSign(state);
				}
			}
		}

		/// <summary>
		/// Passes the animation start event to the traffic signs so that they can start their local animation (e.g. flashing).
		/// </summary>
		/// <param name="animationSpeed">Animation speed.</param>
		public void StartAnimation (float animationSpeed) {
			List<TrafficSignController> trafficSignControllers = roadSignalRenderer.GetSignalControllers();
			if(trafficSignControllers != null) {
				foreach (TrafficSignController ts in trafficSignControllers)
					ts.StartAnimation(animationSpeed);
			}
		}

		/// <summary>
		/// Passes the animation stop event to the traffic signs so that the agents can stop their local animation (e.g. flashing).
		/// </summary>
		public void StopAnimation () {
			List<TrafficSignController> trafficSignControllers = roadSignalRenderer.GetSignalControllers();
			if(trafficSignControllers != null) {
				foreach (TrafficSignController ts in trafficSignControllers)
					ts.StopAnimation();
			}
		}

		/// <summary>
		/// Clears the internal lists of the TrafficSignManager
		/// </summary>
		public void Clear(){
			trafficSignStateProvider.Clear();
			roadSignalRenderer.Clear();
		}
	}
}