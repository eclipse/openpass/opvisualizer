﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Visualizer.Core;
using Zenject;

namespace Visualizer.TrafficSignHandling
{
	public interface ITrafficSignStateProvider{
		void Clear ();
		void AddEvent(SimulationEvent simEvent);
		string GetState(string signalId, int timeStamp);	
	}

	/// <summary>
	///
	/// </summary>
	public class TrafficSignStateProvider: ITrafficSignStateProvider {

		private Dictionary<string, List<TrafficSignEvent>> events = new Dictionary<string, List<TrafficSignEvent>>();

		/// <summary>
		/// Inject all dependencies of this class. Called on startup by the ApplicationInstaller.
		/// </summary>
		[Inject]
		public void Construct(){

		}

		/// <summary>
		/// Clears the TrafficSignStateProvider.
		/// </summary>
		public void Clear(){
			events.Clear();
		}

		/// <summary>
		/// Stores a traffic sign event
		/// </summary>
		public void AddEvent(SimulationEvent simEvent) {
			if(simEvent == null) 
				return;

			TrafficSignEvent trafficSignEvent = SimEventToTrafficSignEvent(simEvent);
			if (trafficSignEvent == null)
				return;

			if (!events.ContainsKey(trafficSignEvent.SignalID))
				events.Add(trafficSignEvent.SignalID, new List<TrafficSignEvent>());

			List<TrafficSignEvent> eventList = events[trafficSignEvent.SignalID];
			eventList.Add(trafficSignEvent);
			eventList = eventList.OrderBy(e => e.TimeStamp).ToList();
		}

		/// <summary>
		/// Returns the state of the given dynamic signal at the given point in time
		/// Returns "-" if no signal state is found at the given time
		/// </summary>
		public string GetState(string signalId, int timeStamp) {
			if (!events.ContainsKey(signalId))
				return "-";
				
			//Reverse search for the first event that is the one before the given sample idx
			List<TrafficSignEvent> eventList = events[signalId];
			for(int idx = eventList.Count-1; idx >= 0; idx--) {
				if(eventList[idx].TimeStamp <= timeStamp)
					return eventList[idx].State;
			}

			//State of the signal is unknown
			return "-";
		}

		/// <summary>
		/// Converts a SimulationEvent to a TrafficSignEvent
		/// </summary>
		private TrafficSignEvent SimEventToTrafficSignEvent(SimulationEvent simEvent){
			TrafficSignEvent trafficSignEvent = new TrafficSignEvent();
			trafficSignEvent.TimeStamp = simEvent.timeStamp;
			if(simEvent.eventParameters == null){
				Debug.LogWarning("TrafficSign event parameters do not contain the mandatory key 'opendrive_id' and 'traffic_light_state'. Event will be ignored.");
				return null;
			}
			if(!simEvent.eventParameters.ContainsKey("opendrive_id")){
				Debug.LogWarning("TrafficSign event parameters do not contain the mandatory key 'opendrive_id'. Event will be ignored.");
				return null;
			}
			if(!simEvent.eventParameters.ContainsKey("traffic_light_state")){
				Debug.LogWarning("TrafficSign event parameters do not contain the mandatory key 'traffic_light_state'. Event will be ignored.");
				return null;
			}
			trafficSignEvent.SignalID = simEvent.eventParameters["opendrive_id"];
			trafficSignEvent.State = simEvent.eventParameters["traffic_light_state"];
			return trafficSignEvent;
		}
	}
}