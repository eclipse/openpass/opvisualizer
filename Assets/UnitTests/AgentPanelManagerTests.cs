﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using Zenject;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class AgentPanelManagerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<AgentPanelManager>().FromNewComponentOnNewGameObject ().AsSingle();

			//Bind mocks
			IDriverPerceptionManager driverPerceptionManagerMock = Substitute.For<IDriverPerceptionManager> ();
			Container.Bind<IDriverPerceptionManager> ().FromInstance (driverPerceptionManagerMock).AsSingle();

			IDisplayOptionsProvider displayOptionsProviderMock = Substitute.For<IDisplayOptionsProvider> ();
			Container.Bind<IDisplayOptionsProvider> ().FromInstance (displayOptionsProviderMock).AsSingle();

			IAgentManager agentManagerMock = Substitute.For<IAgentManager> ();
			Container.Bind<IAgentManager> ().FromInstance (agentManagerMock).AsSingle();

			IPanelResizer panelResizerMock = Substitute.For<IPanelResizer> ();
			Container.Bind<IPanelResizer> ().FromInstance (panelResizerMock).AsSingle();

			ICameraManager cameraManagerMock = Substitute.For<ICameraManager> ();
			Container.Bind<ICameraManager> ().FromInstance (cameraManagerMock).AsSingle();

			IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
			Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();

			IFocusFrameRenderer focusFrameRendererMock = Substitute.For<IFocusFrameRenderer> ();
			Container.Bind<IFocusFrameRenderer> ().FromInstance (focusFrameRendererMock).AsSingle();

			IViewModeHandler viewModeHandlerMock = Substitute.For<IViewModeHandler> ();
			Container.Bind<IViewModeHandler> ().FromInstance (viewModeHandlerMock).AsSingle();

			IAppStorage appStorageMock = Substitute.For<IAppStorage> ();
			Container.Bind<IAppStorage> ().FromInstance (appStorageMock).AsSingle();
			
			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
				{
					ui.gameObject.name = "MainUI";
					ui.gameObject.AddComponent<EventSystem>();
					ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
					ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
				}).NonLazy();
		}

		[Test]
		public void TestInit()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			IPanelResizer panelResizerMock = Container.Resolve<IPanelResizer> ();

			agentPanelManager.Init(root);

			panelResizerMock.Received (1).Init (root);
			Assert.That(root.Q<VisualElement>("info-panel-container").style.width == 0);
			Assert.That(root.Q<VisualElement>("focus-agent-info-panel-container").style.height == 0);
		}

		[Test]
		public void TestFloatingPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();

			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3).Returns(dummyAgent3);

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(displayOptions);

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);

			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 0);
			agentPanelManager.CreateFloatingPanels();
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 3);
		}

		[Test]
		public void TestFloatingPanelClearing()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();

			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3).Returns(dummyAgent3);

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(displayOptions);

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			agentPanelManager.CreateFloatingPanels();
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 3);
			agentPanelManager.ClearFloatingPanels();
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 0);
		}

		[Test]
		public void TestPerceivedAgentFloatingPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();

			Dictionary <string, Agent> perceivedAgents = new Dictionary <string, Agent>();
			perceivedAgents.Add("0 -> 1", dummyAgent1);
			perceivedAgents.Add("0 -> 2", dummyAgent2);
			perceivedAgents.Add("0 -> 3", dummyAgent3);
			driverPerceptionManagerMock.GetAllPerceivedAgents().Returns(perceivedAgents);

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(displayOptions);

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 0);
			agentPanelManager.CreatePerceivedAgentFloatingPanels();
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 3);
		}

		[Test]
		public void TestPerceivedAgentFloatingPanelClearing()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();

			Dictionary <string, Agent> perceivedAgents = new Dictionary <string, Agent>();
			perceivedAgents.Add("0 -> 1", dummyAgent1);
			perceivedAgents.Add("0 -> 2", dummyAgent2);
			perceivedAgents.Add("0 -> 3", dummyAgent3);
			driverPerceptionManagerMock.GetAllPerceivedAgents().Returns(perceivedAgents);

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(displayOptions);

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			agentPanelManager.CreatePerceivedAgentFloatingPanels();
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 3);
			agentPanelManager.ClearFloatingPanels();
			Assert.That(root.Q<VisualElement>("floating-info-panel-layer").childCount == 0);
		}

		[Test]
		public void TestSingleNonFocusAgentPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent1.StaticAgentInfo = new AgentProperties();
			dummyAgent1.StaticAgentInfo.VehicleModelType = VehicleModelType.Car;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);	//the resizer is a fix child
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);
		}

		[Test]
		public void TestMultipleNonFocusAgentPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;
			dummyAgent2.StaticAgentInfo = agentProps;
			dummyAgent3.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent2, displayOptions);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent3, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 4);
		}

		[Test]
		public void TestOnlyCreateNonFocusAgentPanelOnce()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;

			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);
		}

		[Test]
		public void TestNonFocusAgentPanelCloseClick()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;

			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);

			Button closeButton = root.Q<VisualElement>("info-panel-container").Q<Button>("close-btn");
			closeButton.SendEvent(new ClickEvent{target = closeButton});
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
		}

		[Test]
		public void TestSinglePerceivedAgentPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent1.StaticAgentInfo = new AgentProperties();
			dummyAgent1.StaticAgentInfo.VehicleModelType = VehicleModelType.Car;
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);
		}

		[Test]
		public void TestMultiplePerceivedAgentPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;
			dummyAgent2.StaticAgentInfo = agentProps;
			dummyAgent3.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent2, displayOptions);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent3, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 4);
		}

		[Test]
		public void TestOnlyCreatePerceivedAgentPanelOnce()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;

			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);
		}

		[Test]
		public void TestPerceivedAgentPanelCloseClick()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;

			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 2);

			Button closeButton = root.Q<VisualElement>("info-panel-container").Q<Button>("close-btn");
			closeButton.SendEvent(new ClickEvent{target = closeButton});
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
		}

		[Test]
		public void TestMultipleNonFocusAndPerceivedAgentPanelCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 3);
		}

		[Test]
		public void TestClearAllPanels()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
			agentPanelManager.ShowPerceivedAgentPanel(dummyAgent1, displayOptions);
			agentPanelManager.ShowNonFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 3);
			agentPanelManager.Clear();
			Assert.That(root.Q<VisualElement>("info-panel-container").childCount == 1);
		}

		[Test]
		public void TestShowFocusAgentPanel()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 123;
			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("focus-agent-info-panel-container").childCount == 2); //2 resizers are fix
			agentPanelManager.ShowFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("focus-agent-info-panel-container").childCount == 3);
		}

		[Test]
		public void TestOnlyCreateFocusAgentPanelOnce()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 123;
			AgentProperties agentProps = new AgentProperties();
			agentProps.VehicleModelType = VehicleModelType.Car;
			dummyAgent1.StaticAgentInfo = agentProps;

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			AgentPanelManager agentPanelManager = Container.Resolve<AgentPanelManager> ();
			agentPanelManager.Init(root);
			Assert.That(root.Q<VisualElement>("focus-agent-info-panel-container").childCount == 2);
			agentPanelManager.ShowFocusAgentPanel(dummyAgent1, displayOptions);
			agentPanelManager.ShowFocusAgentPanel(dummyAgent1, displayOptions);
			agentPanelManager.ShowFocusAgentPanel(dummyAgent1, displayOptions);
			Assert.That(root.Q<VisualElement>("focus-agent-info-panel-container").childCount == 3);
		}
	}
}