﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using Visualizer.Core;
using Visualizer.Environment;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class TimeAxisHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<TimeAxisHandler>().FromNewComponentOnNewGameObject ().AsSingle();

			//Bind mock classes
			IAnimationController animationControllerMock = Substitute.For<IAnimationController> ();
			Container.Bind<IAnimationController> ().FromInstance (animationControllerMock).AsSingle();

			ISampleContainer sampleContainerMock = Substitute.For<ISampleContainer> ();
			Container.Bind<ISampleContainer> ().FromInstance (sampleContainerMock).AsSingle();

			IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
			Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		private List<SimulationEvent> CreateSimEvents() {
			List<SimulationEvent> simEvents = new List<SimulationEvent>();
			SimulationEvent simEvent1 = new SimulationEvent();
			simEvent1.timeStamp = 1000;
			simEvent1.eventType = "WhateverEvent";
			simEvent1.triggeringEntities = new List<long>{123, 456};
			simEvent1.affectedEntities = new List<long>();
			simEvent1.eventParameters = new Dictionary<string, string>();
			simEvents.Add(simEvent1);
			SimulationEvent simEvent2 = new SimulationEvent();
			simEvent2.timeStamp = 1500;
			simEvent2.eventType = "Collision";
			simEvent2.triggeringEntities = new List<long>();
			simEvent2.affectedEntities = new List<long>();
			simEvent2.eventParameters = new Dictionary<string, string>();
			simEvents.Add(simEvent2);
			return simEvents;
		}

		[Test]
		public void TestTimeAxisSliderInit()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = new List<SimulationEvent>();
			sampleContainerMock.GetEvents().Returns(simEvents);
			
			timeAxisHandler.Init (root);
			Assert.That(root.Q<SliderInt>("time-axis-slider").value == 0);
		}

		[Test]
		public void TestPlaybackTimeLabelInit()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = new List<SimulationEvent>();
			sampleContainerMock.GetEvents().Returns(simEvents);

			timeAxisHandler.Init (root);
			Assert.AreEqual(root.Q<Label>("playback-time-lbl").text, "00,000 / 00,000");
		}

		[Test]
		public void TestPlaybackTimeLabelAfterLoad()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = new List<SimulationEvent>();
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(10000);

			timeAxisHandler.Init (root);

			Assert.AreEqual(root.Q<Label>("playback-time-lbl").text, "00,000 / 10,000");
		}

		[Test]
		public void TestTimeMarkCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			timeAxisHandler.Init (root);

			List<Button> timeMarks = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(timeMarks);
			Assert.That(timeMarks.Count == 2);
			Assert.That(timeMarks[0].Q("image").ClassListContains("red-time-mark") == false);
			Assert.That(timeMarks[0].Q("image").ClassListContains("yellow-time-mark") == true);
			Assert.That(timeMarks[1].Q("image").ClassListContains("red-time-mark") == true);
			Assert.That(timeMarks[1].Q("image").ClassListContains("yellow-time-mark") == false);
		}

		[Test]
		public void TestClear()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);
			
			timeAxisHandler.Init (root);

			List<Button> timeMarks = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(timeMarks);
			Assert.That(timeMarks.Count == 2);

			timeAxisHandler.Clear();

			timeMarks.Clear();
			root.Q("time-mark-container").Query<Button>().ToList(timeMarks);
			Assert.That(timeMarks.Count == 0);
		}

		[Test]
		public void TestUpdateSamplePointer()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);
			sampleContainerMock.TimeStampToSampleIdx(500).Returns(4);
			
			timeAxisHandler.Init (root);

			animationControllerMock.SampleUpdated += Raise.Event<SampleUpdatedEventHandler>(500);

			Assert.AreEqual("00,500 / 01,000", root.Q<Label>("playback-time-lbl").text);
			Assert.AreEqual(root.Q<SliderInt>("time-axis-slider").value, 4);
		}

		[Test]
		public void TestTimeMarkClickWithTriggeringEntities()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			timeAxisHandler.Init (root);

			List<Button> timeMarks = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(timeMarks);
			Assert.That(timeMarks.Count == 2);

			Button button = root.Q<Button>("sensor-positioner-menu-item");
			timeMarks[0].SendEvent(new ClickEvent{target = timeMarks[0]});
			
			animationControllerMock.Received(1).JumpToTimeStamp (1000);
			focusCameraHandlerMock.Received(1).FocusCamera (123);
		}

		[Test]
		public void TestTimeMarkClickWithoutTriggeringEntities()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			timeAxisHandler.Init (root);

			List<Button> timeMarks = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(timeMarks);
			Assert.That(timeMarks.Count == 2);

			Button button = root.Q<Button>("sensor-positioner-menu-item");
			timeMarks[1].SendEvent(new ClickEvent{target = timeMarks[1]});
			
			animationControllerMock.Received(1).JumpToTimeStamp (1500);
			focusCameraHandlerMock.ReceivedWithAnyArgs(0).FocusCamera (default);
		}

		[UnityTest]
		public IEnumerator TestTimeMarkGroupCreation_TwoEventsAtSameTimestamp()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			simEvents[0].timeStamp = 500;
			simEvents[1].timeStamp = 500;
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			// wait one frame to let the root get its real size
			yield return null;

			timeAxisHandler.Init (root);

			// wait one frame to let the groups be created (GeometryChanged is executed)
			yield return null;

			// wait onother frame to let the groups be created (GeometryChanged is executed)
			yield return null;

			List<Button> markers = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(markers);
			Assert.That(markers.Count == 3);
			Assert.That(markers[0].ClassListContains("time-mark-collapsed") == true);  //time marker
			Assert.That(markers[1].ClassListContains("time-mark-collapsed") == true); //time marker
			Assert.That(markers[2].style.opacity == 1); // group marker
		}

		[UnityTest]
		public IEnumerator TestTimeMarkGroupCreation_TwoEventsClose()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			simEvents[0].timeStamp = 1000;
			simEvents[1].timeStamp = 1300;
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(30000);

			// wait one frame to let the root get its real size
			yield return null;

			timeAxisHandler.Init (root);

			// wait one frame to let the groups be created (GeometryChanged is executed)
			yield return null;
	
			List<Button> markers = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(markers);
			Assert.AreEqual(markers.Count, 3);
			Assert.That(markers[0].ClassListContains("time-mark-collapsed") == true);  //time marker
			Assert.That(markers[1].ClassListContains("time-mark-collapsed") == true); //time marker
			Assert.That(markers[2].style.opacity == 1); // group marker
		}

		[UnityTest]
		public IEnumerator TestTimeMarkGroupCreation_TwoEventsFar()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			simEvents[0].timeStamp = 1000;
			simEvents[1].timeStamp = 2500;
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			// wait one frame to let the root get its real size
			yield return null;

			timeAxisHandler.Init (root);

			// wait one frame to let the groups be created (GeometryChanged is executed)
			yield return null;

			// wait onother frame to let the groups be created (GeometryChanged is executed)
			yield return null;

			List<Button> markers = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(markers);
			Assert.That(markers.Count == 2); //only 2 time marks, no group
		}

		[UnityTest]
		public IEnumerator TestTimeMarkGroupCreation_YellowGroupMarker()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			simEvents[0].timeStamp = 1000;
			simEvents[1].timeStamp = 1000;
			simEvents[0].eventType = "NotACollision";
			simEvents[1].eventType = "NotACollision";
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			// wait one frame to let the root get its real size
			yield return null;

			timeAxisHandler.Init (root);

			// wait one frame to let the groups be created (GeometryChanged is executed)
			yield return null;
	
			List<Button> markers = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(markers);
			Assert.That(markers.Count == 3);
			Assert.That(markers[0].Q("image").ClassListContains("yellow-time-mark") == true);  //time marker
			Assert.That(markers[1].Q("image").ClassListContains("yellow-time-mark") == true);  //time marker
			Assert.That(markers[2].ClassListContains("yellow-time-mark-group") == true);  //group marker
		}

		[UnityTest]
		public IEnumerator TestTimeMarkGroupCreation_RedGroupMarker()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			simEvents[0].timeStamp = 1000;
			simEvents[1].timeStamp = 1000;
			simEvents[0].eventType = "Collision";
			simEvents[1].eventType = "NotACollision";
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			// wait one frame to let the root get its real size
			yield return null;

			timeAxisHandler.Init (root);

			// wait one frame to let the groups be created (GeometryChanged is executed)
			yield return null;
	
			List<Button> markers = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(markers);
			Assert.That(markers.Count == 3);
			Assert.That(markers[0].Q("image").ClassListContains("red-time-mark") == true);  //time marker
			Assert.That(markers[1].Q("image").ClassListContains("yellow-time-mark") == true);  //time marker
			Assert.That(markers[2].ClassListContains("red-time-mark-group") == true);  //group marker
		}

		[UnityTest]
		public IEnumerator TestExpandGroupOnMouseEnter()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			TimeAxisHandler timeAxisHandler = Container.Resolve<TimeAxisHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<SimulationEvent> simEvents = CreateSimEvents();
			simEvents[0].timeStamp = 1000;
			simEvents[1].timeStamp = 1000;
			sampleContainerMock.GetEvents().Returns(simEvents);
			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(1000);

			// wait one frame to let the root get its real size
			yield return null;

			timeAxisHandler.Init (root);

			// wait one frame to let the groups be created (GeometryChanged is executed)
			yield return null;
	
			List<Button> markers = new List<Button>();
			root.Q("time-mark-container").Query<Button>().ToList(markers);

			Assert.That(markers[0].ClassListContains("time-mark-collapsed") == true);
			Assert.That(markers[1].ClassListContains("time-mark-collapsed") == true);
			Assert.That(markers[2].style.opacity == 1);

			markers[2].SendEvent(new MouseEnterEvent{target = markers[2]});

			Assert.That(markers[0].ClassListContains("time-mark-expanded") == true);
			Assert.That(markers[1].ClassListContains("time-mark-expanded") == true);
			Assert.That(markers[2].style.opacity == 0);
		}
	}
}