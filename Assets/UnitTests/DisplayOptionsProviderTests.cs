﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using Zenject;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class DisplayOptionsProviderTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<DisplayOptionsProvider>().AsSingle();

			//Bind mocks
			IDriverPerceptionManager driverPerceptionManagerMock = Substitute.For<IDriverPerceptionManager> ();
			Container.Bind<IDriverPerceptionManager> ().FromInstance (driverPerceptionManagerMock).AsSingle();

			IAgentManager agentManagerMock = Substitute.For<IAgentManager> ();
			Container.Bind<IAgentManager> ().FromInstance (agentManagerMock).AsSingle();

			ISampleContainer sampleContainerMock = Substitute.For<ISampleContainer> ();
			Container.Bind<ISampleContainer> ().FromInstance (sampleContainerMock).AsSingle();

			IAppStorage appStorageMock = Substitute.For<IAppStorage> ();
			Container.Bind<IAppStorage> ().FromInstance (appStorageMock).AsSingle();

			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();
		}

		//############ FocusAgent ##############
		[Test]
		public void TestBuildingFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			propsInTrace.Add("ParamKey1");
			propsInTrace.Add("ParamKey2");
			propsInTrace.Add("ParamKey3");
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			selectedKeys.Add("ParamKey1");
			selectedKeys.Add("ParamKey2");
			selectedKeys.Add("ParamKey3");
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);
 
			Assert.That(displayOptions.Count == 8);
			Assert.That(displayOptions[0].Key == "Floating.ID");
			Assert.That(displayOptions[1].Key == "Floating.Tacho");
			Assert.That(displayOptions[2].Key == "Visualized.AOIArrow");
			Assert.That(displayOptions[3].Key == "Visualized.VisibilityDistance");
			Assert.That(displayOptions[4].Key == "Visualized.SensorRange");
			Assert.That(displayOptions[5].Key == "ParamKey1");
			Assert.That(displayOptions[6].Key == "ParamKey2");
			Assert.That(displayOptions[7].Key == "ParamKey3");	
		}

		[Test]
		public void TestEnablingSelectedFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			propsInTrace.Add("ParamKey1");
			propsInTrace.Add("ParamKey2");
			propsInTrace.Add("ParamKey3");
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			selectedKeys.Add("ParamKey1");
			//note that ParamKey2 is not in the list of selected keys
			selectedKeys.Add("ParamKey3");
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);
 
			Assert.That(displayOptions.Count == 8);
			Assert.That(displayOptions[5].Key == "ParamKey1");
			Assert.That(displayOptions[5].Enabled == true);
			Assert.That(displayOptions[6].Key == "ParamKey2");
			Assert.That(displayOptions[6].Enabled == false);	
			Assert.That(displayOptions[7].Key == "ParamKey3");
			Assert.That(displayOptions[7].Enabled == true);
		}

		[Test]
		public void TestLazyLoadingFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			displayOptionsProvider.Get(AgentPanelType.FocusAgent);
 			displayOptionsProvider.Get(AgentPanelType.FocusAgent);
			displayOptionsProvider.Get(AgentPanelType.FocusAgent);

			// we expect Get() to only read the appstorage the first time 
			appStorageMock.Received(1).ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions);
		}

		[Test]
		public void TestClearFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			displayOptionsProvider.Get(AgentPanelType.FocusAgent);
			displayOptionsProvider.Clear();
 			displayOptionsProvider.Get(AgentPanelType.FocusAgent);

			// we expect Get() to read from the appstorage after a clear 
			appStorageMock.Received(2).ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions);
		}

		[Test]
		public void TestTryUpdateFocusAgentDisplayOptionsBeforeCreation()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Whatever.ParamKey1", "ParamLabel1", true));
			displayOptions.Add(new DisplayOption("Whatever.ParamKey2", "ParamLabel2", true));

			displayOptionsProvider.Update(AgentPanelType.FocusAgent, displayOptions);

			appStorageMock.ReceivedWithAnyArgs(0).WriteDisplayOptions (default, default);
			agentManagerMock.ReceivedWithAnyArgs(0).UpdateDisplayOptionVisibility(default, default);
		}

		[Test]
		public void TestUpdateFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent>();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);

			displayOptionsProvider.Update(AgentPanelType.FocusAgent, displayOptions);

			appStorageMock.Received(1).WriteDisplayOptions (AppStorageKey.SelectedFocusAgentDisplayOptions, displayOptions);
			agentManagerMock.ReceivedWithAnyArgs(1).UpdateDisplayOptionVisibility(default, default);
		}

		[Test]
		public void TestAddedVisibilityDistanceToFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.DriverViewDistance = 123;

			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.FocusAgent);
 
			Assert.That(displayOptions.Count == 5);
			Assert.That(displayOptions[3].Key == "Visualized.VisibilityDistance");
			Assert.That(displayOptions[3].Label.Contains("(123m)"));
		}

		//############ NonFocusAgent ##############
		[Test]
		public void TestBuildingNonFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			propsInTrace.Add("ParamKey1");
			propsInTrace.Add("ParamKey2");
			propsInTrace.Add("ParamKey3");
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			selectedKeys.Add("ParamKey1");
			selectedKeys.Add("ParamKey2");
			selectedKeys.Add("ParamKey3");
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
 
			Assert.That(displayOptions.Count == 7);
			Assert.That(displayOptions[0].Key == "Floating.ID");
			Assert.That(displayOptions[1].Key == "Floating.Tacho");
			Assert.That(displayOptions[2].Key == "Visualized.AOIArrow");
			Assert.That(displayOptions[3].Key == "Visualized.SensorRange");
			Assert.That(displayOptions[4].Key == "ParamKey1");
			Assert.That(displayOptions[5].Key == "ParamKey2");
			Assert.That(displayOptions[6].Key == "ParamKey3");	
		}

		[Test]
		public void TestEnablingSelectedNonFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			propsInTrace.Add("ParamKey1");
			propsInTrace.Add("ParamKey2");
			propsInTrace.Add("ParamKey3");
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			selectedKeys.Add("ParamKey1");
			//note that ParamKey2 is not in the list of selected keys
			selectedKeys.Add("ParamKey3");
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
 
			Assert.That(displayOptions.Count == 7);
			Assert.That(displayOptions[4].Key == "ParamKey1");
			Assert.That(displayOptions[4].Enabled == true);
			Assert.That(displayOptions[5].Key == "ParamKey2");
			Assert.That(displayOptions[5].Enabled == false);	
			Assert.That(displayOptions[6].Key == "ParamKey3");
			Assert.That(displayOptions[6].Enabled == true);
		}

		[Test]
		public void TestLazyLoadingNonFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
 			displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
			displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);

			// we expect Get() to only read the appstorage the first time 
			appStorageMock.Received(1).ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions);
		}

		[Test]
		public void TestClearNonFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);
			displayOptionsProvider.Clear();
 			displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);

			// we expect Get() to read from the appstorage after a clear 
			appStorageMock.Received(2).ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions);
		}

		[Test]
		public void TestTryUpdateNonFocusAgentDisplayOptionsBeforeCreation()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Whatever.ParamKey1", "ParamLabel1", true));
			displayOptions.Add(new DisplayOption("Whatever.ParamKey2", "ParamLabel2", true));

			displayOptionsProvider.Update(AgentPanelType.NonFocusAgent, displayOptions);

			appStorageMock.ReceivedWithAnyArgs(0).WriteDisplayOptions (default, default);
			agentManagerMock.ReceivedWithAnyArgs(0).UpdateDisplayOptionVisibility(default, default);
		}

		[Test]
		public void TestUpdateNonFocusAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent>();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.NonFocusAgent);

			displayOptionsProvider.Update(AgentPanelType.NonFocusAgent, displayOptions);

			appStorageMock.Received(1).WriteDisplayOptions (AppStorageKey.SelectedNonFocusAgentDisplayOptions, displayOptions);
			agentManagerMock.ReceivedWithAnyArgs(1).UpdateDisplayOptionVisibility(default, default);
		}

		//############ PerceivedAgent ##############
		[Test]
		public void TestBuildingPerceivedAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();

			List<string> propsInDriverPerception = new List<string>();
			propsInDriverPerception.Add("ParamKey1");
			propsInDriverPerception.Add("ParamKey2");
			propsInDriverPerception.Add("ParamKey3");
			driverPerceptionManagerMock.GetAvailableAgentAttributes().Returns(propsInDriverPerception);

            List<string> selectedKeys = new List<string>();
			selectedKeys.Add("ParamKey1");
			selectedKeys.Add("ParamKey2");
			selectedKeys.Add("ParamKey3");
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
 
			Assert.That(displayOptions.Count == 4);
			Assert.That(displayOptions[0].Key == "Floating.ID");
			Assert.That(displayOptions[1].Key == "ParamKey1");
			Assert.That(displayOptions[2].Key == "ParamKey2");
			Assert.That(displayOptions[3].Key == "ParamKey3");	
		}

		[Test]
		public void TestEnablingSelectedPerceivedAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();

			List<string> propsInDriverPerception = new List<string>();
			propsInDriverPerception.Add("ParamKey1");
			propsInDriverPerception.Add("ParamKey2");
			propsInDriverPerception.Add("ParamKey3");
			driverPerceptionManagerMock.GetAvailableAgentAttributes().Returns(propsInDriverPerception);

            List<string> selectedKeys = new List<string>();
			selectedKeys.Add("ParamKey1");
			//note that ParamKey2 is not in the list of selected keys
			selectedKeys.Add("ParamKey3");
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
 
			Assert.That(displayOptions.Count == 4);
			Assert.That(displayOptions[1].Key == "ParamKey1");
			Assert.That(displayOptions[1].Enabled == true);
			Assert.That(displayOptions[2].Key == "ParamKey2");
			Assert.That(displayOptions[2].Enabled == false);	
			Assert.That(displayOptions[3].Key == "ParamKey3");
			Assert.That(displayOptions[3].Enabled == true);
		}

		[Test]
		public void TestLazyLoadingPerceivedAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();

			List<string> propsInDriverPerception = new List<string>();
			driverPerceptionManagerMock.GetAvailableAgentAttributes().Returns(propsInDriverPerception);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
 			displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
			displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);

			// we expect Get() to only read the appstorage the first time 
			appStorageMock.Received(1).ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions);
		}

		[Test]
		public void TestClearPerceivedAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();

			List<string> propsInDriverPerception = new List<string>();
			driverPerceptionManagerMock.GetAvailableAgentAttributes().Returns(propsInDriverPerception);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent> ();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);
			displayOptionsProvider.Clear();
 			displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);

			// we expect Get() to read from the appstorage after a clear 
			appStorageMock.Received(2).ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions);
		}

		[Test]
		public void TestTryUpdatePerceivedAgentDisplayOptionsBeforeCreation()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();

			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Whatever.ParamKey1", "ParamLabel1", true));
			displayOptions.Add(new DisplayOption("Whatever.ParamKey2", "ParamLabel2", true));

			displayOptionsProvider.Update(AgentPanelType.PerceivedAgent, displayOptions);

			appStorageMock.ReceivedWithAnyArgs(0).WriteDisplayOptions (default, default);
			agentManagerMock.ReceivedWithAnyArgs(0).UpdateDisplayOptionVisibility(default, default);
		}

		[Test]
		public void TestUpdatePerceivedAgentDisplayOptions()
		{
			DisplayOptionsProvider displayOptionsProvider = Container.Resolve<DisplayOptionsProvider> ();
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			List<string> propsInTrace = new List<string>();
			sampleContainerMock.AgentAttributes.Returns(propsInTrace);

			List<string> propsInDriverPerception = new List<string>();
			driverPerceptionManagerMock.GetAvailableAgentAttributes().Returns(propsInDriverPerception);

            List<string> selectedKeys = new List<string>();
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedFocusAgentDisplayOptions).Returns(selectedKeys);
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedNonFocusAgentDisplayOptions).Returns(selectedKeys);
			appStorageMock.ReadDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions).Returns(selectedKeys);

			Agent dummyAgent1 = Container.Resolve<Agent>();

			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);

			List<DisplayOption> displayOptions = displayOptionsProvider.Get(AgentPanelType.PerceivedAgent);

			displayOptionsProvider.Update(AgentPanelType.PerceivedAgent, displayOptions);

			appStorageMock.Received(1).WriteDisplayOptions(AppStorageKey.SelectedPerceivedAgentDisplayOptions, displayOptions);
			agentManagerMock.ReceivedWithAnyArgs(1).UpdateDisplayOptionVisibility(default, default);
		}
	}
}