﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using Zenject;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.Environment;
using Visualizer.AgentHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class AgentPanelHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

			IAppStorage appStorageMock = Substitute.For<IAppStorage> ();
			Container.Bind<IAppStorage> ().FromInstance (appStorageMock).AsSingle();
		}

		[Test]
		public void TestPanelTitle()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();

			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			Assert.That(visualInfoPanel.Q<Label>("agent-id-lbl").text == "Title");
		}

		[Test]
		public void TestTitlebarFocusAgentIconVisibility()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();

			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			Assert.That(visualInfoPanel.Q<VisualElement>("focus-icon").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("perceive-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("switch-focus-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("minimize-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("maximize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("configure-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("close-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("car-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("truck-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("pedestrian-icon").style.display == DisplayStyle.None);
		}

		[Test]
		public void TestTitlebarNonFocusCarIconVisibility()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Car;

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.NonFocusAgent, appStorageMock);

			Assert.That(visualInfoPanel.Q<VisualElement>("focus-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("perceive-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("switch-focus-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("minimize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("maximize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("configure-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("close-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("car-icon").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("truck-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("pedestrian-icon").style.display == DisplayStyle.None);
		}

		[Test]
		public void TestTitlebarNonFocusTruckIconVisibility()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Truck;

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.NonFocusAgent, appStorageMock);

			Assert.That(visualInfoPanel.Q<VisualElement>("focus-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("perceive-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("switch-focus-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("minimize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("maximize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("configure-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("close-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("car-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("truck-icon").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("pedestrian-icon").style.display == DisplayStyle.None);
		}

		[Test]
		public void TestTitlebarNonFocusPedestrianIconVisibility()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Pedestrian;

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.NonFocusAgent, appStorageMock);

			Assert.That(visualInfoPanel.Q<VisualElement>("focus-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("perceive-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("switch-focus-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("minimize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("maximize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("configure-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("close-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("car-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("truck-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("pedestrian-icon").style.display == DisplayStyle.Flex);
		}

		[Test]
		public void TestTitlebarPerceivedAgentIconVisibility()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.PerceivedAgent, appStorageMock);

			Assert.That(visualInfoPanel.Q<VisualElement>("focus-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("perceive-icon").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("switch-focus-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("minimize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("maximize-btn").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("configure-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("close-btn").style.display == DisplayStyle.Flex);
            Assert.That(visualInfoPanel.Q<VisualElement>("car-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("truck-icon").style.display == DisplayStyle.None);
            Assert.That(visualInfoPanel.Q<VisualElement>("pedestrian-icon").style.display == DisplayStyle.None);
		}
		
		[Test]
		public void TestDisplayOptionsNamespace()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Floating.ParamKey1", "ParamLabel1", true));
			displayOptions.Add(new DisplayOption("Visualized.ParamKey2", "ParamLabel2", true));
			displayOptions.Add(new DisplayOption("Whatever.ParamKey3", "ParamLabel3", true));
			displayOptions.Add(new DisplayOption("Whatever.ParamKey4", "ParamLabel4", true));

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			ListView paramListView = visualInfoPanel.Q<ListView>("param-list-view");
			Assert.That(paramListView.itemsSource.Count == 2);
		}

		[Test]
		public void TestDisabledDisplayOptions()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			displayOptions.Add(new DisplayOption("ParamKey2", "ParamLabel2", false));
			displayOptions.Add(new DisplayOption("ParamKey3", "ParamLabel3", false));
			displayOptions.Add(new DisplayOption("ParamKey4", "ParamLabel4", true));

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			ListView paramListView = visualInfoPanel.Q<ListView>("param-list-view");
			Assert.That(paramListView.itemsSource.Count == 2);
		}

		[Test]
		public void TestCorrectParamKey()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Whatever.ParamKey", "ParamLabel", true));

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("Whatever.ParamKey", "123");

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			ListView paramListView = visualInfoPanel.Q<ListView>("param-list-view");
			AgentPanelItem item = paramListView.itemsSource[0] as AgentPanelItem;
			Assert.That(item.Key == "Whatever.ParamKey");
		}

		[Test]
		public void TestCorrectParamValue()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Whatever.ParamKey", "ParamLabel", true));

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("Whatever.ParamKey", "123");

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			ListView paramListView = visualInfoPanel.Q<ListView>("param-list-view");
			AgentPanelItem item = paramListView.itemsSource[0] as AgentPanelItem;
			Assert.That(item.GetValue() == "123");
		}

		[Test]
		public void TestSwitchToAgent()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Param", "Param", true));

			//create a dummy agent
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 11;
			dummyAgent1.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent1.OptionalAttributes.Add("Param", "Agent_1");

			//create another dummy agent
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent2.ID = 22;
			dummyAgent2.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent2.OptionalAttributes.Add("Param", "Agent_2");

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent1, "Title", AgentPanelType.FocusAgent, appStorageMock);

			ListView paramListView = visualInfoPanel.Q<ListView>("param-list-view");
			AgentPanelItem origItem = paramListView.itemsSource[0] as AgentPanelItem;

			Assert.That(origItem.GetValue() == "Agent_1");

			agentPanelInstance.SwitchAgent(dummyAgent2);

			AgentPanelItem newItem = paramListView.itemsSource[0] as AgentPanelItem;
			Assert.That(newItem.GetValue() == "Agent_2");
			Assert.That(visualInfoPanel.Q<Label>("agent-id-lbl").text == "Agent 22");
		}

		[Test]
		public void TestSwitchToNoAgent()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			agentPanelInstance.SwitchAgent(null);

			Assert.That(visualInfoPanel.Q<Label>("agent-id-lbl").text == "None");
		}

		[Test]
		public void TestUpdateDisplayOptions()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			List<DisplayOption> origdisplayOptions = new List<DisplayOption>();
			origdisplayOptions.Add(new DisplayOption("Param1", "Param1", true));

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("Param1", "Value1");
			dummyAgent.OptionalAttributes.Add("Param2", "Value2");

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, origdisplayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			ListView paramListView = visualInfoPanel.Q<ListView>("param-list-view");
			AgentPanelItem origItem = paramListView.itemsSource[0] as AgentPanelItem;

			Assert.That(paramListView.itemsSource.Count == 1);
			Assert.That(origItem.GetValue() == "Value1");

			List<DisplayOption> newDisplayOptions = new List<DisplayOption>();
			newDisplayOptions.Add(new DisplayOption("Param2", "Param2", true));
		
			agentPanelInstance.UpdateDisplayOptions(newDisplayOptions);

			AgentPanelItem newItem = paramListView.itemsSource[0] as AgentPanelItem;
			Assert.That(paramListView.itemsSource.Count == 1);
			Assert.That(newItem.GetValue() == "Value2");
		}

		[Test]
		public void TestGetAgentID()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.ID = 123;

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

			Assert.That(agentPanelInstance.GetAgentID() == 123);
		}

		[Test]
		public void TestChangeWindowMode()
		{
			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			
			//create dummy display options
			List<DisplayOption> displayOptions = new List<DisplayOption>();

			//create a dummy agent
			Agent dummyAgent = Container.Resolve<Agent> ();

			//load the uxml resource
            VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
            
			AgentPanelHandler agentPanelInstance = new AgentPanelHandler(visualInfoPanel, displayOptions, dummyAgent, "Title", AgentPanelType.FocusAgent, appStorageMock);

            VisualElement minimizeBtn = visualInfoPanel.Q<VisualElement>("minimize-btn");
            VisualElement maximizeBtn = visualInfoPanel.Q<VisualElement>("maximize-btn");

			Assert.That(minimizeBtn.style.display == DisplayStyle.Flex);
			Assert.That(maximizeBtn.style.display == DisplayStyle.None);

			agentPanelInstance.ChangeWindowMode(AgentPanelWindowMode.Minimized);

			Assert.That(minimizeBtn.style.display == DisplayStyle.None);
			Assert.That(maximizeBtn.style.display == DisplayStyle.Flex);
		}
	}
}