﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using Zenject;
using System.Collections.Generic;
using Visualizer.TrafficSignHandling;
using Visualizer.World.Environment;
using Visualizer.Core;
using NUnit.Framework;
using UnityEngine;
using NSubstitute;

namespace TrafficLightTests
{
	[TestFixture]
	public class TrafficSignManagerTests : ZenjectUnitTestFixture
	{		
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<TrafficSignManager>().AsSingle();

			//Bind mock classes
			ITrafficSignStateProvider trafficSignStateProviderMock = Substitute.For<ITrafficSignStateProvider> ();
			Container.Bind<ITrafficSignStateProvider> ().FromInstance (trafficSignStateProviderMock).AsSingle();

			IRoadSignalRenderer roadSignalRendererMock = Substitute.For<IRoadSignalRenderer> ();
			Container.Bind<IRoadSignalRenderer> ().FromInstance (roadSignalRendererMock).AsSingle();
		}

		[Test]
		public void TestUpdateSigns()
		{
			TrafficSignManager trafficSignManager = Container.Resolve<TrafficSignManager> ();
			ITrafficSignStateProvider trafficSignStateProviderMock = Container.Resolve<ITrafficSignStateProvider> ();
			IRoadSignalRenderer roadSignalRendererMock = Container.Resolve<IRoadSignalRenderer> ();

			TrafficSignController controller1 = Substitute.For<TrafficSignController>();
			TrafficSignController controller2 = Substitute.For<TrafficSignController>();
			controller1.SignalID.Returns("signal1");
			controller2.SignalID.Returns("signal2");
			trafficSignStateProviderMock.GetState("signal1", 100).Returns("GREEN");	
			trafficSignStateProviderMock.GetState("signal2", 100).Returns("RED");	
	
			List<TrafficSignController> tsControllers = new List<TrafficSignController>();
			tsControllers.Add(controller1);
			tsControllers.Add(controller2);
			roadSignalRendererMock.GetSignalControllers().Returns(tsControllers);
			
			trafficSignManager.UpdateSigns(100);

			controller1.Received(1).UpdateSign("GREEN");
			controller2.Received(1).UpdateSign("RED");
		}

		[Test]
		public void TestStartAnimation()
		{
			TrafficSignManager trafficSignManager = Container.Resolve<TrafficSignManager> ();
			ITrafficSignStateProvider trafficSignStateProviderMock = Container.Resolve<ITrafficSignStateProvider> ();
			IRoadSignalRenderer roadSignalRendererMock = Container.Resolve<IRoadSignalRenderer> ();

			TrafficSignController controller1 = Substitute.For<TrafficSignController>();
			TrafficSignController controller2 = Substitute.For<TrafficSignController>();
	
			List<TrafficSignController> tsControllers = new List<TrafficSignController>();
			tsControllers.Add(controller1);
			tsControllers.Add(controller2);
			roadSignalRendererMock.GetSignalControllers().Returns(tsControllers);
			
			trafficSignManager.StartAnimation(1.2f);

			controller1.Received(1).StartAnimation(1.2f);
			controller2.Received(1).StartAnimation(1.2f);
		}

		[Test]
		public void TestStopAnimation()
		{
			TrafficSignManager trafficSignManager = Container.Resolve<TrafficSignManager> ();
			ITrafficSignStateProvider trafficSignStateProviderMock = Container.Resolve<ITrafficSignStateProvider> ();
			IRoadSignalRenderer roadSignalRendererMock = Container.Resolve<IRoadSignalRenderer> ();

			TrafficSignController controller1 = Substitute.For<TrafficSignController>();
			TrafficSignController controller2 = Substitute.For<TrafficSignController>();
	
			List<TrafficSignController> tsControllers = new List<TrafficSignController>();
			tsControllers.Add(controller1);
			tsControllers.Add(controller2);
			roadSignalRendererMock.GetSignalControllers().Returns(tsControllers);
			
			trafficSignManager.StopAnimation();

			controller1.Received(1).StopAnimation();
			controller2.Received(1).StopAnimation();
		}

		[Test]
		public void TestClear()
		{
			TrafficSignManager trafficSignManager = Container.Resolve<TrafficSignManager> ();
			ITrafficSignStateProvider trafficSignStateProviderMock = Container.Resolve<ITrafficSignStateProvider> ();
			IRoadSignalRenderer roadSignalRendererMock = Container.Resolve<IRoadSignalRenderer> ();

			TrafficSignController controller1 = Substitute.For<TrafficSignController>();
			TrafficSignController controller2 = Substitute.For<TrafficSignController>();
	
			List<TrafficSignController> tsControllers = new List<TrafficSignController>();
			tsControllers.Add(controller1);
			tsControllers.Add(controller2);
			roadSignalRendererMock.GetSignalControllers().Returns(tsControllers);
			
			trafficSignManager.Clear();

			trafficSignStateProviderMock.Received(1).Clear();
		}
	}
}