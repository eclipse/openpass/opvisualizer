﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using Zenject;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.Environment;
using Visualizer.AgentHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class AgentPanelItemTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();
		}

		[Test]
		public void TestGetValue()
		{
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("ParamKey", "123");
			
			AgentPanelItem item = new AgentPanelItem("ParamKey", dummyAgent);
			
			Assert.That(item.GetValue() == "123");
		}

		[Test]
		public void TestGetNonExistingParamValue()
		{
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("ParamKey1", "123");
			
			AgentPanelItem item = new AgentPanelItem("ParamKey2", dummyAgent);
			
			Assert.That(item.GetValue() == "-");
		}

		[Test]
		public void TestGetNonExistingAgentParamValue()
		{		
			AgentPanelItem item = new AgentPanelItem("ParamKey2", null);
			Assert.That(item.GetValue() == "-");
		}

		[Test]
		public void TestValueFormatting()
		{
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("ParamKey1", "123.45678");
			
			AgentPanelItem item = new AgentPanelItem("ParamKey1", dummyAgent);
			
			Assert.That(item.GetValue() == "123,46");
		}

		[Test]
		public void TestLabelCreation()
		{
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("ParamKey1", "123.45678");
			
			AgentPanelItem item = new AgentPanelItem("ParamKey1", dummyAgent);
			
			Assert.That(item.Label == "ParamKey1");
		}

		[Test]
		public void TestLabelCreationFromParamKeyWithNamespace()
		{
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("Namespace.ParamKey1", "123.45678");
			
			AgentPanelItem item = new AgentPanelItem("Namespace.ParamKey1", dummyAgent);
			
			Assert.That(item.Label == "ParamKey1");
		}

		[Test]
		public void TestLabelCreationFromParamKeyWithMultipleNamespaces()
		{
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.OptionalAttributes = new Dictionary<string, string>();
			dummyAgent.OptionalAttributes.Add("Namespace.SubNamespace.ParamKey1", "123.45678");
			
			AgentPanelItem item = new AgentPanelItem("Namespace.SubNamespace.ParamKey1", dummyAgent);
			
			Assert.That(item.Label == "ParamKey1");
		}
	}
}