﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using Zenject;
using Visualizer.TrafficSignHandling;
using NUnit.Framework;
using UnityEngine;
using NSubstitute;

namespace TrafficLightTests
{
	[TestFixture]
	public class TrafficLightControllerTests : ZenjectUnitTestFixture
	{
		private GameObject redBulb = null;
		private GameObject greenBulb = null;
		
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<TrafficLightController>().FromNewComponentOnNewGameObject ().AsSingle();
		}

		private TrafficLightState[] CreateTrafficLightStates() {
			redBulb = new GameObject();
			greenBulb = new GameObject();

			redBulb.AddComponent<MeshRenderer>();
			greenBulb.AddComponent<MeshRenderer>();

			TrafficLightBulbController redBulbController = redBulb.AddComponent<TrafficLightBulbController>();
			TrafficLightBulbController greenBulbController = greenBulb.AddComponent<TrafficLightBulbController>();

			TrafficLightBulbRule redRule1 = new TrafficLightBulbRule();
			TrafficLightBulbRule redRule2 = new TrafficLightBulbRule();
			TrafficLightBulbRule greenRule1 = new TrafficLightBulbRule();
			TrafficLightBulbRule greenRule2 = new TrafficLightBulbRule();

			greenRule1.BulbController = redBulbController;
			greenRule1.BulbState = LightBulbState.Off;
			greenRule2.BulbController = greenBulbController;
			greenRule2.BulbState = LightBulbState.On;

			redRule1.BulbController = redBulbController;
			redRule1.BulbState = LightBulbState.On;
			redRule2.BulbController = greenBulbController;
			redRule2.BulbState = LightBulbState.Off;

			TrafficLightState greenState = new TrafficLightState();
			greenState.Name = "GREEN";
			greenState.BulbRules = new TrafficLightBulbRule[2];
			greenState.BulbRules[0] = greenRule1;
			greenState.BulbRules[1] = greenRule2;
						
			TrafficLightState redState = new TrafficLightState();
			redState.Name = "RED";
			redState.BulbRules = new TrafficLightBulbRule[2];
			redState.BulbRules[0] = redRule1;
			redState.BulbRules[1] = redRule2;

			TrafficLightState[] states = new TrafficLightState[2];
			states[0] = redState;
			states[1] = greenState;

			return states;
		}

		[Test]
		public void TestSingleUpdateSign()
		{
			TrafficLightController trafficLightController = Container.Resolve<TrafficLightController> ();
			trafficLightController.SetTrafficLightStates(CreateTrafficLightStates());

			trafficLightController.UpdateSign("GREEN");
			Assert.That(redBulb.GetComponent<MeshRenderer>().enabled == false);
			Assert.That(greenBulb.GetComponent<MeshRenderer>().enabled == true);
		}

		[Test]
		public void TestMultipleUpdateSign()
		{
			TrafficLightController trafficLightController = Container.Resolve<TrafficLightController> ();
			trafficLightController.SetTrafficLightStates(CreateTrafficLightStates());

			trafficLightController.UpdateSign("GREEN");
			Assert.That(redBulb.GetComponent<MeshRenderer>().enabled == false);
			Assert.That(greenBulb.GetComponent<MeshRenderer>().enabled == true);

			trafficLightController.UpdateSign("RED");
			Assert.That(redBulb.GetComponent<MeshRenderer>().enabled == true);
			Assert.That(greenBulb.GetComponent<MeshRenderer>().enabled == false);

			trafficLightController.UpdateSign("GREEN");
			Assert.That(redBulb.GetComponent<MeshRenderer>().enabled == false);
			Assert.That(greenBulb.GetComponent<MeshRenderer>().enabled == true);
		}

		[Test]
		public void TestInvalidState()
		{
			TrafficLightController trafficLightController = Container.Resolve<TrafficLightController> ();
			trafficLightController.SetTrafficLightStates(CreateTrafficLightStates());

			trafficLightController.UpdateSign("NONEXISTINGSTATE");
			Assert.That(redBulb.GetComponent<MeshRenderer>().enabled == false);
			Assert.That(greenBulb.GetComponent<MeshRenderer>().enabled == false);
		}

		[Test]
		public void TestMissingStates()
		{
			TrafficLightController trafficLightController = Container.Resolve<TrafficLightController> ();
			
			//intentionally not setting the traffic light definition

			trafficLightController.UpdateSign("NOMATTER");
		}
	}
}