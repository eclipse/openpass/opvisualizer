﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading.Tasks;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Utilities;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class StaticAgentInfoWindowHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<StaticAgentInfoWndHandler>().AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		//We use UnityTests as they are executed as Coroutines, which is neccessary to test asynchronous UI behabiour
		[UnityTest]
		public IEnumerator TestDisplayStaticProps()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			StaticAgentInfoWndHandler staticAgentInfoWndHandler = Container.Resolve<StaticAgentInfoWndHandler> ();

			List<GenericListItem> props = new List<GenericListItem>();
			props.Add(new GenericListItem("Key1", "Label1", "1", true));
			props.Add(new GenericListItem("Key2", "Label2", "2", true));
			props.Add(new GenericListItem("Key3", "Label3", "3", true));

            var template = Resources.Load<VisualTreeAsset>("UI/StaticAgentInfoWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			staticAgentInfoWndHandler.Init(1, props, window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("key-lbl").ToList(labels);
			Assert.That(labels.Count == 4);
			Assert.That(labels[0].text == "General");
			Assert.That(labels[1].text == "Label1");
			Assert.That(labels[2].text == "Label2");
			Assert.That(labels[3].text == "Label3");

			labels.Clear();
			listView.Query<Label>("value-lbl").ToList(labels);
			Assert.That(labels.Count == 4);
			Assert.That(labels[0].text == "");
			Assert.That(labels[1].text == "1");
			Assert.That(labels[2].text == "2");
			Assert.That(labels[3].text == "3");
		}

		[UnityTest]
		public IEnumerator TestDisplayStaticPropGrouping()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			StaticAgentInfoWndHandler staticAgentInfoWndHandler = Container.Resolve<StaticAgentInfoWndHandler> ();

			List<GenericListItem> props = new List<GenericListItem>();
			props.Add(new GenericListItem("Group1.Key1", "Label1", "1", true));
			props.Add(new GenericListItem("Group1.Key2", "Label2", "2", true));
			props.Add(new GenericListItem("Group2.Key3", "Label3", "3", true));
			props.Add(new GenericListItem("Group2.Group3.Key4", "Label4", "4", true));

            var template = Resources.Load<VisualTreeAsset>("UI/StaticAgentInfoWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			staticAgentInfoWndHandler.Init(1, props, window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("key-lbl").ToList(labels);
			Assert.That(labels.Count == 7);
			Assert.That(labels[0].text == "Group1");
			Assert.That(labels[1].text == "Label1");
			Assert.That(labels[2].text == "Label2");
			Assert.That(labels[3].text == "Group2");
			Assert.That(labels[4].text == "Label3");
			Assert.That(labels[5].text == "Group3");
			Assert.That(labels[6].text == "Label4");

			labels.Clear();
			listView.Query<Label>("value-lbl").ToList(labels);
			Assert.That(labels.Count == 7);
			Assert.That(labels[0].text == "");
			Assert.That(labels[0].text == "");
			Assert.That(labels[1].text == "1");
			Assert.That(labels[2].text == "2");
			Assert.That(labels[3].text == "");
			Assert.That(labels[4].text == "3");
			Assert.That(labels[5].text == "");
			Assert.That(labels[6].text == "4");
		}

		[UnityTest]
		public IEnumerator TestWindowTitle()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			StaticAgentInfoWndHandler staticAgentInfoWndHandler = Container.Resolve<StaticAgentInfoWndHandler> ();

			List<GenericListItem> props = new List<GenericListItem>();

            var template = Resources.Load<VisualTreeAsset>("UI/StaticAgentInfoWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			staticAgentInfoWndHandler.Init(123, props, window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Assert.That(window.Q<Label>("window-title").text == "Agent 123 Static Info");
		}	

		[UnityTest]
		public IEnumerator TestRemoveID()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			StaticAgentInfoWndHandler staticAgentInfoWndHandler = Container.Resolve<StaticAgentInfoWndHandler> ();

			List<GenericListItem> props = new List<GenericListItem>();
			props.Add(new GenericListItem("Key1", "Label1", "1", true));
			props.Add(new GenericListItem("Id", "ID", "3", true));
			props.Add(new GenericListItem("Key2", "Label2", "2", true));

            var template = Resources.Load<VisualTreeAsset>("UI/StaticAgentInfoWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			staticAgentInfoWndHandler.Init(123, props, window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("key-lbl").ToList(labels);
			Assert.That(labels.Count == 3);
			Assert.That(labels[0].text == "General");
			Assert.That(labels[1].text == "Label1");
			Assert.That(labels[2].text == "Label2");
		}
	}
}