﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Trace;
using Visualizer.UI;
using Visualizer.Core;
using Visualizer.Environment;
using Visualizer.World;
using NUnit.Framework;
using NSubstitute;

namespace CoreTests
{
	[TestFixture]
	public class SimulationLoaderTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<SimulationLoader>().FromNewComponentOnNewGameObject ().AsSingle();

			//Bind mock classes
			Container.Bind<ICameraManager> ().FromInstance (Substitute.For<ICameraManager> ()).AsSingle();
			Container.Bind<IAnimationController> ().FromInstance (Substitute.For<IAnimationController> ()).AsSingle();
			Container.Bind<IDemoController> ().FromInstance (Substitute.For<IDemoController> ()).AsSingle();
			Container.Bind<IWeatherController> ().FromInstance (Substitute.For<IWeatherController> ()).AsSingle();
			Container.Bind<ITraceFileLoader> ().FromInstance (Substitute.For<ITraceFileLoader> ()).AsSingle();
			Container.Bind<ISampleContainer> ().FromInstance (Substitute.For<ISampleContainer> ()).AsSingle();
			Container.Bind<IViewModeHandler> ().FromInstance (Substitute.For<IViewModeHandler> ()).AsSingle();
			Container.Bind<ITimeAxisHandler> ().FromInstance (Substitute.For<ITimeAxisHandler> ()).AsSingle();
			Container.Bind<IAgentManager> ().FromInstance (Substitute.For<IAgentManager> ()).AsSingle();
			Container.Bind<IFocusCameraHandler> ().FromInstance (Substitute.For<IFocusCameraHandler> ()).AsSingle();
			Container.Bind<IPlaybackSpeedHandler> ().FromInstance (Substitute.For<IPlaybackSpeedHandler> ()).AsSingle();
		}

		[Test]
		public void TestLoadSimulation()
		{
			//NOTE: the simulationLoader will have to be implemented as integration test due to some of its dependencies 
			//that cannot be mocked (RoadNetworkManager, MessagePanelController, LoadingPanel) and also because it uses coroutines
			//For now we leave the code here to be converted into an integration test
		
			//SimulationLoader simulationLoader = Container.Resolve<SimulationLoader> ();
			//simulationLoader.LoadSimulation ();
		}
	}
}