﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.EventSystems;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.Trace;
using Visualizer.AgentHandling;
using Visualizer.TrafficSignHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;
using NSubstitute.ReturnsExtensions;

namespace CoreTests
{
	[TestFixture]
	public class JobHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<JobHandler>().AsSingle();

			ISampleContainer sampleContainerMock = Substitute.For<ISampleContainer> ();
			Container.Bind<ISampleContainer> ().FromInstance (sampleContainerMock).AsSingle();

			ITraceFileLoader traceFileLoaderMock = Substitute.For<ITraceFileLoader> ();
			Container.Bind<ITraceFileLoader> ().FromInstance (traceFileLoaderMock).AsSingle();
		}

		[Test]
		public void TestInit()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.GetSample(200).ReturnsNull();
			//intentionally not calling init -> background thread should not start -> loadSample should not be called

			jobHandler.LoadSampleRequest (200);

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (0).LoadSample (200);
		}

		[Test]
		public void TestLoadSampleRequest()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.GetSample(200).ReturnsNull();
			jobHandler.Init();

			jobHandler.LoadSampleRequest (200);

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (1).LoadSample (200);
		}

		[Test]
		public void TestNoLoadSampleRequestIfSampleAlreadyLoaded()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.GetSample(200).Returns(new AgentSample[3]); //-> the sample container already has the sample
			jobHandler.Init();

			jobHandler.LoadSampleRequest (200);

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (0).LoadSample (200);
		}
		
		[Test]
		public void TestForceLoadSampleRequest()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.GetSample(200).Returns(new AgentSample[3]); //-> the sample container already has the sample
			jobHandler.Init();

			jobHandler.LoadSampleRequest (200, true); //-> force

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (1).LoadSample (200);
		}

		[Test]
		public void TestForceLoadSampleRequestMultipleTimes()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.GetSample(200).Returns(new AgentSample[3]); //-> the sample container already has the sample
			jobHandler.Init();

			jobHandler.LoadSampleRequest (200, true); //-> force
			jobHandler.LoadSampleRequest (200, true); //-> force
			jobHandler.LoadSampleRequest (200, true); //-> force
			jobHandler.LoadSampleRequest (200, true); //-> force

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (4).LoadSample (200);
		}

		[Test]
		public void TestNoLoadSampleRequestIfRequestAlreadyQueued()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.GetSample(200).ReturnsNull();
			jobHandler.Init();

			jobHandler.LoadSampleRequest (200); //->queue request
			jobHandler.LoadSampleRequest (200); //->already queued
			jobHandler.LoadSampleRequest (200); //->already queued
			jobHandler.LoadSampleRequest (200); //->already queued

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (1).LoadSample (200); //only one LoadSample
		}

		[Test]
		public void TestLoadSampleRequesPreload()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(2000);
			sampleContainerMock.GetSample(200).ReturnsNull();
			sampleContainerMock.GetSample(300).ReturnsNull();
			sampleContainerMock.GetSample(400).ReturnsNull();
			sampleContainerMock.GetSample(500).ReturnsNull();
			sampleContainerMock.GetSample(600).ReturnsNull();
			sampleContainerMock.GetSample(700).ReturnsNull();
			sampleContainerMock.GetSample(800).ReturnsNull();
			sampleContainerMock.GetSample(900).ReturnsNull();
			sampleContainerMock.GetSample(1000).ReturnsNull();
			sampleContainerMock.GetSample(1100).ReturnsNull();
			sampleContainerMock.GetSample(1200).ReturnsNull();

			jobHandler.Init();

			jobHandler.LoadSampleRequestPreload (200);

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (1).LoadSample (200);
			traceFileLoaderMock.Received (1).LoadSample (300);
			traceFileLoaderMock.Received (1).LoadSample (400);
			traceFileLoaderMock.Received (1).LoadSample (500);
			traceFileLoaderMock.Received (1).LoadSample (600);
			traceFileLoaderMock.Received (1).LoadSample (700);
			traceFileLoaderMock.Received (1).LoadSample (800);
			traceFileLoaderMock.Received (1).LoadSample (900);
			traceFileLoaderMock.Received (1).LoadSample (1000);
			traceFileLoaderMock.Received (1).LoadSample (1100);
			traceFileLoaderMock.Received (1).LoadSample (1200);
		}

		[Test]
		public void TestNoLoadSampleRequestPreloadIfRequestAlreadyQueued()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(2000);
			sampleContainerMock.GetSample(200).ReturnsNull();
			sampleContainerMock.GetSample(300).ReturnsNull();
			sampleContainerMock.GetSample(400).Returns(new AgentSample[3]); //-> the sample container already has the sample
			sampleContainerMock.GetSample(500).ReturnsNull();
			sampleContainerMock.GetSample(600).ReturnsNull();
			sampleContainerMock.GetSample(700).ReturnsNull();
			sampleContainerMock.GetSample(800).ReturnsNull();
			sampleContainerMock.GetSample(900).Returns(new AgentSample[3]); //-> the sample container already has the sample
			sampleContainerMock.GetSample(1000).ReturnsNull();
			sampleContainerMock.GetSample(1100).ReturnsNull();
			sampleContainerMock.GetSample(1200).ReturnsNull();

			jobHandler.Init();

			jobHandler.LoadSampleRequestPreload (200);

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (1).LoadSample (200);
			traceFileLoaderMock.Received (1).LoadSample (300);
			traceFileLoaderMock.Received (0).LoadSample (400); //-> should not be queued
			traceFileLoaderMock.Received (1).LoadSample (500);
			traceFileLoaderMock.Received (1).LoadSample (600);
			traceFileLoaderMock.Received (1).LoadSample (700);
			traceFileLoaderMock.Received (1).LoadSample (800);
			traceFileLoaderMock.Received (0).LoadSample (900); //-> should not be queued
			traceFileLoaderMock.Received (1).LoadSample (1000);
			traceFileLoaderMock.Received (1).LoadSample (1100);
			traceFileLoaderMock.Received (1).LoadSample (1200);
		}

		[Test]
		public void TestNoLoadSampleRequestPreloadAfterLastSample()
		{
			JobHandler jobHandler = Container.Resolve<JobHandler> ();
			ITraceFileLoader traceFileLoaderMock = Container.Resolve<ITraceFileLoader> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.SamplingTime.Returns(100);
			sampleContainerMock.GetFirstTimeStamp().Returns(0);
			sampleContainerMock.GetLastTimeStamp().Returns(600);
			sampleContainerMock.GetSample(200).ReturnsNull();
			sampleContainerMock.GetSample(300).ReturnsNull();
			sampleContainerMock.GetSample(400).ReturnsNull();
			sampleContainerMock.GetSample(500).ReturnsNull();
			sampleContainerMock.GetSample(600).ReturnsNull();
			sampleContainerMock.GetSample(700).ReturnsNull();
			sampleContainerMock.GetSample(800).ReturnsNull();
			sampleContainerMock.GetSample(900).ReturnsNull();
			sampleContainerMock.GetSample(1000).ReturnsNull();
			sampleContainerMock.GetSample(1100).ReturnsNull();
			sampleContainerMock.GetSample(1200).ReturnsNull();

			jobHandler.Init();

			jobHandler.LoadSampleRequestPreload (200);

			Thread.Sleep(100); //wait a bit, until the job is pulled from the background queue

			traceFileLoaderMock.Received (1).LoadSample (200);
			traceFileLoaderMock.Received (1).LoadSample (300);
			traceFileLoaderMock.Received (1).LoadSample (400);
			traceFileLoaderMock.Received (1).LoadSample (500);
			traceFileLoaderMock.Received (1).LoadSample (600);
			traceFileLoaderMock.Received (0).LoadSample (700); //-> should not be queued
			traceFileLoaderMock.Received (0).LoadSample (800); //-> should not be queued
			traceFileLoaderMock.Received (0).LoadSample (900); //-> should not be queued
			traceFileLoaderMock.Received (0).LoadSample (1000); //-> should not be queued
			traceFileLoaderMock.Received (0).LoadSample (1100); //-> should not be queued
			traceFileLoaderMock.Received (0).LoadSample (1200); //-> should not be queued
		}
	}
}