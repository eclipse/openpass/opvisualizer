﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using Zenject;
using Visualizer.Core;
using NUnit.Framework;
using NSubstitute;

namespace CoreTests
{
	[TestFixture]
	public class CmdLineInterpreterTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind mock classes
			ISampleContainer sampleContainerMock = Substitute.For<ISampleContainer> ();
			Container.Bind<ISampleContainer> ().FromInstance (sampleContainerMock).AsSingle();

            //Bind test target
       		Container.Bind<CmdLineInterpreter>().AsSingle();
		}

        [Test]
		public void TestSystemMode()
		{
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?sysmode=simplayer";
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			string strValue;
			cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.SystemMode, out strValue);
			Assert.That (strValue == "simplayer");
		}

        [Test]
		public void TestTraceFile()
		{
            string traceFile = "c:\\temp\\simulationoutput.xml";
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?tracefile=" + traceFile;
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			string strValue;
			cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.TraceFile, out strValue);
			Assert.That (strValue == traceFile);
		}

        [Test]
		public void TestSceneryFile()
		{
            string sceneryfile = "c:\\temp\\sceneryconfiguration.xodr";
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?sceneryfile=" + sceneryfile;
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			string strValue;
			cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.SceneryFile, out strValue);
			Assert.That (strValue == sceneryfile);
		}

        [Test]
		public void TestRunId()
		{
            int runId = 123;
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?runid=" + runId;
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			int intValue;
			cmdLineInterpreter.GetCmdLineArgAsInt (CmdLineArgs.RunID, out intValue);
			Assert.That (intValue == runId);
		}

        [Test]
		public void TestVehicleModel()
		{
            string vehicleModel = "car_test";
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?vehiclemodel=" + vehicleModel;
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			string strValue;
			cmdLineInterpreter.GetCmdLineArgAsStr (CmdLineArgs.VehicleModel, out strValue);
			Assert.That (strValue == vehicleModel);
		}

        [Test]
		public void TestTimeStamp()
		{
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?timestamp=1500";
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			int intValue;
			cmdLineInterpreter.GetCmdLineArgAsInt (CmdLineArgs.TimeStamp, out intValue);
			Assert.That (intValue == 1500);
		}

        [Test]
		public void TestAgentId()
		{
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?agentid=3";
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			int intValue;
            cmdLineInterpreter.GetCmdLineArgAsInt (CmdLineArgs.AgentID, out intValue);
			Assert.That (intValue == 3);
		}   

        [Test]
		public void TestMultipleParams()
		{
			string testCmdLine = "c:\\dummyvisualizer.exe startparams?"+
				"stringkey1=stringvalue1" +
				"&stringkey2=stringvalue2" +
				"&intkey1=123" +
				"&intkey2=1234";
			CmdLineInterpreter cmdLineInterpreter = Container.Resolve<CmdLineInterpreter> ();
			cmdLineInterpreter.Init (testCmdLine);

			string strValue;
			cmdLineInterpreter.GetCmdLineArgAsStr ("stringkey1", out strValue);
			Assert.That (strValue == "stringvalue1");

			cmdLineInterpreter.GetCmdLineArgAsStr ("stringkey2", out strValue);
			Assert.That (strValue == "stringvalue2");

			int intValue;
			cmdLineInterpreter.GetCmdLineArgAsInt ("intkey1", out intValue);
			Assert.That (intValue == 123);

			cmdLineInterpreter.GetCmdLineArgAsInt ("intkey2", out intValue);
			Assert.That (intValue == 1234);
		}
	}
}
