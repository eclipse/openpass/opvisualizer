﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using Zenject;
using System.Collections.Generic;
using Visualizer.TrafficSignHandling;
using Visualizer.Core;
using NUnit.Framework;
using NSubstitute;

namespace TrafficLightTests
{
	[TestFixture]
	public class TrafficSignStateProviderTests : ZenjectUnitTestFixture
	{		
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<TrafficSignStateProvider>().AsSingle();
		}

		[Test]
		public void TestAddSingleSignalWithSingleEvent()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();
			SimulationEvent simEvent = new SimulationEvent();
			simEvent.timeStamp = 1000;
			simEvent.eventType = "TrafficLight";
			Dictionary<string, string> eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal123");
			eventParams.Add("traffic_light_state", "GREEN");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			string state = trafficSignStateProvider.GetState("signal123", 200);
			Assert.That(state == "-");

			state = trafficSignStateProvider.GetState("signal123", 2000);
			Assert.That(state == "GREEN");
		}


		[Test]
		public void TestAddSingleSignalWithMultipleEvents()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();

			SimulationEvent simEvent = new SimulationEvent();
			simEvent.timeStamp = 1000;
			simEvent.eventType = "TrafficLight";
			Dictionary<string, string> eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal123");
			eventParams.Add("traffic_light_state", "GREEN");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			simEvent = new SimulationEvent();
			simEvent.timeStamp = 2000;
			simEvent.eventType = "TrafficLight";
			eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal123");
			eventParams.Add("traffic_light_state", "RED");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			string state = trafficSignStateProvider.GetState("signal123", 900);
			Assert.That(state == "-");

			state = trafficSignStateProvider.GetState("signal123", 1100);
			Assert.That(state == "GREEN");

			state = trafficSignStateProvider.GetState("signal123", 2100);
			Assert.That(state == "RED");
		}

		[Test]
		public void TestAddMultipleSignalsWithMultipleEvents()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();

			SimulationEvent simEvent = new SimulationEvent();
			simEvent.timeStamp = 0;
			simEvent.eventType = "TrafficLight";
			Dictionary<string, string> eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal1");
			eventParams.Add("traffic_light_state", "GREEN");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			simEvent = new SimulationEvent();
			simEvent.timeStamp = 1000;
			simEvent.eventType = "TrafficLight";
			eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal1");
			eventParams.Add("traffic_light_state", "RED");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			simEvent = new SimulationEvent();
			simEvent.timeStamp = 0;
			simEvent.eventType = "TrafficLight";
			eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal2");
			eventParams.Add("traffic_light_state", "YELLOW");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			simEvent = new SimulationEvent();
			simEvent.timeStamp = 500;
			simEvent.eventType = "TrafficLight";
			eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal2");
			eventParams.Add("traffic_light_state", "GREEN");
			simEvent.eventParameters = eventParams;
			trafficSignStateProvider.AddEvent(simEvent);

			string state = trafficSignStateProvider.GetState("signal1", 0);
			Assert.That(state == "GREEN");

			state = trafficSignStateProvider.GetState("signal1", 1100);
			Assert.That(state == "RED");

			state = trafficSignStateProvider.GetState("signal2", 0);
			Assert.That(state == "YELLOW");

			state = trafficSignStateProvider.GetState("signal2", 600);
			Assert.That(state == "GREEN");
		}

		[Test]
		public void TestAddInvalidEvent1()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();
			
			SimulationEvent simEvent = null;
			Assert.That(() => trafficSignStateProvider.AddEvent(simEvent), Throws.Nothing);
		}

		[Test]
		public void TestAddInvalidEvent2()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();
			
			SimulationEvent simEvent = new SimulationEvent();
			simEvent.timeStamp = 1000;
			simEvent.eventType = "TrafficLight";
			//No eventParams specified!

			Assert.That(() => trafficSignStateProvider.AddEvent(simEvent), Throws.Nothing);

			string state = trafficSignStateProvider.GetState("signal123", 1100);
			Assert.That(state == "-");
		}

		[Test]
		public void TestAddInvalidEvent3()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();
			
			SimulationEvent simEvent = new SimulationEvent();
			simEvent.timeStamp = 1000;
			simEvent.eventType = "TrafficLight";
			Dictionary<string, string> eventParams = new Dictionary<string, string>();
			eventParams.Add("opendrive_id", "signal123");
			simEvent.eventParameters = eventParams;
			//traffic_light_state not set!

			Assert.That(() => trafficSignStateProvider.AddEvent(simEvent), Throws.Nothing);

			string state = trafficSignStateProvider.GetState("signal123", 1100);
			Assert.That(state == "-");
		}

		[Test]
		public void TestAddInvalidEvent4()
		{
			TrafficSignStateProvider trafficSignStateProvider = Container.Resolve<TrafficSignStateProvider> ();
			
			SimulationEvent simEvent = new SimulationEvent();
			simEvent.timeStamp = 1000;
			simEvent.eventType = "TrafficLight";
			Dictionary<string, string> eventParams = new Dictionary<string, string>();
			eventParams.Add("traffic_light_state", "GREEN");
			simEvent.eventParameters = eventParams;
			//opendrive_id not set!

			Assert.That(() => trafficSignStateProvider.AddEvent(simEvent), Throws.Nothing);

			string state = trafficSignStateProvider.GetState("signal123", 1100);
			Assert.That(state == "-");
		}
	}
}