/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using Visualizer.UI;
using Visualizer.Utilities;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Core;
using NUnit.Framework;
using NSubstitute;
using NSubstitute.ReturnsExtensions;

namespace UITests
{
	[TestFixture]
	public class KeyboardEventHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<KeyboardEventHandler> ().FromNewComponentOnNewGameObject().AsSingle();

			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

			IAgentManager agentManagerMock = Substitute.For<IAgentManager> ();
			Container.Bind<IAgentManager> ().FromInstance (agentManagerMock).AsSingle();

			ISimulationLoader simulationLoaderMock = Substitute.For<ISimulationLoader> ();
			Container.Bind<ISimulationLoader> ().FromInstance (simulationLoaderMock).AsSingle();

			IAnimationController animationControllerMock = Substitute.For<IAnimationController> ();
			Container.Bind<IAnimationController> ().FromInstance (animationControllerMock).AsSingle();

			IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
			Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();

			IUIModeHandler uiModeHandlerMock = Substitute.For<IUIModeHandler> ();
			Container.Bind<IUIModeHandler> ().FromInstance (uiModeHandlerMock).AsSingle();

			ITerrainManipulator terrainManipulatorMock = Substitute.For<ITerrainManipulator> ();
			Container.Bind<ITerrainManipulator> ().FromInstance (terrainManipulatorMock).AsSingle();

			IUnityIO unityIOMock = Substitute.For<IUnityIO> ();
			Container.Bind<IUnityIO> ().FromInstance (unityIOMock).AsSingle();

			IMainUI uiMock = Substitute.For<IMainUI> ();
			Container.Bind<IMainUI> ().FromInstance (uiMock).AsSingle();
		}

	    [Test]
	    public void TestLeftArrow()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);
			unityIOMock.GetKeyDown(KeyCode.LeftArrow).Returns(true);
			keyboardEventHandler.Update();

			animationControllerMock.Received (1).JumpToPreviousSample();
	    }

	    //###### This test is not working reliably so it has been disabled. It would probably run well as an integration test
	    // [Test]
	    // public void TestHoldLeftArrow()
	    // {
		// 	KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

		// 	IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
		// 	ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
		// 	IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
		// 	simulationsLoaderMock.IsSimulationLoaded().Returns(true);

		// 	unityIOMock.GetKeyDown(KeyCode.LeftArrow).Returns(true);
		// 	keyboardEventHandler.Update();

		// 	Thread.Sleep(400); //wait to make sure, the spooling starts
		// 	unityIOMock.GetKey(KeyCode.LeftArrow).Returns(true);
		// 	unityIOMock.GetKeyDown(KeyCode.LeftArrow).Returns(false);

		// 	keyboardEventHandler.Update();
		// 	Thread.Sleep(10); //wait to make sure, the next spoolstep executes
		// 	keyboardEventHandler.Update();
		// 	Thread.Sleep(10); //wait to make sure, the next spoolstep executes
		// 	keyboardEventHandler.Update();
		// 	Thread.Sleep(10); //wait to make sure, the next spoolstep executes
		// 	keyboardEventHandler.Update();

		// 	animationControllerMock.Received (5).JumpToPreviousSample();
	    // }

	    [Test]
	    public void TestRightArrow()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);
			unityIOMock.GetKeyDown(KeyCode.RightArrow).Returns(true);
			keyboardEventHandler.Update();

			animationControllerMock.Received (1).JumpToNextSample();
	    }

	    //###### This test is not working reliably so it has been disabled. It would probably run well as an integration test
	    // [Test]
	    // public void TestHoldRightArrow()
	    // {
		// 	KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

		// 	IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
		// 	ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
		// 	IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
		// 	simulationsLoaderMock.IsSimulationLoaded().Returns(true);

		// 	unityIOMock.GetKeyDown(KeyCode.RightArrow).Returns(true);
		// 	keyboardEventHandler.Update();

		// 	Thread.Sleep(400); //wait to make sure, the spooling starts
		// 	unityIOMock.GetKey(KeyCode.RightArrow).Returns(true);
		// 	unityIOMock.GetKeyDown(KeyCode.RightArrow).Returns(false);

		// 	keyboardEventHandler.Update();
		// 	Thread.Sleep(10); //wait to make sure, the next spoolstep executes
		// 	keyboardEventHandler.Update();
		// 	Thread.Sleep(10); //wait to make sure, the next spoolstep executes
		// 	keyboardEventHandler.Update();
		// 	Thread.Sleep(10); //wait to make sure, the next spoolstep executes
		// 	keyboardEventHandler.Update();

		// 	animationControllerMock.Received (5).JumpToNextSample();
	    // }

		[Test]
	    public void TestDownArrow()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);
			
			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent3.TargetPosition = new Vector3(1f, 2f, 3f);
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3, false).Returns(dummyAgent3);
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent2); //begin from agent 2

			unityIOMock.GetKeyDown(KeyCode.DownArrow).Returns(true);
			keyboardEventHandler.Update();

			focusCameraHandlerMock.Received (1).FocusCamera(3);
	    }

		[Test]
	    public void TestDownArrowMultipleTimes()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent3.TargetPosition = new Vector3(1f, 2f, 3f);
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3, false).Returns(dummyAgent3);
			unityIOMock.GetKeyDown(KeyCode.DownArrow).Returns(true);

			//agent 1 -> 2
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent1); 
			keyboardEventHandler.Update();
			focusCameraHandlerMock.Received (1).FocusCamera(2);

			//agent 2 -> 3
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent2);
			keyboardEventHandler.Update();
			focusCameraHandlerMock.Received (1).FocusCamera(3);

			//agent 3 -> 1
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent3);
			keyboardEventHandler.Update();
			focusCameraHandlerMock.Received (1).FocusCamera(1);
	    }

		[Test]
	    public void TestDownArrowWhenNoVisibleAgents()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3, false).Returns(dummyAgent3);
			unityIOMock.GetKeyDown(KeyCode.DownArrow).Returns(true);

			//all three agents are hidden -> camera focus should not change
			focusCameraHandlerMock.GetFocusedAgent().ReturnsNull(); 
			keyboardEventHandler.Update();
			focusCameraHandlerMock.ReceivedWithAnyArgs (0).FocusCamera(default);
	    }

		[Test]
	    public void TestDownArrowWhenNoAgentsInScene()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			unityIOMock.GetKeyDown(KeyCode.DownArrow).Returns(true);

			//all three agents are hidden -> camera focus should not change
			focusCameraHandlerMock.GetFocusedAgent().ReturnsNull(); 
			keyboardEventHandler.Update();
			focusCameraHandlerMock.ReceivedWithAnyArgs (0).FocusCamera(default);
	    }

		[Test]
	    public void TestUpArrow()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent3.TargetPosition = new Vector3(1f, 2f, 3f);
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3, false).Returns(dummyAgent3);
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent2); //begin from agent 2

			unityIOMock.GetKeyDown(KeyCode.UpArrow).Returns(true);
			keyboardEventHandler.Update();

			focusCameraHandlerMock.Received (1).FocusCamera(1);
	    }

		[Test]
	    public void TestUpArrowMultipleTimes()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent3.TargetPosition = new Vector3(1f, 2f, 3f);
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3, false).Returns(dummyAgent3);
			unityIOMock.GetKeyDown(KeyCode.UpArrow).Returns(true);

			//agent 3 -> 2
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent3); 
			keyboardEventHandler.Update();
			focusCameraHandlerMock.Received (1).FocusCamera(2);

			//agent 2 -> 1
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent2);
			keyboardEventHandler.Update();
			focusCameraHandlerMock.Received (1).FocusCamera(1);

			//agent 1 -> 3
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent1);
			keyboardEventHandler.Update();
			focusCameraHandlerMock.Received (1).FocusCamera(3);
	    }

		[Test]
	    public void TestUpArrowWhenNoVisibleAgents()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3, false).Returns(dummyAgent3);
			unityIOMock.GetKeyDown(KeyCode.UpArrow).Returns(true);

			//all three agents are hidden -> camera focus should not change
			focusCameraHandlerMock.GetFocusedAgent().ReturnsNull(); 
			keyboardEventHandler.Update();
			focusCameraHandlerMock.ReceivedWithAnyArgs (0).FocusCamera(default);
	    }

		[Test]
	    public void TestUpArrowWhenNoAgentsInScene()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			unityIOMock.GetKeyDown(KeyCode.UpArrow).Returns(true);

			//all three agents are hidden -> camera focus should not change
			focusCameraHandlerMock.GetFocusedAgent().ReturnsNull(); 
			keyboardEventHandler.Update();
			focusCameraHandlerMock.ReceivedWithAnyArgs (0).FocusCamera(default);
	    }

		[Test]
	    public void TestSpaceWhilePlaybackRunning()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			animationControllerMock.IsPlaying().Returns(true);
			unityIOMock.GetKeyDown(KeyCode.Space).Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			keyboardEventHandler.Update();
			animationControllerMock.Received (1).StopPlayback();
	    }

		[Test]
	    public void TestSpaceWhilePlaybackNotRunning()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			animationControllerMock.IsPlaying().Returns(false);
			unityIOMock.GetKeyDown(KeyCode.Space).Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			keyboardEventHandler.Update();
			animationControllerMock.Received (1).StartPlayback();
	    }

		[Test]
	    public void TestAnyKeyInSensorPositionerMode()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(false);
			uIModeHandlerMock.GetMode().Returns(UIMode.SensorPositioner);

			unityIOMock.GetKeyDown(KeyCode.Space).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.LeftArrow).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.RightArrow).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.UpArrow).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.DownArrow).Returns(true);
			
			keyboardEventHandler.Update();
			animationControllerMock.Received (0).StopPlayback();
			animationControllerMock.Received (0).StartPlayback();
			animationControllerMock.Received (0).JumpToPreviousSample();
			animationControllerMock.Received (0).JumpToNextSample();
			focusCameraHandlerMock.ReceivedWithAnyArgs (0).FocusCamera(default);
	    }

		[Test]
	    public void TestAnyKeyIfSimulationIsNotRunning()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(false);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			unityIOMock.GetKeyDown(KeyCode.Space).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.LeftArrow).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.RightArrow).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.UpArrow).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.DownArrow).Returns(true);
			
			keyboardEventHandler.Update();
			animationControllerMock.Received (0).StopPlayback();
			animationControllerMock.Received (0).StartPlayback();
			animationControllerMock.Received (0).JumpToPreviousSample();
			animationControllerMock.Received (0).JumpToNextSample();
			focusCameraHandlerMock.ReceivedWithAnyArgs (0).FocusCamera(default);
	    }

		[Test]
	    public void TestAlt_2()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();
	
			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			animationControllerMock.IsPlaying().Returns(false);
			unityIOMock.GetKey(KeyCode.LeftAlt).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.Alpha2).Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			keyboardEventHandler.Update();
			simulationsLoaderMock.Received (1).ReloadTraceFile();
	    }

		[Test]
	    public void TestAlt_3()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();
	
			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			animationControllerMock.IsPlaying().Returns(false);
			unityIOMock.GetKey(KeyCode.LeftAlt).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.Alpha3).Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			keyboardEventHandler.Update();
			simulationsLoaderMock.Received (1).ReloadSceneryFile();
	    }

		[Test]
	    public void TestAlt_4()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();	
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			animationControllerMock.IsPlaying().Returns(false);
			unityIOMock.GetKey(KeyCode.LeftAlt).Returns(true);
			unityIOMock.GetKeyDown(KeyCode.Alpha4).Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			int origLevel = QualitySettings.GetQualityLevel();
			keyboardEventHandler.Update();
			Assert.That(origLevel != QualitySettings.GetQualityLevel());

			QualitySettings.SetQualityLevel(origLevel, true);
	    }

		[Test]
	    public void TestNoHandlingIfDisabled()
	    {
			KeyboardEventHandler keyboardEventHandler = Container.Resolve<KeyboardEventHandler> ();

			IUIModeHandler uIModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();	
			ISimulationLoader simulationsLoaderMock = Container.Resolve<ISimulationLoader> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			simulationsLoaderMock.IsSimulationLoaded().Returns(true);
			uIModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);
			unityIOMock.GetKeyDown(KeyCode.RightArrow).Returns(true);
			keyboardEventHandler.EnableHandling = false;

			keyboardEventHandler.Update();

			animationControllerMock.Received (0).JumpToNextSample();
	    }
	}
}