/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using Zenject;
using NUnit.Framework;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using NSubstitute;

[TestFixture]
public class AgentManagerTests : ZenjectUnitTestFixture
{
	AgentProperties agent1Prop;
	AgentProperties agent2Prop;
	AgentProperties agent3Prop;

	[SetUp]
	public void CommonInstall()
	{
		//Bind test target
		Container.Bind<AgentManager>().AsSingle();

		//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
		Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

		Agent dummyAgent1 = Container.Resolve<Agent> ();
		dummyAgent1.ID = 15;

		Agent dummyAgent2 = Container.Resolve<Agent> ();
		dummyAgent2.ID = 16;

		Agent dummyAgent3 = Container.Resolve<Agent> ();
		dummyAgent3.ID = 17;

		//Bind mock classes
		IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
		Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();

		IAgentFactory agentFactoryMock = Substitute.For<IAgentFactory> ();
		Container.Bind<IAgentFactory> ().FromInstance (agentFactoryMock).AsSingle();

		agent1Prop = new AgentProperties();
		agent1Prop.ID = 15;

		agent2Prop = new AgentProperties();
		agent2Prop.ID = 16;

		agent3Prop = new AgentProperties();
		agent3Prop.ID = 17;

		agentFactoryMock.SpawnAgent (agent1Prop).Returns (dummyAgent1);
		agentFactoryMock.SpawnAgent (agent2Prop).Returns (dummyAgent2);
		agentFactoryMock.SpawnAgent (agent3Prop).Returns (dummyAgent3);
	}

	//Helper function
	private void Spawn3Agents(AgentManager agentManager){
		List<AgentProperties> agentProps = new List<AgentProperties> ();
		agentProps.Add (agent1Prop);
		agentProps.Add (agent2Prop);
		agentProps.Add (agent3Prop);
		agentManager.SpawnAgents (agentProps);

		//must initialize the agents 
		//Not very nice solution for a unit test that should be only tesing the AgentManager, but it works
		agentManager.GetAgent (agent1Prop.ID).Start ();
		agentManager.GetAgent (agent2Prop.ID).Start ();	
		agentManager.GetAgent (agent3Prop.ID).Start ();
	}

    [Test]
    public void SpawnAgents()
    {
		AgentManager agentManager = Container.Resolve<AgentManager> ();
		List<AgentProperties> agentProps = new List<AgentProperties> ();
		agentProps.Add (agent1Prop);
		agentManager.SpawnAgents (agentProps);

		//Must not throw an exception
		agentManager.GetAgent (15);
    }

	[Test]
	public void TryToGetInvalidAgent()
	{
		AgentManager agentManager = Container.Resolve<AgentManager> ();
		List<AgentProperties> agentProps = new List<AgentProperties> ();
		agentProps.Add (agent1Prop);
		agentManager.SpawnAgents (agentProps);

		//Must throw an exception
		Assert.That(()=> agentManager.GetAgent (1), Throws.Exception);
	}

	[Test]
	public void GetAgentCount()
	{
		AgentManager agentManager = Container.Resolve<AgentManager> ();

		Spawn3Agents (agentManager);

		Assert.That(agentManager.GetAgentCount () == 3);
	}

	[Test]
	public void GetAgentIDs()
	{
		AgentManager agentManager = Container.Resolve<AgentManager> ();

		Spawn3Agents (agentManager);

		List<int> agentIDs = agentManager.GetAgentIDs ();
		Assert.That(agentIDs.Count == 3);
		Assert.That(agentIDs[0] == 15);
		Assert.That(agentIDs[1] == 16);
		Assert.That(agentIDs[2] == 17);
	}

	[Test]
	public void SetAnimationInterval()
	{
		AgentManager agentManager = Container.Resolve<AgentManager> ();

		Spawn3Agents (agentManager);

		agentManager.SetAnimationInterval (123f);

		Assert.That (agentManager.GetAgent (15).AnimationInterval == 123f);
		Assert.That (agentManager.GetAgent (16).AnimationInterval == 123f);
		Assert.That (agentManager.GetAgent (17).AnimationInterval == 123f);
	}
}