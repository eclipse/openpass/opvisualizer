﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading.Tasks;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.Trace;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class RunSelectorWindowHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<RunSelectorWndHandler>().AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		//We use UnityTests as they are executed as Coroutines, which is neccessary to test asynchronous UI behabiour
		[UnityTest]
		public IEnumerator TestDisplayRuns()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			RunSelectorWndHandler runSelectorWndHandler = Container.Resolve<RunSelectorWndHandler> ();

			List<Run> runs = new List<Run>();
			runs.Add(new Run(1, 0));
			runs.Add(new Run(2, 0));

            var template = Resources.Load<VisualTreeAsset>("UI/RunSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			runSelectorWndHandler.Init(window, "C:\\tracefile.xml", runs);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("run-id-lbl").ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "1");
			Assert.That(labels[1].text == "2");
		}

		[UnityTest]
		public IEnumerator TestDisplayEventCount()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			RunSelectorWndHandler runSelectorWndHandler = Container.Resolve<RunSelectorWndHandler> ();

			List<Run> runs = new List<Run>();
			runs.Add(new Run(1, 123));
			runs.Add(new Run(2, 234));

            var template = Resources.Load<VisualTreeAsset>("UI/RunSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			runSelectorWndHandler.Init(window, "C:\\tracefile.xml", runs);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("run-info-lbl").ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "(123 events)");
			Assert.That(labels[1].text == "(234 events)");
		}

		[UnityTest]
		public IEnumerator TestDisableOKButtonIfNoRunIsSelected()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			RunSelectorWndHandler runSelectorWndHandler = Container.Resolve<RunSelectorWndHandler> ();

			List<Run> runs = new List<Run>();
			runs.Add(new Run(1, 123));
			runs.Add(new Run(2, 234));

            var template = Resources.Load<VisualTreeAsset>("UI/RunSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			runSelectorWndHandler.Init(window, "C:\\tracefile.xml", runs);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == false);
		}

		[UnityTest]
		public IEnumerator TestEnableOKButtonIfRunIsSelected()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			RunSelectorWndHandler runSelectorWndHandler = Container.Resolve<RunSelectorWndHandler> ();

			List<Run> runs = new List<Run>();
			runs.Add(new Run(1, 123));
			runs.Add(new Run(2, 234));

            var template = Resources.Load<VisualTreeAsset>("UI/RunSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			runSelectorWndHandler.Init(window, "C:\\tracefile.xml", runs);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);
			
			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == true);
		}
		
		[UnityTest]
		public IEnumerator TestOKButtonDoesNotWorkIfNoRunIsSelected()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			RunSelectorWndHandler runSelectorWndHandler = Container.Resolve<RunSelectorWndHandler> ();

			List<Run> runs = new List<Run>();
			runs.Add(new Run(1, 123));
			runs.Add(new Run(2, 234));

            var template = Resources.Load<VisualTreeAsset>("UI/RunSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			runSelectorWndHandler.Init(window, "C:\\tracefile.xml", runs);
	
			bool runSelectedWasCalled = false;
			runSelectorWndHandler.RunSelected += (path, id) => runSelectedWasCalled = true;

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Button okButton = window.Q<Button>("ok-btn");
			okButton.SendEvent(new ClickEvent{target = okButton});

			Assert.That(runSelectedWasCalled == false);
		}

		[UnityTest]
		public IEnumerator TestRunSelection()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			RunSelectorWndHandler runSelectorWndHandler = Container.Resolve<RunSelectorWndHandler> ();

			List<Run> runs = new List<Run>();
			runs.Add(new Run(1, 123));
			runs.Add(new Run(2, 234));

            var template = Resources.Load<VisualTreeAsset>("UI/RunSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			runSelectorWndHandler.Init(window, "C:\\tracefile.xml", runs);
	
			int selectedID = -1;
			string selectedPath = "";
			runSelectorWndHandler.RunSelected += (path, id) => { selectedID = id; selectedPath = path; };

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);

			Button okButton = window.Q<Button>("ok-btn");
			okButton.SendEvent(new ClickEvent{target = okButton});

			Assert.That(selectedID == 2);
			Assert.That(selectedPath == "C:\\tracefile.xml");
		}
	}
}