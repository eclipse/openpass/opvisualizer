﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using Zenject;
using Visualizer.UI;
using Visualizer.Utilities;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class TreeListTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{

		}

        [Test]
		public void TestCreationFromDOWithNoRootNameSpace()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			testTarget.CreateFromDisplayOptions(displayOptions);
			Assert.That(testTarget.Count == 1);
			Assert.That(testTarget[0].Level == 0);
			Assert.That(testTarget[0].Group == false);
			Assert.That(testTarget[0].Label == "ParamLabel1");
			Assert.That(testTarget[0].Key == "ParamKey1");
		}

        [Test]
		public void TestCreationFromDOWithRootNameSpace()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("ParamKey", "ParamLabel", true));
			testTarget.CreateFromDisplayOptions(displayOptions, "Root");
			Assert.That(testTarget.Count == 2);

			Assert.That(testTarget[0].Level == 0);
			Assert.That(testTarget[0].Group == true);
			Assert.That(testTarget[0].Label == "Root");
			Assert.That(testTarget[0].Key == "Root");

			Assert.That(testTarget[1].Level == 1);
			Assert.That(testTarget[1].Group == false);
			Assert.That(testTarget[1].Label == "ParamLabel");
			Assert.That(testTarget[1].Key == "Root.ParamKey");
		}

        [Test]
		public void TestCreationFromDOWithGroup()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Group1.Key2", "Label2", true));

			testTarget.CreateFromDisplayOptions(displayOptions);
			Assert.That(testTarget.Count == 2);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");

			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Label2");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key2");
			Assert.AreEqual(testTarget[0].Enabled, true);
		}
   
        [Test]
		public void TestCreationFromDODisabledItem()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Group1.Key3", "Label3", false));
			testTarget.CreateFromDisplayOptions(displayOptions);
			Assert.That(testTarget.Count == 2);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");
			
			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Label3");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key3");
			Assert.AreEqual(testTarget[1].Enabled, false);
		}

        [Test]
		public void TestCreationFromDOEnabledItem()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Group1.Key4", true));
			testTarget.CreateFromDisplayOptions(displayOptions);
			Assert.That(testTarget.Count == 2);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");
			
			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Key4");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key4");
			Assert.AreEqual(testTarget[1].Enabled, true);
		}

        [Test]
		public void TestCreationFromDOMultipleGroupLevels()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Group1.Group2.Key5", true));
			testTarget.CreateFromDisplayOptions(displayOptions);
			Assert.That(testTarget.Count == 3);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");

			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, true);
			Assert.AreEqual(testTarget[1].Label, "Group2");
			Assert.AreEqual(testTarget[1].Key, "Group2");

			Assert.AreEqual(testTarget[2].Level, 2);
			Assert.AreEqual(testTarget[2].Group, false);
			Assert.AreEqual(testTarget[2].Label, "Key5");
			Assert.AreEqual(testTarget[2].Key, "Group1.Group2.Key5");
		}

        [Test]
		public void TestCreationFromDOMultipleGroups()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> displayOptions = new List<DisplayOption>();
			displayOptions.Add(new DisplayOption("Group1.Key1", true));
			displayOptions.Add(new DisplayOption("Group2.Key2", true));
			displayOptions.Add(new DisplayOption("Group3.Key3", true));
			testTarget.CreateFromDisplayOptions(displayOptions);
			Assert.That(testTarget.Count == 6);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");

			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Key1");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key1");

			Assert.AreEqual(testTarget[2].Level, 0);
			Assert.AreEqual(testTarget[2].Group, true);
			Assert.AreEqual(testTarget[2].Label, "Group2");
			Assert.AreEqual(testTarget[2].Key, "Group2");

			Assert.AreEqual(testTarget[3].Level, 1);
			Assert.AreEqual(testTarget[3].Group, false);
			Assert.AreEqual(testTarget[3].Label, "Key2");
			Assert.AreEqual(testTarget[3].Key, "Group2.Key2");

			Assert.AreEqual(testTarget[4].Level, 0);
			Assert.AreEqual(testTarget[4].Group, true);
			Assert.AreEqual(testTarget[4].Label, "Group3");
			Assert.AreEqual(testTarget[4].Key, "Group3");

			Assert.AreEqual(testTarget[5].Level, 1);
			Assert.AreEqual(testTarget[5].Group, false);
			Assert.AreEqual(testTarget[5].Label, "Key3");
			Assert.AreEqual(testTarget[5].Key, "Group3.Key3");
		}

        [Test]
		public void TestCreationFromGLWithNoRootNameSpace()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("ParamKey1", "ParamLabel1", "ParamValue1", true));
			testTarget.CreateFromGenericList(genericList);
			Assert.That(testTarget.Count == 1);
			Assert.That(testTarget[0].Level == 0);
			Assert.That(testTarget[0].Group == false);
			Assert.That(testTarget[0].Label == "ParamLabel1");
			Assert.That(testTarget[0].Value == "ParamValue1");
			Assert.That(testTarget[0].Key == "ParamKey1");
		}

        [Test]
		public void TestCreationFromGLWithRootNameSpace()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("ParamKey", "ParamLabel", true));
			testTarget.CreateFromGenericList(genericList, "Root");
			Assert.That(testTarget.Count == 2);

			Assert.That(testTarget[0].Level == 0);
			Assert.That(testTarget[0].Group == true);
			Assert.That(testTarget[0].Label == "Root");
			Assert.That(testTarget[0].Key == "Root");

			Assert.That(testTarget[1].Level == 1);
			Assert.That(testTarget[1].Group == false);
			Assert.That(testTarget[1].Label == "ParamLabel");
			Assert.That(testTarget[1].Value == "");
			Assert.That(testTarget[1].Key == "Root.ParamKey");
		}

        [Test]
		public void TestCreationFromGLWithGroup()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("Group1.Key2", "Label2", true));

			testTarget.CreateFromGenericList(genericList);
			Assert.That(testTarget.Count == 2);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");

			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Label2");
			Assert.AreEqual(testTarget[1].Value, "");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key2");
			Assert.AreEqual(testTarget[1].Enabled, true);
		}
   
        [Test]
		public void TestCreationFromGLDisabledItem()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("Group1.Key3", "Label3", "Value3", false));
			testTarget.CreateFromGenericList(genericList);
			Assert.That(testTarget.Count == 2);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");
			
			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Label3");
			Assert.AreEqual(testTarget[1].Value, "Value3");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key3");
			Assert.AreEqual(testTarget[1].Enabled, false);
		}

        [Test]
		public void TestCreationFromGLEnabledItem()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("Group1.Key4", true));
			testTarget.CreateFromGenericList(genericList);
			Assert.That(testTarget.Count == 2);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");
			
			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Key4");
			Assert.AreEqual(testTarget[1].Value, "");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key4");
			Assert.AreEqual(testTarget[1].Enabled, true);
		}

        [Test]
		public void TestCreationFromGLMultipleGroupLevels()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("Group1.Group2.Key5", "Label5", "Value5", true));
			testTarget.CreateFromGenericList(genericList);
			Assert.That(testTarget.Count == 3);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");

			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, true);
			Assert.AreEqual(testTarget[1].Label, "Group2");
			Assert.AreEqual(testTarget[1].Key, "Group2");

			Assert.AreEqual(testTarget[2].Level, 2);
			Assert.AreEqual(testTarget[2].Group, false);
			Assert.AreEqual(testTarget[2].Label, "Label5");
			Assert.AreEqual(testTarget[2].Value, "Value5");
			Assert.AreEqual(testTarget[2].Key, "Group1.Group2.Key5");
		}

        [Test]
		public void TestCreationFromGLMultipleGroups()
		{
			TreeList testTarget = new TreeList();
			
			List<GenericListItem> genericList = new List<GenericListItem>();
			genericList.Add(new GenericListItem("Group1.Key1", "Label1", "Value1", true));
			genericList.Add(new GenericListItem("Group2.Key2", "Label2", "Value2", true));
			genericList.Add(new GenericListItem("Group3.Key3", "Label3", "Value3", true));
			testTarget.CreateFromGenericList(genericList);
			Assert.That(testTarget.Count == 6);

			Assert.AreEqual(testTarget[0].Level, 0);
			Assert.AreEqual(testTarget[0].Group, true);
			Assert.AreEqual(testTarget[0].Label, "Group1");
			Assert.AreEqual(testTarget[0].Key, "Group1");

			Assert.AreEqual(testTarget[1].Level, 1);
			Assert.AreEqual(testTarget[1].Group, false);
			Assert.AreEqual(testTarget[1].Label, "Label1");
			Assert.AreEqual(testTarget[1].Value, "Value1");
			Assert.AreEqual(testTarget[1].Key, "Group1.Key1");

			Assert.AreEqual(testTarget[2].Level, 0);
			Assert.AreEqual(testTarget[2].Group, true);
			Assert.AreEqual(testTarget[2].Label, "Group2");
			Assert.AreEqual(testTarget[2].Key, "Group2");

			Assert.AreEqual(testTarget[3].Level, 1);
			Assert.AreEqual(testTarget[3].Group, false);
			Assert.AreEqual(testTarget[3].Label, "Label2");
			Assert.AreEqual(testTarget[3].Value, "Value2");
			Assert.AreEqual(testTarget[3].Key, "Group2.Key2");

			Assert.AreEqual(testTarget[4].Level, 0);
			Assert.AreEqual(testTarget[4].Group, true);
			Assert.AreEqual(testTarget[4].Label, "Group3");
			Assert.AreEqual(testTarget[4].Key, "Group3");

			Assert.AreEqual(testTarget[5].Level, 1);
			Assert.AreEqual(testTarget[5].Group, false);
			Assert.AreEqual(testTarget[5].Label, "Label3");
			Assert.AreEqual(testTarget[5].Value, "Value3");
			Assert.AreEqual(testTarget[5].Key, "Group3.Key3");
		}

        [Test]
		public void TestReadingWithNoRootNameSpace()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			testTarget.CreateFromDisplayOptions(origDisplayOptions);

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();

			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Label, readDisplayOptions[0].Label);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
		}

        [Test]
		public void TestReadingWithRootNameSpace()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("ParamKey", "ParamLabel", true));
			testTarget.CreateFromDisplayOptions(origDisplayOptions, "Root");

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();
			Assert.That(readDisplayOptions.Count == 1);
			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Label, readDisplayOptions[0].Label);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
		}

        [Test]
		public void TestReadingWithGroup()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("Group1.Key2", "Label2", true));

			testTarget.CreateFromDisplayOptions(origDisplayOptions);

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();
			Assert.That(readDisplayOptions.Count == 1);
			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Label, readDisplayOptions[0].Label);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
		}
   
        [Test]
		public void TestReadingDisabledItem()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("Group1.Key3", "Label3", false));
			testTarget.CreateFromDisplayOptions(origDisplayOptions);

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();
			Assert.That(readDisplayOptions.Count == 1);
			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Label, readDisplayOptions[0].Label);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
		}

        [Test]
		public void TestReadingEnabledItem()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("Group1.Key4", true));
			testTarget.CreateFromDisplayOptions(origDisplayOptions);

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();
			Assert.That(readDisplayOptions.Count == 1);
			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
		}

        [Test]
		public void TestReadingMultipleGroupLevels()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("Group1.Group2.Key5", true));
			testTarget.CreateFromDisplayOptions(origDisplayOptions);

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();
			Assert.That(readDisplayOptions.Count == 1);
			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
		}

        [Test]
		public void TestReadingMultipleGroups()
		{
			TreeList testTarget = new TreeList();
			
			List<DisplayOption> origDisplayOptions = new List<DisplayOption>();
			origDisplayOptions.Add(new DisplayOption("Group1.Key1", true));
			origDisplayOptions.Add(new DisplayOption("Group2.Key2", true));
			origDisplayOptions.Add(new DisplayOption("Group3.Key3", true));
			testTarget.CreateFromDisplayOptions(origDisplayOptions);

			List<DisplayOption> readDisplayOptions = testTarget.ToDisplayOptionList();
			Assert.That(readDisplayOptions.Count == 3);
			Assert.AreEqual(origDisplayOptions[0].Key, readDisplayOptions[0].Key);
			Assert.AreEqual(origDisplayOptions[0].Enabled, readDisplayOptions[0].Enabled);
			Assert.AreEqual(origDisplayOptions[1].Key, readDisplayOptions[1].Key);
			Assert.AreEqual(origDisplayOptions[1].Enabled, readDisplayOptions[1].Enabled);
			Assert.AreEqual(origDisplayOptions[2].Key, readDisplayOptions[2].Key);
			Assert.AreEqual(origDisplayOptions[2].Enabled, readDisplayOptions[2].Enabled);
		}
	}
}
