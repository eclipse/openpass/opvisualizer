﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.Environment;
using Visualizer.AgentHandling;
using Visualizer.TrafficSignHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;
using NSubstitute.ReturnsExtensions;

namespace CoreTests
{
	[TestFixture]
	public class AnimationControllerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<AnimationController>().FromNewComponentOnNewGameObject ().AsSingle();

			//Bind mock classes
			IWeatherController weatherControllerMock = Substitute.For<IWeatherController> ();
			Container.Bind<IWeatherController> ().FromInstance (weatherControllerMock).AsSingle();

			IMainUI uiMock = Substitute.For<IMainUI> ();
			Container.Bind<IMainUI> ().FromInstance (uiMock).AsSingle();

			IDriverPerceptionManager driverPerceptionManagerMock = Substitute.For<IDriverPerceptionManager> ();
			Container.Bind<IDriverPerceptionManager> ().FromInstance (driverPerceptionManagerMock).AsSingle();

			ISampleContainer sampleContainerMock = Substitute.For<ISampleContainer> ();
			Container.Bind<ISampleContainer> ().FromInstance (sampleContainerMock).AsSingle();

			IAgentManager agentManagerMock = Substitute.For<IAgentManager> ();
			Container.Bind<IAgentManager> ().FromInstance (agentManagerMock).AsSingle();

			ITrafficSignManager trafficSignManagerMock = Substitute.For<ITrafficSignManager> ();
			Container.Bind<ITrafficSignManager> ().FromInstance (trafficSignManagerMock).AsSingle();

			IJobHandler jobHandlerMock = Substitute.For<IJobHandler> ();
			Container.Bind<IJobHandler> ().FromInstance (jobHandlerMock).AsSingle();
		}

		[Test]
		public void TestInit()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.SamplingTime.Returns (250);
			animationController.Init ();

			agentManagerMock.Received (1).SetAnimationInterval (0.25f);
		}

		[Test]
		public void TestSetPlaybackSpeed()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			sampleContainerMock.SamplingTime.Returns (250);
			animationController.SetPlaybackSpeed (2f);

			agentManagerMock.Received (1).SetAnimationInterval (0.125f);
		}

		[Test]
		public void TestStartAnimation()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IWeatherController weatherControllerMock = Container.Resolve<IWeatherController> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();

			bool startEventWasFired = false;
			animationController.PlaybackStarted += () => { startEventWasFired = true; };

			float playbackSpeed = 0.5f;
			animationController.SetPlaybackSpeed (playbackSpeed);
			animationController.StartPlayback();
			bool isPlaying = animationController.IsPlaying ();

			Assert.That(isPlaying == true);
			Assert.That(startEventWasFired == true);
			agentManagerMock.Received (1).StartAgentAnimation (playbackSpeed);
			weatherControllerMock.Received (1).StartAnimation (playbackSpeed);
			trafficSignManagerMock.Received (1).StartAnimation (playbackSpeed);
		}

		//We use UnityTests, which is neccessary to test asynchronous coroutine behabiour
		[UnityTest]
		public IEnumerator TestLoadSampleRequestIfNotLoaded()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IWeatherController weatherControllerMock = Container.Resolve<IWeatherController> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IJobHandler jobHandlerMock = Container.Resolve<IJobHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();


			float playbackSpeed = 0.5f;
			animationController.SetPlaybackSpeed (playbackSpeed);
			animationController.StartPlayback();
			bool isPlaying = animationController.IsPlaying ();

			Assert.That(isPlaying == true);

			// pretend that the sample is not yet loaded
			sampleContainerMock.GetSample(0).ReturnsNull();

			// wait one frame to let the coroutin execute
			yield return null;

			trafficSignManagerMock.ReceivedWithAnyArgs (0).UpdateSigns(default);
			driverPerceptionManagerMock.ReceivedWithAnyArgs (0).UpdatePerceivedWorld (default);
			jobHandlerMock.Received (1).LoadSampleRequest(0);
		}

		//We use UnityTests, which is neccessary to test asynchronous coroutine behabiour
		[UnityTest]
		public IEnumerator TestNoLoadSampleRequestIfLoaded()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IWeatherController weatherControllerMock = Container.Resolve<IWeatherController> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IJobHandler jobHandlerMock = Container.Resolve<IJobHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			animationController.StartPlayback();

			// the sample is already loaded
			AgentSample[] agentSample = new AgentSample[1];
			sampleContainerMock.GetSample (0).Returns (agentSample);

			// wait one frame to let the coroutin execute
			yield return null;

			agentManagerMock.Received (1).UpdateAgents(agentSample, 0);
			trafficSignManagerMock.Received (1).UpdateSigns(0);
			driverPerceptionManagerMock.Received (1).UpdatePerceivedWorld (0);
			jobHandlerMock.Received (0).LoadSampleRequest(0);
			jobHandlerMock.Received (1).LoadSampleRequestPreload(0);
		}

		//We use UnityTests, which is neccessary to test asynchronous coroutine behabiour
		[UnityTest]
		public IEnumerator TestInterruptPlaybackWhileLoadingSample()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IWeatherController weatherControllerMock = Container.Resolve<IWeatherController> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IJobHandler jobHandlerMock = Container.Resolve<IJobHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			animationController.StartPlayback();

			// pretend that the sample is not yet loaded
			sampleContainerMock.GetSample(0).ReturnsNull();

			bool pendingEventWasFired = false;
			animationController.PlaybackPending += () => { pendingEventWasFired = true; };

			// wait one frame to let the coroutin execute
			yield return null;

			jobHandlerMock.Received (1).LoadSampleRequest(0);
			Assert.That(pendingEventWasFired == true);
		}

		//We use UnityTests, which is neccessary to test asynchronous coroutine behabiour
		[UnityTest]
		public IEnumerator TestResumePlaybackAfterSampleLoaded()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IWeatherController weatherControllerMock = Container.Resolve<IWeatherController> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IJobHandler jobHandlerMock = Container.Resolve<IJobHandler> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();

			animationController.StartPlayback();

			// pretend that the sample is not yet loaded
			sampleContainerMock.GetSample(0).ReturnsNull();

			// wait one frame to let the coroutin execute
			yield return null;

			jobHandlerMock.Received (1).LoadSampleRequest(0);

			bool startEventWasFired = false;
			animationController.PlaybackStarted += () => { startEventWasFired = true; };

			// the sample is already loaded
			AgentSample[] agentSample = new AgentSample[1];
			sampleContainerMock.GetSample (0).Returns (agentSample);

			// here we imitate a new, delayed invocation of DisplaySample (see AnimationController.RequestSample)
			animationController.JumpToTimeStamp(0);

			Assert.That(startEventWasFired == true);
		}

		[Test]
		public void TestStopAnimation()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			IWeatherController weatherControllerMock = Container.Resolve<IWeatherController> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();

			bool stopEventWasFired = false;
			animationController.PlaybackStopped += () => { stopEventWasFired = true; };

			animationController.StartPlayback();
			animationController.StopPlayback();
			bool isPlaying = animationController.IsPlaying ();

			Assert.That(isPlaying == false);
			Assert.That(stopEventWasFired == true);
			agentManagerMock.Received (1).StopAgentAnimation ();
			weatherControllerMock.Received (1).StopAnimation ();
			trafficSignManagerMock.Received (1).StopAnimation ();
		}

		[Test]
		public void TestJumpToTimeStamp()
		{
			AnimationController animationController = Container.Resolve<AnimationController> ();
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			ISampleContainer sampleContainerMock = Container.Resolve<ISampleContainer> ();
			ITrafficSignManager trafficSignManagerMock = Container.Resolve<ITrafficSignManager> ();

			int timeStamp = 1200;
			AgentSample[] agentSample = new AgentSample[1];
			sampleContainerMock.GetSample (timeStamp).Returns (agentSample);
			sampleContainerMock.GetSampleCount ().Returns (1000);

			animationController.JumpToTimeStamp (timeStamp);

			agentManagerMock.Received (1).UpdateAgents (agentSample, timeStamp);
			trafficSignManagerMock.Received (1).UpdateSigns (timeStamp);
		}
	}
}