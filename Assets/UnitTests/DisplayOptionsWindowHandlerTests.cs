﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading.Tasks;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class DisplayOptionsWindowHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<DisplayOptionsWndHandler>().AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		//We use UnityTests as they are executed as Coroutines, which is neccessary to test asynchronous UI behabiour
		[UnityTest]
		public IEnumerator TestListCeation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			DisplayOptionsWndHandler displayOptionsWndHandler = Container.Resolve<DisplayOptionsWndHandler> ();

            var template = Resources.Load<VisualTreeAsset>("UI/DisplayOptionsWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			ListView listView = window.Q<ListView>();

			List<DisplayOption> originalDisplayOptions = new List<DisplayOption>();
			originalDisplayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));
			originalDisplayOptions.Add(new DisplayOption("ParamKey2", "ParamLabel2", true));
			originalDisplayOptions.Add(new DisplayOption("ParamKey3", "ParamLabel3", true));

			displayOptionsWndHandler.Init(originalDisplayOptions, listView);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			var labels = new List<Label>();
			listView.Query<Label>("key-lbl").ToList(labels);
			Assert.That(labels[0].text == "Textual");
			Assert.That(labels[1].text == "ParamLabel1");
			Assert.That(labels[2].text == "ParamLabel2");
			Assert.That(labels[3].text == "ParamLabel3");
		}

		[UnityTest]
		public IEnumerator TestListGroupCeation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			DisplayOptionsWndHandler displayOptionsWndHandler = Container.Resolve<DisplayOptionsWndHandler> ();

            var template = Resources.Load<VisualTreeAsset>("UI/DisplayOptionsWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			ListView listView = window.Q<ListView>();

			List<DisplayOption> originalDisplayOptions = new List<DisplayOption>();
			originalDisplayOptions.Add(new DisplayOption("Group1.ParamKey1", "ParamLabel1", true));
			originalDisplayOptions.Add(new DisplayOption("Group1.ParamKey2", "ParamLabel2", true));
			originalDisplayOptions.Add(new DisplayOption("Group2.ParamKey3", "ParamLabel3", true));

			displayOptionsWndHandler.Init(originalDisplayOptions, listView);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			var labels = new List<Label>();
			listView.Query<Label>("key-lbl").ToList(labels);
			Assert.That(labels[0].text == "Group1");
			Assert.That(labels[1].text == "ParamLabel1");
			Assert.That(labels[2].text == "ParamLabel2");
			Assert.That(labels[3].text == "Group2");
			Assert.That(labels[4].text == "ParamLabel3");
		}

		[UnityTest]
		public IEnumerator TestListChangeSingle()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			DisplayOptionsWndHandler displayOptionsWndHandler = Container.Resolve<DisplayOptionsWndHandler> ();

            var template = Resources.Load<VisualTreeAsset>("UI/DisplayOptionsWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			ListView listView = window.Q<ListView>();

			List<DisplayOption> originalDisplayOptions = new List<DisplayOption>();
			originalDisplayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", true));

			displayOptionsWndHandler.Init(originalDisplayOptions, listView);

			// wait one frame so that the list is build (GeometryChanged is executed)
			yield return null;

			List<DisplayOption> newDisplayOptions = new List<DisplayOption>();
			displayOptionsWndHandler.ListChanged += (changedList) => newDisplayOptions = changedList;
		
			//get the first checkbox (note the first elem of a list is a group level with an invisible checkbox)
			var toggles = new List<Toggle>();
			listView.Query<Toggle>().ToList(toggles);
			Toggle firstToggle = toggles[1];	
			Assert.That(firstToggle != null);

			//simulate a klick on the togggle
			firstToggle.value = false;

			Assert.That(newDisplayOptions.Count == 1);
			Assert.That(newDisplayOptions[0].Enabled == false);
		}

		[UnityTest]
		public IEnumerator TestListChangeMultiple()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			DisplayOptionsWndHandler displayOptionsWndHandler = Container.Resolve<DisplayOptionsWndHandler> ();

            var template = Resources.Load<VisualTreeAsset>("UI/DisplayOptionsWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			ListView listView = window.Q<ListView>();

			List<DisplayOption> originalDisplayOptions = new List<DisplayOption>();
			originalDisplayOptions.Add(new DisplayOption("ParamKey1", "ParamLabel1", false));
			originalDisplayOptions.Add(new DisplayOption("ParamKey2", "ParamLabel2", false));
			originalDisplayOptions.Add(new DisplayOption("ParamKey3", "ParamLabel3", false));

			displayOptionsWndHandler.Init(originalDisplayOptions, listView);

			// wait one frame so that the list is build (GeometryChanged is executed)
			yield return null;

			List<DisplayOption> newDisplayOptions = new List<DisplayOption>();
			displayOptionsWndHandler.ListChanged += (changedList) => newDisplayOptions = changedList;
		
			//get the first and the third checkbox (note the first elem of a list is a group level with an invisible checkbox)
			var toggles = new List<Toggle>();
			listView.Query<Toggle>().ToList(toggles);
			Toggle firstToggle = toggles[1];	
			Toggle thirdToggle = toggles[3];	
			Assert.That(firstToggle != null);
			Assert.That(thirdToggle != null);

			//simulate a klick on the two togggles
			firstToggle.value = true;
			thirdToggle.value = true;

			Assert.That(newDisplayOptions.Count == 3);

			Assert.That(newDisplayOptions[0].Enabled == true);
			Assert.That(newDisplayOptions[1].Enabled == false);
			Assert.That(newDisplayOptions[2].Enabled == true);
		}
	}
}