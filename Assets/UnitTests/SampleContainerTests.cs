﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using Visualizer.Core;
using Visualizer.AgentHandling;
using NUnit.Framework;
using NSubstitute;

namespace CoreTests
{
	[TestFixture]
	public class SampleContainerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<SampleContainer>().AsSingle();
		}

		[Test]
		public void TestInit()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			Assert.AreEqual (sampleContainer.GetSampleCount (), 0);
			Assert.AreEqual (sampleContainer.SamplingTime, -1);
		}

		[Test]
		public void TestClear()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create a sample (a snapshot) of 2 agents
			AgentSample[] sample = new AgentSample[1];

			//set some values of the snapshot of the first and second agent
			AgentSample agentSample = new AgentSample();
			sample [0] = agentSample;

			//add the sample to the sample container
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			
			SimulationEvent simEvent = new SimulationEvent();
			sampleContainer.AddEvent (simEvent);

			sampleContainer.SamplingTime = 100;

			Assert.AreEqual (sampleContainer.GetSampleCount(), 3);
			Assert.AreEqual (sampleContainer.GetFirstTimeStamp(), 0);
			Assert.AreEqual (sampleContainer.GetLastTimeStamp(), 200);
			Assert.AreEqual (sampleContainer.SamplingTime, 100);
			Assert.AreEqual (sampleContainer.GetEvents().Count, 1);

			sampleContainer.Clear ();

			Assert.AreEqual (sampleContainer.GetSampleCount(), 0);
			Assert.AreEqual (sampleContainer.GetFirstTimeStamp(), -1);
			Assert.AreEqual (sampleContainer.GetLastTimeStamp(), -1);
			Assert.AreEqual (sampleContainer.SamplingTime, -1);
			Assert.AreEqual (sampleContainer.GetEvents().Count, 0);
		}

		[Test]
		public void AddingGettingSample()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create a sample (a snapshot) of 2 agents
			AgentSample[] sample = new AgentSample[2];

			//set some values of the snapshot of the first and second agent
			AgentSample agentSample1 = new AgentSample();
			agentSample1.AgentID = 14;
			agentSample1.XPosition = 1111;
			sample [0] = agentSample1;

			AgentSample agentSample2 = new AgentSample();
			agentSample2.AgentID = 15;
			agentSample2.XPosition = 1112;
			sample [1] = agentSample2;

			//add the sample to the sample container
			sampleContainer.AddSample (3000, sample);

			Assert.AreEqual (sampleContainer.GetSample(3000)[0].AgentID, 14);
			Assert.AreEqual (sampleContainer.GetSample(3000)[0].XPosition, 1111);
			Assert.AreEqual (sampleContainer.GetSample(3000)[1].AgentID, 15);
			Assert.AreEqual (sampleContainer.GetSample(3000)[1].XPosition, 1112);
		}

		[Test]
		public void TestTryToGetNonExistingSample()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create a sample (a snapshot) of 2 agents
			AgentSample[] sample = new AgentSample[2];

			//set some values of the snapshot of the first and second agent
			AgentSample agentSample1 = new AgentSample();
			sample [0] = agentSample1;

			AgentSample agentSample2 = new AgentSample();
			sample [1] = agentSample2;

			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			Assert.AreEqual (sampleContainer.GetSample(200), null);
		}

		[Test]
		public void AddingGettingMultipleEvents()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create an event
			SimulationEvent simEvent1 = new SimulationEvent();
			simEvent1.timeStamp = 300;
			sampleContainer.AddEvent (simEvent1);

			//create another event
			SimulationEvent simEvent2 = new SimulationEvent();
			simEvent2.timeStamp = 500;
			sampleContainer.AddEvent (simEvent2);

			List <SimulationEvent> events = sampleContainer.GetEvents ();

			Assert.AreEqual (events.Count, 2);
			Assert.AreEqual (events[0].timeStamp, 300);			
			Assert.AreEqual (events[1].timeStamp, 500);
		}

        [Test]
		public void TestEventParameters()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create an event
			SimulationEvent simEvent1 = new SimulationEvent();
			Dictionary<string, string> eventParams = new Dictionary<string, string>();
			eventParams.Add("key", "value");
			simEvent1.eventParameters = eventParams;
			sampleContainer.AddEvent (simEvent1);

			List <SimulationEvent> events = sampleContainer.GetEvents ();
            Assert.AreEqual(events[0].eventParameters["key"], "value");
		}

        [Test]
		public void TestEventType()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create an event
			SimulationEvent simEvent1 = new SimulationEvent();
			simEvent1.eventType = "VeryBadEvent";
			sampleContainer.AddEvent (simEvent1);

			List <SimulationEvent> events = sampleContainer.GetEvents ();
            Assert.AreEqual(events[0].eventType, "VeryBadEvent");
		}

        [Test]
		public void TestAffectedEntities()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create an event
			SimulationEvent simEvent1 = new SimulationEvent();
			List<long> affectedEntities = new List<long>();
			affectedEntities.Add(3);
			affectedEntities.Add(5);
			simEvent1.affectedEntities = affectedEntities;
			sampleContainer.AddEvent (simEvent1);

			List <SimulationEvent> events = sampleContainer.GetEvents ();
            Assert.AreEqual(events[0].affectedEntities[0], 3);
			Assert.AreEqual(events[0].affectedEntities[1], 5);
		}

        [Test]
		public void TestTriggeringEntities()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			//create an event
			SimulationEvent simEvent1 = new SimulationEvent();
			List<long> triggeringEntities = new List<long>();
			triggeringEntities.Add(13);
			triggeringEntities.Add(15);
			simEvent1.triggeringEntities = triggeringEntities;
			sampleContainer.AddEvent (simEvent1);

			List <SimulationEvent> events = sampleContainer.GetEvents ();
            Assert.AreEqual(events[0].triggeringEntities[0], 13);
			Assert.AreEqual(events[0].triggeringEntities[1], 15);
		}

		[Test]
		public void TestTimeStampToSampleIdx_SamplesBeginAtZero()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			int sampleIdx = sampleContainer.TimeStampToSampleIdx (300);

			Assert.AreEqual (sampleIdx, 3);
		}

		[Test]
		public void TestTimeStampToSampleIdx_DelayedSamplesBegin()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (700, sample);
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (600, sample);

			int sampleIdx = sampleContainer.TimeStampToSampleIdx (600);

			Assert.AreEqual (sampleIdx, 3);
		}

		[Test]
		public void TestTimeStampToSampleIdx_GapInSamples()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			//gap
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (600, sample);

			int sampleIdx = sampleContainer.TimeStampToSampleIdx (600);

			Assert.AreEqual (sampleIdx, 6);
		}

		[Test]
		public void TestSampleIdxToTimeStamp_SamplesBeginAtZero()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			int timeStamp = sampleContainer.SampleIdxToTimeStamp (3);

			Assert.AreEqual (timeStamp, 300);
		}

		[Test]
		public void TestSampleIdxToTimeStamp_DelayedSamplesBegin()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (700, sample);
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (600, sample);
			
			int timeStamp = sampleContainer.SampleIdxToTimeStamp (4);

			Assert.AreEqual(timeStamp, 700);
		}

		[Test]
		public void TestSampleIdxToTimeStamp_GapInSamples()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			//gap
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (600, sample);

			int timeStamp = sampleContainer.SampleIdxToTimeStamp (3);

			Assert.AreEqual (timeStamp, 300);
		}

		[Test]
		public void TestFirstTimeStamp_SamplesBeginAtZero()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			int timeStamp = sampleContainer.GetFirstTimeStamp ();

			Assert.AreEqual (timeStamp, 0);
		}

		[Test]
		public void TestFirstTimeStamp_DelayedSamplesBegin()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (700, sample);
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (600, sample);

			int timeStamp = sampleContainer.GetFirstTimeStamp ();

			Assert.AreEqual (timeStamp, 300);
		}

		[Test]
		public void TestFirstTimeStamp_GapInSamples()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			//gap
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (600, sample);

			int timeStamp = sampleContainer.GetFirstTimeStamp ();

			Assert.AreEqual (timeStamp, 0);
		}

		[Test]
		public void TestLastTimeStamp_SamplesBeginAtZero()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			int timeStamp = sampleContainer.GetLastTimeStamp ();

			Assert.AreEqual (timeStamp, 400);
		}

		[Test]
		public void TestLastTimeStamp_DelayedSamplesBegin()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (700, sample);
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (600, sample);

			int timeStamp = sampleContainer.GetLastTimeStamp ();

			Assert.AreEqual (timeStamp, 700);
		}

		[Test]
		public void TestLastTimeStamp_GapInSamples()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			//gap
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (600, sample);

			int timeStamp = sampleContainer.GetLastTimeStamp ();

			Assert.AreEqual (timeStamp, 600);
		}

		[Test]
		public void TestSampleCount_SamplesBeginAtZero()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			int count = sampleContainer.GetSampleCount ();

			Assert.AreEqual (count, 5);
		}

		[Test]
		public void TestSampleCount_DelayedSamplesBegin()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (700, sample);
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (600, sample);

			int count = sampleContainer.GetSampleCount ();

			Assert.AreEqual (count, 5);
		}

		[Test]
		public void TestSampleCount_GapInSamples()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			//gap
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (600, sample);

			int count = sampleContainer.GetSampleCount ();

			Assert.AreEqual (count, 5);
		}

		[Test]
		public void TestSamplingTime_SamplesBeginAtZero()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			sampleContainer.AddSample (200, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (400, sample);

			int samplingTime = sampleContainer.SamplingTime;

			Assert.AreEqual (samplingTime, 100);
		}

		[Test]
		public void TestSamplingTime_DelayedSamplesBegin()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (300, sample);
			sampleContainer.AddSample (700, sample);
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (600, sample);

			int samplingTime = sampleContainer.SamplingTime;

			Assert.AreEqual (samplingTime, 100);
		}

		[Test]
		public void TestSamplingTime_GapInSamples()
		{
			SampleContainer sampleContainer = Container.Resolve<SampleContainer> ();

			AgentSample[] sample = new AgentSample[1];
			AgentSample agentSample = new AgentSample();
			sample[0] = agentSample;

			sampleContainer.SamplingTime = 100;
			sampleContainer.AddSample (0, sample);
			sampleContainer.AddSample (100, sample);
			//gap
			sampleContainer.AddSample (400, sample);
			sampleContainer.AddSample (500, sample);
			sampleContainer.AddSample (600, sample);

			int samplingTime = sampleContainer.SamplingTime;

			Assert.AreEqual (samplingTime, 100);
		}
	}
}