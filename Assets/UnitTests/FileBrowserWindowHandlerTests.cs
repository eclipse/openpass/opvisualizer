﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading.Tasks;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.UI;
using Visualizer.Utilities;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class FileBrowserWindowHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind mocked classes
			IAppStorage appStorageMock = Substitute.For<IAppStorage> ();
			IFileHelper fileHelperMock = Substitute.For<IFileHelper> ();
			Container.Bind<IAppStorage> ().FromInstance (appStorageMock).AsSingle();
			Container.Bind<IFileHelper> ().FromInstance (fileHelperMock).AsSingle();

			//Bind test target
			Container.Bind<FileBrowserWndHandler>().AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		//We use UnityTests as they are executed as Coroutines, which is neccessary to test asynchronous UI behabiour
		[UnityTest]
		public IEnumerator TestReadLastSelectedDirectory()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels[0].text == "Folder1");
			Assert.That(labels[1].text == "Folder2");
			Assert.That(labels[2].text == "File1.txt");
			Assert.That(labels[3].text == "File2.txt");
		}

		[UnityTest]
		public IEnumerator TestSaveLastSelectedDirectory()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\ABC");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			appStorageMock.Received(1).Write("LastSelectedPath", "C:\\ABC");
		}

		[UnityTest]
		public IEnumerator TestFileFilter()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.xml", FileType.File, false));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>().ToList(labels);
			Assert.That(!labels[0].ClassListContains("disabled-list-item"));
			Assert.That(!labels[1].ClassListContains("disabled-list-item"));
			Assert.That(!labels[2].ClassListContains("disabled-list-item"));
			Assert.That(labels[3].ClassListContains("disabled-list-item"));
		}

		[UnityTest]
		public IEnumerator TestFileIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File3.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var folderIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-folder").ToList(folderIcons);
			Assert.That(folderIcons[0].style.display == DisplayStyle.Flex);
			Assert.That(folderIcons[1].style.display == DisplayStyle.Flex);
			Assert.That(folderIcons[2].style.display == DisplayStyle.None);
			Assert.That(folderIcons[3].style.display == DisplayStyle.None);
			Assert.That(folderIcons[4].style.display == DisplayStyle.None);

			var fileIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-file").ToList(fileIcons);
			Assert.That(fileIcons[0].style.display == DisplayStyle.None);
			Assert.That(fileIcons[1].style.display == DisplayStyle.None);
			Assert.That(fileIcons[2].style.display == DisplayStyle.Flex);
			Assert.That(fileIcons[3].style.display == DisplayStyle.Flex);
			Assert.That(fileIcons[4].style.display == DisplayStyle.Flex);
		}

		[UnityTest]
		public IEnumerator TestFileHighlight()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var items = new List<VisualElement>();
			listView.Query<VisualElement>(className: "unity-list-view__item").ToList(items);
			Assert.That(!items[2].ClassListContains("unity-collection-view__item--selected"));

			listView.SetSelection(2);
			yield return null;

			Assert.That(items[2].ClassListContains("unity-collection-view__item--selected"));
		}

		[UnityTest]
		public IEnumerator TestFileSelection()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);
			string selectedFile = "";
			fileBrowserWndHandler.FileSelected += (path) => selectedFile = path;

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(2);

			Button okButton = window.Q<Button>("ok-btn");
			okButton.SendEvent(new ClickEvent{target = okButton});

			Assert.That(selectedFile == "C:\\Test\\File1.txt");
		}

		[UnityTest]
		public IEnumerator TestCancelFileSelection()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);
			string selectedFile = "abcd";
			fileBrowserWndHandler.FileSelected += (path) => selectedFile = path;

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(2);

			Button cancelButton = window.Q<Button>("cancel-btn");
			cancelButton.SendEvent(new ClickEvent{target = cancelButton});

			Assert.That(selectedFile == "");
		}

		[UnityTest]
		public IEnumerator TestEnableOKButtonAfterClickOnActiveFile()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == false);

			ListView listView = window.Q<ListView>();
			listView.SetSelection(2);

			Assert.That(okButton.enabledInHierarchy == true);
		}

		[UnityTest]
		public IEnumerator TestDisableOKButtonAfterClickOnInactiveFile()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, false));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == false);

			ListView listView = window.Q<ListView>();
			listView.SetSelection(2);

			Assert.That(okButton.enabledInHierarchy == false);
		}

		[UnityTest]
		public IEnumerator TestDisableOKButtonAfterClickOnFolder()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			List<FileListEntry> fileList = new List<FileListEntry>();
			fileList.Add(new FileListEntry("Folder1", FileType.Folder, true)); //even if we set active true, folders must always disable okButton 
			fileList.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList);
			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == false);

			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);

			Assert.That(okButton.enabledInHierarchy == false);
		}

		[UnityTest]
		public IEnumerator TestExitFolder()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();

			List<FileListEntry> fileList1 = new List<FileListEntry>();
			fileList1.Add(new FileListEntry("Folder1", FileType.Folder, true));
			fileList1.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList1.Add(new FileListEntry("File1.txt", FileType.File, true));
			fileList1.Add(new FileListEntry("File2.txt", FileType.File, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test", "txt").Returns(fileList1);

			List<FileListEntry> fileList2 = new List<FileListEntry>();
			fileList2.Add(new FileListEntry("Folder1.1", FileType.Folder, true));
			fileList2.Add(new FileListEntry("File1.1.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("C:\\Test\\Folder1", "txt").Returns(fileList2);

			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Test\\Folder1");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "Folder1.1");
			Assert.That(labels[1].text == "File1.1.txt");

			//click exit
			Button exitFolderButton = window.Q<Button>("exit-folder-btn");
			exitFolderButton.SendEvent(new ClickEvent{target = exitFolderButton});

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;

			labels.Clear();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 4);
			Assert.That(labels[0].text == "Folder1");
			Assert.That(labels[1].text == "Folder2");
			Assert.That(labels[2].text == "File1.txt");
			Assert.That(labels[3].text == "File2.txt");
		}

		[UnityTest]
		public IEnumerator TestPathButtonCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();

			List<FileListEntry> fileList = new List<FileListEntry>();
			fileHelperMock.ReadDirectoryContents("C:\\Folder1\\Folder2\\Folder3", "txt").Returns(fileList);

			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Folder1\\Folder2\\Folder3");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			VisualElement pathButtonContainer = window.Q<VisualElement>("path-btn-container");
			var pathButtons = new List<Button>();
			pathButtonContainer.Query<Button>().ToList(pathButtons);
			Assert.That(pathButtons.Count == 4);
			Assert.That(pathButtons[0].text == "C:\\");
			Assert.That(pathButtons[1].text == "Folder1\\");
			Assert.That(pathButtons[2].text == "Folder2\\");
			Assert.That(pathButtons[3].text == "Folder3\\");
		}

		[UnityTest]
		public IEnumerator TestPathButtonFunction()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();

			List<FileListEntry> fileList1 = new List<FileListEntry>();
			fileList1.Add(new FileListEntry("Folder2", FileType.Folder, true));
			fileList1.Add(new FileListEntry("File2.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("C:\\Folder1\\", "txt").Returns(fileList1);

			List<FileListEntry> fileList2 = new List<FileListEntry>();
			fileList2.Add(new FileListEntry("Folder3", FileType.Folder, true));
			fileList2.Add(new FileListEntry("File3.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("C:\\Folder1\\Folder2", "txt").Returns(fileList2);

			List<FileListEntry> fileList3 = new List<FileListEntry>();
			fileList3.Add(new FileListEntry("Folder4", FileType.Folder, true));
			fileList3.Add(new FileListEntry("File4.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("C:\\Folder1\\Folder2\\Folder3", "txt").Returns(fileList3);

			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\Folder1\\Folder2\\Folder3");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;

			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "Folder4");
			Assert.That(labels[1].text == "File4.txt");

			//click Folder1
			VisualElement pathButtonContainer = window.Q<VisualElement>("path-btn-container");
			List<Button> pathButtons = new List<Button>();
			pathButtonContainer.Query<Button>().ToList(pathButtons);
			pathButtons[1].SendEvent(new ClickEvent{target = pathButtons[1]});

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;

			labels.Clear();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "Folder2");
			Assert.That(labels[1].text == "File2.txt");
		}

		[UnityTest]
		public IEnumerator TestDriveButtonCreation()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();

			List<FileListEntry> fileList1 = new List<FileListEntry>();
			fileList1.Add(new FileListEntry("FolderC", FileType.Folder, true));
			fileList1.Add(new FileListEntry("FileC.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("C:\\", "txt").Returns(fileList1);

			List<FileListEntry> fileList2 = new List<FileListEntry>();
			fileList2.Add(new FileListEntry("FolderD", FileType.Folder, true));
			fileList2.Add(new FileListEntry("FileD.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("D:\\", "txt").Returns(fileList2);

			List<FileListEntry> fileList3 = new List<FileListEntry>();
			fileList3.Add(new FileListEntry("FolderE", FileType.Folder, true));
			fileList3.Add(new FileListEntry("FileE.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("E:\\", "txt").Returns(fileList3);

			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:", "D:", "E:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			VisualElement driveButtonContainer = window.Q<VisualElement>("drive-btn-container");
			var driveButtons = new List<Button>();
			driveButtonContainer.Query<Button>().ToList(driveButtons);
			Assert.That(driveButtons.Count == 3);
			Assert.That(driveButtons[0].text == "C:");
			Assert.That(driveButtons[1].text == "D:");
			Assert.That(driveButtons[2].text == "E:");
		}

		[UnityTest]
		public IEnumerator TestDriveButtonFunction()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FileBrowserWndHandler fileBrowserWndHandler = Container.Resolve<FileBrowserWndHandler> ();

			IAppStorage appStorageMock = Container.Resolve<IAppStorage> ();
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();

			List<FileListEntry> fileList1 = new List<FileListEntry>();
			fileList1.Add(new FileListEntry("FolderC", FileType.Folder, true));
			fileList1.Add(new FileListEntry("FileC.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("C:\\", "txt").Returns(fileList1);

			List<FileListEntry> fileList2 = new List<FileListEntry>();
			fileList2.Add(new FileListEntry("FolderD", FileType.Folder, true));
			fileList2.Add(new FileListEntry("FileD.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("D:", "txt").Returns(fileList2);

			List<FileListEntry> fileList3 = new List<FileListEntry>();
			fileList3.Add(new FileListEntry("FolderE", FileType.Folder, true));
			fileList3.Add(new FileListEntry("FileE.txt", FileType.Folder, true));
			fileHelperMock.ReadDirectoryContents("E:", "txt").Returns(fileList3);

			fileHelperMock.DirectoryExists(default).ReturnsForAnyArgs(true);
			fileHelperMock.GetSystemDrives().Returns(new List<string>{"C:", "D:", "E:"});
			appStorageMock.Read("LastSelectedPath").Returns("C:\\");

            var template = Resources.Load<VisualTreeAsset>("UI/FileBrowserWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			fileBrowserWndHandler.Init(window, "txt", 300);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;

			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "FolderC");
			Assert.That(labels[1].text == "FileC.txt");

			//click D:\\
			VisualElement driveButtonContainer = window.Q<VisualElement>("drive-btn-container");
			List<Button> driveButtons = new List<Button>();
			driveButtonContainer.Query<Button>().ToList(driveButtons);
			driveButtons[1].SendEvent(new ClickEvent{target = driveButtons[1]});

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;

			labels.Clear();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "FolderD");
			Assert.That(labels[1].text == "FileD.txt");

			//click E:\\
			driveButtons[2].SendEvent(new ClickEvent{target = driveButtons[2]});

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;

			labels.Clear();
			listView.Query<Label>().ToList(labels);
			Assert.That(labels.Count == 2);
			Assert.That(labels[0].text == "FolderE");
			Assert.That(labels[1].text == "FileE.txt");
		}
	}
}