/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using Visualizer.UI;
using Visualizer.Utilities;
using Visualizer.AgentHandling;
using Visualizer.CameraHandling;
using Visualizer.Core;
using NUnit.Framework;
using NSubstitute;
using NSubstitute.ReturnsExtensions;

namespace UITests
{
	[TestFixture]
	public class MouseEventHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<MouseEventHandler> ().FromNewComponentOnNewGameObject().AsSingle();

			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

			ISimulationLoader simulationLoaderMock = Substitute.For<ISimulationLoader> ();
			Container.Bind<ISimulationLoader> ().FromInstance (simulationLoaderMock).AsSingle();

			IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
			Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();

			IUIModeHandler uiModeHandlerMock = Substitute.For<IUIModeHandler> ();
			Container.Bind<IUIModeHandler> ().FromInstance (uiModeHandlerMock).AsSingle();

			IPanelResizer panelResizerMock = Substitute.For<IPanelResizer> ();
			Container.Bind<IPanelResizer> ().FromInstance (panelResizerMock).AsSingle();

			IRoadExplorer roadExplorerMock = Substitute.For<IRoadExplorer> ();
			Container.Bind<IRoadExplorer> ().FromInstance (roadExplorerMock).AsSingle();

			IMainUI uiMainMock = Substitute.For<IMainUI> ();
			Container.Bind<IMainUI> ().FromInstance (uiMainMock).AsSingle();

			IUnityIO unityIOMock = Substitute.For<IUnityIO> ();
			Container.Bind<IUnityIO> ().FromInstance (unityIOMock).AsSingle();
		}

	    [Test]
	    public void TestLeftMouseClickOnRoad()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IPanelResizer panelResizerMock = Container.Resolve<IPanelResizer> ();
			IRoadExplorer roadExplorerMock = Container.Resolve<IRoadExplorer> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			panelResizerMock.Received(1).HandleMouseUp(mousePos);
			roadExplorerMock.Received(1).HighlightTransform(mousePos);
	    }

	    [Test]
	    public void TestLeftMouseClickOnRoadInSensorPositionerMode()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IPanelResizer panelResizerMock = Container.Resolve<IPanelResizer> ();
			IRoadExplorer roadExplorerMock = Container.Resolve<IRoadExplorer> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SensorPositioner);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			//must not highlight road
			roadExplorerMock.ReceivedWithAnyArgs(0).HighlightTransform(default);
	    }

	    [Test]
	    public void TestRightMouseClickOnAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "Agent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//right mouse down
			unityIOMock.GetMouseButtonDown(1).Returns(true);
			mouseEventHandler.Update();

			//right mouse up
			unityIOMock.GetMouseButtonDown(1).Returns(false);
			unityIOMock.GetMouseButtonUp(1).Returns(true);
			mouseEventHandler.Update();

			uiMock.ReceivedWithAnyArgs(1).ShowAgentPopupMenu(clickedAgent, default);
	    }

	    [Test]
	    public void TestRightMouseClickOnAgentInSensorPositionerMode()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SensorPositioner);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "Agent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//right mouse down
			unityIOMock.GetMouseButtonDown(1).Returns(true);
			mouseEventHandler.Update();

			//right mouse up
			unityIOMock.GetMouseButtonDown(1).Returns(false);
			unityIOMock.GetMouseButtonUp(1).Returns(true);
			mouseEventHandler.Update();

			//must not show agent menu
			uiMock.ReceivedWithAnyArgs(0).ShowAgentPopupMenu(default, default);
	    }

	    [Test]
	    public void TestRightMouseClickOnPerceivedAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "PerceivedAgent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//right mouse down
			unityIOMock.GetMouseButtonDown(1).Returns(true);
			mouseEventHandler.Update();

			//right mouse up
			unityIOMock.GetMouseButtonDown(1).Returns(false);
			unityIOMock.GetMouseButtonUp(1).Returns(true);
			mouseEventHandler.Update();

			uiMock.ReceivedWithAnyArgs(1).ShowPerceivedAgentPopupMenu(clickedAgent, default);
	    }

	    [Test]
	    public void TestCtrlLeftMouseClickOnAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "Agent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//ctrl + left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			unityIOMock.GetKey(KeyCode.LeftControl).Returns(true);
			mouseEventHandler.Update();

			//Assert that the opening of an agent panel is triggered 
			uiMock.Received(1).ShowNonFocusAgentPanel(clickedAgent);
	    }

	    [Test]
	    public void TestCtrlLeftMouseClickOnPerceivedAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "PerceivedAgent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//ctrl + left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			unityIOMock.GetKey(KeyCode.LeftControl).Returns(true);
			mouseEventHandler.Update();

			//Assert that the opening of an agent panel is triggered 
			uiMock.Received(1).ShowPerceivedAgentPanel(clickedAgent);
	    }

	    [Test]
	    public void TestCtrlLeftMouseClickOnNonAgentObject()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//ctrl + left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			unityIOMock.GetKey(KeyCode.LeftControl).Returns(true);
			mouseEventHandler.Update();

			//Assert that the opening of an agent panel is triggered 
			uiMock.ReceivedWithAnyArgs(0).ShowNonFocusAgentPanel(default);
			uiMock.ReceivedWithAnyArgs(0).ShowPerceivedAgentPanel(default);
	    }

	    [Test]
	    public void TestShiftLeftMouseClickOnAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "Agent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//shift + left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			unityIOMock.GetKey(KeyCode.LeftShift).Returns(true);
			mouseEventHandler.Update();

			//Assert that the displaying of the agent's driver perception is triggered 
			uiMock.Received(1).ShowDriverPerception(clickedAgent);
	    }

	    [Test]
	    public void TestLeftMouseDrag()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IPanelResizer panelResizerMock = Container.Resolve<IPanelResizer> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);

			unityIOMock.GetMouseButton(0).Returns(true);
			mouseEventHandler.Update();

			panelResizerMock.Received(1).HandleMouseDrag(mousePos, false);
	    }

		[Test]
	    public void TestDoubleClickOnAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "Agent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			Thread.Sleep(100); //wait a bit, between the two clicks
			
			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			//Assert that the camera is focused on the clicked agent
			focusCameraHandlerMock.Received(1).FocusCamera (clickedAgent.ID);
	    }

		[Test]
	    public void TestDoubleClickOnPerceivedAgent()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "PerceivedAgent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			Thread.Sleep(100); //wait a bit, between the two clicks

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			//Assert that the camera focus change is not triggered
			focusCameraHandlerMock.Received(0).FocusCamera (clickedAgent.ID);
	    }

		[Test]
	    public void TestDoubleClickOnAgentInSensorPositioner()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SensorPositioner);

			GameObject clickedObject = new GameObject();
			Agent clickedAgent = clickedObject.AddComponent<Agent>();
			clickedAgent.ID = 1;
			clickedAgent.tag = "Agent";
			clickedAgent.TargetPosition = new Vector3(1f, 2f, 3f);
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			Thread.Sleep(100); //wait a bit, between the two clicks

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			//Assert that the camera is not focused
			focusCameraHandlerMock.ReceivedWithAnyArgs(0).FocusCamera (default);
	    }

		[Test]
	    public void TestDoubleClickOnTerrain()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SimulationPlayer);

			GameObject clickedObject = new GameObject();
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			Thread.Sleep(100); //wait a bit, between the two clicks

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			//Assert that camera focused release is triggered
			focusCameraHandlerMock.Received(1).FocusCamera (-1);
	    }

		[Test]
	    public void TestDoubleClickOnTerrainInSensorPositioner()
	    {
			MouseEventHandler mouseEventHandler = Container.Resolve<MouseEventHandler> ();

			IMainUI uiMock = Container.Resolve<IMainUI> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IUIModeHandler uiModeHandlerMock = Container.Resolve<IUIModeHandler> ();
			Vector3 mousePos = new Vector3(1f, 2f, 3f);
			unityIOMock.GetMousePos().Returns(mousePos);
			unityIOMock.IsMouseOverUI().Returns(false);
			uiModeHandlerMock.GetMode().Returns(UIMode.SensorPositioner);

			GameObject clickedObject = new GameObject();
			unityIOMock.GetGameObjectAtScreenPoint(default).ReturnsForAnyArgs(clickedObject);

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			Thread.Sleep(100); //wait a bit, between the two clicks

			//left mouse down
			unityIOMock.GetMouseButtonDown(0).Returns(true);
			mouseEventHandler.Update();

			//left mouse up
			unityIOMock.GetMouseButtonDown(0).Returns(false);
			unityIOMock.GetMouseButtonUp(0).Returns(true);
			mouseEventHandler.Update();

			//Assert that camera focused release is not triggered
			focusCameraHandlerMock.ReceivedWithAnyArgs(0).FocusCamera (default);
	    }
	}
}