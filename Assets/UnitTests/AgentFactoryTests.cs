/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using Visualizer.AgentHandling;
using Visualizer.Utilities;
using NUnit.Framework;
using NSubstitute;
using NSubstitute.ReturnsExtensions;

namespace AgentTest
{

	[TestFixture]
	public class AgentFactoryTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind test target
			Container.Bind<AgentFactory> ().AsSingle();

			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent>().FromNewComponentOnNewGameObject().AsTransient();

			//Bind mock classes
			IUnityIO unityIOMock = Substitute.For<IUnityIO> ();
			Container.Bind<IUnityIO>().FromInstance (unityIOMock).AsSingle();
		}

	    [Test]
	    public void CheckSpawnedAgentID()
	    {
			AgentFactory agentFactory = Container.Resolve<AgentFactory> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();

			//Create dummy agent properties that we will use as input for the spawning
			AgentProperties agentProps = new AgentProperties ();
			agentProps.ID = 5;
			agentProps.VehicleModelName = "car_test";
			agentProps.Sensors = new List<SensorAttributes> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.gameObject.AddComponent<BoxCollider>();

			unityIOMock.LoadAsset("car_test").Returns (dummyAgent.gameObject);

			Agent spawnedAgent = agentFactory.SpawnAgent (agentProps);

			Assert.That (spawnedAgent.ID == 5);
	    }

	    [Test]
	    public void CheckSpawnedAgentDriverViewDistance()
	    {
			AgentFactory agentFactory = Container.Resolve<AgentFactory> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();

			//Create dummy agent properties that we will use as input for the spawning
			AgentProperties agentProps = new AgentProperties ();
			agentProps.ID = 5;
			agentProps.DriverViewDistance = 123f;
			agentProps.VehicleModelName = "car_test";
			agentProps.Sensors = new List<SensorAttributes> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.gameObject.AddComponent<BoxCollider>();

			unityIOMock.LoadAsset("car_test").Returns (dummyAgent.gameObject);

			Agent spawnedAgent = agentFactory.SpawnAgent (agentProps);

			Assert.That (spawnedAgent.DriverViewDistance == 123f);
	    }

	    [Test]
	    public void CheckSpawnedAgentStaticInfo()
	    {
			AgentFactory agentFactory = Container.Resolve<AgentFactory> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();

			//Create dummy agent properties that we will use as input for the spawning
			AgentProperties agentProps = new AgentProperties ();
			agentProps.ID = 5;
			agentProps.VehicleModelName = "car_test";
			agentProps.Sensors = new List<SensorAttributes> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.gameObject.AddComponent<BoxCollider>();

			unityIOMock.LoadAsset("car_test").Returns (dummyAgent.gameObject);

			Agent spawnedAgent = agentFactory.SpawnAgent (agentProps);

			Assert.That (spawnedAgent.StaticAgentInfo == agentProps);
	    }

	    [Test]
	    public void CheckSpawnedAgentTag()
	    {
			AgentFactory agentFactory = Container.Resolve<AgentFactory> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();

			//Create dummy agent properties that we will use as input for the spawning
			AgentProperties agentProps = new AgentProperties ();
			agentProps.ID = 5;
			agentProps.VehicleModelName = "car_test";
			agentProps.Sensors = new List<SensorAttributes> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.gameObject.AddComponent<BoxCollider>();

			unityIOMock.LoadAsset("car_test").Returns (dummyAgent.gameObject);

			Agent spawnedAgent = agentFactory.SpawnAgent (agentProps);

			Assert.That (spawnedAgent.tag == "Agent");
	    }

		[Test]
		public void TryToSpawnNonExistingVehicleType()
		{
			AgentFactory agentFactory = Container.Resolve<AgentFactory> ();
			IUnityIO unityIOMock = Container.Resolve<IUnityIO> ();

			//Create dummy agent properties that we will use as input for the spawning
			AgentProperties agentProps = new AgentProperties ();
			agentProps.ID = 5;
			agentProps.VehicleModelName = "nonexisting_car_test";
			agentProps.Sensors = new List<SensorAttributes> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.gameObject.AddComponent<BoxCollider>();

			// spawn must fall back to default vehicle if vehicle type is unknown
			unityIOMock.LoadAsset("nonexisting_car_test").ReturnsNull();

			LogAssert.Expect(LogType.Error, "Could not load vehicle type 'nonexisting_car_test'. Falling back to default vehicle.");

			Agent spawnedAgent = agentFactory.SpawnAgent (agentProps);

			Assert.That (spawnedAgent.ID == 5);
		}
	}
}