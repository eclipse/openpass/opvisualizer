﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading.Tasks;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class FocusAgentSelectorWindowHandlerTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{

			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

			IAgentManager agentManagerMock = Substitute.For<IAgentManager> ();
			Container.Bind<IAgentManager> ().FromInstance (agentManagerMock).AsSingle();

			IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
			Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();

			IAnimationController animationControllerMock = Substitute.For<IAnimationController> ();
			Container.Bind<IAnimationController> ().FromInstance (animationControllerMock).AsSingle();

			//Bind test target
			Container.Bind<FocusAgentSelectorWndHandler>().AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		//We use UnityTests as they are executed as Coroutines, which is neccessary to test asynchronous UI behabiour
		[UnityTest]
		public IEnumerator TestDisplayAvailableAgents()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			Agent dummyAgent3 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent3.ID = 3;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentIDs.Add(3);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);
			agentManagerMock.GetAgent(3).Returns(dummyAgent3);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-lbl").ToList(labels);
			Assert.That(labels[0].text == "None");
			Assert.That(labels[1].text == "Agent 1");
			Assert.That(labels[2].text == "Agent 2");
			Assert.That(labels[3].text == "Agent 3");
		}

		[UnityTest]
		public IEnumerator TestCarIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.ID = 1;
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Car;

			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var agentIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-agent-car").ToList(agentIcons);
			Assert.That(agentIcons.Count == 2);
			Assert.That(agentIcons[0].style.display == DisplayStyle.Flex);
			Assert.That(agentIcons[1].style.display == DisplayStyle.Flex);
		}

		[UnityTest]
		public IEnumerator TestPedestrianIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.ID = 1;
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Pedestrian;

			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var agentIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-agent-pedestrian").ToList(agentIcons);
			Assert.That(agentIcons.Count == 2);
			Assert.That(agentIcons[0].style.display == DisplayStyle.None);
			Assert.That(agentIcons[1].style.display == DisplayStyle.Flex);
		}

		[UnityTest]
		public IEnumerator TestTruckIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.ID = 1;
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Truck;

			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var agentIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-agent-truck").ToList(agentIcons);
			Assert.That(agentIcons.Count == 2);
			Assert.That(agentIcons[0].style.display == DisplayStyle.None);
			Assert.That(agentIcons[1].style.display == DisplayStyle.Flex);
		}

		[UnityTest]
		public IEnumerator TestUnknownAgentTypeIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.ID = 1;
			dummyAgent.StaticAgentInfo = new AgentProperties();
			dummyAgent.StaticAgentInfo.VehicleModelType = VehicleModelType.Unknown;

			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent);
			agentManagerMock.GetAgent(1, false).Returns(dummyAgent);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var agentIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-agent-car").ToList(agentIcons);
			Assert.That(agentIcons.Count == 2);
			Assert.That(agentIcons[0].style.display == DisplayStyle.Flex);
			Assert.That(agentIcons[1].style.display == DisplayStyle.Flex);
		}

		[UnityTest]
		public IEnumerator TestFocusAgentLabelHighlight()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent1);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-lbl").ToList(labels);
			Assert.That(!labels[0].ClassListContains("selected-list-item"));
			Assert.That(labels[1].ClassListContains("selected-list-item"));
			Assert.That(!labels[2].ClassListContains("selected-list-item"));
		}

		[UnityTest]
		public IEnumerator TestFocusAgentSublabelHighlight()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent1);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-sub-lbl").ToList(labels);
			Assert.That(!labels[0].ClassListContains("selected-list-item"));
			Assert.That(labels[1].ClassListContains("selected-list-item"));
			Assert.That(!labels[2].ClassListContains("selected-list-item"));
		}

		[UnityTest]
		public IEnumerator TestFocusAgentIconHighlight()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			focusCameraHandlerMock.GetFocusedAgent().Returns(dummyAgent1);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var agentIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-agent-truck").ToList(agentIcons);
			Assert.That(!agentIcons[0].ClassListContains("selected-list-icon"));
			Assert.That(agentIcons[1].ClassListContains("selected-list-icon"));
			Assert.That(!agentIcons[2].ClassListContains("selected-list-icon"));
		}

		[UnityTest]
		public IEnumerator TestSubLabel()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.StaticAgentInfo = new AgentProperties();
			dummyAgent1.StaticAgentInfo.TypeGroupName = "WhateverAgentType";
			dummyAgent2.StaticAgentInfo = new AgentProperties();
			dummyAgent2.StaticAgentInfo.TypeGroupName = "Common";
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-sub-lbl").ToList(labels);
			Assert.That(labels[0].text == "");
			Assert.That(labels[1].text == "(WhateverAgentType)");
			Assert.That(labels[2].text == "");
		}

		[UnityTest]
		public IEnumerator TestSubLabelCollisionAgent()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.CrashStatus = Crash.WillCrash;
			dummyAgent2.CrashStatus = Crash.NoCrash;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-sub-lbl").ToList(labels);
			Assert.That(labels[0].text == "");
			Assert.That(labels[1].text == " (Collision)");
			Assert.That(labels[2].text == "");
		}

		[UnityTest]
		public IEnumerator TestSubLabelEgoCollisionAgent()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.StaticAgentInfo = new AgentProperties();
			dummyAgent1.StaticAgentInfo.TypeGroupName = "Ego";
			dummyAgent1.CrashStatus = Crash.WillCrash;
			dummyAgent2.CrashStatus = Crash.NoCrash;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-sub-lbl").ToList(labels);
			Assert.That(labels[0].text == "");
			Assert.That(labels[1].text == "(Ego) (Collision)");
			Assert.That(labels[2].text == "");
		}

		[UnityTest]
		public IEnumerator TestDisabledAgentLabel()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-lbl").ToList(labels);
			Assert.That(!labels[0].ClassListContains("disabled-list-item"));
			Assert.That(!labels[1].ClassListContains("disabled-list-item"));
			Assert.That(labels[2].ClassListContains("disabled-list-item"));
		}

		[UnityTest]
		public IEnumerator TestDisabledAgentSublabel()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var labels = new List<Label>();
			listView.Query<Label>("agent-sub-lbl").ToList(labels);
			Assert.That(!labels[0].ClassListContains("disabled-list-item"));
			Assert.That(!labels[1].ClassListContains("disabled-list-item"));
			Assert.That(labels[2].ClassListContains("disabled-list-item"));
		}

		[UnityTest]
		public IEnumerator TestDisabledAgentIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			var agentIcons = new List<VisualElement>();
			listView.Query<VisualElement>("icon-agent-truck").ToList(agentIcons);
			Assert.That(!agentIcons[0].ClassListContains("disabled-list-icon"));
			Assert.That(!agentIcons[1].ClassListContains("disabled-list-icon"));
			Assert.That(agentIcons[2].ClassListContains("disabled-list-icon"));
		}

		[UnityTest]
		public IEnumerator TestEnableOKButtonIfAgentIsSelected()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);

			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == true);
		}

		[UnityTest]
		public IEnumerator TestDisableOKButtonIfNoAgentIsSelected()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == false);
		}

		[UnityTest]
		public IEnumerator TestDisableOKButtonIfDisabledAgentIsSelected()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent1.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);

			Button okButton = window.Q<Button>("ok-btn");
			Assert.That(okButton.enabledInHierarchy == false);
		}

		[UnityTest]
		public IEnumerator TestAgentSelection()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);
	
			bool focusAgentSelectedWasCalled = false;
			focusAgentSelectorWndHandler.FocusAgentSelected += () => focusAgentSelectedWasCalled = true;

			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);

			Button okButton = window.Q<Button>("ok-btn");
			okButton.SendEvent(new ClickEvent{target = okButton});

			Assert.That(focusAgentSelectedWasCalled == true);
			focusCameraHandlerMock.Received(1).FocusCamera(1);
		}

		[UnityTest]
		public IEnumerator TestOKButtonDoesNotWorkOnDisabledAgents()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);
	
			bool focusAgentSelectedWasCalled = false;
			focusAgentSelectorWndHandler.FocusAgentSelected += () => focusAgentSelectedWasCalled = true;

			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();

			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(2);

			Button okButton = window.Q<Button>("ok-btn");
			okButton.SendEvent(new ClickEvent{target = okButton});

			Assert.That(focusAgentSelectedWasCalled == false);
			focusCameraHandlerMock.Received(0).FocusCamera(2);
		}

		[UnityTest]
		public IEnumerator TestUpdateListOnSampleUpdate()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			FocusAgentSelectorWndHandler focusAgentSelectorWndHandler = Container.Resolve<FocusAgentSelectorWndHandler> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();

			Agent dummyAgent1 = Container.Resolve<Agent> ();
			Agent dummyAgent2 = Container.Resolve<Agent> ();
			dummyAgent1.ID = 1;
			dummyAgent2.ID = 2;
			dummyAgent1.TargetPosition = new Vector3(1f, 2f, 3f);
			dummyAgent2.TargetPosition = Vector3.zero;
			IAgentManager agentManagerMock = Container.Resolve<IAgentManager> ();
			List<int> agentIDs = new List<int>();
			agentIDs.Add(1);
			agentIDs.Add(2);
			agentManagerMock.GetAgentIDs().Returns(agentIDs);
			agentManagerMock.GetAgent(1).Returns(dummyAgent1);
			agentManagerMock.GetAgent(2).Returns(dummyAgent2);

            var template = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = template.CloneTree();
			root.Add(window);
			
			focusAgentSelectorWndHandler.Init(window);
	
			// wait one frame to let the list be built (GeometryChanged is executed)
			yield return null;
		
			ListView listView = window.Q<ListView>();
			listView.SetSelection(1);

			var labels = new List<Label>();
			listView.Query<Label>("agent-lbl").ToList(labels);
			Assert.That(!labels[0].ClassListContains("disabled-list-item"));
			Assert.That(!labels[1].ClassListContains("disabled-list-item"));
			Assert.That(labels[2].ClassListContains("disabled-list-item"));

			//agent is not anymore hidden
			dummyAgent2.TargetPosition = new Vector3(1f, 2f, 3f);
			agentManagerMock.GetAgent(2, false).Returns(dummyAgent2);

			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			animationControllerMock.SampleUpdated += Raise.Event<SampleUpdatedEventHandler>(123);

			// wait one frame to let the list be rebuilt
			yield return null;
			
			labels.Clear();
			listView.Query<Label>("agent-lbl").ToList(labels);
			Assert.That(!labels[0].ClassListContains("disabled-list-item"));
			Assert.That(!labels[1].ClassListContains("disabled-list-item"));
			Assert.That(!labels[2].ClassListContains("disabled-list-item"));

			Button okButton = window.Q<Button>("ok-btn");
			okButton.SendEvent(new ClickEvent{target = okButton});

			focusCameraHandlerMock.Received(0).FocusCamera(2);
		}
	}
}