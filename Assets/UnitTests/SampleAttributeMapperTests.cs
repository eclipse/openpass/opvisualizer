﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using Visualizer.Utilities;
using Visualizer.Trace;
using NUnit.Framework;
using NSubstitute;

namespace CoreTests
{
	[TestFixture]
	public class SampleAttributeMapperTests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			//Bind mocked classes
			IFileHelper fileHelperMock = Substitute.For<IFileHelper> ();
			Container.Bind<IFileHelper> ().FromInstance (fileHelperMock).AsSingle();

			//Bind test target
			Container.Bind<SampleAttributeMapper>().AsSingle();
		}

		[Test]
		public void TestInit()
		{
			SampleAttributeMapper sampleAttributeMapper = Container.Resolve<SampleAttributeMapper> ();

			string fileContent = "{}";
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			string fileName = "\\Config\\SampleAttrMaps.json";
			fileHelperMock.ReadFile(fileName).Returns(fileContent);
			fileHelperMock.FileExists(fileName).Returns(true);

			Assert.That(() => sampleAttributeMapper.Init(), Throws.Nothing);
		}

		[Test]
		public void TestInitInvalidFile()
		{
			SampleAttributeMapper sampleAttributeMapper = Container.Resolve<SampleAttributeMapper> ();

			string fileContent = "invalid content";
			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			string fileName = "\\Config\\SampleAttrMaps.json";
			fileHelperMock.ReadFile(fileName).Returns(fileContent);
			fileHelperMock.FileExists(fileName).Returns(true);

			Assert.That(() => sampleAttributeMapper.Init(), Throws.Exception);
		}

		[Test]
		public void TestInitNonExistentFile()
		{
			SampleAttributeMapper sampleAttributeMapper = Container.Resolve<SampleAttributeMapper> ();

			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			string fileName = "\\Config\\SampleAttrMaps.json";
			fileHelperMock.FileExists(fileName).Returns(false);

			Assert.That(() => sampleAttributeMapper.Init(), Throws.Nothing);
		}

		[Test]
		public void TestValueMapping_ExistingValues()
		{
			SampleAttributeMapper sampleAttributeMapper = Container.Resolve<SampleAttributeMapper> ();

			string fileContent = @"
			{
				AttributeMapList: [ 
					{
						AttributeName: 'TestAttribute',
						ValueMap: 
						{
							'0': 'zero',
							'1': 'one',
							'2': 'two'
						}
					}
				]
			}";

			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			string fileName = "\\Config\\SampleAttrMaps.json";
			fileHelperMock.ReadFile(fileName).Returns(fileContent);
			fileHelperMock.FileExists(fileName).Returns(true);

			sampleAttributeMapper.Init ();
			Assert.That(sampleAttributeMapper.MapValue("TestAttribute", "0") == "zero");
			Assert.That(sampleAttributeMapper.MapValue("TestAttribute", "1") == "one");
			Assert.That(sampleAttributeMapper.MapValue("TestAttribute", "2") == "two");
		}
		
		[Test]
		public void TestValueMapping_NonExistingValues()
		{
			SampleAttributeMapper sampleAttributeMapper = Container.Resolve<SampleAttributeMapper> ();

			string fileContent = @"
			{
				AttributeMapList: [ 
					{
						AttributeName: 'TestAttribute',
						ValueMap: 
						{
							'0': 'zero',
							'1': 'one',
							'2': 'two'
						}
					}
				]
			}";

			IFileHelper fileHelperMock = Container.Resolve<IFileHelper> ();
			string fileName = "\\Config\\SampleAttrMaps.json";
			fileHelperMock.ReadFile(fileName).Returns(fileContent);
			fileHelperMock.FileExists(fileName).Returns(true);

			sampleAttributeMapper.Init ();
			Assert.That(sampleAttributeMapper.MapValue("TestAttribute", "4") == "4");
			Assert.That(sampleAttributeMapper.MapValue("NotMappedAttribute", "1") == "1");
		}

		[Test]
		public void TestValueMapping_NonInitializedMapper()
		{
			SampleAttributeMapper sampleAttributeMapper = Container.Resolve<SampleAttributeMapper> ();

			Assert.That(sampleAttributeMapper.MapValue("TestAttribute", "1") == "1");
		}
	}
}