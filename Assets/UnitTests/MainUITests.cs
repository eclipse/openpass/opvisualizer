﻿/********************************************************************************
 * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

using System;
using System.Linq;
using System.Threading;
using Zenject;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Visualizer.Core;
using Visualizer.CameraHandling;
using Visualizer.AgentHandling;
using Visualizer.Utilities;
using Visualizer.Trace;
using Visualizer.UI;
using NUnit.Framework;
using NSubstitute;

namespace UITests
{
	[TestFixture]
	public class MainUITests : ZenjectUnitTestFixture
	{
		[SetUp]
		public void CommonInstall()
		{
			IDriverPerceptionManager driverPerceptionManagerMock = Substitute.For<IDriverPerceptionManager> ();
			IAnimationController animationControllerMock = Substitute.For<IAnimationController> ();
			IAgentManager agentManagerMock = Substitute.For<IAgentManager> ();
			IFileBrowserWndHandler fileBrowserWndHandlerMock = Substitute.For<IFileBrowserWndHandler> ();
			IRunSelectorWndHandler runSelectorWndHandlerMock = Substitute.For<IRunSelectorWndHandler> ();
			IFocusAgentSelectorWndHandler focusAgentSelectorWndHandlerMock = Substitute.For<IFocusAgentSelectorWndHandler> ();
			IAppVersion appVersionMock = Substitute.For<IAppVersion> ();
			IRoadHighlightModeUIHandler roadHighlightModeUIHandlerMock = Substitute.For<IRoadHighlightModeUIHandler> ();
			IViewModeHandler viewModeHandlerMock = Substitute.For<IViewModeHandler> ();
			IFocusCameraHandler focusCameraHandlerMock = Substitute.For<IFocusCameraHandler> ();
			IFocusFrameRenderer focusFrameRendererMock = Substitute.For<IFocusFrameRenderer> ();
			IPlaybackSpeedHandler playbackSpeedHandlerMock = Substitute.For<IPlaybackSpeedHandler> ();
			ITimeAxisHandler timeAxisHandlerMock = Substitute.For<ITimeAxisHandler> (); 
			IAgentPanelManager agentPanelManagerMock = Substitute.For<IAgentPanelManager> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Substitute.For<IDisplayOptionsProvider> ();
			IWindowController windowControllerMock = Substitute.For<IWindowController> ();
			IDisplayOptionsWndHandler displayOptionsWndHandlerMock = Substitute.For<IDisplayOptionsWndHandler> ();
			IStaticAgentInfoWndHandler staticAgentInfoWndHandlerMock = Substitute.For<IStaticAgentInfoWndHandler> ();
			IAppStorage appStorageMock = Substitute.For<IAppStorage> ();
			
			//Bind mocked classes
			Container.Bind<IDriverPerceptionManager> ().FromInstance (driverPerceptionManagerMock).AsSingle();
			Container.Bind<IAnimationController> ().FromInstance (animationControllerMock).AsSingle();
			Container.Bind<IAgentManager> ().FromInstance (agentManagerMock).AsSingle();
			Container.Bind<IFileBrowserWndHandler> ().FromInstance (fileBrowserWndHandlerMock).AsSingle();
			Container.Bind<IRunSelectorWndHandler> ().FromInstance (runSelectorWndHandlerMock).AsSingle();
			Container.Bind<IFocusAgentSelectorWndHandler> ().FromInstance (focusAgentSelectorWndHandlerMock).AsSingle();
			Container.Bind<IAppVersion> ().FromInstance (appVersionMock).AsSingle();
			Container.Bind<IRoadHighlightModeUIHandler> ().FromInstance (roadHighlightModeUIHandlerMock).AsSingle();
			Container.Bind<IViewModeHandler> ().FromInstance (viewModeHandlerMock).AsSingle();
			Container.Bind<IFocusCameraHandler> ().FromInstance (focusCameraHandlerMock).AsSingle();
			Container.Bind<IFocusFrameRenderer> ().FromInstance (focusFrameRendererMock).AsSingle();
			Container.Bind<IPlaybackSpeedHandler> ().FromInstance (playbackSpeedHandlerMock).AsSingle();
			Container.Bind<ITimeAxisHandler> ().FromInstance (timeAxisHandlerMock).AsSingle();
			Container.Bind<IAgentPanelManager> ().FromInstance (agentPanelManagerMock).AsSingle();
			Container.Bind<IDisplayOptionsProvider> ().FromInstance (displayOptionsProviderMock).AsSingle();
			Container.Bind<IWindowController> ().FromInstance (windowControllerMock).AsSingle();
			Container.Bind<IDisplayOptionsWndHandler> ().FromInstance (displayOptionsWndHandlerMock).AsSingle();
			Container.Bind<IStaticAgentInfoWndHandler> ().FromInstance (staticAgentInfoWndHandlerMock).AsSingle();
			Container.Bind<IAppStorage> ().FromInstance (appStorageMock).AsSingle();

			//Bind the Agent class so that we can create some on the fly when we ask for Container.Resolve<Agent> () (note that it is not a singleton, so every time a new agent is created)
			Container.Bind<Agent> ().FromNewComponentOnNewGameObject().AsTransient();

			//Bind test target
			Container.Bind<MainUI>().FromNewComponentOnNewGameObject().AsSingle();

			// for a functioning UI event handling, we need to set up a UIDocument with the PanelSettings and MainUI, into which we embed our test target
			Container.Bind<UIDocument>().FromNewComponentOnNewGameObject().AsSingle().OnInstantiated((InjectContext _, UIDocument ui) =>
			{
				ui.gameObject.name = "MainUI";
				ui.gameObject.AddComponent<EventSystem>();
				ui.panelSettings = Resources.Load<PanelSettings>("UI/Panel Settings");
				ui.visualTreeAsset = Resources.Load<VisualTreeAsset>("UI/UI-Main");
			}).NonLazy();
		}

		[Test]
		public void TestStartup()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IRoadHighlightModeUIHandler roadHighlightModeUIHandlerMock = Container.Resolve<IRoadHighlightModeUIHandler> ();
			IFocusFrameRenderer focusFrameRendererMock = Container.Resolve<IFocusFrameRenderer> ();
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			
			mainUI.Startup();

			windowControllerMock.Received(1).Init(root);
            agentPanelManagerMock.Received(1).Init(root);
            roadHighlightModeUIHandlerMock.Received(1).Init(root);
            focusFrameRendererMock.Received(1).Init(root);
		}

		[Test]
		public void TestInit()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IViewModeHandler viewModeHandlerMock = Container.Resolve<IViewModeHandler> ();
			IFocusCameraHandler focusCameraHandlerMock = Container.Resolve<IFocusCameraHandler> ();
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IPlaybackSpeedHandler playbackSpeedHandlerMock = Container.Resolve<IPlaybackSpeedHandler> ();
			ITimeAxisHandler timeAxisHandlerMock = Container.Resolve<ITimeAxisHandler> ();
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			
			mainUI.Startup();

			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

           	animationControllerMock.Received(1).Init ();
			viewModeHandlerMock.Received(1).Init (); 
			focusCameraHandlerMock.Received(1).Init ();
			playbackSpeedHandlerMock.Received(1).Init ();
			timeAxisHandlerMock.Received(1).Init (root);
			agentPanelManagerMock.Received(1).ClearFloatingPanels();
			agentPanelManagerMock.Received(1).CreateFloatingPanels();
		}

		[Test]
		public void TestClear()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();	

			MainUI mainUI = Container.Resolve<MainUI> ();
			
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			root.Q<VisualElement>("agent-menu").visible = true;
			root.Q<VisualElement>("main-menu").visible = true;

			mainUI.Clear();

           	driverPerceptionManagerMock.Received(1).Clear ();
			agentPanelManagerMock.Received(1).Clear (); 
			displayOptionsProviderMock.Received(1).Clear ();
			windowControllerMock.Received(1).Clear ();
			Assert.That(root.Q<VisualElement>("agent-menu").visible == false);
			Assert.That(root.Q<VisualElement>("main-menu").visible == false);
		}

		[Test]
		public void TestSimulationInfoTooltip()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			
			MainUI mainUI = Container.Resolve<MainUI> ();

			string origTooltip = root.Q<Label>("simulation-info-icon").tooltip;

			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			Assert.That(root.Q<Label>("simulation-info-icon").tooltip != origTooltip);
		}

		[Test]
		public void TestHideRunSelectorMenuItem()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			mainUI.HideRunSelectorMenuItem(true);
			Assert.That(root.Q<VisualElement>("load-run-menu-item").style.display == DisplayStyle.None);

			mainUI.HideRunSelectorMenuItem(false);
			Assert.That(root.Q<VisualElement>("load-run-menu-item").style.display == DisplayStyle.Flex);
		}

		[Test]
		public void TestOnDriverPerceptionLoadedSuccessfully()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			string origTooltip = root.Q<Label>("simulation-info-icon").tooltip;

			driverPerceptionManagerMock.DriverPerceptionLoaded += Raise.Event<DriverPerceptionChangeHandler>("file.txt", "");

            agentPanelManagerMock.Received(1).CreatePerceivedAgentFloatingPanels();
			Assert.That(root.Q<Label>("simulation-info-icon").tooltip != origTooltip);
		}

		[Test]
		public void TestOnDriverPerceptionLoadedWithFailure()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IDriverPerceptionManager driverPerceptionManagerMock = Container.Resolve<IDriverPerceptionManager> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			driverPerceptionManagerMock.DriverPerceptionLoaded += Raise.Event<DriverPerceptionChangeHandler>("file.txt", "error message");

			windowControllerMock.Received(1).ShowErrorMessage("Loading driver perception failed", "error message");
		}

		[UnityTest]
		public IEnumerator TestMainMenuButtonClick()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			Button mainMenuBtn = root.Q<Button>("main-menu-btn");
			mainMenuBtn.SendEvent(new ClickEvent{target = mainMenuBtn});

			yield return null; //wait one frame
			Assert.That(root.Q<VisualElement>("main-menu").visible == true);
		}

		[Test]
		public void TestGlobalDisplayOptionsButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IDisplayOptionsWndHandler displayOptionsWndHandlerMock = Container.Resolve<IDisplayOptionsWndHandler> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();	
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(new List<DisplayOption>());
			windowControllerMock.OpenWindow(default, default, default, default).ReturnsForAnyArgs(new VisualElement());
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			Button button = root.Q<Button>("global-display-options-btn");
			button.SendEvent(new ClickEvent{target = button});

			displayOptionsWndHandlerMock.ReceivedWithAnyArgs(1).Init(default, default);
			windowControllerMock.Received(1).OpenWindow("UI/DisplayOptionsWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
		}

		[Test]
		public void TestLoadTraceButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IFileBrowserWndHandler fileBrowserWndHandlerMock = Container.Resolve<IFileBrowserWndHandler> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			root.Q<VisualElement>("main-menu").visible = true;
			Button button = root.Q<Button>("load-trace-menu-item");
			button.SendEvent(new ClickEvent{target = button});

			fileBrowserWndHandlerMock.Received(1).Init(Arg.Any<VisualElement>(), "xml", Arg.Any<int>());
			windowControllerMock.Received(1).OpenWindow("UI/FileBrowserWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
		}

		[Test]
		public void TestLoadRunButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			bool readRunListRequestWasCalled = false;
			mainUI.ReadRunListRequest += () => readRunListRequestWasCalled = true;

			root.Q<VisualElement>("main-menu").visible = true;
			Button button = root.Q<Button>("load-run-menu-item");
			button.SendEvent(new ClickEvent{target = button});

			Assert.That(readRunListRequestWasCalled);
		}

		[Test]
		public void TestSensorPositionerButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			bool _switchUIModeRequestWasCalled = false;
			mainUI.SwitchUIModeRequest += (uiMode) => _switchUIModeRequestWasCalled = (uiMode == UIMode.SensorPositioner);

			root.Q<VisualElement>("main-menu").visible = true;
			Button button = root.Q<Button>("sensor-positioner-menu-item");
			button.SendEvent(new ClickEvent{target = button});

			Assert.That(_switchUIModeRequestWasCalled);
		}

		[Test]
		public void TestLoadSceneryButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IFileBrowserWndHandler fileBrowserWndHandlerMock = Container.Resolve<IFileBrowserWndHandler> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			root.Q<VisualElement>("main-menu").visible = true;
			Button button = root.Q<Button>("load-scenery-menu-item");
			button.SendEvent(new ClickEvent{target = button});

			fileBrowserWndHandlerMock.Received(1).Init(Arg.Any<VisualElement>(), "xodr", Arg.Any<int>());
			windowControllerMock.Received(1).OpenWindow("UI/FileBrowserWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
		}

		[Test]
		public void TestUsageHintsButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			root.Q<VisualElement>("main-menu").visible = true;
			Button button = root.Q<Button>("usage-hints-menu-item");
			button.SendEvent(new ClickEvent{target = button});

			windowControllerMock.Received(1).OpenWindow("UI/UsageHintsWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
		}

		[Test]
		public void TestVersionInfoButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IAppVersion appVersionMock = Container.Resolve<IAppVersion> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/VersionInfoWindow");
            VisualElement verisonInfoWindow = template.Instantiate();
			windowControllerMock.OpenWindow(default, default, default, default).ReturnsForAnyArgs(verisonInfoWindow);
			appVersionMock.VersionNumber.Returns("1.2.3");

			root.Q<VisualElement>("main-menu").visible = true;
			Button button = root.Q<Button>("version-info-menu-item");
			button.SendEvent(new ClickEvent{target = button});

			windowControllerMock.Received(1).OpenWindow("UI/VersionInfoWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>(), Arg.Any<int>());
			Assert.That(verisonInfoWindow.Q<Label>("version-lbl").text == "v1.2.3");
		}

		[Test]
		public void TestClickStartPlaybackButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			animationControllerMock.IsPlaying().Returns(false);
			Button button = root.Q<Button>("start-stop-btn");
			button.SendEvent(new ClickEvent{target = button});

			animationControllerMock.Received(1).StartPlayback();
		}

		[Test]
		public void TestClickStopPlaybackButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			animationControllerMock.IsPlaying().Returns(true);
			Button button = root.Q<Button>("start-stop-btn");
			button.SendEvent(new ClickEvent{target = button});

			animationControllerMock.Received(1).StopPlayback();
		}

		[Test]
		public void TestClickStepForwardButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			animationControllerMock.IsPlaying().Returns(true);
			Button button = root.Q<Button>("step-forward-btn");
			button.SendEvent(new ClickEvent{target = button});

			animationControllerMock.Received(1).JumpToNextSample();
		}

		[Test]
		public void TestClickStepBackwardsButton()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			animationControllerMock.IsPlaying().Returns(true);
			Button button = root.Q<Button>("step-back-btn");
			button.SendEvent(new ClickEvent{target = button});

			animationControllerMock.Received(1).JumpToPreviousSample();
		}

		[Test]
		public void TestUpdateStartPlaybackButtonIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			animationControllerMock.PlaybackStarted += Raise.Event<Action>();

			Assert.That(root.Q<Button>("start-stop-btn").ClassListContains("pause-image"));
			Assert.That(!root.Q<Button>("start-stop-btn").ClassListContains("play-image"));
		}

		[Test]
		public void TestUpdateStopPlaybackButtonIcon()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IAnimationController animationControllerMock = Container.Resolve<IAnimationController> ();

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			animationControllerMock.PlaybackStopped += Raise.Event<Action>();

			Assert.That(!root.Q<Button>("start-stop-btn").ClassListContains("pause-image"));
			Assert.That(root.Q<Button>("start-stop-btn").ClassListContains("play-image"));
		}

		[Test]
		public void TestRefreshPanelData()
		{
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.RefreshPanelData();

			agentPanelManagerMock.Received(1).RefreshPanels();
		}

		[Test]
		public void TestShowAgentPopupMenu()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent = Container.Resolve<Agent> ();

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			mainUI.ShowAgentPopupMenu(dummyAgent, new Vector3(1f,2f,3f));

			Assert.That(root.Q<VisualElement>("agent-menu").visible == true);
            Assert.That(root.Q<Button>("perceived-agent-info-menuitem").style.display == DisplayStyle.None);
			Assert.That(root.Q<Button>("dynamic-agent-info-menuitem").style.display == DisplayStyle.Flex);
            Assert.That(root.Q<Button>("static-agent-info-menuitem").style.display == DisplayStyle.Flex);
			Assert.That(root.Q<Button>("driver-perception-menuitem").style.display == DisplayStyle.None);
		}

		[Test]
		public void TestShowPerceivedAgentPopupMenu()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			Agent dummyAgent = Container.Resolve<Agent> ();

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			mainUI.ShowPerceivedAgentPopupMenu(dummyAgent, new Vector3(1f,2f,3f));

			Assert.That(root.Q<VisualElement>("agent-menu").visible == true);
            Assert.That(root.Q<Button>("perceived-agent-info-menuitem").style.display == DisplayStyle.Flex);
			Assert.That(root.Q<Button>("dynamic-agent-info-menuitem").style.display == DisplayStyle.None);
            Assert.That(root.Q<Button>("static-agent-info-menuitem").style.display == DisplayStyle.None);
			Assert.That(root.Q<Button>("driver-perception-menuitem").style.display == DisplayStyle.None);
		}

		[Test]
		public void TestHidePopupMenu()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			root.Q<VisualElement>("agent-menu").visible = true;
			root.Q<VisualElement>("main-menu").visible = true;

			mainUI.HidePopupMenu();

			Assert.That(root.Q<VisualElement>("agent-menu").visible == false);
			Assert.That(root.Q<VisualElement>("main-menu").visible == false);
		}

		[Test]
		public void TestShowNonFocusAgentPanel()
		{
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();	
			Agent dummyAgent = Container.Resolve<Agent> ();
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(new List<DisplayOption>());
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.ShowNonFocusAgentPanel(dummyAgent);

			agentPanelManagerMock.Received(1).ShowNonFocusAgentPanel(dummyAgent, Arg.Any<List<DisplayOption>>());
		}

		[Test]
		public void TestShowPerceivedAgentPanel()
		{
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();	
			Agent dummyAgent = Container.Resolve<Agent> ();
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(new List<DisplayOption>());
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			mainUI.ShowPerceivedAgentPanel(dummyAgent);

			agentPanelManagerMock.Received(1).ShowPerceivedAgentPanel(dummyAgent, Arg.Any<List<DisplayOption>>());
		}

		[Test]
		public void TestShowFocusAgentPanel()
		{
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IDisplayOptionsProvider displayOptionsProviderMock = Container.Resolve<IDisplayOptionsProvider> ();	
			Agent dummyAgent = Container.Resolve<Agent> ();
			displayOptionsProviderMock.Get(default).ReturnsForAnyArgs(new List<DisplayOption>());
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.ShowFocusAgentPanel(dummyAgent);

			agentPanelManagerMock.Received(1).ShowFocusAgentPanel(dummyAgent, Arg.Any<List<DisplayOption>>());
		}

		[Test]
		public void TestShowStaticAgentInfoWindow()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IStaticAgentInfoWndHandler staticAgentInfoWndHandlerMock = Container.Resolve<IStaticAgentInfoWndHandler> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			Agent dummyAgent = Container.Resolve<Agent> ();
			dummyAgent.ID = 123;
			dummyAgent.tag = "Agent";
			AgentProperties props = new AgentProperties();
			props.NonInterpretedProps = new List<GenericListItem>();
			dummyAgent.StaticAgentInfo = props;
			
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.ShowAgentPopupMenu(dummyAgent, Vector3.zero);
			
			root.Q<VisualElement>("agent-menu").visible = true;
			Button button = root.Q<Button>("static-agent-info-menuitem");
			button.SendEvent(new ClickEvent{target = button});

			windowControllerMock.Received(1).OpenWindow("UI/StaticAgentInfoWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
			staticAgentInfoWndHandlerMock.Received(1).Init(123, Arg.Any<List<GenericListItem>>(), Arg.Any<VisualElement>());
		}

		[Test]
		public void TestShowNonFocusAgentDisplayOptionsWindow()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;			
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IDisplayOptionsWndHandler displayOptionsWndHandlerMock = Container.Resolve<IDisplayOptionsWndHandler> ();
			
			VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
			root.Add(visualInfoPanel);

			VisualTreeAsset windowTemplate = Resources.Load<VisualTreeAsset>("UI/DisplayOptionsWindow");
            VisualElement displayOptionsWindow = windowTemplate.Instantiate();

			windowControllerMock.OpenWindow(default, default, default, default).ReturnsForAnyArgs(displayOptionsWindow);
			agentPanelManagerMock.ShowNonFocusAgentPanel(default, default).ReturnsForAnyArgs(visualInfoPanel);

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.ShowNonFocusAgentPanel(null);

			//click configure
			Button button = visualInfoPanel.Q<Button>("configure-btn");
			button.SendEvent(new ClickEvent{target = button});

			windowControllerMock.Received(1).OpenWindow("UI/DisplayOptionsWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
			displayOptionsWndHandlerMock.ReceivedWithAnyArgs(1).Init (default, default);
		}

		[Test]
		public void TestShowPerceivedAgentDisplayOptionsWindow()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;			
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IDisplayOptionsWndHandler displayOptionsWndHandlerMock = Container.Resolve<IDisplayOptionsWndHandler> ();
			
			VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
			root.Add(visualInfoPanel);

			VisualTreeAsset windowTemplate = Resources.Load<VisualTreeAsset>("UI/DisplayOptionsWindow");
            VisualElement displayOptionsWindow = windowTemplate.Instantiate();
			
			windowControllerMock.OpenWindow(default, default, default, default).ReturnsForAnyArgs(displayOptionsWindow);
			agentPanelManagerMock.ShowPerceivedAgentPanel(default, default).ReturnsForAnyArgs(visualInfoPanel);

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.ShowPerceivedAgentPanel(null);

			//click configure
			Button button = visualInfoPanel.Q<Button>("configure-btn");
			button.SendEvent(new ClickEvent{target = button});

			windowControllerMock.Received(1).OpenWindow("UI/DisplayOptionsWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>());
			displayOptionsWndHandlerMock.ReceivedWithAnyArgs(1).Init (default, default);
		}

		[Test]
		public void TestShowFocusAgentSelectorWidow()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;			
			IAgentPanelManager agentPanelManagerMock = Container.Resolve<IAgentPanelManager> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			IFocusAgentSelectorWndHandler focusAgentSelectorWndHandlerMock = Container.Resolve<IFocusAgentSelectorWndHandler> ();
			
			VisualTreeAsset template = Resources.Load<VisualTreeAsset>("UI/AgentPanel");
            VisualElement visualInfoPanel = template.Instantiate();
			root.Add(visualInfoPanel);

			VisualTreeAsset windowTemplate = Resources.Load<VisualTreeAsset>("UI/FocusAgentSelectorWindow");
            VisualElement window = windowTemplate.Instantiate();

			windowControllerMock.OpenWindow(default, default, default, default).ReturnsForAnyArgs(window);
			agentPanelManagerMock.ShowFocusAgentPanel(default, default).ReturnsForAnyArgs(visualInfoPanel);

			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());
			mainUI.ShowFocusAgentPanel(null);

			//click switch focus
			Button button = visualInfoPanel.Q<Button>("switch-focus-btn");
			button.SendEvent(new ClickEvent{target = button});

			windowControllerMock.Received(1).OpenWindow("UI/FocusAgentSelectorWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>(), Arg.Any<int>());
			focusAgentSelectorWndHandlerMock.ReceivedWithAnyArgs(1).Init (default);
		}

		[Test]
		public void TestShowRunSelectorWindow()
		{
			UIDocument uiDoc = Container.Resolve<UIDocument> ();
			VisualElement root = uiDoc.rootVisualElement;
			IRunSelectorWndHandler runSelectorWndHandlerMock = Container.Resolve<IRunSelectorWndHandler> ();
			IWindowController windowControllerMock = Container.Resolve<IWindowController> ();
			MainUI mainUI = Container.Resolve<MainUI> ();
			mainUI.Startup();
			mainUI.Init(UIMode.SimulationPlayer, new SimulationMetaData());

			mainUI.ShowRunSelectorWnd("trace.xml", new List<Run>());

			runSelectorWndHandlerMock.Received(1).Init(Arg.Any<VisualElement>(), "trace.xml", Arg.Any<List<Run>>());
			windowControllerMock.Received(1).OpenWindow("UI/RunSelectorWindow", Arg.Any<string>(), Arg.Any<string>(), Arg.Any<int>(), Arg.Any<int>());
		}
	}
}