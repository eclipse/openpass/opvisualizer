# opVisualizer Source Code Documentation

Build this documentation with Doxygen:

1. Make sure doxygen is installed on your system
2. Navigate to *[opVisualizerRepo]\Doc\SourceCodeDoc* folder in your checked out repository
3. Execute

```
doxygen Doxyfile
```

4. Display the documentation by opening *[opVisualizerRepo]\Doc\SourceCodeDoc\html\index.html* in a browser
