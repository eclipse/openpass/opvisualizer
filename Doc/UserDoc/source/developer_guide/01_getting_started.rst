..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************

.. _developer_guide_getting_started:

Getting Started
---------------

#. Install Unity Hub
#. Install Unity 2021.2.17f1 (Newer versions might work as well)
#. Clone the openPASS Visualizer sources from the GitLab repository (https://gitlab.eclipse.org/eclipse/simopenpass/opvisualizer).
#. In Unity Hub under *Projects* click *Open* and select the local directory with the repository.
#. The first time you start Unity with the project, you will see an error message that safe mode is recommended. Click Ignore.

   .. image:: _static/SafeMode.png
      :alt: Enter Safe Mode Dialog
      :align: center

#. Click *File* > *Open Scene* and choose the file *…Assets/Scenes/MainScene.unity*.
#. Install *Extenject Dependency Injection IOC* from the Asset Store:

   a. Open the Unity Asset Store (https://assetstore.unity.com/).
   b. Search for "Extenject".
   c. Click *Add to My Assets*.
   d. In Unity Editor, navigate to *Window > Package Manager*.
   e. In the Package Manager window select *Packages: My Asserts* and find *Extenject Dependency Injection IOC*. 
   f. Click *Download*.
   g. Click *Import*.
   h. Leave everything in the import settings as default and click *Import*. The plugin will be installed to ./Assets/Plugins/Zenject.

   .. image:: _static/ImportExtenjet.png
      :alt: Import Unity Package Dialog
      :align: center

   .. admonition:: Hint

    For more information on how to install assets, see https://support.unity.com/hc/en-us/articles/210112873-How-do-I-download-an-asset).

   .. admonition:: Hint

    You can also install the Zenject plugin manually by downloading it from https://github.com/Mathijs-Bakker/Extenject/releases and importing it in Unity by selecting *Assets/Import Package/Custom Package...*.

#. Close the Package Manager.
#.	Install NSubstitute: in the file explorer, navigate to *./Assets/Plugins/Zenject/OptionalExtras* and extract the contents of *AutoSubstitute.zip* to *./Assets/Plugins/Zenject/OptionalExtras/TestFramework/Editor*.
#.	In the Unity Editor, open the Zenject Test Framework Assembly Definition (*./Assets/Plugins/Zenject/OptionalExtras/TestFramework/Zenject-TestFramework.asmdef*) and add the *NSubstitute.dll* as Assembly Reference:

   .. image:: _static/ZenjectAssemblyDefinition.png
      :alt: Assembly Definition Dialog
      :align: center

#. Confirm by clicking *Apply* in the Assembly Definition.

