..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************

Building the Application
------------------------

1. In Unity Hub, make sure you installed the unity components for the target build platform:

   .. image:: _static/UnityHub.png
      :alt: Unity Hub Dialog - Installs
   ..

   .. admonition:: Hint

      If your target platform does not appear there, click the Clog Icon > Add Modules and choose the build support modules you need

2. In Unity, set the target platform under *File > Build Settings…* (it is important to do it before compiling the addressable assets):

   .. image:: _static/BuildSettings.png
      :alt: Unity Build Settings Dialog

3. Close the Build Settings window.
4. Reimport GLTF models (sometimes needed when building the application for the first time):

   a. In the Unity project explorer navigate to *Assets/AddressableResources/Public/Vehicles/Lorry/*
   b. Right click on the GLTF model *Lorry.gltf* and select reimport
   c. Repeat the same for the *Oldtimer.gltf* model in directory *Assets/AddressableResources/Public/Vehicles/Oldtimer/* and potientially other GLTF models you have added on your own

5. Compile the addressable assets:

   a. Open the addressable groups window from *Window > AssetManagement > Addressables > Groups*
   b. Trigger the build process by choosing *Build > New Build > Default Build Script*

   .. admonition:: Hint

      The addressables are built under *./Library/com.unity.addressables/aa* and copied automatically to the application’s build output folder during application build. It does not have to be done manually.

6. Close the Addressable Groups window.
7. Compile the application:

   a. Create a target directory where the Visualizer binaries should be generated (e.g. *Build*)
   b. Open *File > Build Settings…*.
   c. Click *Build*.
   d. Select the target directory you just created.

8. Optional: Copy the file *…Assets/Config/SampleAttrMaps.json* to *…[build output folder]/opVisualizer_Data/Config*

   .. admonition:: Hint

      The last step is only needed if the SampleAttrMaps.json is actually used. This is usually not the case for all regular use cases of the opVisualizer. 
