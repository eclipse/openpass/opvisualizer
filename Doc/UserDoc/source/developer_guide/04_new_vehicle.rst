..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************

Adding a new Vehicle Model
--------------------------

You can extend the list of vehicle models that the opVisualizer supports, without changing the visualizer code. All models are implemented as Addressable Assets that can be attached to the visualizer as external file. 

Creating a new Prefab
~~~~~~~~~~~~~~~~~~~~~

The easiest way, to add a new vehicle model to opVisualizer is to duplicate the sample prefab *Oldtimer*, that is shipped with the application and modify it according to your needs. For this tutorial, we will create a new prefab, called “Lorry”.

1. Create a copy of the folder …/Assets/AddressableResources/Public/Vehicles/Oldtimer. We Name the new folder “Lorry”.
2. Paste the file(s) of the 3D-model (e.g. FBX, GLTF, etc) that you would like to integrate into the Lorry folder (in our case 2 files: Lorry.gltf and Lorry.bin, but it can be any 3d format that unity supports, like fbx, obj, .dae, etc.).
3. Rename the file Oldtimer_Prefab.prefab to Lorry_Prefab.prefab
4. Double click the file Lorry prefab in Unitys Project browser to open it

   .. image:: _static/CreatePrefab1.png
      :alt: Create a new prefab

5. Drag the new 3D model (in our case Lorry.gltf) into the prefabs hierarchy and make sure it is on the correct level under PivotPositioner

   .. image:: _static/CreatePrefab2.png
      :alt: Create a new prefab

6.	Delete the *GenericVehicle* GameObject from the hierarchy.
7.	Delete *Oldtimer.bin* and *Oldtimer.gltf* from the *Vehicles/Lorry* folder.


Setting the Correct Pivot Position of the Vehicle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is important that the model has its local zero and pivot point at the middle of its rear axis. The agent is moved around in the world by the prefab’s root level position, so we need to translate the object locally to have the desired longitudinal offset.
To set the pivot point correctly:

1. Add a temporary cube object to the prefab that will help us to mark the world’s zero point (Right click the root object “Lorry_Prefab” > 3D Object > Cube).
2. Modify the cube’s size in the inspector window. Set Position to X:0, Y:0, Z:0, and Scale to X:0.01, Y:0.01, Z:6.

   .. image:: _static/SetPosition.png
      :alt: Setting the vehicle position

3. Reset the Pivot Positioner of the model to 0: click the GameObject *PivotPositioner* and set its Position to X:0, Y:0, Z:0 in the inspector window.
4. Position the GameObject *Lorry* on the longitudinal axis so, that it’s bounding box middle (which is where the Lorry’s GameObject-Gizmo appears) is exactly over the Zero-Line. 

    Tip: use the Scene Gizmo in the right upper corner to switch into top view: click on the text underneath the gizmo to switch to isometric view, then click on the blue arm to set the camera looking along the Z axis. 

   .. image:: _static/SetPosition3.png
      :alt: Setting the vehicle position

5. Position the GameObject *Lorry* along the height axis so, that the lowest point of its wheel gets exactly to the height of the Zero-Line. 

   .. image:: _static/SetPosition4.png
      :alt: Setting the vehicle position

 Tip: use the Scene Gizmo in the right upper corner to switch into isometric side view.

6. Now select the *PivotPositioner* in the hierarchy and move it on the longitudinal axis so, that the rear wheel axis gets exactly over the Zero-Line. Now, the X position that you read in the inspector (2,14 in our case) is the “LongitudinalPivotOffset” parameter that the simulator needs to know. It is stored in VehicleModelsCatalog.xosc. This parameter describes the distance between the bounding box center and the vehicle’s pivot point (the rear axis).

   .. image:: _static/SetPosition5.png
      :alt: Setting the vehicle position

7. We are done with positioning, so delete the GameObject “Zero-Line”.


Setting the Bounding Box of the Vehicle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The prefab must have a correctly sized bounding box. It is used to detect clicks, to display the focus rectangle around the vehicle and to position the tachometer above the vehicle. 
To set the bounding box:

1. Select the root level object *Lorry_Prefab*, click the button *Edit Collider* in the inspector window. The bounding box handles appear. 

   .. image:: _static/SetBoundingBox.png
      :alt: Setting the vehicle's bounding box

2. Move the handles to the edges of the vehicle. 

    Tip: use the Scene Gizmo in the right upper corner to switch into isometric side/front view.

   |pic1| |pic2|

   .. |pic1| image:: _static/SetBoundingBox2.png
      :alt: Setting the vehicle's bounding box
      :width: 39%

   .. |pic2| image:: _static/SetBoundingBox3.png
      :alt: Setting the vehicle's bounding box
      :width: 59%


3. Now, the Box Colliders X, Y and Z size fields that you read in the inspector is the Width, Length and Height parameters of the vehicle that the simulator needs to know. It is stored in VehicleModelsCatalog.xosc.

   .. image:: _static/SetBoundingBox4.png
      :alt: Setting the vehicle's bounding box

   .. admonition:: Hint

      The openPASS simulator outputs the size of the vehicles into simulationOutput.xml under the *Agent* Tag. opVisualizer uses these values to scale the vehicles to the given size. So, ideally, these values are identical to the values of the above described box collider X, Y, Z values, otherwise the vehicle might appear distorted. Animated agents (e.g. some pedestrian agents) are not scaled as their animation might get broken if their body is scaled improportionally.

Setting up the Wheel Rotation of the Vehicle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Make sure there is an agent script attached to the root level of the vehicle prefab (*…Assets/Scripts/AgentHandling/Agent.cs*). The agent script enables linking various subcomponents of the 3D model to the agents business logic. In the next steps, we will go through those options and update our agent. 

1. Click the root level of the prefab and lock the inspector in that view
2. Open the tree of the models hierarchy and find a tire or a wheel. 
3. Drag the wheel subcomponent to the *Model Tire* field of the agent script. This will be used to measure the diameter of the wheel in order to calculate its correct rotation speed.
4. Now, let’s tell the agent script, which model subcomponents should be rotated while the vehicle is moving. It is not necessarily the entire wheel, sometimes it is enough to rotate only the rim of the wheel as the tire’s rotation is not visible due to its homogeneous surface. Depending on the implementation of the model, the wheels might be rotated around the X, the Y or the Z axis. Let’s assume that the wheels must be rotated around the X-axis.
5. Open the group *Model Wheels Y*, create four list items and and drag all wheel GameObjects to the list elements. 
6. If the direction of the wheel-rotation should be opposite, change the *Model Wheel Rotate Backwards* check box.

   .. image:: _static/SetupWheels.png
      :alt: Setting up the wheel rotation

Setting up the Variable Coloring of the Vehicle Body
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some vehicle body parts can have variable color. The available colors for a certain vehicle will be picked pseudo-randomly from a list that you can predefine. The color choice is based on the agent’s ID. To set this feature up:

1. Create a new material that will serve as the variable body paint of the vehicle (the color of the material does not matter as it will be overridden in runtime)
2. Drag the material to all vehicle body parts that should have variable color
3. Drag the material to the Model Paint option of the agent script
4. Add colors to the list of *Body Color Options* according to your needs

   .. image:: _static/SetupColor.png
      :alt: Setting up the vehilce color

Setting up the Position of the Driver’s View Camera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Choose the empty GameObject *Drivers Eyes* from the prefab hierarchy and move it to the position, where the driver’s head is located. 

    Tip: Use the Gizmo of the *DriversEyes* object in the scene view, or its Position fields in the inspector. 

2. Drag the *DriversEyes* GameObject to the *Drivers Eyes* field of the agent script

   .. image:: _static/SetDriversEyes.png
      :alt: Setting up the driver's view position



Setting up the Brake Lights of the Vehicle Model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can optionally define brake lights for the model that turn on/off based on the data from the simulation output. The visual representation consists of 

a. the brake lamps with bloom effect
   
   .. image:: _static/brakelamps.png
      :alt: Brake lamps

b. and the brake light projections that are only visible in *Look Down* view mode of the Visualizer

   .. image:: _static/brakelights.png
      :alt: Brake lights

To set up the brake lights:

1. Make sure there is a Brake Light Controller script attached to the prefab’s root
2. Find the subcomponents in the prefabs hierarchy that represent the brake lamps/bulbs. These GameObjects must have a mesh renderer attached to them.
    
    Tip: If the model does not contain separate brake light bulbs, you can easily create bulb GameObjects using Unity’s cube or cylinder 3D objects. The exact form of these objects has low relevance due to the bloom effect that will be applied when viewing through the camera (the ON material must have an HDR color with intensity above 1 and the cameras bloom post-processing step must be enabled, see Camera > Post-process Volume > Overrides > Bloom)

3. Drag all brake lamp GameObjects to the fields under *Model Brake Lamps* of the *Brake Light Controller* script.
4. Drag the brake light GameObjects from the prefab’s hierarchy to the *Brake Light Controller* script’s Model Brake Lights field. 

   .. image:: _static/setupbrakelights.png
      :alt: Setting up brake lights

5. Move the brake lights to the correct position by using the X/Y/Z arrows of the Light GameObjects Gizmo in the scene view, or their Position fields in the inspector. Choose the “Global” option in the tool rotation dropdown to assure that the preset direction of the light (~45° facing down to the ground) does not change while moving the light sources.

   .. image:: _static/setupbrakelights2.png
      :alt: Setting up brake lights

6. Optionally, you can set individual on/off materials for the brake lamps. If you leave these fields unset the default materials will be applied to the lamps (*…Assets/Resources/Materials/BrakeLampON.mat* and *…/BrakeLampOFF.mat*).

   .. image:: _static/brakelampmaterials.png
      :alt: Setting up specific brake lamp material

Setting up the Turn Signals of the Vehicle Model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can optionally define indicators for the model that flash based on the data from the simulation output. The visual representation consists of 

a)	the indicator **lamps** and 
b)	the indicator **light projections** that are visible in *Look Down* view mode of the Visualizer.

To set up the indicators:

1. Make sure there is a Turn Signal Controller script attached to the prefab’s root
2. Find the subcomponents in the prefabs hierarchy that represent the indicator lamps/bulbs. These GameObjects must have a mesh renderer attached to them

    Tip: If the model does not contain separate indicator light bulbs, you can easily create bulb GameObjects using Unity’s cube or cylinder 3D objects. The exact form of these objects has low relevance due to the bloom effect that will be applied when viewing through the camera (the ON material must have an HDR color with intensity above 1 and the cameras bloom post-processing step must be enabled, see Camera > Post-process Volume > Overrides > Bloom)

3. Drag all indicator GameObjects to the fields under *Model Turn Signal Lamps Left/Right* of the *Turn Signal Controller* script.
4. Drag the turn light GameObjects from the prefab’s hierarchy to the *Turn Signal Controller* script’s Model Turn Lights Left/Right fields. 

   .. image:: _static/SetupTurnSignalController.png
      :alt: Setting up the turn signal Controller

5. Move the turn lights to the correct position by using the X/Y/Z Gizmo arrows of the light GameObjects in the scene view, or their Position fields in the inspector. Choose the *Global* option in the tool rotation dropdown to assure that the preset direction of the light (~45° facing down to the ground) does not change while moving the light sources.

   .. image:: _static/SetupTurnSignalLights.png
      :alt: Setting up the turn signal Controller

6. Optionally, you can set individual on/off materials for the turn lamps. If you leave these fields unset the default materials will be applied to the lamps (*…Assets/Resources/Materials/TurnSignalON.mat* and *…/TurnSignalOFF.mat*).

   .. image:: _static/TurnLampMaterials.png
      :alt: Setting up specific turn signal lamp material

Saving the new Prefab
~~~~~~~~~~~~~~~~~~~~~

Save the prefab by simply leaving the prefab editor with the left arrow in the hierarchy.

.. image:: _static/SavePrefab.png
   :alt: Saving the prefab

.. _MAKE_PREFAB_ADDRESSABLE:

Turn the Prefab into an Addressable Asset
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To enable run time instantiation of the new prefab, it must be an addressable asset. Every vehicle asset is identified through its unique name, that is referenced with the *VehicleModelType* attribute of the agent definition in Simulation output.

1. Open The Addressable Groups window (Window > Asset Management > Addressables > Groups)
2. Drag the new prefab from the project explorer into the Group *Vehicles* in the Addressable Groups window
3. The default addressable name of the asset is its path. Change this to the name that you would like to reference the vehicle (with the *VehicleModelType* attribute). Make sure the name is unique.
4. Change the label of the new entry to *Vehicle*

   .. image:: _static/SetAddressable.png
      :alt: Turning the prefab into an addressable

5. To test the new vehicle in the Unity Editor:

   a. Set up a simulation output file with the new vehicle referenced (SimulationOutput > RunResults > RunResult > Agents > Agent > VehicleModelType = ”lorry”)
   b. Make sure the Play Mode Script option of the *Addressable Groups* window is set to *Use Asset Database* 
 
      .. image:: _static/SetAddressable2.png
         :alt: Turning the prefab into an addressable

   c. Hit the play button
   d. Load the simulation output file that references the new vehicle

6. To create a distributable asset that you can use with the opVisualizer executable, compile the addressable assets into an asset catalog by clicking *Build > New Build > Default Build Script* in the Addressable Groups window.

   .. admonition:: Hint

      The built asset catalog is copied automatically to the application’s build output folder during application build. You can though only update the addressables in an existing opVisualizer installation, without needing to build and distribute the entire application. To update the addressables in an existing installation, copy the built addrassables from *[opVisualizer repo]/Library/com.unity.addressables/aa*, to the applications run time folder *[opVisualizer build folder]/opVisualizer_Data/StreamingAssets/aa*.