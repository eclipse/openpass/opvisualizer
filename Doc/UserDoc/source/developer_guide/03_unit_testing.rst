..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************


Running Unit Tests
------------------
Unit test implementations are under *[ProjectRepo]/Assets/UnitTests*. To run the unit tests, open the Test Runner in the Unity Editor under *Window > General > Test Runner*, 
choose *EditMode* and click *Run All*. 
