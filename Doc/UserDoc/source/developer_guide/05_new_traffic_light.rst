..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************


Adding a new Traffic Light Model
--------------------------------
You can add new traffic light types with their own custom states and rules without modifiyng the Visualizer code. 

1. Duplicate an existing traffic light prefab (e.g. 1.000.002).
2. Add new bulbs if necessary (a bulb is invisible if inactive, so make sure a separate off-state representation of the bulb exists in the model) 
3. Add the script *TrafficLightBulbController* to each new bulb
4. Add new states and rules as necessary:

   .. image:: _static/TrafficLightAddState.png
      :alt: Adding new states and rules
      :width: 400px

5. Link the light bulbs to the state rules by dragging the bulb GameObjects to the *Bulb Controller* field of the traffic light states:
   
   .. image:: _static/TrafficLightLinkBulb.png
      :alt: Linking light bulbs

6. Set the state of each bulb in each traffic light state:

   .. image:: _static/TrafficLightBulbState.png
      :alt: Setting traffic light bulb state
      :width: 400px

7. Save the new traffic light prefab and make it an addressable resource (see :ref:`MAKE_PREFAB_ADDRESSABLE` for more info) 