################################################################################
# Copyright (c) 2020-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
################################################################################
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
import datetime

# -- Path setup --------------------------------------------------------------
sys.path.append(os.path.abspath("_ext"))

# -- Project information -----------------------------------------------------
project = 'openPASS Visualizer'
copyright = f'{datetime.datetime.now().year} BMW AG'
author = 'in-tech GmbH'

# -- Version is generated via cmake
version_file = 'version.txt'
if os.path.exists(version_file):
    with open(version_file) as vf:
        version = vf.read().strip()
        release = version

# -- General configuration ---------------------------------------------------
def setup(app):
   app.add_css_file('css/custom.css')

extensions = []
extensions.append("sphinx_rtd_theme")
extensions.append('sphinx.ext.todo')

templates_path = ['_templates']

exclude_patterns = []

todo_include_todos = True

pdf_stylesheets = ['sphinx', 'kerning', 'a4']
pdf_style_path = ['.', '_styles']
pdf_fit_mode = "shrink" # literal blocks wider than frame
pdf_language = "en_US"
pdf_page_template = 'cutePage'

numfig = True

# -- Options for HTML output -------------------------------------------------

html_static_path = ['_static']
html_theme = 'sphinx_rtd_theme'
html_title = 'openPASS Visualizer Documentation'
html_short_title = 'opVisualizer|Doc'
html_favicon = '_static/openPASS.ico'
html_logo = '_static/openPASS.png'
