..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************

FAQ
===

On which operating systems can opVisualizer be developed?
---------------------------------------------------------

opVisualizer is powered by the game engine Unity. 
Unity also provides a cross-platform IDE for developers that gives you access to all the tools you need for development. 
The opVisualizer code and logic is written in C# that you can develop in the editor of your choice. Please refer to the  :ref:`Developer Guide <developer_guide_getting_started>` for more information.

On which operating systems does opVisualizer run?
-------------------------------------------------

Unity allows to build the application for different targets. The application is developed and tested under Windows. Building for Linux has been tested successfully as well. 
Further, it can also run WebGL-based in a web browser. 

Can opVisualizer deal with large simulation result files?
---------------------------------------------------------

opVisualizer uses an on-the-fly data loading mechanism that allows opening large files fast, while loading data in the background seamlessly during navigation in the virtual world.

Do I need to adapt the visualizer code to make it display my own set of specific data points?
---------------------------------------------------------------------------------------------

The visualizer can read any attribute that the simulator outputs and can display it as raw text during playback, without needing to know their meaning. 
So, for textual display of the data point values, no code adaptation is needed. 
Additionally, you can define your own attribute value dictionary in a configuration file, that the opVisualizer uses to translate the raw data point values into a better readable form. 

Do I need to adapt the visualizer code to use my own 3D vehicle models or road signals?
-----------------------------------------------------------------------------------------

opVisualizer implements an asset loading mechanism that allows you to add your own 3D resources as external files to the application without code adaption. 
The 3D models must be wrapped in a *Prefab* and compiled into a catalog, that you can distribute along with the opVisualizer. 
For more information, please visit the :ref:`Developer Guide <developer_guide_getting_started>`.