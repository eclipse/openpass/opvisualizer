..
   ********************************************************************************
   * Copyright (c) 2016-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
   *
   * This program and the accompanying materials are made available under the
   * terms of the Eclipse Public License 2.0 which is available at
   * http://www.eclipse.org/legal/epl-2.0.
   *
   * SPDX-License-Identifier: EPL-2.0
   ********************************************************************************

User Manual
===========

The openPASS Visualizer (opVisualizer) allows you to analyze openPASS simulation output files, displaying the data in 3D, embedded in the corresponding OpenDRIVE world.
The application lets the user move around in time and space of simulation results and observe various parameters of the simulated scenarios.
The configurability of display options helps you to focus on the information of your specific interest while keeping the user interface clear and reduced.

Opening a Trace File
--------------------
To view an openPASS simulation output file (.xml), choose *Load Trace...* from the application menu |menu_symbol|. 

.. admonition:: Hint

   If the file does not contain an XML-element *SceneryFile* on the root level (which defines the relative path to the OpenDRIVE file), the Visualizer will try to load the scenery file at the default path (same as the trace file) with default file name (sceneryConfiguration.xml). 

If the file contains multiple runs, a run selection dialog will be displayed.

Opening a Specific Run from a Trace File
----------------------------------------
If a loaded trace file contains multiple runs and you would like to switch to another run, choose the *Load Run...* menu item.
The *Load Run...* menu item is not shown if a trace file with only one run is loaded or if no trace file is loaded.

Opening a Scenery File
----------------------
To view an OpenDRIVE file without a corresponding simulation output, choose the *Load Scenery...* menu item.

You can navigate the map file by dragging and scrolling the mouse. Choose either *Look Down* or *Perspective* viewing mode (see :ref:`CHANGE_PERSPECTIVE`).

Navigating Through Simulation Space
-----------------------------------

Moving Around in the World
~~~~~~~~~~~~~~~~~~~~~~~~~~
In order to move the viewing perspective, click and hold the left mouse button on the terrain and move the mouse.
If the viewer perspective is focused on an agent (see :ref:`FOCUSING_AGENT`) and you move away from the focused agent, the agent's focus frame and the agent ID start flashing. 
The flashing reminds you that viewing perspective is attached to the focus agent even if it is out of view, which can be confusing if not kept in mind. 
Clicking the flashing agent ID centers the viewing perspective back to the focus agent.

By scrolling the mouse, you can zoom the viewing perspective away/towards from/to the focus point.
To turn the viewing perspective, click and hold the right mouse button on the terrain and move the mouse.

.. _CHANGE_PERSPECTIVE:

Changing the Viewing Perspective
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can change the viewing perspective by clicking on the |eye_symbol| symbol in the right upper corner of the application window.
If an agent is currently in focus (see :ref:`FOCUSING_AGENT`) the driver view perspective is also available.

.. image:: _static/ViewModeLookDown.png
   :alt: Changing the viewing mode to Look Down

.. image:: _static/ViewModePerspective.png
   :alt: Changing the viewing mode to perspective

.. image:: _static/ViewModeDriver.png
   :alt: Changing the viewing mode to driver

.. _FOCUSING_AGENT:

Focusing on a Specific Agent
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can make the viewing perspective follow an agent, while it is moving around in the world. After loading a trace file, the default viewing perspective is fixed to the Ego Agent (*SimulationOutput > RunResults > RunResult > Agents > Agent > AgentTypeGroupName = "Ego"*), if there is one.

You can change the focus by 

1. double clicking an agent, or
2. clicking the |change_focus_symbol| symbol in the *focus agent panel* and choosing the agent from the list that you would like to focus on:

.. image:: _static/ChangeFocusAgent.png
   :alt: Focusing an agent
..

.. image:: _static/ChangeFocusAgentFromList.png
   :alt: Focusing an agent
..

.. admonition:: Hint

 Agents that are not in the world at the currently active timestamp are grayed out. 

To release the focus, double click the terrain.


Navigating Through Simulation Time
----------------------------------

.. image:: _static/TimeNavigation.png
   :alt: Time navigation

Agent Floating Info
-------------------

You can see above the agent a floating info panel that displays the agent's ID, its speed and acceleration (as far as the trace file contains this information). 
If the agent is not accelerating, the background color of the speedometer is gray. 
The background turns green if the agent is accelerating and red if it's decelerating. 
The stronger the acceleration/deceleration, the stronger the color will get. 
All parts of the floating info panel can be enabled/disabled in the Display Options (|cog_symbol| symbol). 
For more info see :ref:`DISPLAY_OPTIONS`. 

.. image:: _static/AgentFloatingInfo.png
   :alt: Agent Floating Info
   
Displaying Static Agent Information
-----------------------------------
Agent attributes that are not changing during a simulation run can be displayed by
right clicking the agent and choosing *Show Static Agent Info* or by left clicking an agent while holding down the Alt key. 
This information is read from the *Agent* section of the simulation output file as well as from the extended driver simulation output file (*SimulationOutput > RunResults > RunResult > Agents > Agent*).

.. image:: _static/AgentMenu_StaticAgentInfo.png
   :alt: Static agent info menu
..

.. image:: _static/StaticAgentInfo.png
   :alt: Static agent info
..

Displaying Dynamic Agent Information
------------------------------------

Dynamic agent information is a list of agent specific values that can change during a simulation run. 

Focus Agent Information
~~~~~~~~~~~~~~~~~~~~~~~
Dynamic attributes of the focused agent are displayed on an *agent panel* in the left lower corner of the screen. 

.. image:: _static/FocusAgentInfo.png
   :alt: Focus agent info

Non-Focus Agent Information
~~~~~~~~~~~~~~~~~~~~~~~~~~~
You can also display infos of multiple other agents that are not in focus by
right clicking an agent and choosing *Show Dynamic Agent Info* or by left clicking the agent while holding down the Ctrl key. 
These attributes are displayed on an *Agent Panel*, on the right side of the screen, in the hidable dock area. 
This information is read from the *Cyclics* Section of the simulation output file as well as from the extended driver simulation output file (*SimulationOutput > RunResults > RunResult > Cyclics*).

.. image:: _static/NonFocusAgentInfo.png
   :alt: Non-focus agent info

.. _DISPLAY_OPTIONS:

Configuring the Displayed Dynamic Agent Attributes
--------------------------------------------------------------------
To keep a better overview, it is always good to reduce the amount of displayed information on the screen to most necessary. 
For this reason you can configure, which dynamic agent attributes should be displayed on the *Agent Panels*. 

Focused Agent Display Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Click the |cog_symbol| symbol in the header of the focused agent panel in the left lower corner of the screen to show the display options of the currently focused agent.
This filter is only applied for the focused agent (whichever agent is currently in focus).

.. image:: _static/FocusAgentDisplayOptions.png
   :alt: Focus agent display options

Global Agent Display Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Click the |cog_symbol| symbol in the right upper corner of the screen or the |cog_symbol| symbol of a non-focused agent panel on the right side of the screen to show the global display options.
The global display option filter applies for all agents except for the one in focus.

Attribute Mapping 
~~~~~~~~~~~~~~~~~
You can translate the attribute values of the *Agent Panel* for a better readability. The translations are stored
in a configuration file under [opVisualizer Install Folder]/opVisualizer_Data/Config/SampleAttrMaps.json. Here is an example:

.. code-block:: 

      {
         'AttributeMapList': [ 
            {
			      'AttributeName': 'IndicatorState',
			      'ValueMap': 
			      {
			      	'0': 'Unknown',
			      	'1': 'Left',
			      	'2': 'Right',
			      	'3': 'Hazard'
			      }
		      }
         ]
      }

The value of *AttributeName* must match the name of a parameter in the header of the trace file cyclics.
  
Resizing the Agent Info Panels
------------------------------
You can resize the agent info panels by moving the mouse over the edge of the panels and dragging them. The focus agent info panel can be minimized/maximized.

.. image:: _static/ResizingAgentInfoPanels.png
   :alt: Resizing agent info panels

Analyzing Simulation Events
---------------------------
The Visualizer displays simulation events above the time axis. The information is read from the simulation file's *Events* tag. If the event *Name* attribute is *Collision* the marker is red. Every other event is represented by a yellow marker. Events with *Name* attribute *TrafficLight* are not displayed. Multiple events close to each other are grouped.

.. image:: _static/Events1.png
   :alt: Events

Clicking an event moves the perspective to the event's *Triggering Entity* and to the time stamp of the event.

.. image:: _static/Events2.png
   :alt: Events

See event info or un-group events by moving the mouse pointer over the markers.

.. image:: _static/Events3.png
   :alt: Events

Displaying the Driver Perception
--------------------------------
The Visualizer can show the driver's perception during a simulation run. This information is loaded from the extended
driver simulation output file as well as from the agent perception files. 
If this information is present, you can see the option *Show Driver Perception* in the context menu after right clicking an agent.  

.. image:: _static/AgentMenu_DriverPerception.png
   :alt: Agent Menu - Show driver perception

.. image:: _static/DriverPerception.png
   :alt: Displaying the driver perception

When the driver perception display is activated, you can see the vehicles that the driver of the selected 
agent sees in the world, shown as ghost boxes. Right click the perceived vehicle to open it's context menu
and select *Show Perceived Info* to display further information. Alternatively, you can display the infos by
left clicking the perceived vehicle while holding down the Ctrl key.

To display all attributes, that an agent about an other agent perceives, right click the perceived vehicle (ghost box) and choose *Show Perceived Info*.

.. image:: _static/AgentMenu_ShowPerceivedInfo.png
   :alt: Displaying perceived information

Keyboard Shortcuts
------------------
From the application menu |menu_symbol|, choose *Usage Hints* to show a list of keyboard shortcuts that you can use.

.. table::
   :class: tight-table

   =================    =================
   **Key**              **Function**
   Left/Right arrows    Step time back/forward
   Up/Down arrows       Move focus to next/previous agent
   Space                Start/Stop playback
   Alt + 1              Toggle demo mode
   Alt + 2              Reload trace file
   Alt + 3              Reload scenery file
   Alt + 4              Toggle high/low quality display
   Alt + 5              Toggle cursor position display
   =================    =================

Mouse Options
-------------
From the application menu |menu_symbol|, choose *Usage Hints* to show a list of mouse options that you can use.

.. table::
   :widths: 30 70
   :class: tight-table

   ==========================  =============
   **Action**                  **Function**
   Scroll                      Zoom view perspective
   Hold left button            Turn view perspective
   Hold right button           Drag view perspective
   Right click agent           Show agent menu
   Left-click road             Highlight road/lane/lane-section and display it's OpenDRIVE info (click multiple times to highlight overlapping lanes in sequence)
   Left-click road object      Highlight road object and show it's OpenDRIVE info
   Left-click signal           Highlight signal and show it's OpenDRIVE info
   Left-click terrain          Hide road/lane/road-object highlight
   Double click agent          Focus camera on agent
   Double click terrain        Clear agent focus
   Ctrl + Left-click agent     Show dynamic agent attributes
   Alt + Left-click agent      Show static agent attributes
   Shift + Left-click agent    Show driver's perception
   ==========================  =============


Displaying OpenDRIVE Road Component Details 
-------------------------------------------
You can analyze roads, lanes, lane sections, signals and road objects by clicking on these elements. The lane/signal/object is then highlighted yellow and the corresponding information is displayed in the lower right corner of the application.

.. image:: _static/HighlightedSignal.png
   :alt: Signal highlighting
   :width: 49%

.. image:: _static/HighlightedObject.png
   :alt: Object highlighting
   :width: 49%

Repeated clicking highlights overlapping lanes in sequence (e.g. in crossroads).

.. image:: _static/HighlightedLane1.png
   :alt: Lane highlighting 1. click
   :width: 49%

.. image:: _static/HighlightedLane2.png
   :alt: Lane highlighting 2. click
   :width: 49%

.. image:: _static/HighlightedLane3.png
   :alt: Lane highlighting 3. click
   :width: 49%

.. image:: _static/HighlightedLane4.png
   :alt: Lane highlighting 4. click
   :width: 49%

Once a lane is highlighted, you can choose from the dropdown in the right lower corner if you wish to highlight the road, the lane or the lane section at the mouse cursor.

Click the terrain anywhere to hide highlighting.

Displaying Coordinates at Mouse Cursor
--------------------------------------
The Visualizer helps you to analyze OpenDRIVE files by displaying the world coordinates and the road coordinates at the mouse pointer.

World Coordinates
~~~~~~~~~~~~~~~~~
To display the coordinates of the mouse cursor in world coordinate system, press Alt + 5. The coordinates appear in the lower right corner of the application screen.

.. image:: _static/WorldCoordinates.png
   :alt: Displaying World Coordinates

Road Coordinates
~~~~~~~~~~~~~~~~
To display the coordinates of the mouse cursor in road coordinate system, highlight a road/lane/lane section. The coordinates appear in the highlight info panel, in the lower right corner of the application screen.

.. image:: _static/RoadCoordinates.png
   :alt: Displaying Road Coordinates

Placing Buildings into OpenDRIVE road networks
----------------------------------------------
To place simple static 3D building blocks into a scene, add road objects to the OpenDRIVE file with type "building". You can freely set the position and dimensions of the blocks by specifying the attributes *s*, *t*, *width*, *height*, and *length*.

.. code-block::

	  <object type="building" name="One story house" s="100" t="10" zOffset="0" length="30" width="10" height="4" hdg="0" pitch="0" roll="0"/>

.. image:: _static/Buildings.png
   :alt: 3D Buildings

OpenDRIVE Object and Signal Example File
----------------------------------------
You can find an OpenDRIVE file (*OpenDRIVE_objectsignal_demo.xodr*) that demonstrates most of the supported objects and signals, under *[opVisualizer Install Folder]/opVisualizer_Data/Demo*. The repository also contains the file under *[opVisualizer_repo]/Assets/Resources/Sceneries*.

Using the Sensor Positioner
---------------------------
The Sensor Positioner mode can help you to find the local coordinates of a vehicle, where a sensor should be placed.
To start the Sensor Positioner, choose the corresponding menu item.

- Choose a vehicle from the dropdown.
- Move and turn the sensor by typing a value into the number fields or by pressing and holding the arrow buttons left and right of the number fields. 
- Move the viewing perspective around the sensor by dragging the mouse
- Zoom by scrolling the mouse

.. image:: _static/SensorPositioner.png
   :alt: Sensor positioner

Displaying the Application Version
----------------------------------
From the application menu |menu_symbol|, choose *Version info* to show the version of the running application.

Displaying Currently Used Graphic Card
--------------------------------------
From the application menu |menu_symbol|, choose *Version info* to show the name of the graphic card that the application is currently using. 
This information is useful if multiple graphic cards are present in the user's system.

.. |eye_symbol| image:: _static/EyeSymbol.png
    :height: 10pt

.. |cog_symbol| image:: _static/CogSymbol.png
    :height: 10pt

.. |change_focus_symbol| image:: _static/ChangeFocusSymbol.png
    :height: 10pt

.. |menu_symbol| image:: _static/MenuSymbol.png
    :height: 10pt