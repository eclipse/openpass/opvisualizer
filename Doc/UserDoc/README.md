# opVisualizer User Documentation

Build this documentation with Sphinx, a python based documentation generator based on [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) as primary import format.

## Building the documentation (Windows)

1. Get MSYS2 from https://www.msys2.org/
2. Start MSYS2 MinGW 64bit shell and update the system packages:

```
pacman -Syuu
```

If the upgrade requires a restart of MSYS2, resume the upgrade by re-opening the shell and call:

```
pacman -Suu
```

3. Install required packages using the MSYS2 MinGW 64bit shell:

```
pacman -S mingw-w64-x86_64-python-sphinx # Tested with 5.1.1-2
pacman -S mingw-w64-x86_64-python-pip   # Tested with 21.2.1-1
pip3 install sphinx-rtd-theme
```

4. Navigate to *[opVisualizerRepo]\Doc\UserDoc* folder in your checked out repository
5. Execute

```
sphinx-build -M html ./source ./build
```

6. Display the documentation by opening *[opVisualizerRepo]\Doc\UserDoc\build\html\index.html* in a browser

## Further information on Sphinx

- [reStructuredText Primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)
- [A "How to" Guide for Sphinx + ReadTheDocs](https://source-rtd-tutorial.readthedocs.io/en/latest/index.html)
- [Sphinx Documentation](https://www.sphinx-doc.org)
